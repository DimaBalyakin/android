/**
 * Automatically generated file. DO NOT MODIFY
 */
package org.skanword.and;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "org.skanword.and";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 124;
  public static final String VERSION_NAME = "1.10.15";
}
