package com.musselwhizzle.dispatcher.events;



public interface EventListener {
	
	void onEvent(Event event);

}
