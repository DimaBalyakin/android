package com.musselwhizzle.dispatcher.events;

public class SimpleEvent implements Event {

	private String type;
	public String getType() { return type; }
	public void setType(String type) { 
		this.type = type; 
	}
	
	protected Object source;
	public Object getSource() {
		return source;
	}
	public void setSource(Object source) {
		this.source = source;
	}
	
	public SimpleEvent(String type) {
		this.type = type;
	}

}
