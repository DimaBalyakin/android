package org.skanword.and.inappbilling;

import java.util.List;

import org.skanword.and.SmappsScanwords;
import org.skanword.and.inappbilling.googleplayutil.IabHelper;
import org.skanword.and.inappbilling.googleplayutil.IabResult;
import org.skanword.and.inappbilling.googleplayutil.Inventory;
import org.skanword.and.inappbilling.googleplayutil.Purchase;
import org.skanword.and.inappbilling.googleplayutil.SkuDetails;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.util.Log;



public class GooglePlayManager implements StoreManagerInterface {

    String base64EncodedPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAjrJyt7vEpipN9aLbuGHAaUno/VOduGASRUtBXjzZaU+sFbAar/orgTOlvKjN2Izjnrn8/fAcJYvHpLQn6mj7qkX52oTOoOzQapUDLYRNAQ19kGEqxZo94RJdTLSBA+TOyaboWy5O4qU4O+R4hvjEZjYGRclwG7IEvzYB+yN4Ew4KgxO9SsEKoorFxxUMH0fnuMbFhX8geMum/B12bsFK41wma69DChE7HdnIAYDDSzj6CgmReFmV0n6qjyDTMWTFidwkgtixOZdoo4HLDXbMBbjHU8L1Mh9189s6NE6WY6yLe7eXST1DIDx9UkwehKxwupqMIcPFzZRX4OsSvYPO/QIDAQAB";
	private IabHelper mHelper;
	private boolean mStarted = false;
	private StoreManagerDelegateInterface mDelegate = null;
	private Inventory mInventory = null;

	public GooglePlayManager() {
		super();
		// TODO Auto-generated constructor stub

        mHelper = new IabHelper(SmappsScanwords.getContext(), base64EncodedPublicKey);

        // enable debug logging (for a production application, you should set this to false).
        mHelper.enableDebugLogging(true);
        Log.d("", "Starting setup.");
	}
	
	
	
	@Override
	public String getAppStoreName() {
		return "google";
	}


	@Override
	public void initManager(StoreManagerDelegateInterface delegate, Activity callerActivity) {
		mDelegate = delegate;
       
	}
	public void updatePricesForProducts(final List<String> productsIds) {
		if (productsIds.size() == 0 || mStarted)
			return;
		 mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
	            public void onIabSetupFinished(IabResult result) {
	                Log.d("","onIabSetupFinished");

	                if (!result.isSuccess()) {
	                    // Oh noes, there was a problem.
	                    complain("Problem setting up in-app billing: " + result);
	                    return;
	                }

	                // Have we been disposed of in the meantime? If so, quit.
	                if (mHelper == null) return;

	                // IAB is fully set up. Now, let's get an inventory of stuff we own.
	                Log.d("", "Setup successful. Querying inventory.");
	                mHelper.queryInventoryAsync(true, productsIds, mGotInventoryListener);
	            }
	        });
		mStarted = true;
	}
	IabHelper.QueryInventoryFinishedListener mGotInventoryListener = new IabHelper.QueryInventoryFinishedListener() {
        public void onQueryInventoryFinished(IabResult result, Inventory inventory) {
            Log.d("", "Query inventory finished.");

            // Have we been disposed of in the meantime? If so, quit.
            if (mHelper == null) return;

            // Is it a failure?
            if (result.isFailure()) {
                complain("Failed to query inventory: " + result);
                return;
            }

            Log.d("", "Query inventory was successful.");

            /*
             * Check for items we own. Notice that for each purchase, we check
             * the developer payload to see if it's correct! See
             * verifyDeveloperPayload().
             */
            mInventory = inventory;
            if (mDelegate != null) {
            	mDelegate.initComplete();
            }
        }
    };
    @Override
    public String getPriceForProduct(String productId) {
    	if(mInventory != null) {
    		SkuDetails details = mInventory.getSkuDetails(productId);
    		if (details != null) {
    			return details.getPrice();
    		}
    	}
    	return null;
    }
	@Override
	public void clear() {
        Log.d("", "Destroying helper.");
        if (mHelper != null) {
            mHelper.dispose();
            mHelper = null;
        }
	}
	@Override
	public void purchaseProduct(String productId, Activity callerActivity) {
        Log.d("", "Launching purchase flow for " + productId);

        String payload = "";

        mHelper.launchPurchaseFlow(callerActivity, productId, 1,
                mPurchaseFinishedListener, payload);
	}
	
	IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
        public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
            Log.d("", "Purchase finished: " + result + ", purchase: " + purchase);

            // if we were disposed of in the meantime, quit.
            if (mHelper == null) return;

            if (result.isFailure()) {
                complain("Error purchasing: " + result);
                return;
            }
            if (!verifyDeveloperPayload(purchase)) {
                complain("Error purchasing. Authenticity verification failed.");
                return;
            }
            if (mDelegate != null) {
            	mDelegate.purchaseComplete(purchase.getSku(), purchase.toString());
            }

        }
    };
    boolean verifyDeveloperPayload(Purchase p) {
        //String payload = p.getDeveloperPayload();

        /*
         * TODO: verify that the developer payload of the purchase is correct. It will be
         * the same one that you sent when initiating the purchase.
         *
         * WARNING: Locally generating a random string when starting a purchase and
         * verifying it here might seem like a good approach, but this will fail in the
         * case where the user purchases an item on one device and then uses your app on
         * a different device, because on the other device you will not have access to the
         * random string you originally generated.
         *
         * So a good developer payload has these characteristics:
         *
         * 1. If two different users purchase an item, the payload is different between them,
         *    so that one user's purchase can't be replayed to another user.
         *
         * 2. The payload must be such that you can verify it even when the app wasn't the
         *    one who initiated the purchase flow (so that items purchased by the user on
         *    one device work on other devices owned by the user).
         *
         * Using your own server to store and verify developer payloads across app
         * installations is recommended.
         */

        return true;
    }
    void complain(String message) {
        Log.e("", "**** TrivialDrive Error: " + message);
        alert("Error: " + message);
    }

    void alert(String message) {
        AlertDialog.Builder bld = new AlertDialog.Builder(SmappsScanwords.getContext());
        bld.setMessage(message);
        bld.setNeutralButton("OK", null);
        Log.d("", "Showing alert dialog: " + message);
        bld.create().show();
    }
	@Override
	public void handleActivityResult(int requestCode, int resultCode,
			Intent data) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void restorePurchases(Activity activity) {
		// TODO Auto-generated method stub
		
	}



	@Override
	public void consumeProducts(List<String> products) {
		// TODO Auto-generated method stub
		
	}

}
