package org.skanword.and.inappbilling;

import java.util.List;

public interface StoreManagerDelegateInterface {
	public void purchaseComplete(String productId, String recipe);
	public void purchaseCanceled(String productId);
	public void purchaseOwned();
	public void initComplete();
	public void purchasesRestored(List<String> purchases);
}
