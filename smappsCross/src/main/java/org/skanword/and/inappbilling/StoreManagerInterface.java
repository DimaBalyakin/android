package org.skanword.and.inappbilling;

import java.util.List;

import android.app.Activity;
import android.content.Intent;

public interface StoreManagerInterface {
	
	public void initManager(StoreManagerDelegateInterface delegate, Activity callerActivity);
	public void purchaseProduct(String productId, Activity callerActivity);
	public void restorePurchases(Activity activity);
	public void clear();
	public String getPriceForProduct(String productId);
	public void updatePricesForProducts(List<String> poructsIds);
	public void handleActivityResult(int requestCode, int resultCode, Intent data);
	public String getAppStoreName();
	public void consumeProducts(List<String> products);
}
