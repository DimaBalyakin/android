package org.skanword.and.inappbilling;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.onepf.oms.OpenIabHelper;
import org.onepf.oms.SkuManager;
import org.onepf.oms.SkuMappingException;
import org.onepf.oms.appstore.googleUtils.IabHelper;
import org.onepf.oms.appstore.googleUtils.IabResult;
import org.onepf.oms.appstore.googleUtils.Inventory;
import org.onepf.oms.appstore.googleUtils.Purchase;
import org.skanword.and.R;
import org.skanword.and.network.MainNetworkManager;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Base64;
import android.util.Log;

public class UniversalStoreManager implements StoreManagerInterface {

	String base64EncodedPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA5rrzJO4B4406DREi/WvYqTUyAtohs7CFw+cjpu3Xclr0etxhSKrAdZGnbo+BHVLmU48GQWmfaopzmFsFEMlThQB941b6vOUqJHASUYlCun94qpKsN0ltWiIh/SVdANNc5PWG+Ukzl+Dyvg8pFBewwjrO2ofR18P/lE3DeSz1EI8DzcFIARgNqbKrHdKEEN0wMT4xpfzBM2mgiWq4/V0D0adh1S+Pt0u8YdmmTrysHOA5o6AQ3OiGGbnDV6oT4U2McGQ8VnvhrOBwFij40FRS2O9wR7/jpTjehSiXyvEZGzbnhMdbLQRsZ1NvsrhDMI7mSCtsawChzA61JrI/nenBrwIDAQAB";
	private OpenIabHelper mHelper;
	private StoreManagerDelegateInterface mDelegate = null;
	private Inventory mInventory = null;
	private List<String> mPoructsIds;
	private Activity mPurchaseActivity;

	@Override
	public void initManager(StoreManagerDelegateInterface delegate, Activity callerActivity) {
		if (mHelper != null && mHelper.getSetupState() == OpenIabHelper.SETUP_RESULT_SUCCESSFUL)
			return;
		mDelegate = delegate;
		Map<String, String> storeKeysMap = new HashMap<String, String>();
		storeKeysMap
				.put(OpenIabHelper.NAME_GOOGLE,
						base64EncodedPublicKey);
		OpenIabHelper.Options.Builder builder = new OpenIabHelper.Options.Builder()
				.setVerifyMode(OpenIabHelper.Options.VERIFY_EVERYTHING)
				.addStoreKeys(storeKeysMap);
	    builder.setStoreSearchStrategy(OpenIabHelper.Options.SEARCH_STRATEGY_INSTALLER_THEN_BEST_FIT);

	    builder.addPreferredStoreName(OpenIabHelper.NAME_GOOGLE);
		mHelper = new OpenIabHelper(callerActivity,
				builder.build());
		//OpenIabHelper.enableDebugLogging(true);
		mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
			public void onIabSetupFinished(IabResult result) {
				Log.v("",result.getMessage() +
						"  Setup finished."
								+ mHelper.getConnectedAppstoreName());
				if (!result.isSuccess()) {
					return;
				}
				queryProducts();
			}
		});
	}
	
	@Override
	public String getAppStoreName() {
		if (mHelper != null && mHelper.getSetupState() == OpenIabHelper.SETUP_RESULT_SUCCESSFUL)
			return mHelper.getConnectedAppstoreName();
		return "google";
	}

	@Override
	public void purchaseProduct(String productId, Activity callerActivity) {
		mPurchaseActivity = callerActivity;
		if (mHelper != null && mInventory != null ) {
			String payload = "";
			String[] parts = productId.split("\\.");
			if (parts.length == 0)
				return;
			MainNetworkManager.getInstance().serverLog("payment_track", "1. purchaseProduct with id " + productId);
			if (isHint(productId)) {
				mHelper.launchPurchaseFlow(callerActivity, productId,
						InAppManager.RC_REQUEST, mPurchaseFinishedListener, payload);
			} else {
				mHelper.launchSubscriptionPurchaseFlow(callerActivity, productId,
						InAppManager.RC_REQUEST, mPurchaseFinishedListener, payload);
			}
			return;
		} else {

			AlertDialog.Builder builder = new AlertDialog.Builder(
					callerActivity,
					R.style.Theme_Scanword_style_dialog);
			builder.setMessage(
					"Не удалось подключиться к Google Play.")
					.setPositiveButton("Ок",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {

								}
							});
			AlertDialog dialog = builder.create();
			dialog.show();
		}
	}

	@Override
	public void clear() {
		// TODO Auto-generated method stub
		if (mHelper != null)
			mHelper.dispose();
		mHelper = null;
	}

	@Override
	public String getPriceForProduct(String productId) {
		if (mHelper != null && mInventory != null) {
			// mInventory.getSkuDetails(productId).getPrice();
			if (mInventory.getSkuDetails(productId) != null)
				return mInventory.getSkuDetails(productId).getPrice();
		}
		return "";
	}

	@Override
	public void updatePricesForProducts(final List<String> poructsIds) {
		mPoructsIds = new ArrayList<String>(poructsIds);
		for (String sku : poructsIds) {
			try {
				SkuManager.getInstance().mapSku(sku, OpenIabHelper.NAME_GOOGLE,
						sku);
			} catch (SkuMappingException e) {

			}
		}
		queryProducts();
	}
	public void consumeProducts(List<String> products) {
		if (mInventory == null || true)
			return;
		/*Log.v("", "consumeProducts getAllPurchases :"+mInventory.getAllPurchases());
		Log.v("", "consumeProducts getAllOwnedSkus :"+mInventory.getAllOwnedSkus());
		for (Purchase purchase : mInventory.getAllPurchases()) {
			Log.v("",
					"purchase was successful. "
							+ purchase);
		}
		for (String string : products) {
			Purchase purch = mInventory.getPurchase(string);
			if (purch != null) {
				mHelper.consumeAsync(purch, mConsumeFinishedListener);
			}
		}*/
	}
	private void queryProducts() {
		if (mHelper == null || mPoructsIds == null || mHelper.getSetupState() != OpenIabHelper.SETUP_RESULT_SUCCESSFUL) {
			Log.v("", "failed  to query inventory  " + (mHelper == null) + "  " + (mPoructsIds == null));
			if (mHelper != null) {
				Log.v("", "failed  to query inventory  " + mHelper.getSetupState());
			}
			return;
		}
		Log.v("", " trying to query inventory  ");
		mHelper.queryInventoryAsync(true, mPoructsIds, mPoructsIds,
				mGotInventoryListener);
	}

	private IabHelper.QueryInventoryFinishedListener mGotInventoryListener = new IabHelper.QueryInventoryFinishedListener() {
		@Override
		public void onQueryInventoryFinished(IabResult result,
				Inventory inventory) {
			Log.d("", "Query inventory finished.");
			if (result.isFailure()) {
				Log.d("", "Query inventory failed.");
				return;
			}
			mInventory = inventory;
			for (Purchase purchase : mInventory.getAllPurchases()) {
				if (isHint(purchase.getSku())) {
					mHelper.consumeAsync(purchase, mConsumeFinishedListener);

		        	mDelegate.purchaseComplete(purchase.getSku(),  Base64
							.encodeToString(
									purchaseToString(purchase).getBytes(),
									Base64.DEFAULT));
				}
			}
			Log.v("",
					"Query inventory was successful. "
							+ inventory.getAllOwnedSkus());
			if (mDelegate != null) {
				mDelegate.initComplete();
			}
		}
	};

	IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
		public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
			Log.v("", "Purchase finished: " + result + ", purchase: "
					+ purchaseToString(purchase));

			MainNetworkManager.getInstance().serverLog("payment_track", "2. onIabPurchaseFinished with result success " + !result.isFailure());
			if (result.isFailure()) {
				MainNetworkManager.getInstance().serverLog("payment_track", "2.1. onIabPurchaseFinished failed with response message - " + result.getMessage());
				if (result.getResponse() == IabHelper.BILLING_RESPONSE_RESULT_USER_CANCELED && mDelegate != null) {
					
				} else if (result.getResponse() == IabHelper.BILLING_RESPONSE_RESULT_ERROR) {
					
				} else if (result.getResponse() == IabHelper.BILLING_RESPONSE_RESULT_ITEM_ALREADY_OWNED) {
					if (mPurchaseActivity != null) {
						restorePurchases(mPurchaseActivity);
					}
					return;
				}
				if (purchase != null)
					mDelegate.purchaseCanceled(purchase.getSku());
				return;
			}
			if (isHint(purchase.getSku())) {
				MainNetworkManager.getInstance().serverLog("payment_track", "2.2 onIabPurchaseFinished consume hints with id - " + purchase.getSku());
	            mHelper.consumeAsync(purchase, mConsumeFinishedListener);
			}
        	mDelegate.purchaseComplete(purchase.getSku(),  Base64
					.encodeToString(
							purchaseToString(purchase).getBytes(),
							Base64.DEFAULT));
		}
	};
	IabHelper.OnConsumeFinishedListener mConsumeFinishedListener = new IabHelper.OnConsumeFinishedListener() {
		public void onConsumeFinished(Purchase purchase, IabResult result) {
			Log.v("", "Consumption finished. Purchase: " + purchase
					+ ", result: " + result);

			Log.d("", "End consumption flow.");
		}
	};

	boolean verifyDeveloperPayload(Purchase p) {
		//String payload = p.getDeveloperPayload();

		return true;
	}

	@Override
	public void handleActivityResult(int requestCode, int resultCode,
			Intent data) {
		if (mHelper != null)
			mHelper.handleActivityResult(requestCode, resultCode, data);
		
	}

	@Override
	public void restorePurchases(Activity activity) {
		if (mInventory == null) {
			AlertDialog.Builder builder = new AlertDialog.Builder(
					activity,
					R.style.Theme_Scanword_style_dialog);
			builder.setMessage(
					"Не удалось подключиться к Google Play.")
					.setPositiveButton("Ок",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {

								}
							});
			AlertDialog dialog = builder.create();
			dialog.show();
			return;
		}
		
		List<Purchase> purchases = mInventory.getAllPurchases();
		Log.v("", "getAllPurchases :" + mInventory.getPurchaseMap());
		Log.v("", "getAllOwnedSkus :" + mInventory.getAllOwnedSkus());
		List<String> restoredPurchases = new ArrayList<String>();
		for (Purchase purchase : purchases) {
			restoredPurchases.add(Base64
					.encodeToString(
							purchaseToString(purchase).getBytes(),
							Base64.DEFAULT));
		}
		if (mDelegate != null) {
			mDelegate.purchasesRestored(restoredPurchases);
		}
	}
	public boolean isHint(String productId) {
		String[] parts = productId.split("\\.");
		for (String string : parts) {
			if (string.equals("hint"))
				return true;
		}
		return false;
	}
	public String purchaseToString(Purchase purchase) {
		if (purchase == null)
			return "";
        return "{\"orderId\":" + "\""+ purchase.getOrderId()+ "\""
                + ",\"packageName\":" + "\""+ purchase.getPackageName()+ "\""
                + ",\"productId\":" + "\""+ purchase.getSku()+ "\""
                + ",\"purchaseTime\":" + "\""+ purchase.getPurchaseTime()+ "\""
                + ",\"purchaseState\":" + "\""+ purchase.getPurchaseState()+ "\""
                + ",\"developerPayload\":" + "\""+ purchase.getDeveloperPayload()+ "\""
                + ",\"token\":" + "\""+ purchase.getToken()+ "\""
                + "}";
    }
	
}
