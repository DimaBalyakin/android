package org.skanword.and.inappbilling;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.skanword.and.SmappsScanwords;
import org.skanword.and.SmappsScanwords.ActionType;
import org.skanword.and.datamanager.MainDataManager;
import org.skanword.and.network.MainNetworkManager;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;

import com.musselwhizzle.dispatcher.events.SimpleEvent;


public class InAppManager implements StoreManagerDelegateInterface {
	private StoreManagerInterface mStoreManager;
	public static final String EVENT_PRICE_UPDATE = "EVENT_PRICE_UPDATE";
	static final int RC_REQUEST = 10001;
	private static InAppManager instance = null;
	private PurchaseSource lastPurchaseSource;
	private Activity mCallerActivity;

	public InAppManager() {
		super();
		mStoreManager = new UniversalStoreManager();

	}

	public String getStoreName() {
		return mStoreManager.getAppStoreName();
	}

	public void getProductsPrices(final List<String> skus) {
		Log.v("", " getProductsPrices " + skus);

		mStoreManager.updatePricesForProducts(skus);

	}

	public void initStoreManager(Activity activity) {
		mStoreManager.initManager(this, activity);
	}

	public static InAppManager getInstance() {
		if (instance == null) {
			instance = new InAppManager();
		}
		return instance;
	}

	public void clear() {
		if (mStoreManager != null)
			mStoreManager.clear();
	}

	public String getPriceForProductId(String productId) {
		if (mStoreManager != null)
			return mStoreManager.getPriceForProduct(productId);
		return "";
	}
	public void consumeProducts(Map<String, String> products) {
		if (mStoreManager != null && products != null) {
			List<String> produsctIds = new ArrayList<String>();
			produsctIds.addAll(products.values());
			mStoreManager.consumeProducts(produsctIds);
		}
	}
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		Log.v("", "InApp Manger onactivity result");
		mStoreManager.handleActivityResult(requestCode, resultCode, data);
	}

	public void purchaseProduct(String productId, Activity activity,
			PurchaseSource source) {
		lastPurchaseSource = source;
		mCallerActivity = activity;
		if (mStoreManager != null)
			mStoreManager.purchaseProduct(productId, activity);
		else return;
		switch (lastPurchaseSource) {
		case MINISHOP:
			SmappsScanwords.sendGAEvent(ActionType.USER_ACTION,
					"purchase_start_" + productId + "_playscan_minishop"); // GA
																			// Event
			break;
		case SHOP:
			SmappsScanwords.sendGAEvent(ActionType.USER_ACTION,
					"purchase_start_" + productId + "_shop"); // GA Event
			break;
		case LOCKED_TASK:
			SmappsScanwords.sendGAEvent(ActionType.USER_ACTION,
					"purchase_start_" + productId + "_lockedscanwin"); // GA
																		// Event
			break;
		case NEWS:
			SmappsScanwords.sendGAEvent(ActionType.USER_ACTION,
					"purchase_start_" + productId + "_news"); // GA
																		// Event
			break;

		default:
			break;
		}
	}

	public void restorePurchases(Activity activity) {
		if (mStoreManager != null) {
			mCallerActivity = activity;
			mStoreManager.restorePurchases(activity);
		}
	}

	@Override
	public void purchaseComplete(String productId, String recipe) {
		MainNetworkManager.getInstance().serverLog("payment_track", "3. purchaseComplete with id - " + productId);

		
		MainDataManager.getInstance().getPurchasesData()
				.addPurchase(recipe, new Date().getTime(), false);
		MainDataManager.getInstance().storePurchases();
		
		MainNetworkManager.getInstance().serverLog("payment_track", "4. purchaseComplete send data with id " + productId);
		MainNetworkManager.getInstance().purchaseComplete(mCallerActivity, true);
		if (lastPurchaseSource == null)
			return;
		switch (lastPurchaseSource) {
		case MINISHOP:
			SmappsScanwords.sendGAEvent(ActionType.USER_ACTION,
					"purchase_complete_" + productId + "_playscan_minishop"); // GA
																				// Event
			break;
		case SHOP:
			SmappsScanwords.sendGAEvent(ActionType.USER_ACTION,
					"purchase_complete_" + productId + "_shop"); // GA Event
			break;
		case LOCKED_TASK:
			SmappsScanwords.sendGAEvent(ActionType.USER_ACTION,
					"purchase_complete_" + productId + "_lockedscanwin"); // GA
																			// Event
			break;
		case NEWS:
			SmappsScanwords.sendGAEvent(ActionType.USER_ACTION,
					"purchase_complete_" + productId + "_news"); // GA
																			// Event
			break;

		default:
			break;
		}
	}

	@Override
	public void purchaseCanceled(String productId) {
		switch (lastPurchaseSource) {
		case MINISHOP:
			SmappsScanwords.sendGAEvent(ActionType.USER_ACTION,
					"purchase_canceled_" + productId + "_playscan_minishop"); // GA
																				// Event
			break;
		case SHOP:
			SmappsScanwords.sendGAEvent(ActionType.USER_ACTION,
					"purchase_canceled_" + productId + "_shop"); // GA Event
			break;
		case LOCKED_TASK:
			SmappsScanwords.sendGAEvent(ActionType.USER_ACTION,
					"purchase_canceled_" + productId + "_lockedscanwin"); // GA
																			// Event
			break;
		case NEWS:
			SmappsScanwords.sendGAEvent(ActionType.USER_ACTION,
					"purchase_canceled_" + productId + "_news"); // GA
																			// Event
			break;

		default:
			break;
		}

	};

	@Override
	public void purchasesRestored(List<String> purchases) {
		for (String recipe : purchases) {
			MainDataManager.getInstance().getPurchasesData()
					.addPurchase(recipe, new Date().getTime(), true);
		}
		MainDataManager.getInstance().storePurchases();
		MainNetworkManager.getInstance().purchaseComplete(mCallerActivity, false);
	}

	@Override
	public void initComplete() {
		SmappsScanwords.getEventsDispatcher().dispatchEvent(
				new SimpleEvent(InAppManager.EVENT_PRICE_UPDATE));

	}

	public enum PurchaseSource {
		MINISHOP, LOCKED_TASK, SHOP, NEWS
	}

	@Override
	public void purchaseOwned() {
		// TODO Auto-generated method stub
		
	}
}
