
package org.skanword.and;
 
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.skanword.and.datamanager.DataBaseManager;
import org.skanword.and.datamanager.MainDataManager;
import org.skanword.and.datamanager.UserUpdatesManager;
import org.skanword.and.datamanager.UserUpdatesManager.LoginType;
import org.skanword.and.etc.Foreground;
import org.skanword.and.etc.Foreground.Listener;
import org.skanword.and.etc.TopActivityManager;
import org.skanword.and.etc.Utils;
import org.skanword.and.inappbilling.InAppManager;
import org.skanword.and.menu.MainMenuActivity;
import org.skanword.and.network.MainNetworkManager;
import org.skanword.and.network.MainNetworkManager.RequestCompletionBlock;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Application;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.multidex.MultiDex;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;

import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.StandardExceptionParser;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.musselwhizzle.dispatcher.events.EventDispatcher;
import com.musselwhizzle.dispatcher.events.SimpleEvent;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.cache.memory.impl.LruMemoryCache;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;

public class SmappsScanwords extends Application {
 
    public static File CROSSWORDS_DIR;
    public static File TEMP_DIR;
    public static File QUARANTINE_DIR;
 
    public static File CACHE_DIR;
    public static File DEBUG_DIR;
 
    public static final Logger LOG = Logger.getLogger("org.sknanwords.android");
 
    public static final String DEVELOPER_EMAIL = "support@socialgames.bz";
    public static final String APPLICATION_TIMER_EVENT = "APPLICATION_TIMER_EVENT";
     
    public static final String NET_ID = "android";
 
    private static final String PREFERENCES_VERSION_PREF = "preferencesVersion";
    private static final int PREFERENCES_VERSION = 4;
 
    private static Context mContext;
     
 
    private static DataBaseManager dbHelper;
    private static EventDispatcher mEventsDispatcher;
     
    private static String vendorId = "";
    private static String advertisingId = "";
    private static ScheduledExecutorService scheduleExecutorService;
    private static ExecutorService executorService;
    private Timer timerExecutor;
    private static int mTimeOnline;
    private static int mTimeOffline;
    
    private AlertDialog mVersionInformDialog = null;
    private AlertDialog mVersionLockDialog = null;
     
    HashMap<TrackerName, Tracker> mTrackers = new HashMap<TrackerName, Tracker>();
	private TimerTask mTimerTask;
	
     
    public static SmappsScanwords instance = null;

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
 
    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        mContext = this;
        final boolean firstAppStart = getAppSharedPreferences(MainDataManager.GENERAL_PREFERENCES_NAME).getBoolean("first_app_start", true);
        TopActivityManager.init(this);
        Foreground.init(this);
        Foreground.get().addListener(new Listener() {
            boolean firstStart = true;
            @Override
            public void onBecameForeground(Activity foregroundActivity) {
                if (MainMenuActivity.instance != null && MainMenuActivity.instance.hasPermissions()) {
                    Log.v("SkanwordsFunc", "onBecameForeground");
                    Bundle extras = MainMenuActivity.instance.getOpenIntentExtras();
                    boolean localNotif = extras != null ? extras.getBoolean("local_notification", false) : false;
                    boolean pushNotif = extras != null ? extras.getBoolean("push_notification", false) : false;
                    Log.v("SkanwordsNotif", "onBecameForeground" + extras);
                    if (localNotif) {
                        UserUpdatesManager.getInstance().loggedIn(LoginType.LOGGIN_LOCAL_NOTIFICATION, extras.getString("i", null));
                    } else if (pushNotif) {
                        UserUpdatesManager.getInstance().loggedIn(LoginType.LOGGIN_PUSH_NOTIFICATION, extras.getString("i", null));
                    } else if (firstStart) {
                        UserUpdatesManager.getInstance().loggedIn(LoginType.LOGGIN_APP_START, null);
                        SmappsScanwords.sendGAEvent(ActionType.APP_ACTION, "app_enter");
                    } else {
                        UserUpdatesManager.getInstance().loggedIn(LoginType.LOGGIN_APP_FOREGROUND, null);
                        SmappsScanwords.sendGAEvent(ActionType.APP_ACTION, "app_recover");
                    }
                    if ((firstStart && MainNetworkManager.getInstance().getUserAuth() != null) || (!firstStart && MainNetworkManager.getInstance().getUserAuth() == null) || (MainNetworkManager.getInstance().getUserAuth() != null) || !firstAppStart) {
                        MainNetworkManager.getInstance().requestConfigExecWithCompleteBlock(new RequestCompletionBlock() {
                            @Override
                            public void complete(boolean success) {
                                if (success) {
                                    MainNetworkManager.getInstance().requestSync(null);
                                } else {
                                    checkVersions();
                                }
                            }
                        });
                    } else {
                        checkVersions();
                    }
                    firstStart = false;

                    resumeTimer();
                }
            }

            @Override
            public void onBecameBackground(Activity background) {
                if (MainMenuActivity.instance != null && MainMenuActivity.instance.hasPermissions()) {
                    Log.v("SkanwordsFunc", "onBecameBackground");
                    pauseTimer();

                    SmappsScanwords.sendGAEvent(ActionType.APP_ACTION, "app_minimize");
                }
            }
        });
        getAppSharedPreferencesEditor(MainDataManager.GENERAL_PREFERENCES_NAME).putBoolean("first_app_start", false).commit();
        // Check preferences version and perform any upgrades if necessary
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        int prefsVersion = prefs.getInt(PREFERENCES_VERSION_PREF, 0);
        if (prefsVersion != PREFERENCES_VERSION) {
            migratePreferences(prefs, prefsVersion);
        }

        File externalStorageDir = new File(
                Environment.getExternalStorageDirectory(),
                "Android/data/" + getPackageName() + "/files");

        CROSSWORDS_DIR = new File(externalStorageDir, "crosswords");
        TEMP_DIR = new File(externalStorageDir, "temp");
        QUARANTINE_DIR = new File(externalStorageDir, "quarantine");

        CACHE_DIR = getCacheDir();
        DEBUG_DIR = new File(CACHE_DIR, "debug");

        makeDirs();
        if (DEBUG_DIR.isDirectory() || DEBUG_DIR.mkdirs()) {
            File infoFile = new File(DEBUG_DIR, "device.txt");
            try {
                PrintWriter writer = new PrintWriter(infoFile);
                try {
                    writer.println("VERSION INT: " + android.os.Build.VERSION.SDK_INT);
                    writer.println("VERSION RELEASE: " + android.os.Build.VERSION.RELEASE);
                    writer.println("MODEL: " + android.os.Build.MODEL);
                    writer.println("DEVICE: " + android.os.Build.DEVICE);
                    writer.println("DISPLAY: " + android.os.Build.DISPLAY);
                    writer.println("MANUFACTURER: " + android.os.Build.MANUFACTURER);
                } finally {
                    writer.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            LOG.warning("Failed to create directory tree: " + DEBUG_DIR);
        }


        String version = "1.0.4"; // 1.0.4
        SharedPreferences settings = getAppSharedPreferences(MainDataManager.GENERAL_PREFERENCES_NAME);
        if (!settings.getString("db_version", "").equals(version)) {
            clearData();
            getAppSharedPreferencesEditor(MainDataManager.GENERAL_PREFERENCES_NAME).putString("db_version", version).commit();
        }
        mEventsDispatcher = new EventDispatcher();
        MainNetworkManager.getInstance().onCreate(getApplicationContext());
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getApplicationContext())
                .threadPriority(Thread.NORM_PRIORITY - 2)
                .denyCacheImageMultipleSizesInMemory()
                .memoryCache(new LruMemoryCache(2 * 1024 * 1024))
                .memoryCacheSize(2 * 1024 * 1024)
                .memoryCacheSizePercentage(13) // default
                .denyCacheImageMultipleSizesInMemory()
                .diskCacheFileNameGenerator(new Md5FileNameGenerator())
                .diskCacheSize(50 * 1024 * 1024) // 50 Mb
                .tasksProcessingOrder(QueueProcessingType.LIFO)
                .writeDebugLogs() // Remove for release app
                .build();
        // Initialize ImageLoader with configuration.
        ImageLoader.getInstance().init(config);
        retrieveAdvertisingId();
    }

    public void startDataApp() {
        dbHelper = new DataBaseManager(this);
        vendorId = Utils.getAndroidUniqueDeviceId();
        executorService = Executors.newFixedThreadPool(3);
        scheduleExecutorService = Executors.newSingleThreadScheduledExecutor();
        InAppManager.getInstance();

    }

    private void retrieveAdvertisingId () {
    	AsyncTask<Void, Void, String> task = new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                AdvertisingIdClient.Info idInfo = null;
                try {
                    idInfo = AdvertisingIdClient.getAdvertisingIdInfo(getApplicationContext());
                } catch (GooglePlayServicesNotAvailableException e) {
                    e.printStackTrace();
                } catch (GooglePlayServicesRepairableException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                String advertId = null;
                try{
                    advertId = idInfo.getId();
                }catch (NullPointerException e){
                    e.printStackTrace();
                }

                return advertId;
            }

            @Override
            protected void onPostExecute(String advertId) {
                advertisingId = advertId;
            }

        };
        task.execute();
    }
    public String getAdvertisingId() {
    	return advertisingId;
    }
	protected void resumeTimer() {
		pauseTimer();
		mTimerTask = new TimerTask() {
	        @Override
	        public void run() {
	            mEventsDispatcher.dispatchEvent(new SimpleEvent(APPLICATION_TIMER_EVENT));
	            if (Foreground.get().isForeground()) {
	                if(MainNetworkManager.getInstance().hasNetworkConnection())
	                    mTimeOnline++;
	                else
	                    mTimeOffline++;
	            }
	        }
	    };
        timerExecutor = new Timer();
        timerExecutor.scheduleAtFixedRate(mTimerTask , 0, 1000);
	}
	protected void pauseTimer() {
		if (timerExecutor != null)
			timerExecutor.cancel();
	}

	public boolean checkVersions() {
		PackageInfo pInfo = null;
		try {
			pInfo = SmappsScanwords.getContext().getPackageManager().getPackageInfo(getPackageName(), 0);
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}
		Log.v("SkanwordsFunc"," Checkversions" + (pInfo != null)  + "  " +  (MainDataManager.getInstance().getVersions() != null)  + "  " +  (TopActivityManager.get().getTopActivity() != null));
		if (pInfo != null && MainDataManager.getInstance().getVersions() != null) {
			Log.v("SkanwordsFunc"," Checkversions can check build ");
			if (TopActivityManager.get().getTopActivity() != null) {
				if (pInfo.versionCode <= MainDataManager.getInstance().getVersions().getLock().getBuild()) {
					showVersionLockAlert();
					return true;
				} else if (pInfo.versionCode <= MainDataManager.getInstance().getVersions().getInform().getBuild()) {
					showVersionInformAlert();
					return true;
				}
			} else {
				SmappsScanwords.getScheduleExecutorService().schedule(new Runnable() {
				    public void run() {
				    	checkVersions();
				    }
				}, 2, TimeUnit.SECONDS);
			}
		}
		return false;
	}
	private void showVersionLockAlert() {
		Log.v("SkanwordsFunc","showVersionLockAlert");
		TopActivityManager.get().getTopActivity().runOnUiThread(new Runnable() {
			@Override
			public void run() {
				if (mVersionInformDialog != null && mVersionInformDialog.isShowing()) {
					mVersionInformDialog.dismiss();
				}
				if (mVersionLockDialog == null) {
					AlertDialog.Builder builder = new AlertDialog.Builder(
							TopActivityManager.get().getTopActivity(), R.style.Theme_Scanword_style_dialog);
					builder.setMessage(MainDataManager.getInstance().getVersions().getLock().getText());
					builder.setCancelable(false);
					builder.setPositiveButton("Обновить", null);
					mVersionLockDialog = builder.create();
				}
				if  (!mVersionLockDialog.isShowing()) {
					mVersionLockDialog.show();
					mVersionLockDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {            
				          @Override
				          public void onClick(View v)
				          {
								navigateToGooglePlay();
				          }
				    });
				}
			}
		});
	}
	private void showVersionInformAlert() {
		Log.v("SkanwordsFunc","showVersionInformAlert");
		if (mVersionLockDialog != null) {
			return;
		}
		TopActivityManager.get().getTopActivity().runOnUiThread(new Runnable() {
			@Override
			public void run() {
				if (mVersionInformDialog == null) {
					AlertDialog.Builder builder = new AlertDialog.Builder(
							TopActivityManager.get().getTopActivity(), R.style.Theme_Scanword_style_dialog);
					builder.setMessage(MainDataManager.getInstance().getVersions().getInform().getText())
							.setPositiveButton("Обновить",
									new DialogInterface.OnClickListener() {
										public void onClick(
												DialogInterface dialog, int id) {
											navigateToGooglePlay();
										}
									})
							.setNegativeButton("Позже", null);
					mVersionInformDialog = builder.create();
				}
				if (!mVersionInformDialog.isShowing())
					mVersionInformDialog.show();
			}
		});
	}
	public void navigateToGooglePlay() {
		final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
		try {
			Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName));
			intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
		    startActivity(intent);
		} catch (android.content.ActivityNotFoundException anfe) {
			Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName));
			intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
		    startActivity(intent);
		}
	}
    private void clearData() {
        this.deleteDatabase(DataBaseManager.DATABASE_NAME);
        deleteRecursive(CROSSWORDS_DIR, true);
    }
    
    public static void deleteRecursive(File fileOrDirectory, boolean root) {
        if (fileOrDirectory.isDirectory())
            for (File child : fileOrDirectory.listFiles())
                deleteRecursive(child, false);
        if (!root)
            fileOrDirectory.delete();
    }
 
    public static int getTime(boolean online, boolean clear) {
        int retVal = 0;
        if (online) {
            retVal = mTimeOnline;
            if (clear) {
                mTimeOnline = 0;
            }
        } else {
            retVal = mTimeOffline;
            if (clear) {
                mTimeOffline = 0;
            }
        }
         
        return retVal;
    }
 
    public static Context getContext() {
        return mContext;
    }
 
    private void migratePreferences(SharedPreferences prefs, int prefsVersion) {
        LOG.info("Upgrading preferences from version " + prefsVersion + " to to version " + PREFERENCES_VERSION);
 
        SharedPreferences.Editor editor = prefs.edit();
 
        switch (prefsVersion) {
        case 0:
            editor.putBoolean("enableIndividualDownloadNotifications", !prefs.getBoolean("suppressMessages", false));
            // Fall-through
        case 1:
            editor.putBoolean("showRevealedLetters", !prefs.getBoolean("suppressHints", false));
            // Fall-through
        case 2:
            editor.putString("showKeyboard",
                             prefs.getBoolean("forceKeyboard", false) ? "SHOW" : "AUTO");
            // Fall-through
        case 3:
            try {
                // This is ugly.  But I don't see a clean way of detecting
                // what data type a preference is.
                int clueSize = prefs.getInt("clueSize", 12);
                editor.putString("clueSize", Integer.toString(clueSize));
            } catch (ClassCastException e) {
                // Ignore
            }
        }
 
        editor.putInt(PREFERENCES_VERSION_PREF, PREFERENCES_VERSION);
        editor.commit();
    }
 
    public static boolean makeDirs() {
        if (!Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            return false;
        }
 
        for (File dir : new File[]{CROSSWORDS_DIR, TEMP_DIR, QUARANTINE_DIR, DEBUG_DIR}) {
            if (!dir.isDirectory() && !dir.mkdirs()) {
                LOG.warning("Failed to create directory tree: " + dir);
                return false;
            }
        }
 
        return true;
    }
 
 
    @SuppressLint("WorldReadableFiles")
    public static Intent sendDebug(Context context) {
        String filename = "debug.zip";
        File zipFile = new File(context.getFilesDir(), filename);
        if (zipFile.exists()) {
            zipFile.delete();
        }
 
        if (!DEBUG_DIR.exists()) {
            LOG.warning("Can't send debug package, " + DEBUG_DIR + " doesn't exist");
            return null;
        }
 
        saveLogFile();
 
        try {
            ZipOutputStream zos = new ZipOutputStream(
                context.openFileOutput(filename, MODE_WORLD_READABLE));
            try {
                zipDir(DEBUG_DIR.getAbsolutePath(), zos);
            } finally {
                zos.close();
            }
 
            Intent sendIntent = new Intent(Intent.ACTION_SEND);
            sendIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            sendIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[] { DEVELOPER_EMAIL });
            sendIntent.putExtra(Intent.EXTRA_SUBJECT, "Words With Crosses Debug Package");
            Uri uri = Uri.fromFile(zipFile);
            sendIntent.putExtra(Intent.EXTRA_STREAM, uri);
            LOG.info("Sending debug info: " + uri);
            sendIntent.setType("application/octet-stream");
            return sendIntent;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
 
    private static void saveLogFile() {
         
    }
 
    public static void zipDir(String dir2zip, ZipOutputStream zos)
        throws IOException {
        File zipDir = new File(dir2zip);
        String[] dirList = zipDir.list();
        byte[] readBuffer = new byte[4096];
        int bytesIn = 0;
        for (int i = 0; i < dirList.length; i++) {
            File f = new File(zipDir, dirList[i]);
            if (f.isDirectory()) {
                String filePath = f.getPath();
                zipDir(filePath, zos);
                continue;
            }
            FileInputStream fis = new FileInputStream(f);
 
            ZipEntry anEntry = new ZipEntry(f.getPath());
            zos.putNextEntry(anEntry);
            while ((bytesIn = fis.read(readBuffer)) != -1) {
                zos.write(readBuffer, 0, bytesIn);
            }
            fis.close();
        }
    }
     
    public static DataBaseManager getDatabaseHelper() {
        return dbHelper;
    }
 
    public static double getScreenSizeInInches(DisplayMetrics metrics) {
        double x = metrics.widthPixels/metrics.xdpi;
        double y = metrics.heightPixels/metrics.ydpi;
        return Math.hypot(x, y);
    }
 
    public static boolean isTabletish(DisplayMetrics metrics) {
        if (android.os.Build.VERSION.SDK_INT < 11) {
            return false;
        }
 
        double screenInches = getScreenSizeInInches(metrics);
        return (screenInches > 9.0);  // look for a 9" or larger screen.
    }
    public static ExecutorService getExecutorService() {
        return executorService;
    }
    public static ScheduledExecutorService getScheduleExecutorService() {
        return scheduleExecutorService;
    }
    public static String getVendorId () {
        return vendorId;
    }
 
    public static EventDispatcher getEventsDispatcher() {
        return mEventsDispatcher;
    }
     
    @Override
    public void onTerminate() {
        super.onTerminate();
        InAppManager.getInstance().clear();
        MainNetworkManager.getInstance().clear();
    }
    synchronized public Tracker getTracker(TrackerName trackerId) {
        if (!mTrackers.containsKey(trackerId)) {
 
          GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
          Tracker t = analytics.newTracker(R.xml.app_tracker);
          t.enableAdvertisingIdCollection(true);
          analytics.enableAutoActivityReports(this);
          mTrackers.put(trackerId, t);
 
        }
        return mTrackers.get(trackerId);
    }
    public static void sendGAEvent(final ActionType category,final String action) {
        if (SmappsScanwords.instance == null || getExecutorService() == null)
            return;
        getExecutorService().execute(new Runnable() {
            @Override
            public void run() {
                String categoryString = "app_category";
                if (category == ActionType.USER_ACTION)
                    categoryString = "user_category";
                //Log.v("GAEVENT", categoryString + "  " + action);
                Tracker t = SmappsScanwords.instance.getTracker(
                        TrackerName.APP_TRACKER);
                    t.send(new HitBuilders.EventBuilder()
                        .setCategory(categoryString)
                        .setAction(action)
                        .build());
            }
        });
    }
    public static void sendCaughtException(Exception e) {
        Tracker t = SmappsScanwords.instance.getTracker(
                TrackerName.APP_TRACKER);
        t.send(new HitBuilders.ExceptionBuilder()
        	.setDescription(
        			new StandardExceptionParser(SmappsScanwords.getContext(), null)
        			.getDescription(Thread.currentThread().getName(), e))
                .setFatal(false)
                .build()
        		);
    }
     
     
    public static SharedPreferences getAppSharedPreferences(String prefernceName) {
        return getAppSharedPreferences(prefernceName, 0);
    }
    synchronized public static SharedPreferences getAppSharedPreferences(String prefernceName, int mode) {
        return getContext().getSharedPreferences(prefernceName, mode);
    }
    public static SharedPreferences.Editor  getAppSharedPreferencesEditor(String prefernceName) {
        return getAppSharedPreferences(prefernceName).edit();
    }
     
    public enum TrackerName {
        APP_TRACKER,
        GLOBAL_TRACKER,
    }
    public enum ActionType {
        APP_ACTION,
        USER_ACTION,
    }
}