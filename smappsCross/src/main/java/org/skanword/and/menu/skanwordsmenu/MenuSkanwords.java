package org.skanword.and.menu.skanwordsmenu;

import java.util.concurrent.TimeUnit;

import org.skanword.and.R;
import org.skanword.and.SmappsScanwords;
import org.skanword.and.SmappsScanwords.ActionType;
import org.skanword.and.datamanager.MainDataManager;
import org.skanword.and.datamanager.SkanwordsDataManager;
import org.skanword.and.datamanager.SkanwordsDataManager.TaskListDirrectionType;
import org.skanword.and.datamanager.SkanwordsDataManager.TaskListType;
import org.skanword.and.etc.CustomFragment;
import org.skanword.and.etc.TutorialManager;
import org.skanword.and.etc.TutorialManager.TutorialStep;
import org.skanword.and.network.MainNetworkManager;
import org.skanword.and.network.MainNetworkManager.RequestCompletionBlock;

import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.astuetz.PagerSlidingTabStrip;
import com.musselwhizzle.dispatcher.events.Event;
import com.musselwhizzle.dispatcher.events.EventListener;

public class MenuSkanwords extends CustomFragment implements ActionBar.TabListener{

    AppSectionsPagerAdapter mAppSectionsPagerAdapter = null;
    
	private PagerSlidingTabStrip mTabs;
    private ViewPager mViewPager;
    private View mMainView;
    
	private int mTabToOpen = 0;

	private EventListener mUpdateListEventListener;

	private EventListener mTutorialStateListener;

	private static Runnable mUpdateRunnable = null;
	public MenuSkanwords() {
    	Log.v("","MenuScanwords --  ");
	}

    @Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
    	

		mMainView = inflater.inflate(R.layout.menu_page_slider, container, false);
		mAppSectionsPagerAdapter = new AppSectionsPagerAdapter(getChildFragmentManager());
		mTabs = (PagerSlidingTabStrip) mMainView.findViewById(R.id.tabs);
		mTabs.setTextColorResource(R.color.action_bar_title_color);
		mTabs.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
			@Override
			public void onPageSelected(int position) {
				switch (position) {
				case 0:
					SmappsScanwords.sendGAEvent(ActionType.USER_ACTION, "menu_scans_new_enter"); // GA Event
					break;
				case 1:
					SmappsScanwords.sendGAEvent(ActionType.USER_ACTION, "menu_scans_began_enter"); // GA Event
					break;
				case 2:
					SmappsScanwords.sendGAEvent(ActionType.USER_ACTION, "menu_scans_solved_enter"); // GA Event
					break;

				default:
					break;
				}
				openTabForPosition(position, true, false);
			}
			
			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
				
			}
			
			@Override
			public void onPageScrollStateChanged(int state) {
				
			}
		});
		
        mViewPager = (ViewPager) mMainView.findViewById(R.id.pager);
        mViewPager.setAdapter(mAppSectionsPagerAdapter);
        mViewPager.removeAllViewsInLayout();
        mAppSectionsPagerAdapter.notifyDataSetChanged();
        
		mTabs.setViewPager(mViewPager);
		mUpdateListEventListener = new EventListener() {
			@Override
			public void onEvent(Event event) {
				getActivity().runOnUiThread(new Runnable() {
					@Override
					public void run() {
						updateCurrentFragmentPage(true);
					}
				});
			}
		};
		SmappsScanwords.getEventsDispatcher().addListener(SkanwordsDataManager.UPDATE_ISSUES_LIST, mUpdateListEventListener);
		mTutorialStateListener = new EventListener() {
			
			@Override
			public void onEvent(Event event) {
				checkTutorialState();
			}
		};
		SmappsScanwords.getEventsDispatcher().addListener(TutorialManager.TUTORIAL_STATE_CHANGED, mTutorialStateListener);
		return mMainView;
    }
	@Override
	public void updateFragment(boolean byUser) {
		if (MainNetworkManager.getInstance().hasNetworkConnection(byUser? getActivity() : null))
			updateCurrentFragmentPage(false, byUser);
		super.updateFragment(byUser);
	}
	private void requestPageUpdate(final TaskListType listType, final TaskListDirrectionType dirrectionType, boolean byUser) {
		Log.v("SkanwordsFunc", "requestIssuesListExecWithCompleteBlock  " + listType + "  " + dirrectionType + "  " + byUser);
		if (!MainNetworkManager.getInstance().userLoggedIn()) {
			MainDataManager.getInstance().getSkanwordsDataManager().notifyLocalDataUsegae(listType, dirrectionType);
			return;
		}
		Log.v("SkanwordsFunc", "requestIssuesListExecWithCompleteBlock");
		MainNetworkManager.getInstance().requestIssuesListExecWithCompleteBlock(listType, dirrectionType, new RequestCompletionBlock() {
			@Override
			public void complete(boolean success) {
				Log.v("SkanwordsFunc", "requestIssuesListExecWithCompleteBlock complete "+ success);
				if (!success) {
					if (!MainNetworkManager.getInstance().hasNetworkConnection())
						MainDataManager.getInstance().getSkanwordsDataManager().notifyLocalDataUsegae(listType, dirrectionType);
					else {
						if (getActivity() != null)
							getActivity().runOnUiThread(new Runnable() {
								@Override
								public void run() {
									if (getRegisteredFragment(mViewPager, mTabToOpen) != null)
										getRegisteredFragment(mViewPager, mTabToOpen).showLoadingError();
								}
							});
					}
				}
				checkTutorialState();
			}
		}, byUser ? getActivity() : null);
	}
	protected void checkTutorialState() {
		
		boolean show = false;
		if (TutorialManager.getInstance().currentTutorialStep() == TutorialStep.TUTORIAL_START) {
			if (getRegisteredFragment(mViewPager, mTabToOpen) != null && getRegisteredFragment(mViewPager, mTabToOpen).getSetsInfosCount() > 0) {
				show = true;
			}
		} else if (TutorialManager.getInstance().currentTutorialStep() == TutorialStep.TUTORIAL_ISSUE_DOWNLOADED) {
			if (getRegisteredFragment(mViewPager, mTabToOpen) != null && getRegisteredFragment(mViewPager, mTabToOpen).getSetsInfosCount() > 0 && MainDataManager.getInstance().getSkanwordsDataManager().getSetsInfos() != null && MainDataManager.getInstance().getSkanwordsDataManager().getSetsInfos().size() > 0) {
				
				show = true;
			}
		} 
		final boolean fShow = show; 
		if (getActivity() != null)
			getActivity().runOnUiThread(new Runnable() {
				@Override
				public void run() {
					mMainView.findViewById(R.id.tutorialView).setVisibility(fShow ? View.VISIBLE : View.GONE);
					if (TutorialManager.getInstance().currentTutorialStep() == TutorialStep.TUTORIAL_ISSUE_DOWNLOADED) {
						((TextView)mMainView.findViewById(R.id.tutorialText)).setText("Теперь можно открыть выпуск.\nнажмите на него.");
						((ImageView)mMainView.findViewById(R.id.tutorialImage)).setImageResource(R.drawable.tutorial_img_issue_open);
					}
				}
			});
		
		
	}

	public SkanwordsIssuesList getRegisteredFragment(ViewPager container, int position) {
		String name = makeFragmentName(container.getId(), position);
		if (getActivity() == null)
			return null;
		return (SkanwordsIssuesList) getChildFragmentManager().findFragmentByTag(name);
	}

	private static String makeFragmentName(int viewId, int index) {
	    return "android:switcher:" + viewId + ":" + index;
	}
	private void openTabForPosition(int position, boolean update) {
		openTabForPosition(position, update, false);
	}
	private void openTabForPosition(int position, boolean update, boolean byUser) {
		if (getRegisteredFragment(mViewPager, mTabToOpen) != null)
			getRegisteredFragment(mViewPager, mTabToOpen).clearTab();
		mTabToOpen = position;
		Log.v("SkanwordsFunc", "openTabForPosition " + update);
		if (update)
			updateCurrentFragmentPage(true, byUser);
	}
	private void updateCurrentFragmentPage(final boolean toTop) {
		updateCurrentFragmentPage(toTop, false);
	}
	private void updateCurrentFragmentPage(final boolean toTop, final boolean byUser) {
		if (getRegisteredFragment(mViewPager, mTabToOpen) != null) {
			SkanwordsIssuesList f = getRegisteredFragment(mViewPager, mTabToOpen);
			try {
				f.showDownloadingState(true); // show spinner
			} catch (Exception e) {
				e.printStackTrace();
			}
			requestPageUpdate(TaskListType.values()[mTabToOpen], toTop ? TaskListDirrectionType.TOP_DIRRECTION : TaskListDirrectionType.CURRECT_DIRRECTION, byUser);
		} else {
			if (mUpdateRunnable == null) {
				mUpdateRunnable = new Runnable() {
				    public void run() {
			    		updateCurrentFragmentPage(toTop, byUser);
				    }
				};
			}
			SmappsScanwords.getScheduleExecutorService().schedule(mUpdateRunnable , 2, TimeUnit.SECONDS);
		}
	}
	public void selectTab(int position, boolean update) {
		if (mViewPager == null)
			return;
		openTabForPosition(position, update);
		mViewPager.setCurrentItem(mTabToOpen, true);
	}
	public void selectTab(int position) {
		selectTab(position, true);
	}
    public Fragment getVisibleFragment(){
    	Fragment page = (Fragment)mAppSectionsPagerAdapter.instantiateItem(mViewPager, mViewPager.getCurrentItem());
	    if (page != null) {
	         return page;     
	    } 
        return null;
    }
    public void openSortDialog() {
    	if (getVisibleFragment() != null) {
    		((SkanwordsIssuesList) getVisibleFragment()).openSortDialog();
    	}
    }
    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to one of the primary
     * sections of the app.
     */
    public class AppSectionsPagerAdapter extends FragmentPagerAdapter {

        public AppSectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }
        
        
        @Override
        public Fragment getItem(int i) {
        	SkanwordsIssuesList fragment = new SkanwordsIssuesList();
        	fragment.setFragmentPosition(i);
            Bundle args = new Bundle();
            TaskListType type = TaskListType.ISSUE_TASKS;
            switch (i) {
			case 1:
				type = TaskListType.STARTED_TASKS;
				break;
			case 2:
				type = TaskListType.FINISHED_TASKS;
				break;

			default:
				break;
			}
            
            args.putSerializable("list_type", type);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
        	switch (position) {
			case 0:
				return "НОВЫЕ";
			case 1:
				return "НАЧАТЫЕ";
			case 2:
				return "РЕШЕННЫЕ";

			default:
				break;
			}
            return "Section " + (position + 1);
        }
	    @Override
	    public int getItemPosition(Object object){
	        return PagerAdapter.POSITION_NONE;
	    }
    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
		selectTab(mTabToOpen, false);
    	super.onActivityCreated(savedInstanceState);
    }
    @Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		Log.v("" ,"onDestroy -------MenuScanwords------");

		SmappsScanwords.getEventsDispatcher().removeListener(SkanwordsDataManager.UPDATE_ISSUES_LIST, mUpdateListEventListener);
		SmappsScanwords.getEventsDispatcher().removeListener(TutorialManager.TUTORIAL_STATE_CHANGED, mTutorialStateListener);
		super.onDestroy();
	}

	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		Log.v("" ,"onPause -------MenuScanwords------");
		super.onPause();
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		Log.v("" ,"onResume -------MenuScanwords------  " + getView());
		super.onResume();
	}

	@Override
	public void onStart() {
		Log.v("SkanwordsFunc" ,"onStart -------MenuScanwords------");
		updateCurrentFragmentPage(false);
		super.onStart();
	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		Log.v("" ,"onStop -------MenuScanwords------");
		super.onStop();
	}

	@Override
    public void onDestroyView() {
        super.onDestroyView();
    }

	public void onTabSelected(Tab tab, FragmentTransaction ft) {
		// TODO Auto-generated method stub
		Log.v("", "onTabSelected " + tab.getPosition());
		
	}

	public void onTabUnselected(Tab tab, FragmentTransaction ft) {
		// TODO Auto-generated method stub
        mViewPager.setCurrentItem(tab.getPosition());
		Log.v("", "onTabUnselected " + tab.getPosition());
		
	}

	public void onTabReselected(Tab tab, FragmentTransaction ft) {
		// TODO Auto-generated method stub
        mViewPager.setCurrentItem(tab.getPosition());
		Log.v("", "onTabReselected " + tab.getPosition());
		
	}
}