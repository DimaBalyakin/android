package org.skanword.and.menu.skanwordsmenu;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.skanword.and.R;
import org.skanword.and.SmappsScanwords;
import org.skanword.and.SmappsScanwords.ActionType;
import org.skanword.and.datamanager.MainDataManager;
import org.skanword.and.datamanager.Skanword;
import org.skanword.and.datamanager.Skanword.Keyword;
import org.skanword.and.datamanager.Skanword.Keyword.KeywordCell;
import org.skanword.and.datamanager.SkanwordsDataManager.SkanwordsSetsUpdateEvent;
import org.skanword.and.datamanager.SkanwordsDataManager.TaskListType;
import org.skanword.and.etc.AdsManager;
import org.skanword.and.etc.TopActivityManager.TopActivity;
import org.skanword.and.etc.TutorialManager;
import org.skanword.and.etc.TutorialManager.TutorialStep;
import org.skanword.and.etc.Utils;
import org.skanword.and.menu.ArraySectionedListAdapter;
import org.skanword.and.menu.LockedTaskActivity;
import org.skanword.and.menu.MainMenuActivity;
import org.skanword.and.network.MainNetworkManager;
import org.skanword.and.scanwordgame.SkanwordGameActivity;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.musselwhizzle.dispatcher.events.Event;
import com.musselwhizzle.dispatcher.events.EventListener;
import com.sbstrm.appirater.Appirater;

public class SkanwordsTasksList extends Activity implements TopActivity {

	private static int sCurrentSortType = 0;
	private boolean mCurrentSortTypeChanged;
	private ListView mListView = null;
	private TaskListType mListType;
	private List<Skanword> mSkanwordsList;
	private Integer mIssueId = 0;
	private SkanwordsTasksAdapter mAdapter;
	private EventListener mSkanwordsUpdateEventListener;
	private EventListener mTimerEventListener;
	private EventListener mProgressUpdateListener;
	private EventListener mTutorialStateListener;
	

	public static final int TYPE_ROW_DELETE_ISSUE_VIEW = ArraySectionedListAdapter.TYPE_ROW_VIEW + 1;
	protected static final String SKANWORDS_COUNT_OVER_LIMIT = "SKANWORDS_COUNT_OVER_LIMIT";

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.scanwords_tasks_list_view);
		Bundle extras = getIntent().getExtras();
		mListType = (TaskListType) extras.getSerializable("LIST_TYPE");
		if (mListType == TaskListType.ISSUE_TASKS) {
			mIssueId = extras.getInt("ISSUE_ID", 0);
		}
		if (TutorialManager.getInstance().currentTutorialStep() == TutorialStep.TUTORIAL_ISSUE_DOWNLOADED) {
			TutorialManager.getInstance().setTutorialStep(TutorialStep.TUTORIAL_ISSUE_OPENED);

			SmappsScanwords.getEventsDispatcher().dispatchEvent(
					new SkanwordsSetsUpdateEvent(
							MainDataManager.EVENT_SKANWORDS_SETS_INFOS_UPDATE));
		}
		ActionBar actionBar = getActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		setTitle(extras.getString("LIST_TITLE"));
		
		mSkanwordsList = mIssueId > 0 ? MainDataManager.getInstance()
				.getSkanwordsDataManager()
				.getSkanwordsForListType(mListType, mIssueId) : MainDataManager.getInstance()
				.getSkanwordsDataManager()
				.getSkanwordsForListType(mListType);
				

		if (extras.getBoolean(SKANWORDS_COUNT_OVER_LIMIT, false)) {
			if (mSkanwordsList.size() >= MainDataManager.getInstance().getSkanwordsDataManager().getMaxSkanwordsOnPageCount()) {
				AlertDialog.Builder builder = new AlertDialog.Builder(this,
						R.style.Theme_Scanword_style_dialog);
				builder.setMessage(
						"Список отображает " + MainDataManager.getInstance().getSkanwordsDataManager().getMaxSkanwordsOnPageCount() + " сканвордов.\nЧтобы увидеть больше - перейдите к списку выпусков.")
						.setPositiveButton("Ок",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int id) {
										
									}
								});
				// Create the AlertDialog object and return it
				AlertDialog dialog = builder.create();
				dialog.show();
			}
		}		
				
		mSkanwordsUpdateEventListener = new EventListener() {

			public void onEvent(Event event) {

				mSkanwordsList = mIssueId > 0 ? MainDataManager.getInstance()
						.getSkanwordsDataManager()
						.getSkanwordsForListType(mListType, mIssueId) : MainDataManager.getInstance()
						.getSkanwordsDataManager()
						.getSkanwordsForListType(mListType);
				update();

			}
		};
		SmappsScanwords.getEventsDispatcher().addListener(
				MainDataManager.EVENT_SKANWORDS_TASKS_UPDATE,
				mSkanwordsUpdateEventListener);
		mListView = (ListView) findViewById(R.id.tasks_list);
		mListView.setScrollBarStyle(ListView.SCROLLBARS_OUTSIDE_OVERLAY);
		mListView.setBackgroundResource(android.R.color.white);
		

		mTimerEventListener = new EventListener() {
			public void onEvent(Event event) {
				runOnUiThread(new Runnable() {
					public void run() {
						if (mAdapter == null)
							return;
						if (mAdapter.isBuildForVip() != MainDataManager.getInstance().getUserData().isVip()) {
							mAdapter.notifyDataSetInvalidated();
						}
					}
				});
			}
		};
		
		if (mTimerEventListener != null)
			SmappsScanwords.getEventsDispatcher().addListener(
				SmappsScanwords.APPLICATION_TIMER_EVENT, mTimerEventListener);
		
		mProgressUpdateListener = new EventListener() {
			public void onEvent(Event event) {
				runOnUiThread(new Runnable() {
					public void run() {
						setResult(555, null);
					    finish();	
					}
				});
			}
		};
		
		SmappsScanwords.getEventsDispatcher().addListener(
					MainDataManager.EVENT_PROGRESS_UPDATE, mProgressUpdateListener);
		update();

		
	}

	protected void checkTutorialState() {
		
		boolean show = false;
		if (TutorialManager.getInstance().currentTutorialStep() == TutorialStep.TUTORIAL_ISSUE_OPENED) {
			show = true;
		}
		final boolean fShow = show; 
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				findViewById(R.id.tutorialView).setVisibility(fShow ? View.VISIBLE : View.GONE);
			}
		});
		
		
	}
	
	
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add(0, 5, 0, "mytext").setIcon(R.drawable.filter)
				.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			this.finish();
			return true;
		case 5:
			this.openSortDialog();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onPause() {
		super.onPause();
	}

	private void openSortDialog() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this,
				R.style.Theme_Scanword_style_dialog);
		// Set the dialog title
		List<String> listItems = new ArrayList<String>();

		listItems.add("По номеру");
		listItems.add("Нерешенные сверху");

		builder.setSingleChoiceItems(
				listItems.toArray(new CharSequence[listItems.size()]),
				sCurrentSortType, new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog, int which) {
						sCurrentSortType = which;
						mCurrentSortTypeChanged = true;
					}
				}).setPositiveButton("Ок",
				new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog, int which) {
						if (mCurrentSortTypeChanged)
							changeSortType();
						mCurrentSortTypeChanged = false;
					}
				});

		AlertDialog alertDialog = builder.create();
		alertDialog.getWindow().setBackgroundDrawable(
				new ColorDrawable(android.graphics.Color.TRANSPARENT));
		alertDialog.setCancelable(false);
		alertDialog.show();

	}

	private void changeSortType() {
		Log.v("", "changeSortType  " + sCurrentSortType);
		if (mSkanwordsList == null || mSkanwordsList.size() == 0)
			return;
		Collections.sort(mSkanwordsList, new Comparator<Skanword>() {
			public int compare(Skanword lhs, Skanword rhs) {
				if (sCurrentSortType == 0)
					return (int) (lhs.getId() - rhs.getId());
				else {
					if (lhs.isFinished() && rhs.isFinished()) {
						return (int)(lhs.getId() - rhs.getId());
					}
					if (lhs.isFinished())
						return 1;
					if (rhs.isFinished())
						return -1;
					return (int) ((lhs.getQuestionsCount() - lhs
							.getFinishedQuestionsCount()) - ((rhs
							.getQuestionsCount() - rhs.getFinishedQuestionsCount())));
				}
			}
		});
		if (mAdapter != null)
			mAdapter.notifyDataSetChanged();
	}

	public void update() {
		runOnUiThread(new Runnable() {
			public void run() {
				if (mListView == null)
					return;
				setAdapter();
			}
		});
	}

	private void startSkanword(Skanword skanword) {
		Intent intent = new Intent(Intent.ACTION_EDIT, null, this,
				SkanwordGameActivity.class);
		intent.putExtra(SkanwordGameActivity.EXTRA_SKAN_ID, skanword.getId());
		intent.putExtra(SkanwordGameActivity.EXTRA_SET_ID, mIssueId);
		//intent.putExtra(SkanwordGameActivity.EXTRA_SKANWORD_OBJECT, SmappsScanwords.getDatabaseHelper().getFullskanwordData(skanword.getId()));
		this.startActivityForResult(intent, 101);
		SmappsScanwords.sendGAEvent(ActionType.USER_ACTION, "playscan_scanword_enter"); // GA Event
		if ( MainDataManager.getInstance().getSkanwordsDataManager().isSkanwordOffered(skanword.getId()))
			SmappsScanwords.sendGAEvent(ActionType.USER_ACTION, "click_menu_scans_offeredscan_opened"); // GA Event
		MainNetworkManager.getInstance().requestSyncExec();
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == 101) {
			if (resultCode == 556 || resultCode == 555) {
				setResult(resultCode);
				finish();
			} else {
				boolean timeMatch = new Date().getTime() - Appirater.getAppiraterSharedPrefernces(this).getLong(Appirater.PREF_DATE_LAST_SHOW, 0) > 60 * 60 * 115;
				boolean shown = Appirater.getAppiraterSharedPrefernces(this).getBoolean(Appirater.PREF_RATE_CLICKED, false) || Appirater.getAppiraterSharedPrefernces(this).getBoolean(Appirater.PREF_DONT_SHOW, false);
				boolean placeMatch = MainDataManager.getInstance().getSkanwordsDataManager().readyToRateTheApp();
				if (timeMatch && !shown && placeMatch) {
					Appirater.showRateDialog(this);
				} else {
					AdsManager.getInstance().showInterstitial(this);
				}
				MainNetworkManager.getInstance().requestSyncExec();
			}
		} else if (requestCode == LockedTaskActivity.LOCKED_TASK_ACTIVITY_REQUEST_CODE && resultCode == 100) {
			Log.v("", "onActivityResult - scanwordsTaskList  ");
			setResult(resultCode);  
			finish();
		}
	}

	private void openLockedTask(Skanword scanword) {
		MainMenuActivity.openLockedTaskactivity(this, scanword);
	}

	private void setAdapter() {
		Log.v("","setAdapter tasks 2");		
		changeSortType();
		mAdapter = new SkanwordsTasksAdapter(this, R.layout.scanword_row_item,
				mSkanwordsList, mIssueId < 1);
		mListView.setAdapter(mAdapter);
		mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				if (mAdapter.getItemViewType(position) == TYPE_ROW_DELETE_ISSUE_VIEW) {
					AlertDialog.Builder builder = new AlertDialog.Builder(
							SkanwordsTasksList.this,
							R.style.Theme_Scanword_style_dialog);
					builder.setMessage("Выпуск будет удален с сохранением прогресса. Вы всегда можете закачать выпуск заново и продолжить игру.")
							.setPositiveButton("Удалить",
									new DialogInterface.OnClickListener() {
										public void onClick(DialogInterface dialog,
												int id) {
											MainDataManager.getInstance().getSkanwordsDataManager().deleteIssuesWithId(mIssueId);
										    finish();	
										}
									}).setNegativeButton("Отмена", null);
					AlertDialog dialog = builder.create();
					dialog.show();
					
				} else {
					if (MainDataManager.getInstance().getSkanwordsDataManager()
							.isSkanwordFree(mAdapter.getItemForPosition(position).getId()) || MainDataManager.getInstance().getUserData().isVip()) {
						startSkanword(mAdapter.getItemForPosition(position));
					} else {
						openLockedTask(mAdapter.getItemForPosition(position));
					}
				}
			}

		});
	}

	@SuppressLint("UseSparseArrays")
	public static class SkanwordsTasksAdapter extends
			ArraySectionedListAdapter<Skanword> {
		private final int resourceId;
		private final List<Skanword> objectsList;
		private volatile Map<Integer, View> tasksRowViews = new HashMap<Integer, View>();
		private boolean buildForVip;
		private boolean mOneList;

		public SkanwordsTasksAdapter(Context context, int resource,
				List<Skanword> objects, boolean oneList) {
			super(context, resource, objects);
			this.objectsList = objects;
			resourceId = resource;
			this.mOneList = oneList;
			buildForVip = MainDataManager.getInstance().getUserData() != null && MainDataManager.getInstance().getUserData().isVip();
		}

		@Override
		public void notifyDataSetInvalidated() {
			buildForVip = MainDataManager.getInstance().getUserData().isVip();
			super.notifyDataSetInvalidated();
		}
		
		public boolean isBuildForVip() {
			return buildForVip;
		}
		@Override
		protected int getNumberOfViewTypeForRows() {
			return TYPE_ROW_DELETE_ISSUE_VIEW + 1;
		}
		@Override
		protected int getViewTypeForRowInSection(int section, int row) {
			if (row == getNumberOfRowsInSection(section) - 1 && !mOneList)
				return TYPE_ROW_DELETE_ISSUE_VIEW;
			return TYPE_ROW_VIEW;
		}

		private Skanword getItemForPosition(int position) {
			int rowsInSections = 0;
			for (int i = 0; i < getNumberOfSections(); i++) {
				rowsInSections += (getNumberOfRowsInSection(i) + 1);
				if (position < rowsInSections) {
					Skanword item = super.getItem(position - i - 1);
					return item;
				}
			}
			return null;
		}

		@Override
		protected int getNumberOfSections() {
			return 1;
		}

		@Override
		protected int getNumberOfRowsInSection(int section) {
			if (this.objectsList == null)
				return 0;
			return this.objectsList.size() + (!mOneList ? 1 : 0);
		}

		@Override
		protected View newSectionView(Context context, int section,
				ViewGroup parent) {
			return ((LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE))
					.inflate(R.layout.header, parent, false);
		}

		@Override
		protected void bindSectionView(Context context, int section,
				View convertView) {
			if (convertView == null)
				return;
			((TextView) convertView.findViewById(R.id.setLabel))
					.setText(sCurrentSortType == 0 ? "Сканворды по номеру:"
							: "Нерешенные сверху:");
		}

		@Override
		protected View newRowViewInSection(Context context, int section,
				int row, ViewGroup parent) {
			LayoutInflater inflater = LayoutInflater.from(getContext());
			final View view;
			if (getViewTypeForRowInSection(section, row) == TYPE_ROW_DELETE_ISSUE_VIEW) {
				view = inflater.inflate(R.layout.set_delete_row_item, parent,
						false);
			} else {
				 view = inflater.inflate(resourceId, parent, false);
			}
			return view;
		}

		@Override
		public boolean areAllItemsEnabled() {
			return false;
		}

		@Override
		public boolean isEnabled(int position) {
			return getItemViewType(position) == TYPE_ROW || getItemViewType(position) == TYPE_ROW_DELETE_ISSUE_VIEW ;
		}

		@Override
		protected void bindRowViewInSection(Context context, int section,
				int row, final View convertView) {
			if (getViewTypeForRowInSection(section, row) == TYPE_ROW) {
				final Skanword item = super.getItem((row + 1) * (section + 1) - 1);
				buildRowScanwordTask(convertView, item);
			}
		}

		@Override
		public Object getItemInSectionAndRow(int section, int row) {
			return null;
		}

		public static void buildRowViewForScanword(final View convertView,
				final Skanword skanword) {
			if (convertView == null) {
				return;
			}
			TextView nameView = (TextView) convertView
					.findViewById(R.id.scanwordIdLabel);

			ImageView iconPlace = (ImageView) convertView
					.findViewById(R.id.scanwordTypeIcon);
			String wordsCount = "";
			String difficulty = "";
			TextView statusLabel = (TextView) convertView
					.findViewById(R.id.statusLabel);

			statusLabel.setVisibility(View.VISIBLE);
			statusLabel.setTextColor(0xffff8800);
			
			String imgUrl = skanword.getCover();
			
			iconPlace.setImageBitmap(BitmapFactory.decodeFile(skanword.getImageDir() + imgUrl));
			if (skanword.isStarted()
					&& !skanword.isFinished()) {
				//icon.setImageResource(R.drawable.icon_started);
				statusLabel.setVisibility(View.GONE);
				convertView.findViewById(R.id.progressBar1).setVisibility(
						View.VISIBLE);
				int progress = (int)(skanword
						.getFinishedQuestionsCount() / (float) skanword.getQuestionsCount() * 100);
				((ProgressBar) convertView.findViewById(R.id.progressBar1))
						.setProgress(progress);
			} else {
				convertView.findViewById(R.id.progressBar1).setVisibility(
						View.GONE);
				if (skanword.isFinished()) {
					//icon.setImageResource(R.drawable.icon_finished);
					statusLabel.setText("Сканворд решен");
					statusLabel.setTextColor(0xff669900);
				} else {
					//icon.setImageResource(R.drawable.icon_scanword);
					statusLabel.setText("Новый сканворд");
				}
			}
			if (skanword.getFinishedQuestionsCount() > 0
					&& !skanword.isFinished()) {
				wordsCount = "решено "
						+ skanword.getFinishedQuestionsCount()
						+ " из "
						+ skanword.getQuestionsCount();
			} else {
				wordsCount = skanword.getQuestionsCount()
						+ " "
						+ Utils.spellVariantForNumber(skanword
								.getQuestionsCount(), "слово", "слова",
								"слов");
			}
			switch (skanword.getLevel()) {
			case 1:
				difficulty = "легкий";
				break;
			case 2:
				difficulty = "средний";
				break;
			case 3:
				difficulty = "сложный";
				break;

			default:
				break;
			}
			nameView.setText(skanword.toString() + " \u2022 " + difficulty + " \u2022 " + wordsCount);
			nameView.setBackgroundColor(Color.TRANSPARENT);
			buildKeyword((LinearLayout)convertView.findViewById(R.id.keywordLayout), skanword.getKeyword());
		}
		private static void buildKeyword(LinearLayout keywordLayout, Keyword keyword) {
			if (keyword == null) {
				keywordLayout.setVisibility(View.GONE);
				return;
			}

			keywordLayout.setVisibility(View.VISIBLE);
			LayoutInflater inflater = LayoutInflater.from(SmappsScanwords.getContext());		
			keywordLayout.removeAllViewsInLayout();

			for (KeywordCell keywordCell : keyword.getCells()) {

				String letter = keywordCell.getCharacter();
				
				final View letterView = inflater.inflate(R.layout.question_letter, null);
				keywordLayout.addView(letterView);
				LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) letterView.getLayoutParams();
				params.rightMargin = 10;
				params.bottomMargin = 2;
				letterView.setLayoutParams(params);
				letterView.findViewById(R.id.letter_view).setBackgroundResource(keywordCell.isPersist() ? R.drawable.guessed_keyword_letter_cell : R.drawable.unguessed_keyword_letter_cell);
				if (!letter.equalsIgnoreCase(" ")) {
					((TextView)letterView.findViewById(R.id.letter_text)).setText(letter);
				}
			}
		}

		public void buildRowScanwordTask(final View convertView,
				final Skanword scanword) {
			buildRowViewForScanword(convertView, scanword);
			if (convertView != null) {
				tasksRowViews.put(scanword.getId(), convertView);
			}
		}
	}
	@Override
	protected void onStart() {
		checkTutorialState();
		super.onStart();
	}
	@Override
	protected void onDestroy() {
		super.onDestroy();
		SmappsScanwords.getEventsDispatcher().removeListener(
				MainDataManager.EVENT_PROGRESS_UPDATE, mProgressUpdateListener);
		SmappsScanwords.getEventsDispatcher().removeListener(
				MainDataManager.EVENT_SKANWORDS_TASKS_UPDATE,
				mSkanwordsUpdateEventListener);
		if (mTimerEventListener != null)
			SmappsScanwords.getEventsDispatcher().removeListener(
				SmappsScanwords.APPLICATION_TIMER_EVENT, mTimerEventListener);

	}

}
