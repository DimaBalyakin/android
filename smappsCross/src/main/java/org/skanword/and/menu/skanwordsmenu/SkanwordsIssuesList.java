package org.skanword.and.menu.skanwordsmenu;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.skanword.and.R;
import org.skanword.and.SmappsScanwords;
import org.skanword.and.SmappsScanwords.ActionType;
import org.skanword.and.datamanager.MainDataManager;
import org.skanword.and.datamanager.MainDataManager.IDowloadSetProgress;
import org.skanword.and.datamanager.Skanword;
import org.skanword.and.datamanager.SkanwordsDataManager;
import org.skanword.and.datamanager.SkanwordsDataManager.SkanwordsSetsUpdateEvent;
import org.skanword.and.datamanager.SkanwordsDataManager.TaskListDirrectionType;
import org.skanword.and.datamanager.SkanwordsDataManager.TaskListType;
import org.skanword.and.datamanager.SkanwordsDataManager.VisibilityListType;
import org.skanword.and.datamanager.SkanwordsSetInfo;
import org.skanword.and.datamanager.UserUpdatesManager;
import org.skanword.and.etc.TutorialManager;
import org.skanword.and.etc.TutorialManager.TutorialStep;
import org.skanword.and.etc.Utils;
import org.skanword.and.menu.ArraySectionedListAdapter;
import org.skanword.and.menu.IssuesList;
import org.skanword.and.menu.LockedTaskActivity;
import org.skanword.and.menu.MainMenuActivity;
import org.skanword.and.network.MainNetworkManager;
import org.skanword.and.network.MainNetworkManager.RequestCompletionBlock;
import org.skanword.and.scanwordgame.SkanwordGameActivity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.musselwhizzle.dispatcher.events.Event;
import com.musselwhizzle.dispatcher.events.EventListener;
import com.musselwhizzle.dispatcher.events.SimpleEvent;

public class SkanwordsIssuesList extends IssuesList {

	private static int mCurrentSortType = 0;
	private boolean mCurrentSortTypeChanged;
	private ListView mIssuesList = null;
	private SetsAdapter.ItemAction mActions = null;
	private SetsAdapter mAdapter = null;
	private TaskListType listType;

	private int FragmentPosition;
	List<SkanwordsSetInfo> mSetsInfos;

	private EventListener mTimerEventListener;
	private EventListener mSetsInfoUpdateEventListener;
	private EventListener mDownloadTodayIssueEventListener;
	private EventListener mSetDownloadEvent;
	private boolean mVisible;

	public static final String EVENT_SET_DOWNLOAD_PROGRESS = "EVENT_SET_DOWNLOAD_PROGRESS";

	public static final int TYPE_ROW_OTHER_PAGE_VIEW = ArraySectionedListAdapter.TYPE_ROW_VIEW + 1;
	public static final int TYPE_ROW_ONE_LIST_VIEW = TYPE_ROW_OTHER_PAGE_VIEW + 1;
	public static final int TYPE_ROW_TIMER_VIEW = TYPE_ROW_ONE_LIST_VIEW + 1;
	public static final int TYPE_ROW_LAST_SOLVED_VIEW = TYPE_ROW_TIMER_VIEW + 1;

	public TaskListType getListType() {
		return listType;
	}

	public void setListType(TaskListType listType) {
		this.listType = listType;
	}
	public static int currentSortType() {
		return mCurrentSortType;
	}
	
	public SkanwordsIssuesList() {
		
		mCurrentSortTypeChanged = false;
		mActions = new SetsAdapter.ItemAction() {
			@Override
			public void requestPage(boolean next) {
				showDownloadingState(true);
				MainNetworkManager.getInstance().requestIssuesListExecWithCompleteBlock(listType, next ? TaskListDirrectionType.NEXT_DIRRECTION : TaskListDirrectionType.PREVIOUS_DIRRECTION, new RequestCompletionBlock() {
					@Override
					public void complete(boolean success) {
					}
				}, getActivity());
			}
			public void clickItem() {
				Intent intent = new Intent(getActivity(),
						SkanwordsTasksList.class);
				intent.putExtra("LIST_TYPE", mAdapter.getListType());
				intent.putExtra(SkanwordsTasksList.SKANWORDS_COUNT_OVER_LIMIT, true);
				intent.putExtra(
						"LIST_TITLE",
						(mAdapter.getListType() == TaskListType.STARTED_TASKS ? "Начатые"
								: "Решенные"));
				getActivity().startActivityForResult(intent,
						LockedTaskActivity.LOCKED_TASK_ACTIVITY_REQUEST_CODE);
				if (getListType() == TaskListType.STARTED_TASKS) {
					SmappsScanwords.sendGAEvent(ActionType.USER_ACTION,
							"menu_scans_begun_onelist_enter");
				} else if (getListType() == TaskListType.FINISHED_TASKS) {
					SmappsScanwords.sendGAEvent(ActionType.USER_ACTION,
							"menu_scans_solved_onelist_enter");
				}
			}

			public void clickItem(final SkanwordsSetInfo scanwordsSetInfo) {
				if (scanwordsSetInfo == null || !scanwordsSetInfo.hasDownloadedFiles())
					return;
				
				Log.v("", " scanword Issue ScanwordsIssuesList "
						+ LockedTaskActivity.LOCKED_TASK_ACTIVITY_REQUEST_CODE);
				Intent intent = new Intent(getActivity(),
						SkanwordsTasksList.class);
				intent.putExtra("LIST_TYPE", TaskListType.ISSUE_TASKS);
				intent.putExtra("ISSUE_ID", scanwordsSetInfo.getId());
				intent.putExtra("LIST_TITLE",
						"Выпуск от " + scanwordsSetInfo.getName());
				getActivity().startActivityForResult(intent,
						LockedTaskActivity.LOCKED_TASK_ACTIVITY_REQUEST_CODE);
				if (getListType() == TaskListType.ISSUE_TASKS) {
					SmappsScanwords.sendGAEvent(ActionType.USER_ACTION,
							"menu_scans_new_issue_enter");
				} else if (getListType() == TaskListType.STARTED_TASKS) {
					SmappsScanwords.sendGAEvent(ActionType.USER_ACTION,
							"menu_scans_begun_issue_enter");
				} else if (getListType() == TaskListType.FINISHED_TASKS) {
					SmappsScanwords.sendGAEvent(ActionType.USER_ACTION,
							"menu_scans_solved_issue_enter");
				}
			}

			public void clickDownload(final SkanwordsSetInfo scanwordsSetInfo,
					final View rowView) {

				if (TutorialManager.getInstance().currentTutorialStep() == TutorialStep.TUTORIAL_START) {
					TutorialManager.getInstance()
							.setTutorialStep(TutorialStep.TUTORIAL_ISSUE_DOWNLOADING);
				}
				SmappsScanwords.getEventsDispatcher().dispatchEvent(
						new SetDownloadEvent(EVENT_SET_DOWNLOAD_PROGRESS)
								.setSetInfo(scanwordsSetInfo).setProgress(0));
				MainDataManager.getInstance().downloadSetWithId(getActivity(),
						scanwordsSetInfo, new IDowloadSetProgress() {
							@Override
							public void onProgress(int progress) {
								SmappsScanwords
										.getEventsDispatcher()
										.dispatchEvent(
												new SetDownloadEvent(
														EVENT_SET_DOWNLOAD_PROGRESS)
														.setSetInfo(
																scanwordsSetInfo)
														.setProgress(progress));

							}
						});
			}
		};
		mSetsInfoUpdateEventListener = new EventListener() {

			public void onEvent(Event event) {
				SkanwordsSetsUpdateEvent updateEvent = (SkanwordsSetsUpdateEvent)event;
				if (updateEvent.getTaskListType() == listType || (updateEvent.getTaskListType() == TaskListType.ALL_TASKS && mVisible)) {
					update(updateEvent.scrollToTop());
				}
			}
		};
		mDownloadTodayIssueEventListener = new EventListener() {
			public void onEvent(Event event) {
				downloadTodayIssue();
			}
		};
		mSetDownloadEvent = new EventListener() {
			@Override
			public void onEvent(Event event) {
				SetDownloadEvent downloadEvent = (SetDownloadEvent) event;
				setDownloadEvent(downloadEvent.getSetInfo(),
						downloadEvent.getProgress());
			}
		};
		SmappsScanwords.getEventsDispatcher().addListener(
				EVENT_SET_DOWNLOAD_PROGRESS, mSetDownloadEvent);
		SmappsScanwords.getEventsDispatcher().addListener(
				MainDataManager.EVENT_SKANWORDS_SETS_INFOS_UPDATE,
				mSetsInfoUpdateEventListener);
		SmappsScanwords.getEventsDispatcher().addListener(
				MainDataManager.EVENT_DOWNLOAD_TODAY_ISSUE,
				mDownloadTodayIssueEventListener);
	}

	private void setDownloadEvent(final SkanwordsSetInfo scanwordsSetInfo,
			final int progress) {
		if (mAdapter == null)
			return;
		final View rowView = mAdapter.getSetRowView(scanwordsSetInfo);
		final TextView nameView = rowView != null ? (TextView) rowView
				.findViewById(R.id.setLabel) : null;
		if (progress == 0) {
			MainDataManager.getInstance().addDownloadingProgress(
					scanwordsSetInfo);
			if (nameView != null
					&& nameView.getText().equals(
							scanwordsSetInfo.getDisplayName())) {
				mAdapter.buildRowScanwordSetInfo(rowView, scanwordsSetInfo);
			}
			Log.v("",
					"StarDownloading set " + scanwordsSetInfo.getDisplayName());
			return;
		}
		MainDataManager.getInstance()
				.updateProgress(scanwordsSetInfo, progress);
		if (progress == -1) {
			MainDataManager.getInstance().removeDownloadingProgress(
					scanwordsSetInfo);
			if (TutorialManager.getInstance().currentTutorialStep() == TutorialStep.TUTORIAL_ISSUE_DOWNLOADING) {
				TutorialManager.getInstance().setTutorialStep(TutorialStep.TUTORIAL_START);
			}
		} else if (progress == 100) {
			if (TutorialManager.getInstance().currentTutorialStep() == TutorialStep.TUTORIAL_ISSUE_DOWNLOADING) {
				TutorialManager.getInstance()
						.setTutorialStep(TutorialStep.TUTORIAL_ISSUE_DOWNLOADED);
			}
			MainDataManager.getInstance().removeDownloadingProgress(
					scanwordsSetInfo);
			MainMenuActivity.instance.runOnUiThread(new Runnable() {
				public void run() {
					Log.v("", " set downloaded 2");
					if (mAdapter.hasOneList != mAdapter.oneList()) {
						update(false);
						return;
					}
					if (mCurrentSortType == 2 && mSetsInfos != null) {
						mSetsInfos.remove(scanwordsSetInfo);
						mAdapter.remove(scanwordsSetInfo);
						Log.v("", " set downloaded 3");
					} else if (mCurrentSortType == 1 && mSetsInfos != null) {
						mSetsInfos.add(scanwordsSetInfo);
						mAdapter.notifyDataSetChanged();

					}
					if (mSetsInfos.size() == 0) {
						update(false);
					} else {
						Log.v("", " set downloaded  notifyDataSetChanged");
						mAdapter.notifyDataSetChanged();
					}
				}
			});

		}
		Activity activity = getActivity();
		if (activity != null)
			activity.runOnUiThread(new Runnable() {
				public void run() {
					if (nameView != null
							&& nameView.getText().equals(
									scanwordsSetInfo.getDisplayName())
							&& (progress == 100 || progress == -1)) {
						mAdapter.buildRowScanwordSetInfo(rowView,
								scanwordsSetInfo);
					} else
						mAdapter.updateProgressView(scanwordsSetInfo);
				}
			});
	}

	public int getFragmentPosition() {
		return FragmentPosition;
	}

	public void setFragmentPosition(int fragmentPosition) {
		FragmentPosition = fragmentPosition;
	}

	public void clearTab(boolean showSpinner) {
		if (getView() != null) {
			if (!showSpinner)
				getView().findViewById(R.id.loadingSpinner).setVisibility(View.GONE);
			getView().findViewById(R.id.emptyIssuesListLabel).setVisibility(View.GONE);
			getView().findViewById(R.id.issues_list).setVisibility(View.GONE);
		}
	}
	public void clearTab() {
		clearTab(false);
	}
	public void showDownloadingState(final boolean show) {
		getActivity().runOnUiThread(new Runnable() {	
			@Override
			public void run() {
				if (getView() != null) {
					getView().findViewById(R.id.loadingSpinner).setVisibility(show ? View.VISIBLE : View.GONE);
				}
			}
		});
	}
	public void showLoadingError() {
		Log.v("Skanwordsfunc", "show loading error");
		showDownloadingState(false);
		TextView emptyList = (TextView) getView().findViewById(
				R.id.emptyIssuesListLabel);
		if (emptyList != null)
			emptyList.setText("Произошла ошибка\nпри получении списка выпусков.\nПожалуйста, повторите попытку.");
		setAdapter(false);
	}
	
	private void setTimerEventListener(boolean add) {
		if (!add) {
			if (mTimerEventListener != null)
				SmappsScanwords.getEventsDispatcher().removeListener(SmappsScanwords.APPLICATION_TIMER_EVENT, mTimerEventListener);
			mTimerEventListener = null;
		} else if (listType == TaskListType.ISSUE_TASKS) {
			
			setTimerEventListener(false);
			mTimerEventListener = new EventListener() {
				public void onEvent(Event event) {
					if (getActivity() != null) {
						final long timeLeft = MainDataManager.getInstance().getSkanwordsDataManager().timeTillTomorrowIssue();
						getActivity().runOnUiThread(new Runnable() {
							public void run() {
								if (mAdapter == null)
									return;
								if (mAdapter.getNumberOfSections() > 1) {
									View v = mIssuesList.getChildAt(1 - mIssuesList
											.getFirstVisiblePosition());
									if (v == null)
										return;
									SkanwordsSetInfo tommorowSetInfo = MainDataManager
											.getInstance()
											.getSkanwordsDataManager()
											.getTomorrowSet();
									if (tommorowSetInfo == null)
										return;

									TextView timerView = (TextView) v
											.findViewById(R.id.timer);
									if (timerView != null)
										timerView.setText(timeLeft > 0 ? Utils
												.secondsToTimerType(timeLeft) : "");
									TextView nameView = (TextView) v
											.findViewById(R.id.setLabel);
									if (nameView != null)
										nameView.setText(timeLeft > 0 ? "Новый выпуск через:"
												: "Выпуск готов! Обновите список.");
								}
								if (mAdapter.isBuildForVip() != MainDataManager
										.getInstance().getUserData().isVip()) {
									mAdapter.notifyDataSetInvalidated();
								}
							}
						});
					}
				}
			};
			SmappsScanwords.getEventsDispatcher().addListener(
					SmappsScanwords.APPLICATION_TIMER_EVENT, mTimerEventListener);
		}
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		setListType((TaskListType) getArguments().getSerializable("list_type"));
		
		setTimerEventListener(true);

		final View view = inflater.inflate(R.layout.scanword_sets_list,
				container, false);
		mIssuesList = (ListView) view.findViewById(R.id.issues_list);
		mIssuesList.setScrollBarStyle(ListView.SCROLLBARS_OUTSIDE_OVERLAY);
		

		TextView emptyList = (TextView) view
				.findViewById(R.id.emptyIssuesListLabel);
		setEmptyListCaption(emptyList);
		Log.v("", "onCreateView  skanwordissueslist" + listType);
		return view;
	}
	@Override
	public void setMenuVisibility(boolean menuVisible) {
		mVisible = menuVisible;
		super.setMenuVisibility(menuVisible);
	}
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		clearTab(mVisible);
		super.onActivityCreated(savedInstanceState);
	}

	public void downloadTodayIssue() {
		Log.v("", "downloadTodayIssue gor event");
		if (!MainDataManager.getInstance().getOptions().isAutoDownload()
				|| mAdapter == null) {
			return;
		}
		Log.v("", "downloadTodayIssue call adapter");
		mAdapter.downloadTodayIssue();
	}

	public void openSortDialog() {
		Log.v("", "open sort dialog  ScanwordIssuesList  ");
		AlertDialog.Builder builder = new AlertDialog.Builder(
				this.getActivity(), R.style.Theme_Scanword_style_dialog);
		// Set the dialog title
		List<String> listItems = new ArrayList<String>();

		listItems.add("Показать все");
		listItems.add("Только загруженные");
		builder.setSingleChoiceItems(
				listItems.toArray(new CharSequence[listItems.size()]),
				mCurrentSortType, new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog, int which) {
						Log.v("SkanwordsFunc", "onClick change sort type dialog " + which);
						mCurrentSortType = which;
						mCurrentSortTypeChanged = true;
					}
				}).setPositiveButton("Ок",
				new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog, int which) {
						Log.v("SkanwordsFunc", "onClick accept sort type dialog " + mCurrentSortTypeChanged);
						if (mCurrentSortTypeChanged)
							SmappsScanwords.getEventsDispatcher().dispatchEvent(new SimpleEvent(SkanwordsDataManager.UPDATE_ISSUES_LIST));
						mCurrentSortTypeChanged = false;
						TextView emptyList = (TextView) getView().findViewById(
								R.id.emptyIssuesListLabel);
						SkanwordsIssuesList.this.setEmptyListCaption(emptyList);

					}
				});

		AlertDialog alertDialog = builder.create();
		alertDialog.setCancelable(false);
		// alertDialog.setIconAttribute(R.drawable.scanword_style_btn_radio_holo_light);
		alertDialog.show();

	}

	protected void setEmptyListCaption(TextView emptyList) {
		if (mCurrentSortType == 0) {
			if (getListType() == TaskListType.STARTED_TASKS) {
				emptyList
						.setText("У вас нет ни одного сканворда,\nкоторый начат но не решен.\n\nНачните разгадывать сканворд,\nи его выпуск появится тут.");
			} else if (getListType() == TaskListType.FINISHED_TASKS) {
				emptyList
						.setText("Вы пока что не закончили\nни одного сканворда.\n\nРазгадайте все слова в\nсканворде, и его выпуск\nпоявится тут.");
			} else {
				emptyList
						.setText("Выпуски будут загружены,\nкогда появится соединение с Интернет.");
			}
		} else if (mCurrentSortType == 1) {
			if (getListType() == TaskListType.ISSUE_TASKS) {
				emptyList
						.setText("Список пуст.\nУ вас нет загруженных\nновых выпусков.");
			} else if (getListType() == TaskListType.STARTED_TASKS) {
				emptyList
						.setText("Список пуст.\nУ вас нет загруженных\nначатых выпусков.");
			} else {
				emptyList
						.setText("Список пуст.\nУ вас нет загруженных\nрешенных выпусков.");
			}
		}
	}
	public int getSetsInfosCount() {
		return mSetsInfos == null ? 0 : mSetsInfos.size();
	}
	private void setAdapter(boolean scrollToTop) {
		Log.v("", "setAdapter " + listType);
		if (mIssuesList == null || getView() == null)
			return;
		showDownloadingState(false);
		if (mCurrentSortType == 1) {
			mSetsInfos = MainDataManager
					.getInstance()
					.getSkanwordsDataManager()
					.getSortedSetsInfosList(getListType(),
							VisibilityListType.DOWNLOADED_ISSUES);
		} else if (mCurrentSortType == 2) {
			mSetsInfos = MainDataManager
					.getInstance()
					.getSkanwordsDataManager()
					.getSortedSetsInfosList(getListType(),
							VisibilityListType.NOT_DOWNLOADED_ISSUES);
		} else {
			mSetsInfos = MainDataManager.getInstance()
					.getSkanwordsDataManager()
					.getSortedSetsInfosList(getListType());
		}
		final TextView emptyList = (TextView) getView().findViewById(
				R.id.emptyIssuesListLabel);
		if (mSetsInfos == null || mSetsInfos.size() == 0) {
			emptyList.setVisibility(View.VISIBLE);
			mIssuesList.setVisibility(View.INVISIBLE);
			return;
		}
		mIssuesList.setVisibility(View.VISIBLE);
		emptyList.setVisibility(View.INVISIBLE);
		mAdapter = new SetsAdapter(getActivity(), R.layout.set_row_item,
				mSetsInfos, mActions, getListType(), mCurrentSortType);
		
		Parcelable state = mIssuesList.onSaveInstanceState();
		mIssuesList.setAdapter(mAdapter);
		if (!scrollToTop)
			mIssuesList.onRestoreInstanceState(state);

		mIssuesList
				.setOnItemClickListener(new AdapterView.OnItemClickListener() {

					public void onItemClick(AdapterView<?> parent, View view,
							int position, long id) {
						if (mAdapter.getItemViewType(position) == TYPE_ROW_OTHER_PAGE_VIEW) {
							mActions.requestPage(position == mAdapter.getCount() - 1);
						}else if (mAdapter.getItemViewType(position) == TYPE_ROW_ONE_LIST_VIEW) {
							mActions.clickItem();
						} else if (mAdapter.getItemViewType(position) == ArraySectionedListAdapter.TYPE_ROW_VIEW) {
							SkanwordsSetInfo scanwordSetInfo = mAdapter.getItemForPosition(position);
							mActions.clickItem(scanwordSetInfo);
						} else if (mAdapter.getItemViewType(position) == TYPE_ROW_LAST_SOLVED_VIEW) {
							Skanword scanword = MainDataManager.getInstance().getSkanwordsDataManager().getLastPlayedSkanword();
							if (MainDataManager.getInstance().getSkanwordsDataManager().isSkanwordFree(scanword.getId())
									|| MainDataManager.getInstance().getUserData().isVip()) {
								Intent intent = new Intent(Intent.ACTION_EDIT,
										null, getActivity(),
										SkanwordGameActivity.class);
								intent.putExtra(
										SkanwordGameActivity.EXTRA_SKAN_ID,
										scanword.getId());
								//intent.putExtra(SkanwordGameActivity.EXTRA_SKANWORD_OBJECT,SmappsScanwords.getDatabaseHelper().getFullskanwordData(scanword.getId()));
								getActivity().startActivityForResult(intent,
										101);
								SmappsScanwords.sendGAEvent(
										ActionType.USER_ACTION,
										"playscan_scanword_enter"); // GA Event
								if (MainDataManager.getInstance()
										.getSkanwordsDataManager()
										.isSkanwordOffered(scanword.getId()))
									SmappsScanwords
											.sendGAEvent(
													ActionType.USER_ACTION,
													"click_menu_scans_offeredscan_opened"); // GA
																							// Event
								SmappsScanwords.sendGAEvent(
										ActionType.USER_ACTION,
										"click_menu_scans_begun_thelast"); // GA
																			// Event
								UserUpdatesManager.getInstance().saveTime();
								MainNetworkManager.getInstance().requestSyncExec();

							} else {
								MainMenuActivity.openLockedTaskactivity(
										getActivity(), scanword);
							}
						}
					}

				});
		Log.v("", "setAdapter 2");
		downloadTodayIssue();

	}

	public void update(final boolean scrollToTop) {
		if (getActivity() != null) {
			getActivity().runOnUiThread(new Runnable() {
				public void run() {
					setAdapter(scrollToTop);
				}
			});
		}
	}

	public void updateSetInfoRow() {

	}

	@SuppressLint("UseSparseArrays")
	protected static class SetsAdapter extends
			ArraySectionedListAdapter<SkanwordsSetInfo> {
		private final int resourceId;
		private TaskListType listType;
		private boolean buildForVip = false;
		public boolean hasOneList = false;
		private final ItemAction itemAction;
		private List<SkanwordsSetInfo> objectsList;
		private int mCurrentSortType;
		private volatile Map<Integer, View> setsRowViews = new HashMap<Integer, View>();
		private Context mContext;

		public SetsAdapter(Context context, int resource,
				List<SkanwordsSetInfo> objects,
				SetsAdapter.ItemAction itemAction, TaskListType listType, int sortType) {
			super(context, resource, objects);
			mContext = context;
			this.objectsList = objects;
			resourceId = resource;
			this.mCurrentSortType = sortType;
			this.itemAction = itemAction;
			this.listType = listType;
			this.hasOneList = oneList();
			buildForVip = MainDataManager.getInstance().getUserData() != null
					&& MainDataManager.getInstance().getUserData().isVip();
		}

		public TaskListType getListType() {
			return listType;
		}

		@Override
		public void notifyDataSetInvalidated() {
			buildForVip = MainDataManager.getInstance().getUserData().isVip();
			super.notifyDataSetInvalidated();
		}

		public boolean isBuildForVip() {
			return buildForVip;
		}

		public List<SkanwordsSetInfo> getObjectsList() {
			return objectsList;
		}

		public boolean oneList() {
			if (listType == TaskListType.ISSUE_TASKS)
				return false;
			int count = 0;
			for (SkanwordsSetInfo setInfo : objectsList) {
				if (setInfo.hasDownloadedFiles()) {
					count++;
					if (count > 1) {
						return true;
					}
				}
			}
			return false;
		}
		private boolean isPageAvailable(boolean next) {
			return MainDataManager.getInstance().getSkanwordsDataManager().isPageAvailable(next) && mCurrentSortType == 0;
		}

		public void setObjectsList(List<SkanwordsSetInfo> objectsList) {
			this.objectsList = objectsList;
		}

		public void updateVisibleRows(ListView list) {
			int start = list.getFirstVisiblePosition();
			int end = list.getLastVisiblePosition();
			for (int i = start; i < end; i++) {
				View view = list.getChildAt(i - start);
				list.getAdapter().getView(i, view, list);
			}
			Log.v("", " updateVisibleRowupdateVisibleRows ");
		}

		@Override
		public void remove(SkanwordsSetInfo object) {
			objectsList.remove(object);
			this.notifyDataSetChanged();

			// super.remove(object);
		}

		@Override
		public boolean areAllItemsEnabled() {
			return false;
		}

		@Override
		public boolean isEnabled(int position) {
			if (getItemViewType(position) == TYPE_ROW) {
				return true;
			} else if (getItemViewType(position) == TYPE_ROW_LAST_SOLVED_VIEW
					|| getItemViewType(position) == TYPE_ROW_ONE_LIST_VIEW || getItemViewType(position) == TYPE_ROW_OTHER_PAGE_VIEW) {
				return true;
			}
			return false;
		}

		private SkanwordsSetInfo getItemForPosition(int position) {
			int rowsInSections = 0;
			for (int i = 0; i < getNumberOfSections(); i++) {
				if (position < rowsInSections
						+ (getNumberOfRowsInSection(i) + 1)) {
					return getScanwordSetInfoForRowAndSection(position
							- rowsInSections - 1, i);
				}
				rowsInSections += (getNumberOfRowsInSection(i) + 1);
			}
			return null;
		}

		public SkanwordsSetInfo getScanwordSetInfoForRowAndSection(int row,
				int section) {
			row -= (hasOneList ? 1 : 0);
			row -= (isPageAvailable(false) ? 1 : 0);
			row += getViewTypeForRowInSection(0, 0) == TYPE_ROW_TIMER_VIEW ? 1 : 0;
			return super.getItem(row);
		}

		@Override
		protected int getViewTypeForRowInSection(int section, int row) {
			if (getNumberOfSections() > 1 && section == 0 && listType == TaskListType.STARTED_TASKS) {
				return TYPE_ROW_LAST_SOLVED_VIEW;
			}
			if (getNumberOfSections() > 1 && section == 0 && listType == TaskListType.ISSUE_TASKS) {
				return TYPE_ROW_TIMER_VIEW;
			}
			if (isPageAvailable(false) && row == 0) {
				return TYPE_ROW_OTHER_PAGE_VIEW;
			}
			if ((row == 0 && !isPageAvailable(false) && hasOneList) || (isPageAvailable(false) && row == 1 && hasOneList)) {
				return TYPE_ROW_ONE_LIST_VIEW;
			}
			if (isPageAvailable(true) && row == getNumberOfRowsInSection(section) - 1) {
				return TYPE_ROW_OTHER_PAGE_VIEW;
			}
			return TYPE_ROW_VIEW;
		}

		@Override
		protected int getNumberOfViewTypeForRows() {
			return TYPE_ROW_LAST_SOLVED_VIEW + 1;
		}

		@Override
		protected int getNumberOfSections() {
			return (MainDataManager.getInstance().getSkanwordsDataManager()
					.getTomorrowSet() != null && listType == TaskListType.ISSUE_TASKS)
					|| (MainDataManager.getInstance().getSkanwordsDataManager()
							.getLastPlayedSkanword() != null && listType == TaskListType.STARTED_TASKS) ? 2
					: 1;
		}

		@Override
		protected int getNumberOfRowsInSection(int section) {
			if (getNumberOfSections() > 1 && section == 0) {
				return 1;
			}
			int count = this.objectsList.size();
			if (getNumberOfSections() > 1 && listType == TaskListType.ISSUE_TASKS)
				count --;
			count += (hasOneList ? 1 : 0);
			count += (isPageAvailable(true) ? 1 : 0);
			count += (isPageAvailable(false) ? 1 : 0);
			return count;
		}

		@Override
		protected View newSectionView(Context context, int section,
				ViewGroup parent) {
			if (getNumberOfSections() > 1
					&& section == 0
					&& getViewTypeForRowInSection(section, 0) == TYPE_ROW_TIMER_VIEW) {
				View view = new View(getContext());
				return view;
			}
			return ((LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE))
					.inflate(R.layout.header, parent, false);
		}

		@Override
		protected void bindSectionView(Context context, int section,
				View convertView) {
			convertView.setClickable(true);
			if (getNumberOfSections() > 1 && section == 0) {
				if (getViewTypeForRowInSection(section, 0) == TYPE_ROW_TIMER_VIEW) {
					return;
				} else if (convertView.findViewById(R.id.setLabel) != null) {
					((TextView) convertView.findViewById(R.id.setLabel))
							.setText("последний начатый:");
				}
			} else if (convertView.findViewById(R.id.setLabel) != null)
				((TextView) convertView.findViewById(R.id.setLabel))
						.setText("выпуски сканвордов от:");

			// convertView.setEnabled(false);
		}

		@Override
		protected View newRowViewInSection(Context context, int section,
				int row, ViewGroup parent) {

			LayoutInflater inflater = LayoutInflater.from(getContext());
			final View view;
			int type = getViewTypeForRowInSection(section, row);
			
			if (type == TYPE_ROW_TIMER_VIEW) {
				view = inflater.inflate(R.layout.set_timer_row, parent,
						false);
			} else if (type == TYPE_ROW_LAST_SOLVED_VIEW) {
				view = inflater.inflate(R.layout.scanword_row_item, parent,
						false);
			} else if (type == TYPE_ROW_ONE_LIST_VIEW) {
				view = inflater.inflate(R.layout.one_list_row_item, parent,
						false);
			} else if (type == TYPE_ROW_OTHER_PAGE_VIEW) {
				view = inflater.inflate(R.layout.other_page_row_layout, parent,
						false);
			} else  {
				view = inflater.inflate(resourceId, parent, false);
			}

			return view;
		}
		@Override
		protected void bindRowViewInSection(Context context, int section,
				int row, final View convertView) {
			int type = getViewTypeForRowInSection(section, row);
			if (type == TYPE_ROW_VIEW) {
				final SkanwordsSetInfo item = getScanwordSetInfoForRowAndSection(
						row, section);
				buildRowScanwordSetInfo(convertView, item);
			}
			if (type == TYPE_ROW_TIMER_VIEW) {
				final SkanwordsSetInfo item = getItem(0); // some workarond
				buildRowScanwordSetInfo(convertView, item);
			}
			if (type == TYPE_ROW_OTHER_PAGE_VIEW) {
				customizeOtherPageRow(convertView, row != 0);
			}
			if (type == TYPE_ROW_ONE_LIST_VIEW) {
				
			}
			if (type == TYPE_ROW_LAST_SOLVED_VIEW) {
				SkanwordsTasksList.SkanwordsTasksAdapter
				.buildRowViewForScanword(convertView,
						MainDataManager.getInstance()
								.getSkanwordsDataManager()
								.getLastPlayedSkanword());
			}
		}

		private void customizeOtherPageRow(final View convertView, boolean next) {
			TextView textView = (TextView)convertView.findViewById(R.id.otherPageLabelText);
			if (next) {
				textView.setText("Показать следующие выпуски");
			} else {
				textView.setText("Показать новые выпуски");
			}
		}
		public void buildRowScanwordSetInfo(View rowView,
				final SkanwordsSetInfo info) {
			if (rowView == null) {
				rowView = setsRowViews.get(info.getId());
				if (rowView == null)
					return;
			}
			final View convertView = rowView;
			if (convertView.findViewById(R.id.timer) != null) {

				TextView timerView = (TextView) convertView
						.findViewById(R.id.timer);
				long timeLeft = MainDataManager.getInstance().getSkanwordsDataManager().timeTillTomorrowIssue();
				timerView.setText(timeLeft > 0 ? Utils
						.secondsToTimerType(timeLeft) : "");
				TextView nameView = (TextView) convertView
						.findViewById(R.id.setLabel);
				nameView.setText(timeLeft > 0 ? "Новый выпуск через:"
						: "Выпуск готов! Обновите список.");
				return;
			}
//			ImageView icon = (ImageView) convertView
//					.findViewById(R.id.setTypeIcon);

			//UrlImageViewHelper.setUrlDrawable(icon, info.getCover(), R.drawable.default_img);
			/*if ((info.getFreeIds() != null && info.getFreeIds().size() > 0)
					|| buildForVip) {
				icon.setImageResource(R.drawable.icon_scanwords_issue);
			} else
				icon.setImageResource(R.drawable.icon_close_issue);*/
			TextView nameView = (TextView) convertView
					.findViewById(R.id.setLabel);
			nameView.setText(info.toString());
			nameView.setBackgroundColor(Color.TRANSPARENT);
			TextView setProgress = (TextView) convertView
					.findViewById(R.id.setProgress);
			setsRowViews.put(info.getId(), convertView);
			SetStatus status = info.hasDownloadedFiles() ? SetStatus.DOWNLOADED
					: SetStatus.DEFAULT;
			status = MainDataManager.getInstance().setIsDownloading(info) ? SetStatus.DOWNLOADING
					: status;
			final Button downloadButton = (Button) convertView
					.findViewById(R.id.downloadButton);

			//checkTutorialState(convertView, info);
			convertView.findViewById(R.id.arrowIcon).setVisibility(status == SetStatus.DOWNLOADED ? View.VISIBLE : View.GONE);
			if (status != SetStatus.DEFAULT) {
				downloadButton.setVisibility(View.INVISIBLE);

				if (status == SetStatus.DOWNLOADED) {
					setProgress.setVisibility(View.VISIBLE);
					setProgress.setText(MainDataManager.getInstance()
							.getSkanwordsDataManager()
							.getProgressOfSetInfo(info, listType));
				}
			} else {
				downloadButton.setVisibility(View.VISIBLE);
				setProgress.setVisibility(View.GONE);
				downloadButton.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						if (MainNetworkManager.getInstance()
								.hasNetworkConnection((Activity)mContext))
							itemAction.clickDownload(info, convertView);
					}

				});
			}
			convertView.setClickable(!info.hasDownloadedFiles());
			updateProgressView(info);

		}

		// downloading

		public void downloadTodayIssue() {
			for (SkanwordsSetInfo scanwordsSetInfo : objectsList) {
				if (scanwordsSetInfo.isToday()
						&& !scanwordsSetInfo.hasDownloadedFiles()) {
					Log.v("", scanwordsSetInfo.getDisplayName()
							+ " downloadTodayIssue setinfo  "
							+ scanwordsSetInfo.isToday());
					View view = setsRowViews.get(scanwordsSetInfo.getId());
					itemAction.clickDownload(scanwordsSetInfo, view);
					return;
				}
			}
			Log.v("", "downloadTodayIssue  not found setinfo");
		}

		public View getSetRowView(SkanwordsSetInfo info) {
			return setsRowViews.get(info.getId());
		}

		public void updateProgressView(SkanwordsSetInfo setInfo) {
			final View rowView = setsRowViews.get(setInfo.getId());
			if (rowView == null)
				return;

			TextView nameView = (TextView) rowView.findViewById(R.id.setLabel);
			boolean setDownloading = MainDataManager.getInstance()
					.setIsDownloading(setInfo);
			final ProgressBar progressBar = (ProgressBar) rowView
					.findViewById(R.id.progressBar);

			if (!(nameView.getText().equals(setInfo.getDisplayName()))) {
				if (setDownloading) {
				}
				return;
			}
			final Button downloadButton = (Button) rowView
					.findViewById(R.id.downloadButton);
			TextView setProgress = (TextView) rowView
					.findViewById(R.id.setProgress);
			if (setDownloading) {
				progressBar.setVisibility(View.VISIBLE);
				downloadButton.setVisibility(View.GONE);
				setProgress.setVisibility(View.GONE);
			} else {
				progressBar.setVisibility(View.GONE);
				if (!setInfo.hasDownloadedFiles())
					downloadButton.setVisibility(View.VISIBLE);
				return;
			}
			int progress = MainDataManager.getInstance()
					.getSetDownloadingProgress(setInfo.getId());
			progressBar.setMax(100);
			progressBar.setProgress(progress);

		}


		@Override
		public Object getItemInSectionAndRow(int section, int row) {
			return null;
		}

		public static enum SetStatus {
			DEFAULT, DOWNLOAD_DATA, DOWNLOAD_IMAGE, DOWNLOADING, DOWNLOADED
		}

		private static interface ItemAction {
			void clickItem(SkanwordsSetInfo scanwordSetInfo);

			void clickItem();

			void clickDownload(SkanwordsSetInfo scanwordSetInfo, View view);
			
			void requestPage(boolean next);
		}

	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		Log.v("", "onCreate -------ScanwordsIssuesList------ " + listType);
		super.onCreate(savedInstanceState);
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		Log.v("", "onSaveInstanceState -------ScanwordsIssuesList------ "
				+ listType);
		super.onSaveInstanceState(outState);
	}

	@Override
	public void onDestroy() {
		Log.v("", "onDestroy -------ScanwordsIssuesList------ " + listType);
		setTimerEventListener(false);
		SmappsScanwords.getEventsDispatcher().removeListener(
				MainDataManager.EVENT_SKANWORDS_SETS_INFOS_UPDATE,
				mSetsInfoUpdateEventListener);
		SmappsScanwords.getEventsDispatcher().removeListener(
				MainDataManager.EVENT_DOWNLOAD_TODAY_ISSUE,
				mDownloadTodayIssueEventListener);
		SmappsScanwords.getEventsDispatcher().removeListener(
				MainDataManager.EVENT_DOWNLOAD_TODAY_ISSUE, mSetDownloadEvent);
		super.onDestroy();
	}
	
	@Override
	public void onPause() {
		Log.v("", "onPause -------ScanwordsIssuesList------ " + listType);
		super.onPause();
	}

	@Override
	public void onResume() {
		Log.v("", "onResume -------ScanwordsIssuesList------ " + listType);
		super.onResume();
	}

	@Override
	public void onStart() {
		Log.v("", "onStart -------ScanwordsIssuesList------ " + listType);
		super.onStart();
	}

	@Override
	public void onStop() {
		Log.v("", "onStop -------ScanwordsIssuesList------ " + listType);
		super.onStop();
	}

	public class SetDownloadEvent extends SimpleEvent {
		private SkanwordsSetInfo mSetInfo = null;
		private int progress = -1;

		public SetDownloadEvent(String type) {
			super(type);

		}

		public SkanwordsSetInfo getSetInfo() {
			return mSetInfo;
		}

		public SetDownloadEvent setSetInfo(SkanwordsSetInfo mSetInfo) {
			this.mSetInfo = mSetInfo;
			return this;
		}

		public int getProgress() {
			return progress;
		}

		public SetDownloadEvent setProgress(int progress) {
			this.progress = progress;
			return this;
		}

	}
}