package org.skanword.and.menu.shopmenu;

import java.util.List;

import org.skanword.and.R;
import org.skanword.and.SmappsScanwords;
import org.skanword.and.SmappsScanwords.ActionType;
import org.skanword.and.datamanager.MainDataManager;
import org.skanword.and.datamanager.ShopDataManager.Shop.ShopItemsSet.ShopItem;
import org.skanword.and.datamanager.ShopDataManager.ShopProductType;
import org.skanword.and.etc.CustomFragment;
import org.skanword.and.etc.Utils;
import org.skanword.and.inappbilling.InAppManager;
import org.skanword.and.inappbilling.InAppManager.PurchaseSource;
import org.skanword.and.menu.ArraySectionedListAdapter;
import org.skanword.and.network.MainNetworkManager;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.musselwhizzle.dispatcher.events.Event;
import com.musselwhizzle.dispatcher.events.EventListener;

public class MenuShop extends CustomFragment {

	private View mMainView;
	public static final int TYPE_ROW_TIMER_VIEW = ArraySectionedListAdapter.TYPE_ROW_VIEW + 1;
	public static final int TYPE_ROW_SALE_VIEW = TYPE_ROW_TIMER_VIEW + 1;
	public static final int TYPE_ROW_CANCEL_SUBSCRIPTION_VIEW = TYPE_ROW_SALE_VIEW + 1;
	public static final int TYPE_ROW_DESCRIPTION_VIEW = TYPE_ROW_CANCEL_SUBSCRIPTION_VIEW + 1;

	private ListView mList;
	private ShopItemsAdapter mAdapter;
	private ShopProductType mListType;
	private EventListener mTimerEventListener;
	private EventListener mPriceUpdateEventListener;
	
	public MenuShop() {
		super();
	}

	@Override
	public void fragmentOpened() {
		updateFragment();
		super.fragmentOpened();
	}
	@Override
	public void updateFragment(boolean byUser) {
		if (MainNetworkManager.getInstance().hasNetworkConnection(byUser? getActivity() : null))
			MainNetworkManager.getInstance().requestInfoWithCompleteBlock(MainNetworkManager.INFO_SHOP, getActivity(), null);
		super.updateFragment(byUser);
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		Log.v(""," onCreateView  MenuShop");
		mMainView = inflater.inflate(R.layout.shop_menu_layout, container,
				false);
		mList = (ListView) mMainView.findViewById(R.id.shop_list);
		mListType = ShopProductType.HINTS;
		mTimerEventListener = new EventListener() {
			public void onEvent(Event event) {
				if (getActivity() != null && mAdapter != null)
					getActivity().runOnUiThread(new Runnable() {
						public void run() {
							mAdapter.updateTimerItems();
						}
					});
			}
		};
		mList.setScrollBarStyle(ListView.SCROLLBARS_OUTSIDE_OVERLAY);
		mPriceUpdateEventListener = new EventListener() {

			public void onEvent(Event event) {
				update();
			}
		};

		if (mTimerEventListener != null)
			SmappsScanwords.getEventsDispatcher().addListener(
					SmappsScanwords.APPLICATION_TIMER_EVENT, mTimerEventListener);
		
		SmappsScanwords.getEventsDispatcher().addListener(
				InAppManager.EVENT_PRICE_UPDATE, mPriceUpdateEventListener);
		setAdapter();
		return mMainView;
	}
	public void update() {
		if (getActivity() != null) {
			getActivity().runOnUiThread(new Runnable() {
				public void run() {
					setAdapter();
				}
			});
		}
	}

	private void setAdapter() {
		List<ShopItem> items = MainDataManager.getInstance()
				.getShopDataManager().getShopItemsOfType(mListType);
		if (items == null)
			return;
		mAdapter = new ShopItemsAdapter(getActivity(),
				R.layout.shop_item_row, items, mListType);
		mList.setAdapter(mAdapter);
	}
	

	public static class ShopItemsAdapter extends
			ArraySectionedListAdapter<ShopItem> {
		private final List<ShopItem> objectsList;
		private final ShopProductType mType;
		private View premiumTimerItemView = null;
		private View saleItemView = null;
		protected Activity mContext; 

		public ShopItemsAdapter(Context context, int resource,
				List<ShopItem> objects, ShopProductType type) {
			super(context, resource, objects);
			mContext = (Activity) context;
			this.objectsList = objects;
			this.mType = type;
		}
		public void updateTimerItems() {
			if (saleItemView != null && saleItemView.findViewById(R.id.itemNameLabel) != null) {
				Resources res = getContext().getResources();
				((TextView)saleItemView.findViewById(R.id.itemNameLabel)).setText(res.getString(R.string.shop_item_sale_label) + " " + Utils
						.secondsToTimerType(MainDataManager.getInstance().getShopDataManager().getSaleTimeLeft()));
			}
			if (mType != ShopProductType.SUBSCRIPTION)
				return;
			boolean isVip = MainDataManager.getInstance().getUserData().isVip();
			boolean hasTimerView = (premiumTimerItemView != null);
			//Log.v("", hasTimerView + " Notify data set changed " + isVip);
			if (hasTimerView != isVip) {
				notifyDataSetChanged();
			} else {
				if (isVip && hasTimerView)
					((TextView)premiumTimerItemView.findViewById(R.id.itemNameLabel)).setText("Премиум активен:\n" + Utils.secondsToMaxTimeType(MainDataManager.getInstance().getUserData().lastVip()));
				
			}
			//Log.v("", " sale time left " + MainDataManager.getInstance().getShopDataManager().getSaleTimeLeft());
		}
		
		@Override
		public void notifyDataSetChanged() {
			premiumTimerItemView = null;
			saleItemView = null;
			super.notifyDataSetChanged();
		}
		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent) {
			return super.getView(position, convertView, parent);
		}

		@Override
		protected int getNumberOfSections() {
			if (mType == ShopProductType.HINTS && MainDataManager.getInstance().getShopDataManager().shopItemsTypeWithSale(mType)) {
				return 2;
			} else if (mType == ShopProductType.SUBSCRIPTION) {
				return 2;
			}
			return 1;
		}

		@Override
		protected int getNumberOfRowsInSection(int section) {
			if (mType == ShopProductType.SUBSCRIPTION && section == 0) {
				int count = 1;
				if (MainDataManager.getInstance().getShopDataManager().shopItemsTypeWithSale(mType))
					count ++;
				if (MainDataManager.getInstance().getUserData().isVip())
					count ++;
				return count;
			} else if (mType == ShopProductType.HINTS && MainDataManager.getInstance().getShopDataManager().shopItemsTypeWithSale(mType) && section == 0) {
				return 1;
			} else if (mType == ShopProductType.SUBSCRIPTION && section == 1 && MainDataManager.getInstance().getUserData().isVip()) {
				return this.objectsList.size() + 1;
			}
			return this.objectsList.size();
		}

		@Override
		protected View newSectionView(Context context, int section,
				ViewGroup parent) {
			if (mType == ShopProductType.HINTS && MainDataManager.getInstance().getShopDataManager().shopItemsTypeWithSale(mType) && section == 0) {
				View v = new View(getContext());
				return v;
			}
			return ((LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE))
					.inflate(R.layout.header, parent, false);
		}

		@Override
		protected void bindSectionView(Context context, int section,
				View convertView) {
			if (mType == ShopProductType.HINTS && MainDataManager.getInstance().getShopDataManager().shopItemsTypeWithSale(mType) && section == 0) {
				return;
			}
			if (convertView == null)
				return;
			if (mType == ShopProductType.HINTS) {
				((TextView) convertView.findViewById(R.id.setLabel))
						.setText("варианты пакетов:");
			} else {
				if (section == 0) {
					((TextView) convertView.findViewById(R.id.setLabel))
							.setText("премиум-доступ – это");
				} else {
					((TextView) convertView.findViewById(R.id.setLabel))
							.setText("варианты доступа:");
				}
			}
		}

		@Override
		protected View newRowViewInSection(Context context, int section,
				int row, ViewGroup parent) {
			LayoutInflater inflater = LayoutInflater.from(getContext());
			int resId = R.layout.shop_item_row;


			if (mType == ShopProductType.HINTS) {
				if (MainDataManager.getInstance().getShopDataManager().shopItemsTypeWithSale(mType) && section == 0) {
					resId = R.layout.shop_item_sale_timer;
				}
			} else {
				if (getViewTypeForRowInSection(section, row) == TYPE_ROW_DESCRIPTION_VIEW) {
					resId = R.layout.premium_description_item_layout;
				} else if (getViewTypeForRowInSection(section, row) == TYPE_ROW_SALE_VIEW) {
					resId = R.layout.shop_item_sale_timer;
				} else if (getViewTypeForRowInSection(section, row) == TYPE_ROW_TIMER_VIEW) {
					resId = R.layout.premium_timer_item_layout;
				} else if (getViewTypeForRowInSection(section, row) == TYPE_ROW_CANCEL_SUBSCRIPTION_VIEW) {
					resId = R.layout.shop_cancel_subscription_item_row;
				}
			}
			final View convertView = inflater.inflate(resId, parent, false);

			return convertView;
		}

		@Override
		public boolean areAllItemsEnabled() {
			return false;
		}

		@Override
		protected int getViewTypeForRowInSection(int section, int row) {
			if (mType == ShopProductType.SUBSCRIPTION) {
				if (section == 0) {
					if (row == 0) {
						return TYPE_ROW_DESCRIPTION_VIEW;
					} else if (row == 1 && MainDataManager.getInstance().getUserData().isVip()) {
						return TYPE_ROW_TIMER_VIEW;
					} else {
						return TYPE_ROW_SALE_VIEW;
					}
				} else {
					if (row >= this.objectsList.size()) {
						return TYPE_ROW_CANCEL_SUBSCRIPTION_VIEW;
					}
					return TYPE_ROW_VIEW;
				}
			} else {
				if (MainDataManager.getInstance().getShopDataManager().shopItemsTypeWithSale(mType) && section == 0) {
					return TYPE_ROW_SALE_VIEW;
				}
				return TYPE_ROW_VIEW;
			}
		}

		@Override
		protected int getNumberOfViewTypeForRows() {
			return TYPE_ROW_DESCRIPTION_VIEW + 1;
		}
		@Override
		public boolean isEnabled(int position) {
			if (getRowForPosition(position) < 0)
				return false;
			return true;
		}

		@Override
		protected void bindRowViewInSection(Context context, int section,
				int row, final View convertView) {
			final ShopItem item = getItemInSectionAndRow(section, row);

			if (item != null) {
				buildRowView(convertView, item);
			} else {
				buildAdditionalRowView(convertView, getViewTypeForRowInSection(section, row));
			}
		}

		@Override
		public ShopItem getItemInSectionAndRow(int section, int row) {
			if (mType == ShopProductType.SUBSCRIPTION && section == 0)
				return null;

			if (mType == ShopProductType.HINTS && MainDataManager.getInstance().getShopDataManager().shopItemsTypeWithSale(mType) && section == 0) {
				return null;
			}
			if (row >= this.objectsList.size())
				return null;
			return getItem(row);
		}
		private void buildAdditionalRowView(final View convertView,
				final int rowType) {
			if (rowType == TYPE_ROW_SALE_VIEW) 
				saleItemView = convertView;
			if (rowType == TYPE_ROW_TIMER_VIEW)  {
				Log.v("", "buildAdditionalRowView  " + rowType);
				premiumTimerItemView = convertView;
			}
			if (rowType == TYPE_ROW_CANCEL_SUBSCRIPTION_VIEW) {
				Button buyButton = (Button) convertView
						.findViewById(R.id.cancelSubscription);
				buyButton.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						AlertDialog.Builder builder = new AlertDialog.Builder(
								mContext,
								R.style.Theme_Scanword_style_dialog);
						builder.setMessage(
								"Для отмены подписки необходимо перейти в Google Wallet.")
								.setPositiveButton("Перейти",
										new DialogInterface.OnClickListener() {
											public void onClick(DialogInterface dialog,
													int id) {
												Intent browse = new Intent( Intent.ACTION_VIEW , Uri.parse( "https://wallet.google.com/manage/#subscriptions:"  ) );
			        	                        mContext.startActivity( browse );
											}
										}).setNegativeButton("Отмена", null);
						AlertDialog dialog = builder.create();
						dialog.show();
					}
				});
			}
			updateTimerItems();
			convertView.setClickable(true);
		}

		private void buildRowView(final View convertView, final ShopItem item) {
			TextView label = (TextView) convertView
					.findViewById(R.id.itemNameLabel);
			label.setText(item.getName());
			convertView.setClickable(true);
			((LinearLayout) convertView.findViewById(R.id.sale_label)).setVisibility(item.isSale() && MainDataManager.getInstance().getShopDataManager().saleEnabled() ? View.VISIBLE : View.GONE);
			((ImageView) convertView.findViewById(R.id.itemTypeIcon))
					.setBackgroundResource(getIconId(item, mType));
			Button buyButton = (Button) convertView
					.findViewById(R.id.buyButton);
			buyButton.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					if (MainNetworkManager.getInstance().hasNetworkConnection(mContext))
						InAppManager.getInstance().purchaseProduct(item.getId(), mContext, PurchaseSource.SHOP);
					SmappsScanwords.sendGAEvent(ActionType.USER_ACTION, "click_menu_shop_buy_" + item.getId()); // GA Event
				}
			});
			String price = InAppManager.getInstance().getPriceForProductId(item.getId());
			price = price.equals("") ? "купить" : price;
			if (price != null)
				buyButton.setText(price);
		}

		public static int getIconId(ShopItem item, ShopProductType type) {
			String name = item.getIconName();
			if (name == null) {
				if (type == ShopProductType.HINTS) {
					return R.drawable.tip12;
				} else {
					return R.drawable.month1;
				}
			}
			if (name.equals("hint5")) {
				return R.drawable.tip500;
			} else if (name.equals("hint4")) {
				return R.drawable.tip110;
			} else if (name.equals("hint3")) {
				return R.drawable.tip30;
			} else if (name.equals("hint2")) {
				return R.drawable.tip12;
			} else if (name.equals("hint1")) {
				return R.drawable.tip5;
			} else if (name.equals("vip2")) {
				return R.drawable.year;
			} else if (name.equals("vip1")) {
				return R.drawable.month1;
			}
			if (type == ShopProductType.HINTS) {
				return R.drawable.tip12;
			} else {
				return R.drawable.month1;
			}
		}

	}

	@Override
	public void onDestroy() {
		Log.v("", "onDestroy -------MenuShop------");
		if (mTimerEventListener != null)
			SmappsScanwords.getEventsDispatcher().removeListener(
					SmappsScanwords.APPLICATION_TIMER_EVENT, mTimerEventListener);
		SmappsScanwords.getEventsDispatcher().removeListener(
				InAppManager.EVENT_PRICE_UPDATE, mPriceUpdateEventListener);
		super.onDestroy();
	}

	@Override
	public void onPause() {
		Log.v("", "onPause -------MenuShop------");
		super.onPause();
	}

	@Override
	public void onResume() {
		Log.v("", "onResume -------MenuShop------");
		super.onResume();
	}

	@Override
	public void onStart() {
		Log.v("", "onStart -------MenuShop------");
		super.onStart();
	}

	@Override
	public void onStop() {
		Log.v("", "onStop -------MenuShop------");
		super.onStop();
	}
}
