package org.skanword.and.menu;

import org.skanword.and.R;
import org.skanword.and.SmappsScanwords;
import org.skanword.and.SmappsScanwords.ActionType;
import org.skanword.and.datamanager.MainDataManager;
import org.skanword.and.etc.AdsManager;
import org.skanword.and.etc.SoundsManager;
import org.skanword.and.etc.Utils;
import org.skanword.and.network.DailyBonusObject;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.qustom.dialog.QustomDialogBuilder;

public class HintsBonusDialog extends QustomDialogBuilder {

    private static HintsBonusDialogDelegate mDelegate;
	private boolean mScratched = false;
	private DailyBonusObject mDailyBonus = null;
	private static AlertDialog mDialog = null;
	private boolean mHadRewardedVideo = false;


	public HintsBonusDialog(Context context, int theme) {
		super(context, theme);
	}

	public HintsBonusDialog(Context context) {
        super(context);
	}
	
	@Override
	protected int getLayoutresId() {
    	layoutResId = R.layout.custom_dialog_layout;
		return super.getLayoutresId();
	} 
	
	private View mDialogView = null;
	
	public void setDialog(AlertDialog dialog) {
		mDialog = dialog;
	}
	
	public static AlertDialog getDialog() {
		return mDialog;
	}
	
	public View getDialogView() {
		return mDialogView;
	}

	public void setDialogView(View mDialog) {
		this.mDialogView = mDialog;
	}
	
	public void setDailyBonus(DailyBonusObject bonus) {
		mDailyBonus = bonus;
	}
	
	public static void showDialogInContext(Context context, HintsBonusDialogDelegate delegate, DailyBonusObject bonusObject) {
		if (bonusObject == null)
			return;

		SmappsScanwords.sendGAEvent(ActionType.APP_ACTION, "hourly_bonus_show"); // GA Event
		
		mDelegate = delegate;
		LayoutInflater li = LayoutInflater.from(context);
		final View view = li.inflate(R.layout.hints_bonus_dialog_view, null);
        
        final HintsBonusDialog qustomDialogBuilder = new HintsBonusDialog(context, R.style.Theme_Scanword_style_dialog);
        qustomDialogBuilder.setDividerColor("#f38d02").setTitle("Бесплатные подсказки").setTitleColor("#f38d02");
        
        qustomDialogBuilder.setCustomView(view);
        final AlertDialog dialog = qustomDialogBuilder.show();
        qustomDialogBuilder.setDailyBonus(bonusObject);
        qustomDialogBuilder.setDialogView(view);
        dialog.setCancelable(false);
        qustomDialogBuilder.setDialog(dialog);
        
        if (bonusObject.getScratchedPosition() > 0) {
        	qustomDialogBuilder.placeVariants(bonusObject.getScratchedPosition());
        	qustomDialogBuilder.scratchViews(bonusObject.getScratchedPosition(), true);
        } else {
    		ImageView scratch = (ImageView) view.findViewById(R.id.scratch_1);
    		scratch.setOnClickListener(qustomDialogBuilder.scratchClickListener);
    		scratch.setSoundEffectsEnabled(false);
    		scratch = (ImageView) view.findViewById(R.id.scratch_2);
    		scratch.setOnClickListener(qustomDialogBuilder.scratchClickListener);
    		scratch.setSoundEffectsEnabled(false);
    		scratch = (ImageView) view.findViewById(R.id.scratch_3);
    		scratch.setOnClickListener(qustomDialogBuilder.scratchClickListener);
    		scratch.setSoundEffectsEnabled(false);
        }
        
		view.findViewById(R.id.chooseRandomButton).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				int rand = Utils.randInt(1, 3);
				int resID = SmappsScanwords.getContext().getResources().getIdentifier("scratch_"+rand, "id", SmappsScanwords.getContext().getPackageName());
	    		ImageView scratch = (ImageView) view.findViewById(resID);
	    		qustomDialogBuilder.scratchClickListener.onClick(scratch);
				SmappsScanwords.sendGAEvent(ActionType.USER_ACTION, "hourly_bonus_any"); // GA Event
			}
		});
		view.findViewById(R.id.getButton).setOnClickListener(qustomDialogBuilder.takeClickListener);
		view.findViewById(R.id.doubleButton).setOnClickListener(qustomDialogBuilder.doubleClickListener);
        
	}
	
	public static boolean isShowing() {
		return getDialog() != null && getDialog().isShowing();
	}

	public static void showCompleteBonusDialogInContext(Context context, DailyBonusObject bonusObject, boolean doubled) {
		LayoutInflater li = LayoutInflater.from(context);
		View view = li.inflate(R.layout.bonus_complete_dialog_view, null);
        
        final HintsBonusDialog qustomDialogBuilder = new HintsBonusDialog(context, R.style.Theme_Scanword_style_dialog);
        qustomDialogBuilder.setCustomView(view);
        final AlertDialog dialog = qustomDialogBuilder.show();
        qustomDialogBuilder.setDialogView(view);
        qustomDialogBuilder.setDailyBonus(bonusObject);
        qustomDialogBuilder.setDialog(dialog);
        
        TextView headLabel = (TextView) view.findViewById(R.id.headLabel); 
        int hintsCount = bonusObject.getWin() * (doubled ? 2 : 1);
        boolean hasRewardedVideo = AdsManager.getInstance().hasRewardedVideo(true);
        if (!hasRewardedVideo) {
        	headLabel.setText("Поздравляем!");
        	view.findViewById(R.id.simpleHintsCountsArea).setVisibility(View.VISIBLE);
        	((TextView)view.findViewById(R.id.hintsReceivedLabel)).setText("Вам начислено " + hintsCount 
    				+ " " + Utils.spellVariantForNumber(hintsCount, "подсказку", "подсказки", "подсказок"));
        } else {
        	view.findViewById(R.id.videoHintsCountsArea).setVisibility(View.VISIBLE);
        	headLabel.setText("Начислено " + hintsCount 
            				+ " " + Utils.spellVariantForNumber(hintsCount, "подсказку", "подсказки", "подсказок") + ":");
        	((TextView)view.findViewById(R.id.hintsCountLabel)).setText(""+bonusObject.getWin());
        	((TextView)view.findViewById(R.id.doubledLabel)).setText(doubled ? "да" : " нет");
        	((TextView)view.findViewById(R.id.doubledLabel)).setTextColor(doubled ? Color.parseColor("#99cc00") : Color.parseColor("#ff4444"));
        }
        
        CheckBox checkbox = (CheckBox) view.findViewById(R.id.checkBox1);
        checkbox.setChecked(true);
        

		view.findViewById(R.id.closeButton).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				mDialog = null;
				dialog.cancel();
			}
		});
		dialog.setOnCancelListener(qustomDialogBuilder.mCompleteDialogCancelListener);
        
	}
	
	public View.OnClickListener scratchClickListener = new View.OnClickListener() {
		private boolean mScratched = false;

		
		@Override
		public void onClick(View v) {
			if (!mScratched) {
				mScratched = true;
				scratch(v);
			}
		}
	};
	public View.OnClickListener takeClickListener = new View.OnClickListener() {

		@Override
		public void onClick(View v) {
			AlertDialog a = mDialog;
			mDialog = null;
			if (mDelegate != null)
				mDelegate.onTake(mHadRewardedVideo);
			if (a != null)
				a.dismiss();
			SmappsScanwords.sendGAEvent(ActionType.USER_ACTION, "hourly_bonus_take"); // GA Event
		}
	};
	public View.OnClickListener doubleClickListener = new View.OnClickListener() {

		@Override
		public void onClick(View v) {
			AlertDialog a = mDialog;
			mDialog = null;
			if (mDelegate != null)
				mDelegate.onDouble();
			if (a != null)
				a.dismiss();
			SmappsScanwords.sendGAEvent(ActionType.USER_ACTION, "hourly_bonus_double"); // GA Event
		}
	};
	
	public OnCancelListener mCompleteDialogCancelListener = new OnCancelListener() {
		@Override
		public void onCancel(DialogInterface dialog) {
			CheckBox checkbox = (CheckBox) getDialogView()
					.findViewById(R.id.checkBox1);
			if (checkbox.isChecked()) {
				SmappsScanwords.sendGAEvent(ActionType.APP_ACTION, "hourly_bonus_end_notify"); // GA Event
			} else {
				SmappsScanwords.sendGAEvent(ActionType.APP_ACTION, "hourly_bonus_end_not_notify"); // GA Event
			}
			SmappsScanwords.getAppSharedPreferencesEditor(MainDataManager.GENERAL_PREFERENCES_NAME).putBoolean("daily_bonus_notify", checkbox.isChecked());
		}
	};

	Integer otherViewNum1;
	Integer otherViewNum2;
	public void placeVariants(int scratchedPosition) {
		TextView textView;
    	TextView hintsLabel;
    	if (scratchedPosition == 1) {
    		otherViewNum1 = 2;
    		otherViewNum2 = 3;
    		
    	} else if (scratchedPosition == 2) {
    		otherViewNum1 = 1;
    		otherViewNum2 = 3;
    		
    	} else if (scratchedPosition == 3) {
    		otherViewNum1 = 1;
    		otherViewNum2 = 2;
    		
    	} else {
    		return;
    	}
    	int hintsCount = mDailyBonus.getWin();
		int resID = SmappsScanwords.getContext().getResources().getIdentifier("textHints"+scratchedPosition, "id", SmappsScanwords.getContext().getPackageName());
    	textView = (TextView) mDialogView.findViewById(resID);
    	textView.setText("" + mDailyBonus.getWin());
    	textView.setTextColor(Color.parseColor("#f38d02"));
		resID = SmappsScanwords.getContext().getResources().getIdentifier("textHintsLabel"+scratchedPosition, "id", SmappsScanwords.getContext().getPackageName());
    	hintsLabel = (TextView) mDialogView.findViewById(resID);
    	hintsLabel.setText(Utils.spellVariantForNumber(mDailyBonus.getWin(), "подсказка", "подсказки", "подсказок"));
    	hintsLabel.setTextColor(Color.parseColor("#f38d02"));
    	

    	hintsCount = mDailyBonus.getOtherVariant(1);
		resID = SmappsScanwords.getContext().getResources().getIdentifier("textHints"+otherViewNum1, "id", SmappsScanwords.getContext().getPackageName());
    	textView = (TextView) mDialogView.findViewById(resID);
    	textView.setText("" + hintsCount);
		resID = SmappsScanwords.getContext().getResources().getIdentifier("textHintsLabel"+otherViewNum1, "id", SmappsScanwords.getContext().getPackageName());
    	hintsLabel = (TextView) mDialogView.findViewById(resID);
    	hintsLabel.setText(Utils.spellVariantForNumber(hintsCount, "подсказка", "подсказки", "подсказок"));

    	hintsCount = mDailyBonus.getOtherVariant(2);
    	resID = SmappsScanwords.getContext().getResources().getIdentifier("textHints"+otherViewNum2, "id", SmappsScanwords.getContext().getPackageName());
    	textView = (TextView) mDialogView.findViewById(resID);
    	textView.setText("" + hintsCount);
		resID = SmappsScanwords.getContext().getResources().getIdentifier("textHintsLabel"+otherViewNum2, "id", SmappsScanwords.getContext().getPackageName());
    	hintsLabel = (TextView) mDialogView.findViewById(resID);
    	hintsLabel.setText(Utils.spellVariantForNumber(hintsCount, "подсказка", "подсказки", "подсказок"));
    	
	}

	private void scratchViews(int scratchedPosition, final boolean b) {
		
		new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
			
			@Override
			public void run() {
				int resID = SmappsScanwords.getContext().getResources().getIdentifier("scratch_"+otherViewNum1, "id", SmappsScanwords.getContext().getPackageName());
				Log.v("SkanwordsFunc", "resId - " + resID);
				Drawable drawable = (AnimationDrawable) mDialogView.findViewById(resID).getBackground();
		        if (drawable instanceof AnimationDrawable) {
		        	AnimationDrawable animDrawable = (AnimationDrawable) drawable;
		            animDrawable.stop();
		            if (b) {
		                animDrawable.selectDrawable(7);
		            } else {
		                animDrawable.selectDrawable(0);
		                animDrawable.start();
		            }
		        }
				resID = SmappsScanwords.getContext().getResources().getIdentifier("scratch_"+otherViewNum2, "id", SmappsScanwords.getContext().getPackageName());
				Log.v("SkanwordsFunc", "resId - " + resID);
		        drawable = (AnimationDrawable) mDialogView.findViewById(resID).getBackground();
		        if (drawable instanceof AnimationDrawable) {
		        	AnimationDrawable animDrawable = (AnimationDrawable) drawable;
		            animDrawable.stop();
		            if (b) {
		                animDrawable.selectDrawable(7);
		            } else {
		                animDrawable.selectDrawable(0);
		                animDrawable.start();
		            }
		        }
		        if (!b && getDialog() != null) {
		        	SoundsManager.playSound(getDialog().getOwnerActivity(), SoundsManager.SOUND_BONUS_SCRATCH_REST);
		        }
			}
		}, b ? 0 : 1000);
    	
		mScratched = true;
    	mDialogView.findViewById(R.id.chooseRandomPanel).setVisibility(View.GONE);
    	mDialogView.findViewById(R.id.bonusPanel).setVisibility(View.VISIBLE);
    	mHadRewardedVideo = AdsManager.getInstance().hasRewardedVideo(true);
    	if (mHadRewardedVideo) {
    		((TextView)mDialogView.findViewById(R.id.lowLabel)).setText("Просмотр видеоролика удвоит подсказки");
    		((Button)mDialogView.findViewById(R.id.getButton)).setText("Не удваивать");
			SmappsScanwords.sendGAEvent(ActionType.USER_ACTION, "hourly_bonus_video_available"); // GA Event
    	} else {
    		mDialogView.findViewById(R.id.lowLabel).setVisibility(View.INVISIBLE);
    		mDialogView.findViewById(R.id.doubleButton).setVisibility(View.GONE);
			SmappsScanwords.sendGAEvent(ActionType.USER_ACTION, "hourly_bonus_video_unavailable"); // GA Event
    	}
		int resID = SmappsScanwords.getContext().getResources().getIdentifier("scratch_"+scratchedPosition, "id", SmappsScanwords.getContext().getPackageName());
    	Drawable drawable = (AnimationDrawable) mDialogView.findViewById(resID).getBackground();
        if (drawable instanceof AnimationDrawable) {
            AnimationDrawable animDrawable = (AnimationDrawable) drawable;
            animDrawable.stop();
            if (b) {
                animDrawable.selectDrawable(7);
            } else {
                animDrawable.selectDrawable(0);
                animDrawable.start();
            }
        }
        if (!b && getDialog() != null) {
			try {

				SoundsManager.playSound(getDialog().getOwnerActivity(), SoundsManager.SOUND_BONUS_SCRATCH);
			} catch (Exception e) {
				e.printStackTrace();
			}
        }
	}
    public void scratch(View v) {
    	if (mScratched)
    		return;
    	int position = 0;
    	if (v.getId() == R.id.scratch_1) {
    		position = 1;
    		
    	} else if (v.getId() == R.id.scratch_2) {
    		position = 2;
    		
    	} else if (v.getId() == R.id.scratch_3) {
    		position = 3;
    		
    	} else {
    		return;
    	}
    	placeVariants(position);
    	scratchViews(position, false);
        mDailyBonus.setScratchedPosition(position);
        MainDataManager.getInstance().rewriteDailyBonusObjectRecieved(mDailyBonus);
    }
    
    public static interface HintsBonusDialogDelegate {
    	void onCancel();
    	void onDouble();
    	void onTake(boolean hadRewardedVideo);
    }
}
