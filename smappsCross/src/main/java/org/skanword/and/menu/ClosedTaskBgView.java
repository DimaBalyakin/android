package org.skanword.and.menu;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.util.AttributeSet;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;

public class ClosedTaskBgView extends View {

	private Paint mPaint = new Paint();
	private Path mPath = new Path();
	
	public ClosedTaskBgView(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
		WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
		Display display  = wm.getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
        final float scale = getContext().getResources().getDisplayMetrics().density;
		float offset = (int) (15 * scale + 0.5f);
        mPaint.setStyle(Paint.Style.FILL);
        // Use Color.parseColor to define HTML colors
        mPaint.setColor(Color.parseColor("#FCF2D6"));
        mPath.reset();
        mPath.moveTo(0, 0);
        mPath.lineTo(size.x - offset * 2, 0);
        mPath.lineTo(size.x - offset * 2, size.y * 0.45f);
        mPath.lineTo(size.x / 2 - offset, size.y * 0.5f);
        mPath.lineTo(0, size.y * 0.45f);
        mPath.lineTo(0, 0);
        
	}


	@Override
	protected void onDraw(Canvas canvas) {
		// TODO Auto-generated method stub
        super.onDraw(canvas);
        canvas.drawPath(mPath, mPaint);
	}
}
