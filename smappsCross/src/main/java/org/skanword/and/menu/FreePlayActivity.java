package org.skanword.and.menu;

import org.skanword.and.R;
import org.skanword.and.SmappsScanwords;
import org.skanword.and.SmappsScanwords.ActionType;
import org.skanword.and.etc.TopActivityManager.TopActivity;

import android.app.ActionBar;
import android.app.Activity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;

public class FreePlayActivity extends Activity implements TopActivity {

	public static final int CLOSE_ACTIVITY_RESULT = 500;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.free_play_activity_layout);
		ActionBar actionBar = getActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		setTitle(R.string.play_free_activity_title);
		getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

		((Button) findViewById(R.id.premiumButton))
				.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						SmappsScanwords.sendGAEvent(ActionType.USER_ACTION,	"click_menu_scans_iwantfree_gopremium"); // GA Event
						setResult(100);
						closeToMainPage();
					}
				});

		((LinearLayout) findViewById(R.id.toScanwordsButton))
				.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						SmappsScanwords.sendGAEvent(ActionType.USER_ACTION,
								"click_menu_scans_iwantfree_goissues"); // GA
																		// Event
						setResult(RESULT_OK);
						closeToMainPage();
					}
				});
	}

	private void closeToMainPage() {
		finish();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			setResult(RESULT_CANCELED, null);
			this.finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
