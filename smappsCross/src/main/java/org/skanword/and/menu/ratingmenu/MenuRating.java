package org.skanword.and.menu.ratingmenu;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.skanword.and.R;
import org.skanword.and.SmappsScanwords;
import org.skanword.and.SmappsScanwords.ActionType;
import org.skanword.and.datamanager.MainDataManager;
import org.skanword.and.datamanager.RatingDataManager.RatingType;
import org.skanword.and.datamanager.RatingDataManager.UsersRating;
import org.skanword.and.etc.CustomFragment;
import org.skanword.and.etc.Utils;
import org.skanword.and.menu.ArraySectionedListAdapter;
import org.skanword.and.menu.MainMenuActivity;
import org.skanword.and.network.MainNetworkManager;
import org.skanword.and.network.socialnetworks.SocialNetworkManager.SocialNetwork;

import android.app.ActionBar.Tab;
import android.app.ActionBar.TabListener;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.astuetz.PagerSlidingTabStrip;
import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;
import com.musselwhizzle.dispatcher.events.Event;
import com.musselwhizzle.dispatcher.events.EventListener;

import de.hdodenhof.circleimageview.CircleImageView;

public class MenuRating extends CustomFragment implements TabListener {

	private AppSectionsPagerAdapter mAppSectionsPagerAdapter;
	private View mMainView;
	private ViewPager mViewPager;
	private PagerSlidingTabStrip mTabs;
	private static Activity sActivity = null; 
	private View mAuthButtons;
	private EventListener mUserInfoUpdateEventListener;

	public MenuRating() {
		super();
		Log.v("", "MenuShop constructor");

	}

	@Override
	public void fragmentOpened() {
		updateFragment();
		super.fragmentOpened();
	}
	@Override
	public void updateFragment(boolean byUser) {
		if (MainNetworkManager.getInstance().hasNetworkConnection(byUser? getActivity() : null))
			MainNetworkManager.getInstance().requestInfoWithCompleteBlock(MainNetworkManager.INFO_RATING, getActivity(), null);
		super.updateFragment(byUser);
	}
	public void findUser() {
		if (!MainNetworkManager.getInstance().isUserAuthorised()) {
			AlertDialog.Builder builder = new AlertDialog.Builder(this.getActivity(), R.style.Theme_Scanword_style_dialog);
	        builder.setMessage("Авторизируйтесь, чтобы учавствовать в рейтинге.")
	               .setPositiveButton("Ок", new DialogInterface.OnClickListener() {
	                   public void onClick(DialogInterface dialog, int id) {
	                	   
	                   }
	               });
	        AlertDialog dialog = builder.create();
	        dialog.show();
		} else {
			RatingTabFragment fragment = (RatingTabFragment)getVisibleFragment();
			fragment.scrolltoUser();
		}
		SmappsScanwords.sendGAEvent(ActionType.USER_ACTION,	"click_menu_rating_finduser"); // GA Event
	}
    public Fragment getVisibleFragment(){
    	Fragment page = (Fragment)mAppSectionsPagerAdapter.instantiateItem(mViewPager, mViewPager.getCurrentItem());
	    // based on the current position you can then cast the page to the correct 
	    // class and call the method:
    	Log.v("",page + " getVisibleFragment " + "android:switcher:" + R.id.pager + ":" + mViewPager.getCurrentItem());
	    if (page != null) {
	         return page;     
	    } 
        return null;
    }
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		Log.v(""," onCreateView  MenuRating");
		mMainView = inflater.inflate(R.layout.rating_menu_layout, container,
				false);

		mAuthButtons = mMainView.findViewById(R.id.ratingAuthButtons);
		mAuthButtons.setVisibility(MainNetworkManager.getInstance().isUserAuthorised() ? View.GONE : View.VISIBLE);
		mAppSectionsPagerAdapter = new AppSectionsPagerAdapter(
				getChildFragmentManager());
		mViewPager = (ViewPager) mMainView.findViewById(R.id.shop_pager);
		mViewPager.setAdapter(mAppSectionsPagerAdapter);
		mTabs = (PagerSlidingTabStrip) mMainView.findViewById(R.id.shop_tabs);
		mTabs.setTextColorResource(R.color.action_bar_title_color);

		mTabs.setOnPageChangeListener(new OnPageChangeListener() {
			@Override
			public void onPageSelected(int position) {
				switch (position) {
				case 0:
					SmappsScanwords.sendGAEvent(ActionType.USER_ACTION, "menu_rating_all_enter"); // GA Event
					break;
				case 1:
					SmappsScanwords.sendGAEvent(ActionType.USER_ACTION, "menu_rating_friends_enter"); // GA Event
					break;

				default:
					break;
				}
			}
			
			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
				
			}
			
			@Override
			public void onPageScrollStateChanged(int state) {
				
			}
		});
		mTabs.setViewPager(mViewPager);
		sActivity = getActivity();


        final Activity act = MainMenuActivity.instance;
        View.OnClickListener authListener = new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (v.getId() == R.id.fbAuthButton) {
					MainNetworkManager.getInstance().loginInSocialNetwork(SocialNetwork.SOCIAL_NETWORK_FB, act, true);
				} else if (v.getId() == R.id.okAuthButton) {
					MainNetworkManager.getInstance().loginInSocialNetwork(SocialNetwork.SOCIAL_NETWORK_OK, act, true);
				} else if (v.getId() == R.id.vkAuthButton) {
					MainNetworkManager.getInstance().loginInSocialNetwork(SocialNetwork.SOCIAL_NETWORK_VK, act, true);
				}
			}
		};
		Button authButton = (Button) mAuthButtons.findViewById(R.id.okAuthButton);
		authButton.setOnClickListener(authListener);
		authButton = (Button) mAuthButtons.findViewById(R.id.fbAuthButton);
		authButton.setOnClickListener(authListener);
		authButton = (Button) mAuthButtons.findViewById(R.id.vkAuthButton);
		authButton.setOnClickListener(authListener);

		mUserInfoUpdateEventListener = new EventListener() {

			public void onEvent(Event event) {
				if (getActivity() != null) {
					getActivity().runOnUiThread(new Runnable() {
						public void run() {
							mAuthButtons.setVisibility(MainNetworkManager.getInstance().isUserAuthorised() ? View.GONE : View.VISIBLE);
						}
					});
				}
			}
		};
		
		SmappsScanwords.getEventsDispatcher().addListener(
				MainDataManager.EVENT_USER_INFO_UPDATE, mUserInfoUpdateEventListener);
		
		
		return mMainView;
	}
	
	public static Activity getRatingActivity() {
		return sActivity;
	}
	/**
	 * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
	 * one of the primary sections of the app.
	 */
	public static class AppSectionsPagerAdapter extends FragmentPagerAdapter {

		private Set<Object> fragments = new HashSet<Object>();

		public AppSectionsPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int i) {
			RatingTabFragment fragment = new RatingTabFragment();
			Bundle args = new Bundle();
			args.putSerializable("LIST_TYPE",
					i == 0 ? RatingType.SCANWORD_COMMON
							: RatingType.SCANWORD_FRIENDS);
			fragment.setArguments(args);
			return fragment;
		}

		@Override
		public int getCount() {
			return 2;
		}

		@Override
		public CharSequence getPageTitle(int position) {
			switch (position) {
			case 0:
				return "СРЕДИ ВСЕХ";
			case 1:
				return "СРЕДИ ДРУЗЕЙ";

			default:
				break;
			}
			return "Section " + (position + 1);
		}

		@Override
		public Object instantiateItem(ViewGroup container, int position) {
			// TODO Auto-generated method stub
			Object item = super.instantiateItem(container, position);

			fragments.add(item);
			return item;
		}

		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			// TODO Auto-generated method stub
			super.destroyItem(container, position, object);
		}

	}

	public void onTabSelected(Tab tab, FragmentTransaction ft) {
		// TODO Auto-generated method stub

	}

	public void onTabUnselected(Tab tab, FragmentTransaction ft) {
		// TODO Auto-generated method stub

	}

	public void onTabReselected(Tab tab, FragmentTransaction ft) {
		mViewPager.setCurrentItem(tab.getPosition());

	}

	public static class RatingTabFragment extends Fragment {

		private ListView mList;
		private RatingItemsAdapter mAdapter;
		private RatingType mListType;
		private EventListener mRatingUpdateEventListener;
		private View mView; 
		
		
		public RatingTabFragment() {
			super();
			
		}

		@Override
		public void onSaveInstanceState(Bundle outState) {
			super.onSaveInstanceState(outState);
			Log.v("", "  onSaveInstanceState ");
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			mView = inflater.inflate(R.layout.rating_list_layout,
					container, false);
			mList = (ListView) mView.findViewById(R.id.rating_list);
			mListType = (RatingType) getArguments().getSerializable(
					"LIST_TYPE");
			mList.setScrollBarStyle(ListView.SCROLLBARS_OUTSIDE_OVERLAY);
			mRatingUpdateEventListener = new EventListener() {

				public void onEvent(Event event) {
					update();
				}
			};
			
			SmappsScanwords.getEventsDispatcher().addListener(
					MainDataManager.EVENT_RATING_UPDATE, mRatingUpdateEventListener);
			setAdapter();

			
			return mView;

		}
		public void scrolltoUser() {
			if (mAdapter == null || mAdapter.getUserPosition() < 0)
				return;
			int h1 = mList.getHeight();
			int h2 = RatingItemsAdapter.sRowHeight;
			mList.smoothScrollToPositionFromTop(mAdapter.getUserPosition(), h1/2 - h2/2, 100);
		}

		@Override
		public void onStart() {
			super.onStart();
		}
		
		@Override
		public void onDestroy() {
			// TODO Auto-generated method stub
			super.onDestroy();
			SmappsScanwords.getEventsDispatcher().removeListener(
					MainDataManager.EVENT_RATING_UPDATE, mRatingUpdateEventListener);
		}

		public void update() {
			if (getActivity() != null) {
				getActivity().runOnUiThread(new Runnable() {
					public void run() {
						setAdapter();
					}
				});
			}
		}

		private void setAdapter() {
			List<UsersRating> items = mListType == RatingType.SCANWORD_COMMON ? MainDataManager.getInstance()
					.getRatingDataManager().getCommonRating() : MainDataManager.getInstance()
					.getRatingDataManager().getFriendsRating();
			if (items == null || items.size() == 0) {
				if (mListType == RatingType.SCANWORD_FRIENDS) {
					mList.setVisibility(View.GONE);
					mView.findViewById(R.id.ratingEmptyLabel).setVisibility(View.VISIBLE);
				}
				return;
			}
			mList.setVisibility(View.VISIBLE);
			mView.findViewById(R.id.ratingEmptyLabel).setVisibility(View.GONE);
			mAdapter = new RatingItemsAdapter(getActivity(),
					R.layout.rating_user_row_item_layout, items);
			mList.setAdapter(mAdapter);
		}
	}

	public static class RatingItemsAdapter extends
			ArraySectionedListAdapter<UsersRating> {
		private final List<UsersRating> objectsList;
		public static int sRowHeight = 0;
		private int sectionsCount = 1;
		private int rowsInFirstSection = 0;

		public RatingItemsAdapter(Context context, int resource,
				List<UsersRating> objects) {
			super(context, resource, objects);
			Log.v("", "SetsAdapter");
			this.objectsList = objects;
			rowsInFirstSection = objectsList.size();
			for (int i = 1; i < objectsList.size(); i++) {
				UsersRating userprev = objectsList.get(i - 1);
				UsersRating usercur = objectsList.get(i);
				if (usercur.getUserPlace() - userprev.getUserPlace() > 1) {
					sectionsCount = 2;
					rowsInFirstSection = i+1;
					break;
				}
			}
		}
		
		public int getUserPosition() {
			if (!MainNetworkManager.getInstance().isUserAuthorised())
				return -1;
			for (UsersRating user : objectsList) {
				if (user.getSocialUser().getUserId().equals(MainNetworkManager.getInstance().getUserAuth().getUserSocialId())) {
					return objectsList.indexOf(user) + sectionsCount;
				}
			}
			return - 1;
		}
		
		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent) {
			return super.getView(position, convertView, parent);
		}
		
		@Override
		protected int getNumberOfSections() {
			return sectionsCount;
		}

		@Override
		protected int getNumberOfRowsInSection(int section) {
			if (section == 0) {
				return rowsInFirstSection;
			}
			return this.objectsList.size() - rowsInFirstSection;
		}

		@Override
		protected View newSectionView(Context context, int section,
				ViewGroup parent) {
			return ((LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE))
					.inflate(R.layout.header, parent, false);
		}

		@Override
		protected void bindSectionView(Context context, int section,
				View convertView) {
			if (convertView == null || objectsList.size() < 1)
				return;
			((TextView) convertView.findViewById(R.id.setLabel)).setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
				}
			});
			if (section == 0) {
				((TextView) convertView.findViewById(R.id.setLabel))
				.setText(objectsList.get(0).getUserPlace()+"-"+objectsList.get(rowsInFirstSection - 1).getUserPlace() + " место");
			} else {				((TextView) convertView.findViewById(R.id.setLabel))
				.setText(objectsList.get(rowsInFirstSection).getUserPlace()+"-"+objectsList.get(objectsList.size() - 1).getUserPlace() + " место");
			}
		}

		@Override
		protected View newRowViewInSection(Context context, int section,
				int row, ViewGroup parent) {
			LayoutInflater inflater = LayoutInflater.from(getContext());
			int resId = R.layout.rating_user_row_item_layout;

			final View convertView = inflater.inflate(resId, parent, false);
			sRowHeight = convertView.getHeight();
			return convertView;
		}


		@Override
		protected int getViewTypeForRowInSection(int section, int row) {
			return TYPE_ROW_VIEW;
		}


		@Override
		protected void bindRowViewInSection(Context context, int section,
				int row, final View convertView) {
			final UsersRating item = getItemInSectionAndRow(section, row);

			if (item != null) {
				buildRowView(convertView, item);
			}
		}

		@Override
		public UsersRating getItemInSectionAndRow(int section, int row) {
			if (section == 0) {
				return getItem(row);
			}
			return getItem(row + rowsInFirstSection);
		}
		
		private void buildRowView(final View convertView, final UsersRating item) {
			if (convertView == null)
				return;
			TextView userName = (TextView)convertView.findViewById(R.id.userNameLabel);
			userName.setText(item.getSocialUser().getUserName());
			TextView placeLabel = (TextView)convertView.findViewById(R.id.placeLabel);
			placeLabel.setText(item.getUserPlace()+" место");
			TextView pointsLabel = (TextView)convertView.findViewById(R.id.pointsLabel);
			pointsLabel.setText(item.getUserScore() + " " + Utils.spellVariantForNumber(Integer.parseInt(item.getUserScore()), "очко", "очка", "очков"));
			CircleImageView profileImage = ((CircleImageView) convertView.findViewById(R.id.profile_image));
    		if (item.getSocialUser().getUserPhoto() != null && item.getSocialUser().getUserPhoto().length() > 2) {
    			/*DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
    					.showImageForEmptyUri(R.drawable.avatar)
    					.showImageOnFail(R.drawable.avatar).build();
    			ImageLoader.getInstance().displayImage(item.getSocialUser().getUserPhoto(), (ImageView) convertView.findViewById(R.id.profile_image), options);*/
    			UrlImageViewHelper.setUrlDrawable(profileImage, item.getSocialUser().getUserPhoto(), R.drawable.avatar);
    		} else {
    			profileImage.setImageResource(R.drawable.avatar);
    		}
    		if (MainNetworkManager.getInstance().isUserAuthorised() && item.getSocialUser().getUserId().equals(MainNetworkManager.getInstance().getUserAuth().getUserSocialId())) {
        		profileImage.setBorderColor(0xff679C09);
    		} else {
        		profileImage.setBorderColor(0xfff6f6f6);
    		}
    		convertView.setClickable(true);
		}

	}

	@Override
	public void onDestroy() {
		Log.v("", "onDestroy -------MenuShop------");
		sActivity = null;
		SmappsScanwords.getEventsDispatcher().removeListener(
				MainDataManager.EVENT_USER_INFO_UPDATE, mUserInfoUpdateEventListener);
		super.onDestroy();
	}

	@Override
	public void onPause() {
		Log.v("", "onPause -------MenuShop------");
		super.onPause();
	}

	@Override
	public void onResume() {
		Log.v("", "onResume -------MenuShop------");
		super.onResume();
	}

	@Override
	public void onStart() {
		Log.v("", "onStart -------MenuShop------");
		super.onStart();
	}

	@Override
	public void onStop() {
		Log.v("", "onStop -------MenuShop------");
		super.onStop();
	}
}
