package org.skanword.and.menu.newsmenu;

import java.util.List;
import org.skanword.and.R;
import org.skanword.and.SmappsScanwords;
import org.skanword.and.SmappsScanwords.ActionType;
import org.skanword.and.datamanager.MainDataManager;
import org.skanword.and.datamanager.NewsDataManager.News;
import org.skanword.and.datamanager.ShopDataManager.ShopProductType;
import org.skanword.and.etc.CustomFragment;
import org.skanword.and.inappbilling.InAppManager;
import org.skanword.and.inappbilling.InAppManager.PurchaseSource;
import org.skanword.and.menu.MainMenuActivity;
import org.skanword.and.menu.MainMenuActivity.AppPage;
import org.skanword.and.network.MainNetworkManager;
import org.skanword.and.network.socialnetworks.SocialNetworkManager.SocialNetwork;
import android.annotation.SuppressLint;
import android.app.ActionBar.Tab;
import android.app.ActionBar.TabListener;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;
import com.musselwhizzle.dispatcher.events.Event;
import com.musselwhizzle.dispatcher.events.EventListener;
import com.qustom.dialog.QustomDialogBuilder;

public class MenuNews  extends CustomFragment implements TabListener{

	private ListView mList;
	private ListAdapter mAdapter;
	private EventListener mNewsUpdateEvent;
	private View mMainView;
	
	public MenuNews() {
		super();
		Log.v("", "MenuShop constructor");

		// TODO Auto-generated constructor stub
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mMainView = inflater.inflate(R.layout.activity_news, container,
				false);
		mList = (ListView) mMainView.findViewById(R.id.news_list);
		mList.setScrollBarStyle(ListView.SCROLLBARS_OUTSIDE_OVERLAY);
		mNewsUpdateEvent = new EventListener() {
			@Override
			public void onEvent(Event event) {
				update();
			}
		};

		if (mNewsUpdateEvent != null) {
			SmappsScanwords.getEventsDispatcher().addListener(
				MainDataManager.EVENT_NEWS_UPDATE, mNewsUpdateEvent);
			SmappsScanwords.getEventsDispatcher().addListener(
					MainDataManager.EVENT_USER_INFO_UPDATE, mNewsUpdateEvent);
		}
		update();
		return mMainView;
	}
	@Override
	public void fragmentOpened() {
		updateFragment();
		super.fragmentOpened();
	}
	@Override
	public void updateFragment(boolean byUser) {
		if (MainNetworkManager.getInstance().hasNetworkConnection(byUser? getActivity() : null))
			MainNetworkManager.getInstance().requestInfoWithCompleteBlock(MainNetworkManager.INFO_NEWS, getActivity(), null);
		super.updateFragment(byUser);
	}
	@Override
	public void onDestroy() {
		if (mNewsUpdateEvent != null) {
			SmappsScanwords.getEventsDispatcher().removeListener(
				MainDataManager.EVENT_NEWS_UPDATE, mNewsUpdateEvent);
			SmappsScanwords.getEventsDispatcher().removeListener(
					MainDataManager.EVENT_USER_INFO_UPDATE, mNewsUpdateEvent);
		}
		super.onDestroy();
	}
	public void update() {
		getActivity().runOnUiThread(new Runnable() {
			public void run() {
				if (mList == null)
					return;
				setAdapter();
			}
		});
	}
	

	private void setAdapter() {
		if (MainDataManager.getInstance().getNewsDataManager().getNews() == null)
			return;
		mAdapter = new NewsItemsAdapter(getActivity(), R.layout.news_row_item, getActivity(), 
				MainDataManager.getInstance().getNewsDataManager().getNews());
		mList.setAdapter(mAdapter);

	}
	public static class NewsItemsAdapter extends
			ArrayAdapter<News> {
		private int mResourceId;

		public NewsItemsAdapter(Context context, int resource,
				Activity activity, List<News> objects) {
			super(context, resource, objects);
			mResourceId = resource;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			if (convertView != null) {
			} else {
				convertView = createRowView(parent);
			}
			rebuildView(convertView, getItem(position));
			return convertView;
		}
		private void rebuildView(View view, final News news) {
			TextView titleLabel = (TextView)view.findViewById(R.id.titleLabel);
			titleLabel.setText(news.getTitle());
			TextView dateLabel = (TextView)view.findViewById(R.id.dateLabel);
			dateLabel.setText(news.getDate());
			TextView textLabel = (TextView)view.findViewById(R.id.newsText);
			textLabel.setText(news.getText());
			boolean hasAction = news.getAction() != null && !news.getAction().equals("");
			if (hasAction) {
				if (news.getAction().equals("authorise") && MainNetworkManager.getInstance().isUserAuthorised()) {
					hasAction = false;
				} else if (news.getAction().equals("activate_premium") && MainDataManager.getInstance().getUserData().isVip()) {
					hasAction = false;
				}
			}
			if (hasAction) {
				Button actionButton = (Button)view.findViewById(R.id.actionButton);
				actionButton.setText(actionToName(news.getAction())); 
				actionButton.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						doActionForNews(news);
					}
				});
			}
			
			ImageView newsIcon = (ImageView)view.findViewById(R.id.newsIcon);
			
			if (news.getImage() != null && !news.getImage().equals("")) {
    			UrlImageViewHelper.setUrlDrawable(newsIcon, news.getImage(), R.drawable.icon);
    		} else {
    			newsIcon.setImageResource(R.drawable.icon);
    		}
			
			view.findViewById(R.id.devideLine).setVisibility(hasAction ? View.VISIBLE : View.GONE);
			view.findViewById(R.id.actionButton).setVisibility(hasAction ? View.VISIBLE : View.GONE);
			
		}
		private void doActionForNews(News news) {
			if  (news.getAction() == null)
				return;
			SmappsScanwords.sendGAEvent(ActionType.USER_ACTION, "click_news_action_" + news.getAction()); // GA Event
			if (news.getAction().equals("issues")) {
				if (MainMenuActivity.instance != null) {
					MainMenuActivity.instance
							.navigateToAppPage(AppPage.PAGE_SKANWORDS);
				}
			} else  if (news.getAction().equals("shop")) {
				if (MainMenuActivity.instance != null) {
					MainMenuActivity.instance
							.navigateToAppPage(AppPage.PAGE_HINTS);
				}
			} else if (news.getAction().equals("follow_link") && news.getLink() != null) {
				Intent browse = new Intent( Intent.ACTION_VIEW , Uri.parse(news.getLink()) );
				MainMenuActivity.instance.startActivity( browse );
			} else if (news.getAction().equals("activate_premium")) {
				//InAppManager.getInstance().purchaseProduct(MainDataManager.getInstance().getShopDataManager().getDefaultShopItemOfType(ShopProductType.SUBSCRIPTION).getId(), MainMenuActivity.instance, PurchaseSource.NEWS);
			} else if (news.getAction().equals("authorise")) {
				showAuthDialog();
			}
		}
		private String actionToName(String action) {
			if (action.equals("issues")) {
				return "Перейти к сканвордам";
			} else  if (action.equals("shop")) {
				return "Перейти в магазин";
			} else if (action.equals("follow_link")) {
				return "Перейти";
			} else if (action.equals("activate_premium")) {
				return "Активировать";
			} else if (action.equals("authorise")) {
				return "Авторизоваться";
			}
			return "Клик";
		}
		private View createRowView(ViewGroup parent) {
			LayoutInflater inflater = LayoutInflater.from(getContext());			 
			final View view = inflater.inflate(mResourceId, parent, false);
			return view;
		}
		@SuppressLint("InflateParams")
		private void showAuthDialog() {
			LayoutInflater li = LayoutInflater.from(MainMenuActivity.instance);
			View view= li.inflate(R.layout.auth_dialog_layout, null);
	        
	        QustomDialogBuilder qustomDialogBuilder = new QustomDialogBuilder(MainMenuActivity.instance, R.style.Theme_Scanword_style_dialog). 
	                setDividerColor("#44000000").setTitle("Авторизироваться через:").setTitleColor("#000000");
	        qustomDialogBuilder.setPositiveButton("Отмена", new DialogInterface.OnClickListener() {
	                    public void onClick(DialogInterface dialog, int id) {
	                    	
	                    }
	                });
	        
	        qustomDialogBuilder.setCustomView(view);
	        final AlertDialog dialog = qustomDialogBuilder.show();
	        View.OnClickListener authListener = new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if (v.getId() == R.id.fbAuthButton) {
						MainNetworkManager.getInstance().loginInSocialNetwork(SocialNetwork.SOCIAL_NETWORK_FB, MainMenuActivity.instance, true);
					} else if (v.getId() == R.id.okAuthButton) {
						MainNetworkManager.getInstance().loginInSocialNetwork(SocialNetwork.SOCIAL_NETWORK_OK, MainMenuActivity.instance, true);
					} else if (v.getId() == R.id.vkAuthButton) {
						MainNetworkManager.getInstance().loginInSocialNetwork(SocialNetwork.SOCIAL_NETWORK_VK, MainMenuActivity.instance, true);
					}
					dialog.dismiss();
				}
			};
			Button authButton = (Button) view.findViewById(R.id.okAuthButton);
			authButton.setOnClickListener(authListener);
			authButton = (Button) view.findViewById(R.id.fbAuthButton);
			authButton.setOnClickListener(authListener);
			authButton = (Button) view.findViewById(R.id.vkAuthButton);
			authButton.setOnClickListener(authListener);
		}
	}
	@Override
	public void onTabSelected(Tab tab, FragmentTransaction ft) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onTabUnselected(Tab tab, FragmentTransaction ft) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onTabReselected(Tab tab, FragmentTransaction ft) {
		// TODO Auto-generated method stub
		
	}
}
