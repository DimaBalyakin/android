package org.skanword.and.menu;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;
import org.skanword.and.R;
import org.skanword.and.network.MainNetworkManager;
import org.skanword.and.network.socialnetworks.SocialNetworkManager.SocialUser;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

public class AuthorisationAcceptActivity extends Activity {

	private StatsListAdapter mAdapter;
	private ListView mOptionsList;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.authorisation_accept_activity_layout);
	    ActionBar actionBar = getActionBar();
	    actionBar.setDisplayHomeAsUpEnabled(true);
		String userInfo = getIntent().getStringExtra("user_info");
		SocialUser user = (SocialUser) getIntent().getSerializableExtra("user_social");
		TextView name = (TextView) findViewById(R.id.userNameLabel);
		name.setText(user.getFirstName() + " " + user.getSecondName() + " " + getResources().getString(R.string.progress_exist_label));
		if (user.getAvatarUrl() != null)
			ImageLoader.getInstance().displayImage(user.getAvatarUrl(), (ImageView) findViewById(R.id.profile_image));
		JSONObject json = null;
		try {
			json = new JSONObject(userInfo);
			json = json.getJSONObject("user_info");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		((Button)findViewById(R.id.cancelAuthButton)).setOnClickListener(new OnClickListener() {					
			public void onClick(View v) {
				setResult(RESULT_CANCELED);
			    finish();							
			}
		});
		((Button)findViewById(R.id.acceptAuthButton)).setOnClickListener(new OnClickListener() {					
			public void onClick(View v) {
				MainNetworkManager.getInstance().acceptAuthorisation(MainMenuActivity.instance, true);
				setResult(RESULT_OK);
			    finish();							
			}
		});
	    setTitle("Внимание!");
	    List<StatData> stats = new ArrayList<AuthorisationAcceptActivity.StatData>();
	    stats.add(new StatData("", ""));
	    if (json != null) {
		    try {
			    stats.add(new StatData("Решено сканвордов:", json.getInt("cross_amount_solved") + ""));
			    stats.add(new StatData("Начато сканвордов:", json.getInt("cross_amount_started") + ""));
			    stats.add(new StatData("Очков сканвордов:", json.getInt("cross_user_score") + ""));
			    stats.add(new StatData("Подсказок:", json.getInt("hints") + ""));
				stats.add(new StatData("Премиум:", json.getInt("vip") > 0 ? "Активен" : "Не активен"));
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    }
        mAdapter = new StatsListAdapter(this, R.layout.statistic_row_layout, stats);
        mOptionsList = (ListView) findViewById(R.id.statsListView);
        mOptionsList.setAdapter(mAdapter);
	    
	}
    public boolean onOptionsItemSelected(MenuItem item) {
    	switch (item.getItemId()) {
	    case android.R.id.home:
	        this.finish();
	        return true;
	    }
	    return super.onOptionsItemSelected(item);
	}
    public static class StatsListAdapter extends ArrayAdapter<StatData> {
    	private int mResourceId;
    	
		public StatsListAdapter(Context context, int textViewResourceId,
				List<StatData> objects) {
			super(context, textViewResourceId, objects);
			// TODO Auto-generated constructor stub
			mResourceId = textViewResourceId;
		}
		

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			if (convertView == null) {
				convertView = createRowView(parent);
			}
			rebuildView(convertView, getItem(position), position);
			return convertView;
		}
		private View createRowView(ViewGroup parent) {
			LayoutInflater inflater = LayoutInflater.from(getContext());			 
			final View view = inflater.inflate(mResourceId, parent, false);
			return view;
		}
		private void rebuildView(View view, StatData stat, int position) {
			TextView statName = (TextView) view.findViewById(R.id.statNameLabel);
			TextView statValue = (TextView) view.findViewById(R.id.statValueLabel);
			statName.setText(stat.getName());
			statValue.setText(stat.getValue());
			view.setClickable(true);
		}
    }
    public static class StatData {
    	private String mName;
    	private String mValue;
    	
    	
    	
		public StatData(String mName, String mValue) {
			super();
			this.mName = mName;
			this.mValue = mValue;
		}
		public String getName() {
			return mName;
		}
		public void setName(String mName) {
			this.mName = mName;
		}
		public String getValue() {
			return mValue;
		}
		public void setValue(String mValue) {
			this.mValue = mValue;
		}
    	
    }
}
