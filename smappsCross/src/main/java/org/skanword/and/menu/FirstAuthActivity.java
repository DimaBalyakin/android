package org.skanword.and.menu;

import org.skanword.and.R;
import org.skanword.and.SmappsScanwords;
import org.skanword.and.SmappsScanwords.ActionType;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class FirstAuthActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.first_auth_activity_layout);
		final Activity activity = this;
    	View.OnClickListener authListener = new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				int result = 0;
				if (v.getId() == R.id.fbAuthButton) {
					result = 1;
					SmappsScanwords.sendGAEvent(ActionType.USER_ACTION,	"click_popup_login_fb"); // GA Event
					//MainNetworkManager.getInstance().loginInSocialNetwork(SocialNetwork.SOCIAL_NETWORK_FB, act);
				} else if (v.getId() == R.id.okAuthButton) {
					result = 2;
					SmappsScanwords.sendGAEvent(ActionType.USER_ACTION,	"click_popup_login_ok"); // GA Event
					//MainNetworkManager.getInstance().loginInSocialNetwork(SocialNetwork.SOCIAL_NETWORK_OK, act);
				} else if (v.getId() == R.id.vkAuthButton) {
					result = 3;
					SmappsScanwords.sendGAEvent(ActionType.USER_ACTION,	"click_popup_login_vk"); // GA Event
					//MainNetworkManager.getInstance().loginInSocialNetwork(SocialNetwork.SOCIAL_NETWORK_VK, act);
				} else if (v.getId() == R.id.cancelAuthButton) {
					result = 4;
					SmappsScanwords.sendGAEvent(ActionType.USER_ACTION,	"click_popup_login_later"); // GA Event

					//MainNetworkManager.getInstance().loginInSocialNetwork(SocialNetwork.SOCIAL_NETWORK_NO, act);
				}
				activity.setResult(result);
				activity.finish();
			}
		};
		Button authButton = (Button) findViewById(R.id.okAuthButton);
		authButton.setOnClickListener(authListener);
		authButton = (Button) findViewById(R.id.fbAuthButton);
		authButton.setOnClickListener(authListener);
		authButton = (Button) findViewById(R.id.vkAuthButton);
		authButton.setOnClickListener(authListener);
		authButton = (Button) findViewById(R.id.cancelAuthButton);
		authButton.setOnClickListener(authListener);
	}

}
