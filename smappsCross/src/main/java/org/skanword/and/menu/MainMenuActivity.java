package org.skanword.and.menu;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.skanword.and.R;
import org.skanword.and.SmappsScanwords;
import org.skanword.and.SmappsScanwords.ActionType;
import org.skanword.and.datamanager.MainDataManager;
import org.skanword.and.datamanager.ShopDataManager.ShopProductType;
import org.skanword.and.datamanager.Skanword;
import org.skanword.and.datamanager.UserUpdatesManager;
import org.skanword.and.etc.AdsManager;
import org.skanword.and.etc.AdsManager.AdsEventsListener;
import org.skanword.and.etc.CustomFragment;
import org.skanword.and.etc.CustomReferralReceiver;
import org.skanword.and.etc.LocalNotificationsManager;
import org.skanword.and.etc.SoundsManager;
import org.skanword.and.etc.TopActivityManager.TopActivity;
import org.skanword.and.inappbilling.InAppManager;
import org.skanword.and.menu.HintsBonusDialog.HintsBonusDialogDelegate;
import org.skanword.and.menu.newsmenu.MenuNews;
import org.skanword.and.menu.ratingmenu.MenuRating;
import org.skanword.and.menu.shopmenu.MenuShop;
import org.skanword.and.menu.skanwordsmenu.MenuSkanwords;
import org.skanword.and.network.DailyBonusObject;
import org.skanword.and.network.MainNetworkManager;
import org.skanword.and.network.MainNetworkManager.RequestCompletionBlock;
import org.skanword.and.network.socialnetworks.SocialNetworkManager.SocialNetwork;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender.SendIntentException;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.google.android.gms.appstate.AppStateManager;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.drive.Drive;
import com.google.android.gms.games.Games;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.musselwhizzle.dispatcher.events.Event;
import com.musselwhizzle.dispatcher.events.EventListener;
import com.sbstrm.appirater.Appirater;

public class MainMenuActivity extends FragmentActivity implements TopActivity {

	public static final String PROPERTY_REG_ID = "registration_id";
	private static final String PROPERTY_APP_VERSION = "appVersion";
	private static final String CAMPAIGN_SOURCE_PARAM = "utm_source";
	String SENDER_ID = "496252195507";
	private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

	private DrawerLayout mDrawerLayout;
	private ActionBarDrawerToggle mDrawerToggle;
	private CharSequence mDrawerTitle;
	private CharSequence mTitle;
	private LeftPanel mLeftPanelView;
	private MenuSkanwords mMenuScanwords;
	private MenuShop mMenuShop;
	private MenuRating mMenuRating;
	private MenuNews mMenuNews;
	public static MainMenuActivity instance = null;
	private int mCurrentTag = -1;
	private Bundle openIntentExtras = null;
	private CustomFragment mCurrentFragment = null;
	private EventListener mUserInfoEventListener;
	private EventListener mPushTokenEventListener;
	private EventListener mNotificationsEventListener;
	private boolean fistEnter;
	private String mReferrer;
	private EventListener mTimerEventListener;
	private GoogleApiClient mGoogleApiClient;
	private static boolean hasPermissions = false;
	private Bundle savedInstanceState;

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		instance = this;
		super.onCreate(savedInstanceState);

		if (hasPermissions) {
			startDataApp(savedInstanceState);
		} else {
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
				if (showPermissionsWindow()) {
					this.savedInstanceState = savedInstanceState;
				} else {
					//... Permission has already been granted, obtain the UUID
					hasPermissions = true;
					startDataApp(savedInstanceState);
				}
			} else {
				//... No need to request permission, obtain the UUID
				hasPermissions = true;
				startDataApp(savedInstanceState);
			}
		}
	}

	private boolean showPermissionsWindow() {
		List<String> permissions = new ArrayList<>(2);
		if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
			permissions.add(android.Manifest.permission.READ_PHONE_STATE);
		}
		if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
			permissions.add(android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
		}

		if (!permissions.isEmpty()) {
			ActivityCompat.requestPermissions(this, permissions.toArray(new String[0]), 1);
		}

		return !permissions.isEmpty();
}

	@Override
	public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
		super.onRequestPermissionsResult(requestCode, permissions, grantResults);
		Log.v("", "Permission: "+ Arrays.toString(permissions) + "was "+ Arrays.toString(grantResults));
		if (!showPermissionsWindow()) {
			hasPermissions = true;
			startDataApp(savedInstanceState);
			savedInstanceState = null;
			//resume tasks needing this permission
		}
	}

	public boolean hasPermissions() {
		return hasPermissions;
	}

	private void startDataApp(Bundle savedInstanceState) {
		SmappsScanwords.instance.startDataApp();
		Intent intent = this.getIntent();
		Uri uri = intent.getData();
		Log.v("", "uri-some  "  + uri );
		Appirater.appLaunched(this);
		SoundsManager.init(instance);
		setContentView(R.layout.main_menu_activity_layout);
		fistEnter = SmappsScanwords.getAppSharedPreferences(MainDataManager.GENERAL_PREFERENCES_NAME).getBoolean("first_enter", true);
		SmappsScanwords.getAppSharedPreferencesEditor(MainDataManager.GENERAL_PREFERENCES_NAME).putBoolean("first_enter", false).commit();
		checkStartIntent(getIntent());
		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mLeftPanelView = (LeftPanel) findViewById(R.id.left_drawer);
		mLeftPanelView.setController(this);
		mTitle = mDrawerTitle = getTitle();
		mMenuScanwords = new MenuSkanwords();
		mMenuShop = new MenuShop();
		mMenuRating = new MenuRating();
		mMenuNews = new MenuNews();
		mDrawerToggle = new ActionBarDrawerToggle(this, /* host Activity */
				mDrawerLayout, /* DrawerLayout object */
				R.drawable.ic_drawer, /* nav drawer icon to replace 'Up' caret */
				R.string.drawer_open, /* "open drawer" description */
				R.string.drawer_close /* "close drawer" description */
		) {

			/** Called when a drawer has settled in a completely closed state. */
			public void onDrawerClosed(View view) {
				super.onDrawerClosed(view);
				getActionBar().setTitle(mTitle);
			}

			/** Called when a drawer has settled in a completely open state. */
			public void onDrawerOpened(View drawerView) {
				super.onDrawerOpened(drawerView);
				getActionBar().setTitle(mDrawerTitle);
			}
		};
		mDrawerLayout.setDrawerListener(mDrawerToggle);
		UserUpdatesManager.getInstance();
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setHomeButtonEnabled(true);

		if (savedInstanceState != null
				&& savedInstanceState.containsKey("curMenu")) {
			this.openMenu(savedInstanceState.getInt("curMenu"));
		} else {
			this.openMenu(0);
		}
		mUserInfoEventListener = new EventListener() {
			public void onEvent(Event event) {
				runOnUiThread(new Runnable() {
					public void run() {
						mLeftPanelView.updateUserInfo();
					}
				});
			}
		};
		mPushTokenEventListener = new EventListener() {
			public void onEvent(Event event) {
				runOnUiThread(new Runnable() {
					public void run() {
						getPushToken();
					}

				});
			}
		};
		mNotificationsEventListener = new EventListener() {
			public void onEvent(Event event) {
				runOnUiThread(new Runnable() {
					public void run() {
						updateNotifications(true);
					}
				});
			}
		};

		mTimerEventListener = new EventListener() {
			@Override
			public void onEvent(Event event) {
				runOnUiThread(new Runnable() {
					public void run() {
						if (MainDataManager.getInstance().getDailyBonus() != null && (MainDataManager.getInstance().getDailyBonus().getDownloadedTime() - new Date().getTime() + MainDataManager.getInstance().getDailyBonus().getTimeLeft() * 1000) < 0 && MainNetworkManager.getInstance().hasNetworkConnection()) {
							showBonusPla();
						}
					}
				});
			}
		};
		SmappsScanwords.getEventsDispatcher().addListener(SmappsScanwords.APPLICATION_TIMER_EVENT, mTimerEventListener);

		if (mNotificationsEventListener != null)
			SmappsScanwords.getEventsDispatcher().addListener(
					MainDataManager.EVENT_NOTIFICATIONS_UPDATE,
					mNotificationsEventListener);
		if (mNetworkStateChangedEventListener != null)
			SmappsScanwords.getEventsDispatcher().addListener(
					MainNetworkManager.EVENT_NETWORK_STATE_CHANGED_ALL,
					mNetworkStateChangedEventListener);
		if (mPushTokenEventListener != null)
			SmappsScanwords.getEventsDispatcher().addListener(
					MainDataManager.EVENT_NO_TOKEN, mPushTokenEventListener);
		if (mUserInfoEventListener != null)
			SmappsScanwords.getEventsDispatcher().addListener(
					MainDataManager.EVENT_USER_INFO_UPDATE,
					mUserInfoEventListener);

		MainNetworkManager.getInstance().requestConfigExecWithCompleteBlock(new RequestCompletionBlock() {
			@Override
			public void complete(boolean success) {
				MainNetworkManager.getInstance().requestSync(new RequestCompletionBlock() {
					@Override
					public void complete(boolean success) {
						if (mCurrentFragment != null) {
							MainMenuActivity.this.runOnUiThread(new Runnable() {
								@Override
								public void run() {
									mCurrentFragment.updateFragment();
								}
							});
						}
					}
				});
			}
		});

		AdsManager.getInstance().init(instance);
		setupGoogleApiClient();
	}

	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		// TODO Auto-generated method stub
		super.onWindowFocusChanged(hasFocus);
	}
	private void setupGoogleApiClient() {
		mGoogleApiClient = new GoogleApiClient.Builder(this)
        .addConnectionCallbacks(new ConnectionCallbacks() {
			
			@Override
			public void onConnectionSuspended(int arg0) {
				MainMenuActivity.this.mGoogleApiClient.connect();
				Log.v("SkanwordsFunc", "onConnectionSuspended");
			}
			
			@Override
			public void onConnected(Bundle arg0) {
				Log.v("SkanwordsFunc", "onConnected");
				
			}
		})
        .addOnConnectionFailedListener(new OnConnectionFailedListener() {
			
			@Override
			public void onConnectionFailed(ConnectionResult arg0) {
				try {
					arg0.startResolutionForResult(MainMenuActivity.this, 200);
				} catch (SendIntentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				Log.v("SkanwordsFunc", "onConnectionFailed " + arg0.getErrorCode());
				
			}
		})
        .addApi(Games.API).addScope(Games.SCOPE_GAMES) // Games
        .addApi(AppStateManager.API).addScope(AppStateManager.SCOPE_APP_STATE) // AppState
        .addScope(Drive.SCOPE_APPFOLDER) // SavedGames
        .build();
	}
	public void showBonusPla() {
		if (!MainNetworkManager.getInstance().hasNetworkConnection(this) || HintsBonusDialog.isShowing() || MainDataManager.getInstance().getDailyBonus().getScratchedPosition() > 0)
			return;
		Log.v("SkanwordsFunc", "scratched position " + MainDataManager.getInstance().getDailyBonus().getScratchedPosition());
		HintsBonusDialog.showDialogInContext(MainMenuActivity.this, new HintsBonusDialogDelegate() {
			
        	private void showCompl(final boolean doubled, final boolean hadRewardedVideo) {
        		
        		final DailyBonusObject bonus = MainDataManager.getInstance().getDailyBonus();
        		if (bonus == null) {
        			//something goes wrong
        			return;
        		}
				MainDataManager.getInstance().addUserHints(bonus.getWin(), false);
				UserUpdatesManager.getInstance().bonusRecieved(doubled, hadRewardedVideo, true);
				MainDataManager.getInstance().rewriteDailyBonusObjectRecieved(null);
				MainNetworkManager.getInstance().requestSyncExec();
        		
        		MainMenuActivity.this.runOnUiThread(new Runnable() {
					@Override
					public void run() {
						HintsBonusDialog.showCompleteBonusDialogInContext(MainMenuActivity.this, bonus, doubled);
					}
				});
        	}
        	
			@Override    
			public void onTake(boolean hadRewardedVideo) {
				showCompl(false, hadRewardedVideo); 
			}
			
			@Override
			public void onDouble() {
				AdsManager.getInstance().showVideoOffer(MainMenuActivity.this,
						new AdsEventsListener() {
							@Override
							public void onComplete() {
								super.onComplete();
								Log.v("SkanwordsAds", "onComplete listener");
								showCompl(true, true);
							}
							@Override
							public void onSkipped() {
								super.onSkipped();
								Log.v("SkanwordsAds", "videoSkipped listener");
								showCompl(false, true);
							}
							@Override
							public void onNoAds() {
								Log.v("SkanwordsAds", "onNoAds listener");
							}
						}, "hh", false);
			}
			
			@Override
			public void onCancel() {
				
			}
		}, MainDataManager.getInstance().getDailyBonus());
	}
	private EventListener  mNetworkStateChangedEventListener = new EventListener() {
		@Override
		public void onEvent(Event event) {
			Log.v("SkanwordsFunc", " mNetworkStateChangedEventListener event");
			if (mCurrentFragment != null) {
				MainMenuActivity.this.runOnUiThread(new Runnable() {
					@Override
					public void run() {
						mCurrentFragment.updateFragment();
					}
				});
			}
		}
	}; 
	public boolean isFirstStart() {
		return fistEnter;
	}


	@Override
	protected void onNewIntent(Intent intent) {
		checkStartIntent(intent);
		super.onNewIntent(intent);
	}

	public Bundle getOpenIntentExtras() {
		return openIntentExtras;
	}

	public void clearOpenIntentExtras() {
		openIntentExtras = null;
	}

	private void checkStartIntent(Intent intent) {
		if (intent != null) {
			openIntentExtras = intent.getExtras();
		}
	}

	public void updateNotifications(boolean total) {
		LocalNotificationsManager.createLocalNotifications(
				getApplicationContext(), total);
	}

	private void getPushToken() {
		Log.v("", " getPushToken getpush token");
		if (checkPlayServices()) {
			Log.v("", "getPushToken checkPlayServices ");
			String regid = getRegistrationId(getApplicationContext());

			Log.v("", "getPushToken getRegistrationId " + regid);
			if (regid.isEmpty()) {
				registerInBackground();
			} else {
				sendRegistrationIdToBackend(regid);
			}
		} else {
			Log.i("", "No valid Google Play Services APK found.");
		}
	}

	private void registerInBackground() {
		SmappsScanwords.getExecutorService().execute(new Runnable() {

			@Override
			public void run() {
				Log.v("", "registerInBackground ");
				GoogleCloudMessaging gcm = GoogleCloudMessaging
						.getInstance(MainMenuActivity.this);
				try {
					if (gcm == null) {
						gcm = GoogleCloudMessaging
								.getInstance(MainMenuActivity.this
										.getApplicationContext());
					}
					String regid = gcm.register(SENDER_ID);
					Log.v("", "registerInBackground " + regid);
					sendRegistrationIdToBackend(regid);
					storeRegistrationId(
							MainMenuActivity.this.getApplicationContext(),
							regid);
				} catch (IOException ex) {
					Log.v("",
							"registerInBackground IOException"
									+ ex.getMessage());
				}
			}
		});
	}

	private void sendRegistrationIdToBackend(final String regid) {
		SmappsScanwords.getExecutorService().execute(new Runnable() {
			@Override
			public void run() {
				MainNetworkManager.getInstance().sendPushToken(regid);
			}
		});
	}

	private void storeRegistrationId(Context context, String regId) {
		final SharedPreferences prefs = getGCMPreferences(context);
		int appVersion = getAppVersion(context);
		Log.i("", "Saving regId on app version " + appVersion);
		SharedPreferences.Editor editor = prefs.edit();
		editor.putString(PROPERTY_REG_ID, regId);
		editor.putInt(PROPERTY_APP_VERSION, appVersion);
		editor.commit();
	}

	private boolean checkPlayServices() {
		int resultCode = GooglePlayServicesUtil
				.isGooglePlayServicesAvailable(this);
		if (resultCode != ConnectionResult.SUCCESS) {
			if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
				GooglePlayServicesUtil.getErrorDialog(resultCode, this,
						PLAY_SERVICES_RESOLUTION_REQUEST).show();
			} else {
				Log.i("", "This device is not supported.");
				finish();
			}
			return false;
		}
		return true;
	}

	private String getRegistrationId(Context context) {
		final SharedPreferences prefs = getGCMPreferences(context);
		String registrationId = prefs.getString(PROPERTY_REG_ID, "");
		if (registrationId.isEmpty()) {
			Log.i("", "Registration not found.");
			return "";
		}
		// Check if app was updated; if so, it must clear the registration ID
		// since the existing regID is not guaranteed to work with the new
		// app version.
		int registeredVersion = prefs.getInt(PROPERTY_APP_VERSION,
				Integer.MIN_VALUE);
		int currentVersion = getAppVersion(context);
		if (registeredVersion != currentVersion) {
			Log.i("", "App version changed.");
			return "";
		}
		return registrationId;
	}

	private static int getAppVersion(Context context) {
		try {
			PackageInfo packageInfo = context.getPackageManager()
					.getPackageInfo(context.getPackageName(), 0);
			return packageInfo.versionCode;
		} catch (NameNotFoundException e) {
			// should never happen
			throw new RuntimeException("Could not get package name: " + e);
		}
	}

	private SharedPreferences getGCMPreferences(Context context) {
		// This sample app persists the registration ID in shared preferences,
		// but
		// how you store the regID in your app is up to you.
		return getSharedPreferences(MainMenuActivity.class.getSimpleName(),
				Context.MODE_PRIVATE);
	}

	public void navigateToAppPage(AppPage page) {
		switch (page) {
		case PAGE_SUBSCRIPTION:
			mLeftPanelView.selectItem(1);
			break;
		case PAGE_HINTS:
			mLeftPanelView.selectItem(1);
			break;
		case PAGE_SKANWORDS:
			mLeftPanelView.selectItem(0);
			//mMenuScanwords.selectTab(0);
			break;

		default:
			break;
		}
	}

	public MainMenuActivity() {
		super();
		Log.v("", " MainMenuActivity  --");
		// TODO Auto-generated constructor stub
	}


	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putInt("curMenu", mCurrentTag);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		Log.v("", resultCode + "  onActivityResult   " + requestCode);
		if (requestCode == 99) {
			if (resultCode == 1) {
				MainNetworkManager.getInstance().loginInSocialNetwork(
						SocialNetwork.SOCIAL_NETWORK_FB, this, true);
			} else if (resultCode == 2) {
				MainNetworkManager.getInstance().loginInSocialNetwork(
						SocialNetwork.SOCIAL_NETWORK_OK, this, true);
			} else if (resultCode == 3) {
				MainNetworkManager.getInstance().loginInSocialNetwork(
						SocialNetwork.SOCIAL_NETWORK_VK, this, true);
			} else {
			}
			return;
		} else if (requestCode == 101) {
			AdsManager.getInstance().showInterstitial(this);
		} else if (requestCode == LockedTaskActivity.LOCKED_TASK_ACTIVITY_REQUEST_CODE) {
			if (resultCode == 100) {
				this.navigateToAppPage(AppPage.PAGE_SUBSCRIPTION);
			}

		}
		if (resultCode == 556) {
			AlertDialog.Builder builder = new AlertDialog.Builder(MainMenuActivity.this,
					R.style.Theme_Scanword_style_dialog);
			builder.setMessage("Загруженные файлы для этого выпуска повреждены. Пожалуйста, загрузите выпуск снава.")
					.setPositiveButton("Ок",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {
								}
							}).setTitle("Ошибка");
			AlertDialog dialog = builder.create();
			dialog.show();
		} else if (resultCode == 555) {
			
		}
		InAppManager.getInstance().onActivityResult(requestCode, resultCode,
				data);
		MainNetworkManager.getInstance().onActivityResult(this, requestCode,
				resultCode, data);
	}

	@Override
	protected void onStart() {
		super.onStart();
		if (hasPermissions) {
		/*if (!mGoogleApiClient.isConnected() && !mGoogleApiClient.isConnecting())
			mGoogleApiClient.connect();*/
			updateNotifications(false);
			Intent intent = this.getIntent();
			mReferrer = CustomReferralReceiver.getReferrer(getApplicationContext());
			Log.v("", " mReferrer " + mReferrer);
		}
	}


	@Override
	protected void onResume() {
		InAppManager.getInstance().initStoreManager(instance);
		AdsManager.getInstance().activityResumed(this);
		super.onResume();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		Log.v("", "onDestroy  main activity ");
		UserUpdatesManager.getInstance().saveCurrentData();

        SmappsScanwords.getEventsDispatcher().removeListener(SmappsScanwords.APPLICATION_TIMER_EVENT, mTimerEventListener);
		if (mUserInfoEventListener != null)
			SmappsScanwords.getEventsDispatcher().removeListener(
					MainDataManager.EVENT_USER_INFO_UPDATE,
					mUserInfoEventListener);
		if (mNotificationsEventListener != null)
			SmappsScanwords.getEventsDispatcher().removeListener(
					MainDataManager.EVENT_NOTIFICATIONS_UPDATE,
					mNotificationsEventListener);
		if (mPushTokenEventListener != null)
			SmappsScanwords.getEventsDispatcher().removeListener(
					MainDataManager.EVENT_NO_TOKEN, mPushTokenEventListener);
		if (mNetworkStateChangedEventListener != null)
			SmappsScanwords.getEventsDispatcher().removeListener(
					MainNetworkManager.EVENT_NETWORK_STATE_CHANGED_ALL, mNetworkStateChangedEventListener);
		super.onDestroy();
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		// Sync the toggle state after onRestoreInstanceState has occurred.
		if (hasPermissions) {
			mDrawerToggle.syncState();
		}
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		mDrawerToggle.onConfigurationChanged(newConfig);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Pass the event to ActionBarDrawerToggle, if it returns
		// true, then it has handled the app icon touch event
		Log.v("  ", "menu item " + item.getItemId());
		if (mDrawerToggle.onOptionsItemSelected(item)) {
			return true;
		} else if (item.getItemId() == 5) {
			
			mCurrentFragment.updateFragment(true);
			SmappsScanwords.sendGAEvent(ActionType.USER_ACTION,
					"click_menu_update"); // GA Event

		} else if (item.getItemId() == 6) {
			Log.v("", "open sort dialog  MainMenuActivity");
			this.getMenuScanwords().openSortDialog();
		} else if (item.getItemId() == 7) {
			if (MainNetworkManager.getInstance().hasNetworkConnection(MainMenuActivity.this)) {
				InAppManager.getInstance().restorePurchases(
						MainMenuActivity.instance);
				SmappsScanwords.sendGAEvent(ActionType.USER_ACTION,
						"click_menu_shop_restorepurchases"); // GA Event
			}
		} else if (item.getItemId() == 8) {
			getMenuRating().findUser();
		}

		// Handle your other action bar items...

		return super.onOptionsItemSelected(item);
	}

	public static void openLockedTaskactivity(Activity context,
			Skanword scanword) {
		if (MainDataManager.getInstance().getShopDataManager()
				.getDefaultShopItemOfType(ShopProductType.SUBSCRIPTION) != null
				&& InAppManager
						.getInstance()
						.getPriceForProductId(
								MainDataManager
										.getInstance()
										.getShopDataManager()
										.getDefaultShopItemOfType(
												ShopProductType.SUBSCRIPTION)
										.getId()).equals("")) {
			AlertDialog.Builder builder = new AlertDialog.Builder(context,
					R.style.Theme_Scanword_style_dialog);
			builder.setMessage(
					MainDataManager.getInstance().getSkanwordsDataManager()
							.isSkanwordOffered(scanword.getId()) ? "Активируйте премиум-доступ или откройте этот сканворд за просмотр видео (нужен Интернет)."
							: "Активируйте премиум-доступ в магазине, чтобы играть во все закрытые сканворды.")
					.setPositiveButton("Ок",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {
								}
							}).setTitle("Сканворд закрыт");
			AlertDialog dialog = builder.create();
			dialog.show();
			return;
		}
		Intent intent = new Intent(Intent.ACTION_EDIT, null, context,
				LockedTaskActivity.class);
		intent.putExtra(LockedTaskActivity.EXTRA_TASK_OFFER_TARGET, "c"
				+ scanword.getId());
		intent.putExtra(LockedTaskActivity.EXTRA_TASK_ID, scanword.getId());
		intent.putExtra(LockedTaskActivity.EXTRA_TASK_WITH_OFFER,
				MainDataManager.getInstance().getSkanwordsDataManager()
						.isSkanwordOffered(scanword.getId()));
		context.startActivityForResult(intent,
				LockedTaskActivity.LOCKED_TASK_ACTIVITY_REQUEST_CODE);
		SmappsScanwords.sendGAEvent(ActionType.USER_ACTION,
				"menu_scans_lockedscanwin_enter");
		SmappsScanwords
				.sendGAEvent(
						ActionType.USER_ACTION,
						MainDataManager.getInstance().getSkanwordsDataManager()
								.isSkanwordOffered(scanword.getId()) ? "click_menu_scans_offeredscan_locked"
								: "click_menu_scans_lockedscan"); // GA Event
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		switch (mCurrentTag) {
		case 0:
			menu.add(0, 6, 0, "Сортировка").setIcon(R.drawable.filter)
					.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
			menu.add(0, 5, 0, "Обновить").setIcon(R.drawable.update)
					.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
			break;
		case 1:
			menu.add(0, 7, 0, "ВОССТАНОВИТЬ").setShowAsAction(
					MenuItem.SHOW_AS_ACTION_ALWAYS
							| MenuItem.SHOW_AS_ACTION_WITH_TEXT);
			break;
		case 2:
			menu.add(0, 8, 0, "Найти меня").setIcon(R.drawable.icon_find_me)
					.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
			menu.add(0, 5, 0, "Обновить").setIcon(R.drawable.update)
					.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
			break;
		case 3:
			menu.add(0, 5, 0, "Обновить").setIcon(R.drawable.update)
					.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
			break;
		}
		Log.v("", "onCreateOptionsMenu");
		return true;
	}

	public void openMenu(int menuTag) {
		Log.v("", "open menu with tag " + menuTag);
		
		if (menuTag == mCurrentTag) {
			mCurrentFragment.fragmentOpened();
			mDrawerLayout.closeDrawer(mLeftPanelView);
			return;
		}
		mDrawerLayout.closeDrawer(mLeftPanelView);
		CustomFragment fragment = null;
		String title = "";
		mCurrentTag = menuTag;
		switch (menuTag) {
		case 0:
			fragment = this.getMenuScanwords();
			title = "Сканворды";
			SmappsScanwords.sendGAEvent(ActionType.USER_ACTION,
					"menu_scans_enter");
			break;
		case 1:
			fragment = this.getMenuShop();
			title = "Магазин";
			SmappsScanwords.sendGAEvent(ActionType.USER_ACTION,
					"menu_shop_enter");
			break;
		case 2:
			fragment = this.getMenuRating();
			title = "Рейтинг";
			SmappsScanwords.sendGAEvent(ActionType.USER_ACTION,
					"menu_rating_enter");
			break;
		case 3:
			fragment = this.getMenuNews();
			title = "Новости";
			MainDataManager.getInstance().getNewsDataManager()
					.updateReadedNews();
			SmappsScanwords.sendGAEvent(ActionType.USER_ACTION,
					"menu_news_enter");
			break;

		default:
			break;
		}
		mTitle = mDrawerTitle = title;
		setTitle(title);
		if (fragment == null || fragment.isAdded())
			return;
		Bundle args = new Bundle();
		fragment.setArguments(args);
		mCurrentFragment = fragment;
		fragment.fragmentOpened();
		FragmentManager fragmentManager = getSupportFragmentManager();
		fragmentManager.beginTransaction()
				.replace(R.id.content_frame, mCurrentFragment).commitAllowingStateLoss();
		invalidateOptionsMenu();
	}



	private MenuRating getMenuRating() {
		return mMenuRating;
	}

	private MenuNews getMenuNews() {
		return mMenuNews;
	}

	public MenuSkanwords getMenuScanwords() {
		return mMenuScanwords;
	}

	public MenuShop getMenuShop() {
		return mMenuShop;
	}

	public static enum AppPage {
		PAGE_SUBSCRIPTION, PAGE_HINTS, PAGE_SKANWORDS
	}
}
