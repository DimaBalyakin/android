package org.skanword.and.menu.optionsmenu;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import org.skanword.and.R;
import org.skanword.and.SmappsScanwords;
import org.skanword.and.datamanager.MainDataManager;
import org.skanword.and.menu.ArraySectionedListAdapter;
import org.skanword.and.menu.MainMenuActivity;
import org.skanword.and.network.MainNetworkManager;

import android.app.ActionBar;
import android.app.Activity;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;

import com.musselwhizzle.dispatcher.events.SimpleEvent;
import com.qustom.dialog.QustomDialogBuilder;
import com.sbstrm.appirater.Appirater;

public class OptionsActivity extends FragmentActivity {

	private ListView mList;
	private OptionsItemsAdapter mAdapter;
	private final FragmentActivity mActivity;
	public static final int TYPE_ROW_CHECKBOX_VIEW = ArraySectionedListAdapter.TYPE_ROW_VIEW + 1;
	public static final int TYPE_ROW_RADIO_VIEW = TYPE_ROW_CHECKBOX_VIEW + 1;
	public static final int TYPE_ROW_TUGGLE_VIEW = TYPE_ROW_RADIO_VIEW + 1;

	public OptionsActivity() {
		mActivity = this;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_options);
		ActionBar actionBar = getActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);

		mList = (ListView) findViewById(R.id.options_list);
		mAdapter = new OptionsItemsAdapter(this, R.layout.shop_item_row,
				mActivity);
		mList.setAdapter(mAdapter);
		mList.setOnItemClickListener(new OptionsItemClickListener());
		mList.setScrollBarStyle(ListView.SCROLLBARS_OUTSIDE_OVERLAY);

	}

	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			this.finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public static class OptionsItemsAdapter extends
			ArraySectionedListAdapter<String> {
		private Activity mActivity;
		private Map<View, Integer> radioButtonsGroup = new HashMap<View, Integer>();

		public OptionsItemsAdapter(Context context, int resource,
				Activity activity) {
			super(context, resource);
			mActivity = activity;
		}

		@Override
		public boolean areAllItemsEnabled() {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public boolean isEnabled(int position) {
			return true;
		}

		@Override
		protected int getNumberOfSections() {
			return 4;
		}

		@Override
		protected int getNumberOfRowsInSection(int section) {
			int count = 0;
			switch (section) {
			case 0:
				count = 3;
				break;
			case 1:
				count = 2;
				break;
			case 2:
				count = 4;
				break;
			case 3:
				count = 2;
				break;

			default:
				break;
			}
			return count;
		}

		@Override
		protected int getNumberOfViewTypeForRows() {
			return TYPE_ROW_TUGGLE_VIEW;
		}

		@Override
		protected int getViewTypeForRowInSection(int section, int row) {
			if (row == 0 && (section == 0 || section == 2))
				return TYPE_ROW_TUGGLE_VIEW;
			if (section == 1 || (section == 0 && row != 0)
					|| (section == 2 && row == 3)) {
				return TYPE_ROW_CHECKBOX_VIEW;
			}
			if (section == 2 && (row == 1 || row == 2))
				return TYPE_ROW_RADIO_VIEW;
			return TYPE_ROW_VIEW;
		}

		@Override
		public Object getItemInSectionAndRow(int section, int row) {
			return null;
		}

		@Override
		protected View newSectionView(Context context, int section,
				ViewGroup parent) {
			return ((LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE))
					.inflate(R.layout.options_list_header, parent, false);
		}

		@Override
		protected void bindSectionView(Context context, int section,
				View convertView) {
			if (convertView == null)
				return;
			TextView header = ((TextView) convertView
					.findViewById(R.id.setLabel));
			header.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
				}
			});
			String headerText = "";
			switch (section) {
			case 0:
				headerText = "СКАНВОРД";
				break;
			case 1:
				headerText = "ПОДКЛЮЧЕНИЕ";
				break;
			case 2:
				headerText = "УВЕДОМЛЕНИЕ О НОВЫХ ВЫПУСКАХ";
				break;
			case 3:
				headerText = "ИНФОРМАЦИЯ";
				break;

			default:
				break;
			}
			header.setText(headerText);
		}

		@Override
		protected View newRowViewInSection(Context context, int section,
				int row, ViewGroup parent) {
			LayoutInflater inflater = LayoutInflater.from(getContext());
			View view = null;

			if (row == 0 && (section == 0 || section == 2))
				view = inflater.inflate(R.layout.options_tuggle_item_row,
						parent, false);
			else if (section == 1 || (section == 0 && row != 0)
					|| (section == 2 && row == 3)) {
				view = inflater.inflate(R.layout.options_checkbox_item_row,
						parent, false);
			} else if (section == 2 && (row == 1 || row == 2)) {
				view = inflater.inflate(R.layout.options_radiobtn_item_row,
						parent, false);
			} else
				view = inflater.inflate(R.layout.options_simple_item_row,
						parent, false);
			return view;
		}

		@Override
		protected void bindRowViewInSection(Context context, final int section,
				final int row, View convertView) {
			TextView name = (TextView) convertView
					.findViewById(R.id.optionName);
			switch (section) {
			case 0:
				bindScanwordSection(convertView, row);
				break;
			case 1:
				bindConnectionSection(convertView, row);
				break;
			case 2:
				bindInformSection(convertView, row);
				break;
			case 3:
				bindInfoSection(convertView, row);
				break;

			default:
				name.setText("unknown");
				break;
			}
			final Switch switchbutton = (Switch) convertView
					.findViewById(R.id.switchButton1);
			if (switchbutton != null) {
				switchbutton.setOnCheckedChangeListener(new OnCheckedChangeListener() {
					
					@Override
					public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
						optionClicked(section, row, switchbutton.isChecked());
					}
				});
			}
			final CheckBox checkBox = (CheckBox) convertView
					.findViewById(R.id.checkBox1);
			if (checkBox != null) {
				checkBox.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						optionClicked(section, row, checkBox.isChecked());
					}
				});
			}

		}

		private void bindInformSection(View convertView, final int row) {
			TextView name = (TextView) convertView
					.findViewById(R.id.optionName);
			final RadioButton radioButton = (RadioButton) convertView
					.findViewById(R.id.radioButton1);
			if (radioButton != null) {
				radioButtonsGroup.put(radioButton, 1);
				if ((MainDataManager.getInstance().getOptions()
						.isDefaultNotificationsTime() && row == 1)
						|| (!MainDataManager.getInstance().getOptions()
								.isDefaultNotificationsTime() && row == 2)) {
					radioButton.setChecked(true);
				}
				radioButton.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						onRadioButtonClicked(radioButton, row);
					}
				});
			}
			if (row == 0) {
				name.setText("Уведомлять о новых выпусках");
				Switch switchbutton = (Switch) convertView
						.findViewById(R.id.switchButton1);
				switchbutton.setChecked(MainDataManager.getInstance()
						.getOptions().isInformNewIssues());
			} else if (row == 1) {
				name.setText("Сразу");
				final TextView timeLabel = (TextView) convertView
						.findViewById(R.id.timeLabel);
				timeLabel.setVisibility(View.GONE);
				convertView.findViewById(R.id.defaultTimeLabel).setVisibility(
						View.VISIBLE);
				name.setGravity(Gravity.BOTTOM);

			} else if (row == 2) {
				name.setText("Задать вручную");
				final TextView timeLabel = (TextView) convertView
						.findViewById(R.id.timeLabel);
				timeLabel.setVisibility(View.VISIBLE);
				name.setGravity(Gravity.CENTER_VERTICAL);
				convertView.findViewById(R.id.defaultTimeLabel).setVisibility(
						View.GONE);
				timeLabel.setPaintFlags(timeLabel.getPaintFlags()
						| Paint.UNDERLINE_TEXT_FLAG);
				timeLabel
						.setText(String.format("%02d", MainDataManager
								.getInstance().getOptions()
								.getCustomNotificationTimeHour())
								+ ":"
								+ String.format("%02d", MainDataManager
										.getInstance().getOptions()
										.getCustomNotificationTimeMinute()));
				timeLabel.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						String[] parts = ((String) timeLabel.getText())
								.split(":");
						int hour = Integer.parseInt(parts[0]);
						int minute = Integer.parseInt(parts[1]);
						TimePickerDialog mTimePicker;
						mTimePicker = new TimePickerDialog(mActivity,
								new TimePickerDialog.OnTimeSetListener() {
									@Override
									public void onTimeSet(
											TimePicker timePicker,
											int selectedHour, int selectedMinute) {
										timeLabel.setText(String.format("%02d",
												selectedHour)
												+ ":"
												+ String.format("%02d",
														selectedMinute));
										MainDataManager
												.getInstance()
												.getOptions()
												.setCustomNotificationTimeHour(
														selectedHour);
										MainDataManager
												.getInstance()
												.getOptions()
												.setCustomNotificationTimeMinute(
														selectedMinute);
										MainDataManager.getInstance()
												.storeOptions();
										if (MainMenuActivity.instance != null) {
											MainMenuActivity.instance
													.updateNotifications(true);
										}
									}
								}, hour, minute, true);
						mTimePicker.setTitle("Select Time");
						mTimePicker.show();

					}
				});
			} else {
				CheckBox checkbox = (CheckBox) convertView
						.findViewById(R.id.checkBox1);
				checkbox.setChecked(MainDataManager.getInstance().getOptions()
						.isDisabledNotificationsSounds());
				name.setText("Без звука");
			}
		}

		private void bindConnectionSection(View convertView, int row) {
			TextView name = (TextView) convertView
					.findViewById(R.id.optionName);
			CheckBox checkbox = (CheckBox) convertView
					.findViewById(R.id.checkBox1);
			if (row == 0) {
				name.setText("Загружать свежие выпуски автоматически");
				checkbox.setChecked(MainDataManager.getInstance().getOptions()
						.isAutoDownload());
			} else {
				name.setText("Использовать только WiFi");
				checkbox.setChecked(MainDataManager.getInstance().getOptions()
						.isOnlyWifi());
			}
		}

		private void bindScanwordSection(View convertView, int row) {
			TextView name = (TextView) convertView
					.findViewById(R.id.optionName);
			CheckBox checkbox = (CheckBox) convertView
					.findViewById(R.id.checkBox1);
			if (row == 0) {
				name.setText("Звуки");
				Switch switchbutton = (Switch) convertView
						.findViewById(R.id.switchButton1);
				switchbutton.setChecked(MainDataManager.getInstance()
						.getOptions().isSounds());
			} else if (row == 1) {
				name.setText("Пропускать разгаданные буквы при вводе слова");
				checkbox.setChecked(MainDataManager.getInstance().getOptions()
						.isSkipGuessedLetters());
			} else if (row == 2) {
				name.setText("Увеличить макс. масштаб");
				checkbox.setChecked(MainDataManager.getInstance().getOptions()
						.isIncreasedRatio());
			}
		}

		private void bindInfoSection(View convertView, int row) {
			TextView name = (TextView) convertView
					.findViewById(R.id.optionName);
			if (row == 0) {
				name.setText("О программе");
			} else {
				name.setText("Оставить отзыв на Google Play");
			}
		}

		public void onRadioButtonClicked(RadioButton button, int row) {
			Iterator<Entry<View, Integer>> it = radioButtonsGroup.entrySet()
					.iterator();
			while (it.hasNext()) {
				Entry<View, Integer> entry = it.next();
				RadioButton mappedButton = (RadioButton) entry.getKey();
				mappedButton.setChecked(mappedButton == button);
			}
			MainDataManager.getInstance().getOptions()
					.setDefaultNotificationsTime(row == 1);
			MainDataManager.getInstance().storeOptions();
			if (MainMenuActivity.instance != null) {
				MainMenuActivity.instance.updateNotifications(true);
			}
		}

		private void optionClicked(int section, int row, boolean enabled) {
			if (section == 0) {
				if (row == 0) {
					MainDataManager.getInstance().getOptions()
							.setSounds(enabled);
				} else if (row == 1) {
					MainDataManager.getInstance().getOptions()
							.setSkipGuessedLetters(enabled);
				} else {
					MainDataManager.getInstance().getOptions()
							.setIncreasedRatio(enabled);
				}
			} else if (section == 1) {
				if (row == 0) {
					MainDataManager.getInstance().getOptions()
							.setAutoDownload(enabled);
					SmappsScanwords
							.getEventsDispatcher()
							.dispatchEvent(
									new SimpleEvent(
											MainDataManager.EVENT_DOWNLOAD_TODAY_ISSUE));
				} else {
					MainDataManager.getInstance().getOptions()
							.setOnlyWifi(enabled);
				}
			} else if (section == 2) {
				if (row == 0) {
					MainDataManager.getInstance().getOptions()
							.setInformNewIssues(enabled);
				} else {
					MainDataManager.getInstance().getOptions()
							.setDisabledNotificationsSounds(enabled);
				}
				if (MainMenuActivity.instance != null) {
					MainMenuActivity.instance.updateNotifications(true);
				}
			}
			MainDataManager.getInstance().storeOptions();
		}
	}

	private class OptionsItemClickListener implements
			ListView.OnItemClickListener {
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			int row = mAdapter.getRowForPosition(position);
			int section = mAdapter.getSectionForPosition(position);
			if (section == 3) {
				if (row == 0) {
					LayoutInflater li = LayoutInflater.from(mActivity);
					View dialogView = li.inflate(R.layout.info_dialog_layout,
							null);
					QustomDialogBuilder qustomDialogBuilder = new QustomDialogBuilder(
							mActivity, R.style.Theme_Scanword_style_dialog);
					qustomDialogBuilder.setPositiveButton("Oк",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {

								}
							}).setNegativeButton("Условия",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {
									Intent browse = new Intent(
											Intent.ACTION_VIEW,
											Uri.parse("http://skanword.org/privacy"));

									startActivity(browse);
								}
							});
					TextView versionLabel = (TextView)dialogView.findViewById(R.id.versionLabel);
					if (versionLabel != null) {
						try {
							PackageInfo pInfo = SmappsScanwords.getContext().getPackageManager().getPackageInfo(getPackageName(), 0);
							String version = pInfo.versionName;
							int versionCode = pInfo.versionCode;
							versionLabel.setText("Версия: " + version+ " (" +versionCode +")" + (MainNetworkManager.isRelese() ? "p": "t"));
						} catch (NameNotFoundException e) {
							e.printStackTrace();
						}
					}
					TextView userIDLabel = (TextView)dialogView.findViewById(R.id.userIDLabel);
					userIDLabel.setText("Пользователь: " + (MainNetworkManager.getInstance().getUserAuth() != null ? MainNetworkManager.getInstance().getUserAuth().getUserId() : "Неизвестно"));
					qustomDialogBuilder.setCustomView(dialogView);
					qustomDialogBuilder.show();
				} else {
					Appirater.rateApp(mActivity);
				}
			}
		}
	}
}
