package org.skanword.and.menu;

import java.util.ArrayList;
import java.util.List;

import org.skanword.and.R;
import org.skanword.and.SmappsScanwords;
import org.skanword.and.SmappsScanwords.ActionType;
import org.skanword.and.datamanager.MainDataManager;
import org.skanword.and.etc.Utils;
import org.skanword.and.menu.MainMenuActivity.AppPage;
import org.skanword.and.menu.optionsmenu.OptionsActivity;
import org.skanword.and.network.MainNetworkManager;
import org.skanword.and.network.socialnetworks.SocialNetworkManager.SocialNetwork;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.net.Uri;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

public class LeftPanel extends LinearLayout {

	private ListView mOptionsList;
	private MainMenuActivity mController;
	private PanelListAdapter mAdapter;
	private View mMainView;
	
	
	public LeftPanel(Context context) {
		super(context);
        initView();
		// TODO Auto-generated constructor stub
	}
	
	

	public LeftPanel(Context context, AttributeSet attrs) {
		super(context, attrs);
        initView();
		// TODO Auto-generated constructor stub
	}
	
	
	public MainMenuActivity getController() {
		return mController;
	}

	public void setController(MainMenuActivity mController) {
		this.mController = mController;
	}

	@SuppressLint("NewApi")
	public LeftPanel(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
        initView();
		// TODO Auto-generated constructor stub
	}
    private void initView() {
    	mMainView = LayoutInflater.from(
                this.getContext().getApplicationContext()).inflate(
                R.layout.left_panel_view_layout, null);
    	mMainView.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
    	setClickable(true);
    	View premiumButton = mMainView.findViewById(R.id.panelPremiumButton);
    	premiumButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				mController.navigateToAppPage(AppPage.PAGE_SUBSCRIPTION);
				SmappsScanwords.sendGAEvent(ActionType.USER_ACTION,	"click_menu_leftpanel_subscription"); // GA Event
			}
		});
    	View hintsButton = mMainView.findViewById(R.id.panelHintsButton);
    	hintsButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				mController.navigateToAppPage(AppPage.PAGE_HINTS);
				SmappsScanwords.sendGAEvent(ActionType.USER_ACTION,	"click_menu_leftpanel_hints"); // GA Event

			}
		});
    	
    	View.OnClickListener authListener = new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (v.getId() == R.id.authFBButton) {
					MainNetworkManager.getInstance().loginInSocialNetwork(SocialNetwork.SOCIAL_NETWORK_FB, mController, true);
				} else if (v.getId() == R.id.authOKButton) {
					MainNetworkManager.getInstance().loginInSocialNetwork(SocialNetwork.SOCIAL_NETWORK_OK, mController, true);
				} else if (v.getId() == R.id.authVKButton) {
					MainNetworkManager.getInstance().loginInSocialNetwork(SocialNetwork.SOCIAL_NETWORK_VK, mController, true);
				} else if (v.getId() == R.id.exitIcon) {
					MainNetworkManager.getInstance().loginInSocialNetwork(SocialNetwork.SOCIAL_NETWORK_NO, mController, true);
				}
			}
		};
		ImageView authButton = (ImageView) mMainView.findViewById(R.id.authOKButton);
		authButton.setOnClickListener(authListener);
		authButton = (ImageView) mMainView.findViewById(R.id.authFBButton);
		authButton.setOnClickListener(authListener);
		authButton = (ImageView) mMainView.findViewById(R.id.authVKButton);
		authButton.setOnClickListener(authListener);
		authButton = (ImageView) mMainView.findViewById(R.id.exitIcon);
		authButton.setOnClickListener(authListener);
    	
        addView(mMainView);

	    List<String> optionsList = new ArrayList<String>();
	    optionsList.add("Сканворды");
	    optionsList.add("Магазин");
	    optionsList.add("Рейтинг");
	    optionsList.add("Новости");
	    optionsList.add("НАСТРОЙКИ");
	    optionsList.add("ОБРАТНАЯ СВЯЗЬ");

        mAdapter = new PanelListAdapter(this.getContext(), R.layout.left_panel_item_layout, optionsList);
        mOptionsList = (ListView) findViewById(R.id.options_list);
        mOptionsList.setAdapter(mAdapter);
        mOptionsList.setOnItemClickListener(new DrawerItemClickListener());
        
        updateUserInfo();
    }
    public void updateUserInfo() {
    	if (MainDataManager.getInstance().getUserData() == null)
    		return;
    	int hintsCount = MainDataManager.getInstance().getUserData().getHints();
    	TextView hintsLabel = (TextView) mMainView.findViewById(R.id.panelHintsLabel);
    	TextView hintsCountLabel = (TextView) mMainView.findViewById(R.id.panelHintsCountLabel);
    	hintsLabel.setText(Utils.spellVariantForNumber(hintsCount, "ПОДСКАЗКА", "ПОДСКАЗКИ", "ПОДСКАЗОК"));
    	hintsCountLabel.setText(""+hintsCount);
    	
    	mMainView.findViewById(R.id.authorisePanel).setVisibility(!MainNetworkManager.getInstance().isUserAuthorised() ? View.VISIBLE : View.GONE);
    	mMainView.findViewById(R.id.userPanel).setVisibility(MainNetworkManager.getInstance().isUserAuthorised() ? View.VISIBLE : View.GONE);
    	
    	if (!MainNetworkManager.getInstance().isUserAuthorised()) {
    		Resources res = getResources();
        	TextView guestNumberLabel = (TextView) mMainView.findViewById(R.id.guestNumberLabel);
        	guestNumberLabel.setText(res.getString(R.string.guest_label) + MainNetworkManager.getInstance().getUserName());
    	} else {
    		ImageView snIcon = (ImageView) mMainView.findViewById(R.id.socialNetworkIcon);
    		switch (MainNetworkManager.getInstance().getUserAuth().getSocialNetwork()) { 
			case SOCIAL_NETWORK_FB:
				snIcon.setImageResource(R.drawable.icon_mini_fb);
				break;
			case SOCIAL_NETWORK_OK: 
				snIcon.setImageResource(R.drawable.icon_mini_ok);
				break;
			case SOCIAL_NETWORK_VK:
				snIcon.setImageResource(R.drawable.icon_mini_vk);
				break;

			default:
				break;
			}
    		if (MainNetworkManager.getInstance().getUserAuth().getAvatarUrl() != null) {
    			DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
    					.showImageForEmptyUri(R.drawable.avatar)
    					.showImageOnFail(R.drawable.avatar).build();
    			ImageLoader.getInstance().displayImage(MainNetworkManager.getInstance().getUserAuth().getAvatarUrl(), (ImageView) findViewById(R.id.profile_image), options);
    		} else {
    			((ImageView) findViewById(R.id.profile_image)).setImageResource(R.drawable.avatar);
    		}
        	TextView userName = (TextView) mMainView.findViewById(R.id.userNameLabel);
        	userName.setText(MainNetworkManager.getInstance().getUserAuth().getFullName());
        	
    	}
    	
    }
    public static class PanelListAdapter extends ArrayAdapter<String> {
    	
    	private int selectedItem;
    	private int mResourceId;
    	
		public PanelListAdapter(Context context, int textViewResourceId,
				List<String> objects) {
			super(context, textViewResourceId, objects);
			mResourceId = textViewResourceId;
		}
		
		public void setSelectedItem(int selectedItem) {
			this.selectedItem = selectedItem;
		}
		

		@Override
		public int getItemViewType(int position) {
			// TODO Auto-generated method stub
			return position > 3 ? 1 : 0;
		}

		@Override
		public int getViewTypeCount() {
			// TODO Auto-generated method stub
			return 2;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			if (convertView == null) {
				convertView = createRowView(parent, getItemViewType(position));
			}
			rebuildView(convertView, getItem(position), position);
			return convertView;
		}
		private void rebuildView(View view, String name, int position) {
			ImageView icon = (ImageView) view.findViewById(R.id.menuIcon);
			View devider = (View) view.findViewById(R.id.listDevider);
			devider.setVisibility(position < 3 ? View.GONE : View.VISIBLE);
			switch (position) {
			case 0:
				icon.setBackgroundResource(R.drawable.icon_scanwords_issue);
				break;
			case 1:
				icon.setBackgroundResource(R.drawable.icon_menu_shop);
				break;
			case 2:
				icon.setBackgroundResource(R.drawable.icon_menu_rating);
				break;
			case 3:
				icon.setBackgroundResource(R.drawable.icon_menu_news);
				break;
			case 4:
				icon.setBackgroundResource(R.drawable.icon_menu_setting);
				break;
			case 5:
				icon.setBackgroundResource(R.drawable.icon_menu_mail);
				break;

			default:
				break;
			}
			TextView menuName = (TextView) view.findViewById(R.id.menuName);
			menuName.setText(name);
			int markerCount = (position == 3 ? MainDataManager.getInstance().getNewsDataManager().getNotReadNewsCount() : 0);
		    TextView marker = (TextView) view.findViewById(R.id.markerCount);
		    View markerView = view.findViewById(R.id.markerView);
		    if (markerView != null)
		    	markerView.setVisibility(markerCount > 0 ? View.VISIBLE : View.GONE);
		    if (marker != null)
		    	marker.setText(""+markerCount);
			if (position == selectedItem) {
				menuName.setTypeface(null, Typeface.BOLD);
			} else {
				menuName.setTypeface(null, Typeface.NORMAL);
			}
		}
		private View createRowView(ViewGroup parent, int type) {
			LayoutInflater inflater = LayoutInflater.from(getContext());			 
			final View view = inflater.inflate(type == 0 ? mResourceId : R.layout.left_panel_additional_item_layout, parent, false);
			return view;
		}
    }

	private class DrawerItemClickListener implements ListView.OnItemClickListener {
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        	if (position == 5) {
        		String subject = "Сканворд дня (Android) " + "["+ (MainNetworkManager.getInstance().getUserAuth() != null ? MainNetworkManager.getInstance().getUserAuth().getUserId() : "(Неизвестно)")
        				+"|"+ SmappsScanwords.getVendorId() +"]";
        		Intent intent=new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
        	            "mailto","support@socialgames.bz", null));
        		intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        		getController().startActivity(Intent.createChooser(intent, "Send mail"));
        	} else if (position == 4) {
				Intent intent = new Intent(Intent.ACTION_EDIT, null, getController(), OptionsActivity.class);
				getController().startActivity(intent);
        	} else {
                selectItem(position);
        	}
        		
        }
    }

    public void selectItem(int position) {
    	mAdapter.setSelectedItem(position);
    	if (mController != null) {
    		Log.v("", "selectItem "+ position);
    		mController.openMenu(position);
    	}
    	mAdapter.notifyDataSetChanged();
    }
	
}
