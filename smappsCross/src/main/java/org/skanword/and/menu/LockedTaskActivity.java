package org.skanword.and.menu;

import org.skanword.and.R;
import org.skanword.and.SmappsScanwords;
import org.skanword.and.SmappsScanwords.ActionType;
import org.skanword.and.datamanager.MainDataManager;
import org.skanword.and.datamanager.ShopDataManager.ShopProductType;
import org.skanword.and.etc.AdsManager;
import org.skanword.and.etc.AdsManager.AdsEventsListener;
import org.skanword.and.etc.TopActivityManager.TopActivity;
import org.skanword.and.etc.Utils;
import org.skanword.and.inappbilling.InAppManager;
import org.skanword.and.inappbilling.InAppManager.PurchaseSource;
import org.skanword.and.network.MainNetworkManager;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.Display;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;

public class LockedTaskActivity extends Activity implements TopActivity {

	public static final String EXTRA_TASK_WITH_OFFER = "EXTRA_TASK_WITH_OFFER";
	public static final String EXTRA_TASK_OFFER_TARGET = "EXTRA_TASK_OFFER_TARGET";
	public static final String EXTRA_TASK_ID = "EXTRA_TASK_ID";
	public static final int LOCKED_TASK_ACTIVITY_REQUEST_CODE = 105;

	private boolean mOfferEnabled = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.locked_task_activity_layout);
		ActionBar actionBar = getActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);

		Bundle extras = getIntent().getExtras();
		mOfferEnabled = extras.getBoolean(EXTRA_TASK_WITH_OFFER);
		final String offerTarget = extras.getString(EXTRA_TASK_OFFER_TARGET);
		final Integer taskId = extras.getInt(EXTRA_TASK_ID);
		if (mOfferEnabled) {
			((Button) findViewById(R.id.openButton))
					.setVisibility(View.INVISIBLE);
		} else {
			((Button) findViewById(R.id.openOfferButton))
					.setVisibility(View.INVISIBLE);
		}

		((Button) findViewById(R.id.openOfferButton))
				.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						SmappsScanwords.sendGAEvent(ActionType.USER_ACTION,
								"click_menu_scans_lockedscanwin_offerbtn"); // GA
																			// Event
						AdsManager.getInstance().showVideoOffer(
								LockedTaskActivity.this,
								new AdsEventsListener() {
									@Override
									public void onComplete() {
										super.onComplete();
										MainDataManager.getInstance()
												.getSkanwordsDataManager()
												.openScanwordForOffer(taskId);
										MainNetworkManager.getInstance().requestSyncExec();
										finish();
									}

									@Override
									public void onNoAds() {
										super.onNoAds();
										AlertDialog.Builder builder = new AlertDialog.Builder(
												LockedTaskActivity.this,
												R.style.Theme_Scanword_style_dialog);
										builder.setMessage(
												"Извините, сейчас у нас нет предложений для вас. Пожалуйста, попробуйте позже.")
												.setPositiveButton(
														"Ок",
														new DialogInterface.OnClickListener() {
															public void onClick(
																	DialogInterface dialog,
																	int id) {

															}
														});
										AlertDialog dialog = builder.create();
										dialog.show();
									}
								}, offerTarget, false);
					}
				});
		((Button) findViewById(R.id.activatePremiumButton))
				.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						activatePremium();
					}
				});

		((Button) findViewById(R.id.openButton))
				.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						playFree();
					}
				});

		setTitle("Сканворд закрыт");

		TextView bonusLabel = (TextView) findViewById(R.id.bonusLabel);
		bonusLabel.setText(Html.fromHtml("<font size=\"30\"><b>"
				+ MainDataManager.getInstance().getShopDataManager()
						.getDefaultShopItemOfType(ShopProductType.SUBSCRIPTION)
						.getVipScanwordsNew()
				+ "</b></font> "
				+ Utils.spellVariantForNumber(
						MainDataManager
								.getInstance()
								.getShopDataManager()
								.getDefaultShopItemOfType(
										ShopProductType.SUBSCRIPTION)
								.getVipScanwordsNew(), "новый", "новых",
						"новых")
				+ " "
				+ Utils.spellVariantForNumber(
						MainDataManager
								.getInstance()
								.getShopDataManager()
								.getDefaultShopItemOfType(
										ShopProductType.SUBSCRIPTION)
								.getVipScanwordsNew(), "сканворд", "сканворда",
						"сканвордов")
				+ " и <font size=\"30\"><b>"
				+ MainDataManager.getInstance().getShopDataManager()
						.getDefaultShopItemOfType(ShopProductType.SUBSCRIPTION)
						.getVipScanwordsArchive()
				+ "</b></font> "
				+ Utils.spellVariantForNumber(
						MainDataManager
								.getInstance()
								.getShopDataManager()
								.getDefaultShopItemOfType(
										ShopProductType.SUBSCRIPTION)
								.getVipScanwordsArchive(), "сканворд",
						"сканворда", "сканвордов")
				+ " из архива<br><br>Бонус - отключение рекламы!"));
		TextView priceLabel = (TextView) findViewById(R.id.priceLabel);
		priceLabel
				.setText(Html.fromHtml("Доступ ко всем<br>сканвордам всего:"));
		((TextView) findViewById(R.id.priceCountLabel)).setText(Html
				.fromHtml("<b>"
						+ InAppManager.getInstance().getPriceForProductId(
								MainDataManager
										.getInstance()
										.getShopDataManager()
										.getDefaultShopItemOfType(
												ShopProductType.SUBSCRIPTION)
										.getId()) + "</b>"));
		((TextView) findViewById(R.id.durationLabel)).setText(Html
				.fromHtml("<b>за "
						+ MainDataManager
								.getInstance()
								.getShopDataManager()
								.getDefaultShopItemOfType(
										ShopProductType.SUBSCRIPTION).getName()
						+ "</b>"));
	}

	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		super.onWindowFocusChanged(hasFocus);
		if (hasFocus) {
			WindowManager wm = (WindowManager) this
					.getSystemService(Context.WINDOW_SERVICE);
			Display display = wm.getDefaultDisplay();
			Point size = new Point();
			display.getSize(size);

			/**/

			/*
			 * TextView additionLabel = (TextView)
			 * findViewById(R.id.additionLabel); params = (LayoutParams)
			 * additionLabel.getLayoutParams(); params.height = (int)(size.y *
			 * 0.5f) - findViewById(R.id.activatePremiumButton).getBottom();
			 * additionLabel.setLayoutParams(params);
			 */

			LinearLayout priceLayout = (LinearLayout) findViewById(R.id.priceLayout);
			LayoutParams params = (LayoutParams) priceLayout.getLayoutParams();
			params.height = (int) (size.y * 0.49f);
			priceLayout.setLayoutParams(params);

			TextView bonusLabel = (TextView) findViewById(R.id.bonusLabel);
			params = (LayoutParams) bonusLabel.getLayoutParams();
			params.height = findViewById(R.id.openButton).getTop()
					- (int) (size.y * 0.51f);
			bonusLabel.setLayoutParams(params);

			Log.v("", findViewById(R.id.openButton).getTop()
					+ " params.height " + params.height);
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// this.render();
		Log.v("", requestCode + " ---------onActivityResult---------  "
				+ requestCode);
		if (requestCode == 99 && resultCode != RESULT_CANCELED) {
			setResult(resultCode);
			this.finish();
		}
		InAppManager.getInstance().onActivityResult(requestCode, resultCode,
				data);
	}

	private void activatePremium() {
		InAppManager.getInstance().purchaseProduct(
				MainDataManager.getInstance().getShopDataManager()
						.getDefaultShopItemOfType(ShopProductType.SUBSCRIPTION)
						.getId(), this, PurchaseSource.LOCKED_TASK);
		SmappsScanwords.sendGAEvent(ActionType.USER_ACTION,
				"click_menu_scans_lockedscanwin_vipactivate"); // GA Event
	}

	private void playFree() {
		Log.v("", " ---------playFree---------  ");
		SmappsScanwords.sendGAEvent(ActionType.USER_ACTION,
				"click_menu_scans_lockedscanwin_iwantfree"); // GA Event
		SmappsScanwords.sendGAEvent(ActionType.USER_ACTION,
				"menu_scans_playfreewin_enter");
		Intent intent = new Intent(Intent.ACTION_EDIT, null, this,
				FreePlayActivity.class);
		this.startActivityForResult(intent, 99);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			this.finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
