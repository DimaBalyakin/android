package org.skanword.and.menu;

import org.skanword.and.R;
import org.skanword.and.SmappsScanwords;
import org.skanword.and.SmappsScanwords.ActionType;
import org.skanword.and.etc.Utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.qustom.dialog.QustomDialogBuilder;

public class HintForVideoDialog extends QustomDialogBuilder {

	private static AlertDialog mDialog = null;
    private static HintForVideoDialogDelegate mDelegate;


	public HintForVideoDialog(Context context, int theme) {
		super(context, theme);
	}

	public HintForVideoDialog(Context context) {
        super(context);
	}
	
	@Override
	protected int getLayoutresId() {
    	layoutResId = R.layout.custom_dialog_layout;
		return super.getLayoutresId();
	} 
	
	private View mDialogView = null;
	
	public void setDialog(AlertDialog dialog) {
		mDialog = dialog;
	}
	
	public static AlertDialog getDialog() {
		return mDialog;
	}
	
	public View getDialogView() {
		return mDialogView;
	}

	public void setDialogView(View mDialog) {
		this.mDialogView = mDialog;
	}

	public static boolean isShowing() {
		return getDialog() != null && getDialog().isShowing();
	}

	public static void showsDialogInContext(Context context, HintForVideoDialogDelegate delegate) {
		LayoutInflater li = LayoutInflater.from(context);
		View view = li.inflate(R.layout.hint_for_video_dialog_view, null);
        mDelegate = delegate;
        final HintForVideoDialog qustomDialogBuilder = new HintForVideoDialog(context, R.style.Theme_Scanword_style_dialog);
        qustomDialogBuilder.setCustomView(view);
        final AlertDialog dialog = qustomDialogBuilder.show();
        qustomDialogBuilder.setDialogView(view);
        qustomDialogBuilder.setDialog(dialog); 
		SmappsScanwords.sendGAEvent(ActionType.APP_ACTION, "hfv_dialog_show"); // GA Event
 
		view.findViewById(R.id.closeButton).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (mDelegate != null)
					mDelegate.onCancel();
				mDialog = null;
				mDelegate = null;
				dialog.cancel();
				SmappsScanwords.sendGAEvent(ActionType.APP_ACTION, "hfv_dialog_close"); // GA Event
			}
		});
		view.findViewById(R.id.watchButton).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (mDelegate != null)
					mDelegate.onWatch();
				mDialog = null;
				mDelegate = null;
				dialog.cancel();
				SmappsScanwords.sendGAEvent(ActionType.APP_ACTION, "hfv_dialog_watch"); // GA Event
			}
		});
		dialog.setOnCancelListener(new OnCancelListener() {
			
			@Override
			public void onCancel(DialogInterface dialog) {
				if (mDelegate != null)
					mDelegate.onCancel();
				mDialog = null;
				mDelegate = null;
				SmappsScanwords.sendGAEvent(ActionType.APP_ACTION, "hfv_dialog_canceled"); // GA Event
			}
		});
        
	}

	public static void showCompleteWatchDialogInContext(Context context) {
		LayoutInflater li = LayoutInflater.from(context);
		View view = li.inflate(R.layout.bonus_complete_dialog_view, null);
        
        final HintForVideoDialog qustomDialogBuilder = new HintForVideoDialog(context, R.style.Theme_Scanword_style_dialog);
        qustomDialogBuilder.setCustomView(view);
        final AlertDialog dialog = qustomDialogBuilder.show();
        qustomDialogBuilder.setDialogView(view);
        qustomDialogBuilder.setDialog(dialog);
        
        
        TextView headLabel = (TextView) view.findViewById(R.id.headLabel);
        int hintsCount = 1;
    	headLabel.setText("Поздравляем!");
    	view.findViewById(R.id.simpleHintsCountsArea).setVisibility(View.VISIBLE);
    	((TextView)view.findViewById(R.id.hintsReceivedLabel)).setText("Вам начислено " + hintsCount 
				+ " " + Utils.spellVariantForNumber(hintsCount, "подсказку", "подсказки", "подсказок"));
        
        
    	view.findViewById(R.id.dev1).setVisibility(View.GONE);
    	view.findViewById(R.id.notificationArea).setVisibility(View.GONE);

		view.findViewById(R.id.closeButton).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				mDialog = null;
				dialog.cancel();
			}
		});
        
	}
    public interface HintForVideoDialogDelegate {
    	void onCancel();
    	void onWatch();
    }
}
