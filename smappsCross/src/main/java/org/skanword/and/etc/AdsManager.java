package org.skanword.and.etc;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.skanword.and.SmappsScanwords;
import org.skanword.and.SmappsScanwords.ActionType;
import org.skanword.and.datamanager.MainDataManager;
import org.skanword.and.datamanager.UserUpdatesManager;
import org.skanword.and.network.ConfigObject.AppAds.AppAdsObject;
import org.skanword.and.network.MainNetworkManager;

import android.app.Activity;
import android.app.Application.ActivityLifecycleCallbacks;
import android.os.Bundle;
import android.util.Log;

import com.chartboost.sdk.CBLocation;
import com.chartboost.sdk.Chartboost;
import com.chartboost.sdk.ChartboostDelegate;
import com.chartboost.sdk.Model.CBError.CBImpressionError;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.jirbo.adcolony.AdColony;
import com.jirbo.adcolony.AdColonyAd;
import com.jirbo.adcolony.AdColonyAdAvailabilityListener;
import com.jirbo.adcolony.AdColonyAdListener;
import com.jirbo.adcolony.AdColonyV4VCAd;
import com.jirbo.adcolony.AdColonyV4VCListener;
import com.jirbo.adcolony.AdColonyV4VCReward;
import com.jirbo.adcolony.AdColonyVideoAd;
import com.unity3d.ads.android.IUnityAdsListener;
import com.unity3d.ads.android.UnityAds;
import com.vungle.publisher.AdConfig;
import com.vungle.publisher.EventListener;
import com.vungle.publisher.Orientation;
import com.vungle.publisher.VunglePub;

public class AdsManager {

	// Info bit masks
	public static final int ADS_VIDEO = 1 << 0; // 0x01
	public static final int ADS_INTERSTITIAL = 1 << 1; // 0x02
	public static final int ADS_REWARDED_VIDEO = 1 << 2; // 0x04
	public static final int ADS_VIDEO_INTERSTITIAL = 1 << 3; // 0x08

	public static final String CHARTBOOST_APP_ID = "544f6552c909a61dd07e26b1";
	public static final String CHARTBOOST_APP_SIGNATURE = "66497a33c1f9cc5e6e30b24a1da5b00413dc1646";
	public static final String CHARTBOOST_APP_ID_TEST = "55a60c09c909a651b1382a16";
	public static final String CHARTBOOST_APP_SIGNATURE_TEST = "b2e65e64a989e6f83ae1c16b1e6da29b7ed65c89";

	public static final String ADCOLONY_APP_ID = "app6c9183410a6348c38c";
	public static final String ADCOLONY_SKANWORDS_ZONE_ID = "vz570e63ec9adf494fa7";
	public static final String ADCOLONY_HINTS_ZONE_ID = "vz8fb15c012ffc4708ad";

	public static final String ADCOLONY_APP_ID_TEST = "app6c9183410a6348c38c";
	public static final String ADCOLONY_SKANWORDS_VIDINT_ZONE_ID = "vz11bbe79247b74b5ba6";

	public static final String VUNGLE_APP_ID = "544f8cd7492b66e30300002c";
	public static final String VUNGLE_APP_ID_TEST = "55a60f639a7820e13a0000e8";

	public static final String ADMOB_INTERSTITIAL_UNIT_ID = "ca-app-pub-9865553155301243/2739874619";
	public static final String ADMOB_BANNER_UNIT_ID = "ca-app-pub-9865553155301243/4216607815";

	public static final String UNITY_ADS_GAME_ID = "131626732";
	public static final String UNITY_ADS_GAME_ID_TEST = "55138";

	private static AdsManager instance = null;
	private AdsEventsListener videoEventsListever = null;
	private List<AppAdsObject> videoOffers = null;
	private List<AppAdsObject> interstitials = null;
	private Activity mCallerActivity;
	private boolean showingInterstitial = false;
	private boolean videoForHints = false;
	final VunglePub vunglePub = VunglePub.getInstance();
	AdConfig mRewardedVideoAdConfig;
	AdConfig mSimpleVideoAdConfig;
	Map<String, Boolean> adcolonyAdsAvailable = new HashMap<String, Boolean>();
	private String mOfferTarget;

	private InterstitialAd mAdmobInterstitial = null;

	public AdsManager() {

	}

	public static AdsManager getInstance() {
		if (instance == null) {
			instance = new AdsManager();
		}
		return instance;
	}

	public void init(Activity activity) {
		Chartboost.startWithAppId(activity,CHARTBOOST_APP_ID,CHARTBOOST_APP_SIGNATURE);
		Chartboost.setDelegate(mChartboostDelegate);
		Chartboost.setImpressionsUseActivities(true);
		Chartboost.onCreate(activity);
		Chartboost.onStart(activity);

		vunglePub.init(activity, VUNGLE_APP_ID);
		vunglePub.setEventListeners(mVungleEventsListener);
		
		mRewardedVideoAdConfig = vunglePub.getGlobalAdConfig();
		mRewardedVideoAdConfig.setSoundEnabled(true);
		mRewardedVideoAdConfig.setBackButtonImmediatelyEnabled(false);
		mRewardedVideoAdConfig.setOrientation(Orientation.autoRotate);
		
		mSimpleVideoAdConfig = new AdConfig();
		mSimpleVideoAdConfig.setBackButtonImmediatelyEnabled(true);

		AdColony.configure(activity, "version:2.1,store:google", ADCOLONY_APP_ID, ADCOLONY_SKANWORDS_ZONE_ID, ADCOLONY_HINTS_ZONE_ID, ADCOLONY_SKANWORDS_VIDINT_ZONE_ID);
		AdColony.addAdAvailabilityListener(adAvailabilityListener);
		AdColony.addV4VCListener(adColonyV4VCListener);

		activity.getApplication().registerActivityLifecycleCallbacks(
				new ActivityLifecycleCallbacks() {

					@Override
					public void onActivityStopped(Activity activity) {
					}

					@Override
					public void onActivityStarted(Activity activity) {
						requestAdmobInterstitial(activity);
					}

					@Override
					public void onActivitySaveInstanceState(Activity activity,
							Bundle outState) {

					}

					@Override
					public void onActivityResumed(Activity activity) {
						UnityAds.changeActivity(activity);
						vunglePub.onResume();
						AdColony.resume(activity); 
					}

					@Override
					public void onActivityPaused(Activity activity) {
						vunglePub.onPause();
						AdColony.pause(); 
					}

					@Override
					public void onActivityDestroyed(Activity activity) {
					}

					@Override
					public void onActivityCreated(Activity activity,
							Bundle savedInstanceState) {
					}
				});
		UnityAds.init(activity, UNITY_ADS_GAME_ID, mUnityAdsListener);

		if (!MainNetworkManager.isRelese()) {
			UnityAds.setTestMode(true);
		}
		// requestAdmobInterstitial();

	}

	public void activityResumed(Activity activity) {
		AdColony.resume(activity);
	}

	synchronized private AppAdsObject getRandomObjectFromList(
			List<AppAdsObject> ads) {
		if (ads == null)
			return null;
		int totalCoef = 0;
		for (AppAdsObject appAdsObject : ads) {
			Log.v("SkanwordsAds", " AppAdsObject " + appAdsObject.getName() + " coef -  "
					+ appAdsObject.getCoefficient());
			totalCoef += appAdsObject.getCoefficient();
		}
		if (totalCoef == 0)
			return null;
		int randoCoef = Utils.randInt(0, totalCoef);
		Log.v("SkanwordsAds", totalCoef + " - totalCoef  " + randoCoef + " -randomCoef ");
		for (AppAdsObject appAdsObject : ads) {
			if (randoCoef <= appAdsObject.getCoefficient()
					&& appAdsObject.getCoefficient() > 0) {
				Log.v("SkanwordsAds", " chose " + appAdsObject.getName() + " coef -  "
						+ appAdsObject.getCoefficient());
				return appAdsObject;
			} else {
				randoCoef -= appAdsObject.getCoefficient();
			}
		}
		return null;
	}

	private void videoNoVideo() {
		if (videoEventsListever != null) {
			mCallerActivity.runOnUiThread(new Runnable() {
				@Override
				public void run() {
					videoEventsListever.onNoAds();
					videoEventsListever = null;
				}
			});
		}
		SmappsScanwords.sendGAEvent(ActionType.APP_ACTION,
				"offervideo_novideopopup_show"); // GA Event
	}
	private void videoSkipped(String vid) {
		Log.v("SkanwordsAds", "videoSkipped " + vid);
		if (videoEventsListever != null) {
			mCallerActivity.runOnUiThread(new Runnable() {
				@Override
				public void run() {
					videoEventsListever.onSkipped();
					videoEventsListever = null;
				}
			});
		}
	}
	private void videoComplete(String offerServiceName) {
		SmappsScanwords.sendGAEvent(ActionType.APP_ACTION, "offervideo_"
				+ offerServiceName + "_enter"); // GA Event
		UserUpdatesManager.getInstance().offerUsed(mOfferTarget,
				offerServiceName);
		if (videoEventsListever != null) {
			mCallerActivity.runOnUiThread(new Runnable() {
				@Override
				public void run() {
					videoEventsListever.onComplete();
					videoEventsListever = null;
				}
			});
		}
	}

	private void rememberInterstitialShowTime() {
		SmappsScanwords
				.getAppSharedPreferencesEditor(
						MainDataManager.GENERAL_PREFERENCES_NAME)
				.putLong("ads_show_time", new Date().getTime()).commit();
	}

	private boolean checkInterstitialTimeDelay() {
		if (MainDataManager.getInstance().getAppAds() == null)
			return false;
		long adsShowTime = SmappsScanwords.getAppSharedPreferences(
				MainDataManager.GENERAL_PREFERENCES_NAME).getLong(
				"ads_show_time", 0);
		if (adsShowTime < 1) {
			return true;
		}
		Log.v("SkanwordsAds",
				" interstitials time delay   delta time-"
						+ (new Date().getTime() - adsShowTime)
						+ "  need to wait - "
						+ MainDataManager.getInstance().getAppAds()
								.getShowDelay() * 1000);
		return (new Date().getTime() - adsShowTime) >= MainDataManager
				.getInstance().getAppAds().getShowDelay() * 1000;
	}

	public void showVideoOffer(Activity callerActivity,
			AdsEventsListener listener, String offerTarget, boolean forHints) {
		videoEventsListever = listener;
		if (!MainNetworkManager.getInstance().hasNetworkConnection(
				callerActivity)) {
			videoEventsListever.onNoAds();
			return;
		}
		mOfferTarget = offerTarget;
		videoForHints = forHints;
		videoOffers = new ArrayList<AppAdsObject>(!forHints ? MainDataManager
				.getInstance().getAppOffers().getVideo() : MainDataManager
				.getInstance().getAppOffers().getVideoHints());
		mCallerActivity = callerActivity;
		showNextVideo();
	}

	public void showAds(Activity callerActivity, AdsEventsListener listener,
			int adsType) {
		if (showingAds() || MainDataManager.getInstance().getUserData().isAdsOff() || MainDataManager.getInstance().getAppOffers().getVideoInterstitial() == null) {
			Log.v("SkanwordsAds", showingAds()  + "  showAds  or adsoff " + MainDataManager.getInstance().getUserData().isAdsOff());
			return;
		}

		mCallerActivity = callerActivity;
		rememberInterstitialShowTime();

		List<AppAdsObject> ads = new ArrayList<AppAdsObject>();

		ads.addAll(MainDataManager.getInstance().getAppOffers().getVideoInterstitial());

		if (!showNextAds(ads, adsType)) {
			Log.v("SkanwordsAds", "showAds  no more ads");
			listener.onNoAds();
		} else {
			Log.v("SkanwordsAds", "showAds  has ads");
		}
	}

	private boolean showNextAds(List<AppAdsObject> ads, int adsType) {
		AppAdsObject appAdsObject = getRandomObjectFromList(ads);
		if (appAdsObject == null) {

			return false; // No ads left
		}
		ads.remove(appAdsObject);

		if (!showAdsOfAdsObject(appAdsObject, adsType)) {
			return showNextAds(ads, adsType); // no ads recursive
		}
		return true; // showed some ads
	}

	private boolean showAdsOfAdsObject(AppAdsObject adsObject, int adsType) {
		boolean result = false;
		;
		Log.v("SkanwordsAds", adsType + "  showAdsOfAdsObject  " + adsObject.getName());

		String adsName = adsObject.getName();
		if (adsName.equals("chartboost")) {
			if ((adsType & ADS_REWARDED_VIDEO) != 0)
				result = showChartboostVideo();
			else if ((adsType & ADS_INTERSTITIAL) != 0
					|| (adsType & ADS_VIDEO_INTERSTITIAL) != 0)
				result = showChartboostInterstitial();
		} else if (adsName.equals("vungle")) {
			if ((adsType & ADS_REWARDED_VIDEO) != 0)
				result = showVungleVideo(mRewardedVideoAdConfig);
			else if ((adsType & ADS_VIDEO_INTERSTITIAL) != 0)
				result = showVungleVideo(mSimpleVideoAdConfig);
		} else if (adsName.equals("adcolony")) {
			if ((adsType & ADS_REWARDED_VIDEO) != 0)
				result = showAdcolonyVideo(ADS_REWARDED_VIDEO);
			else if ((adsType & ADS_VIDEO_INTERSTITIAL) != 0)
				result = showAdcolonyVideo(ADS_VIDEO_INTERSTITIAL);
		} else if (adsName.equals("unity")) {
			try {
				if ((adsType & ADS_REWARDED_VIDEO) != 0) {
					UnityAds.setZone("rewardedVideoZone");
					result = showUnityVideo();
				} else if ((adsType & ADS_VIDEO_INTERSTITIAL) != 0) {
					UnityAds.setZone("defaultVideoAndPictureZone");
					result = showUnityVideo();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else if (adsName.equals("admob")) {
			if ((adsType & ADS_INTERSTITIAL) != 0)
				result = showAdmobInterstitial();
		}
		Log.v("SkanwordsAds", "showAdsOfAdsObject result "+ result);
		return result;
	}
	
	private boolean hasAdcolonyAdsForZoneKey(String key) {
		return !adcolonyAdsAvailable.isEmpty() && adcolonyAdsAvailable.get(key) != null && adcolonyAdsAvailable.get(key);
	}
	
	public boolean hasRewardedVideo(boolean skanwords) {
		boolean networkAvailable = MainNetworkManager.getInstance().hasNetworkConnection();
		boolean cb = Chartboost.hasRewardedVideo(CBLocation.LOCATION_DEFAULT);
		boolean vung = vunglePub.isAdPlayable();
		boolean ac = hasAdcolonyAdsForZoneKey(skanwords ? ADCOLONY_SKANWORDS_ZONE_ID : ADCOLONY_HINTS_ZONE_ID) ;
		boolean un = UnityAds.canShowAds();
		return (cb || vung || ac || un) && networkAvailable;
	}
	public boolean hasInterstitialVideo() {
		return Chartboost.hasRewardedVideo(CBLocation.LOCATION_DEFAULT) || vunglePub.isAdPlayable() || hasAdcolonyAdsForZoneKey(ADCOLONY_SKANWORDS_VIDINT_ZONE_ID) || UnityAds.canShowAds();
	}

	public void showInterstitial(Activity callerActivity) {
		if (showingAds() || !checkInterstitialTimeDelay()
				|| MainDataManager.getInstance().getUserData().isAdsOff())
			return;
		rememberInterstitialShowTime();
		Log.v("SkanwordsAds", " showInterstitial ");
		interstitials = new ArrayList<AppAdsObject>(MainDataManager
				.getInstance().getAppAds().getMainShows());
		mCallerActivity = callerActivity;
		showNextInterstitial();
	}

	private void showNextVideo() {
		AppAdsObject appAdsObject = getRandomObjectFromList(videoOffers);
		if (appAdsObject == null) {
			videoNoVideo(); 
			return;
		}
		videoOffers.remove(appAdsObject);
		showAdsForAdsObject(appAdsObject, true);
	}

	private void showNextInterstitial() {
		AppAdsObject appAdsObject = getRandomObjectFromList(interstitials);
		if (appAdsObject == null) {
			return;
		}
		interstitials.remove(appAdsObject);
		showAdsForAdsObject(appAdsObject, false);
	}

	private void showAdsForAdsObject(AppAdsObject object, boolean video) {
		Log.v("SkanwordsAds", video + "  showAdsForAdsObject  " + object.getName());
		String adsName = object.getName();
		if (video) {
			if (adsName.equals("chartboost")) {
				showChartboostVideo();
				return;
			} else if (adsName.equals("vungle")) {
				showVungleVideo(mRewardedVideoAdConfig);
				return;
			} else if (adsName.equals("adcolony")) {
				showAdcolonyVideo(ADS_REWARDED_VIDEO);
				return;
			} else if (adsName.equals("unity")) {
				try {
					UnityAds.setZone("rewardedVideoZone");
					showUnityVideo();
					return;
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			showNextVideo();
		} else {
			if (adsName.equals("chartboost")) {
				showChartboostInterstitial();
				return;
			} else if (adsName.equals("admob")) {
				showAdmobInterstitial();
				return;
			}
			showNextInterstitial();
		}

	}

	private void interstitialShowFailed() {
		showNextInterstitial();
	}

	private void videoShowFailed(String serviceName) {
		SmappsScanwords.sendGAEvent(ActionType.APP_ACTION, "offervideo_"
				+ serviceName + "_failed"); // GA Event
		showNextVideo();
	}

	private boolean showingAds() {
		return showingInterstitial;
	}

	public static class AdsEventsListener {

		public void onComplete() {
		};

		public void onNoAds() {
		};
		public void onSkipped() {
			
		};

	}

	// CHARTBOOST

	private boolean showChartboostInterstitial() {
		Log.v("SkanwordsAds", "Show interstitial chartboost");
		try {
			if (Chartboost.getDelegate() == null) {
				interstitialShowFailed();
				return false;
			}
		} catch (Exception e) {
			interstitialShowFailed();
			return false;
		}
		if (Chartboost.hasInterstitial(CBLocation.LOCATION_DEFAULT)) {
			Chartboost.showInterstitial(CBLocation.LOCATION_DEFAULT);
			return true;
		} else {
			Chartboost.cacheInterstitial(CBLocation.LOCATION_DEFAULT);
			interstitialShowFailed();
			return false;
		}
	}

	private boolean showChartboostVideo() {
		Log.v("SkanwordsAds", "Show video chartboost");
		if (Chartboost.hasRewardedVideo(CBLocation.LOCATION_DEFAULT)) {
			Chartboost.showRewardedVideo(CBLocation.LOCATION_DEFAULT);
			return true;
		} else {
			Chartboost.cacheRewardedVideo(CBLocation.LOCATION_DEFAULT);
			videoShowFailed(UserUpdatesManager.OFFER_NAME_CHARTBOOST_VIDEO);
			return false;
		}
	}

	private final ChartboostDelegate mChartboostDelegate = new ChartboostDelegate() {

		@Override
		public void didCloseInterstitial(String location) {
			Log.v("SkanwordsAds", "didCloseInterstitial");
			showingInterstitial = false;
			Chartboost.cacheInterstitial(CBLocation.LOCATION_DEFAULT);
			super.didCloseInterstitial(location);
		}

		@Override
		public void didCloseRewardedVideo(String location) {
			Log.v("SkanwordsAds", "didCloseRewardedVideo");
			showingInterstitial = false;
			Chartboost.cacheRewardedVideo(CBLocation.LOCATION_DEFAULT);
			super.didCloseRewardedVideo(location);
		}

		@Override
		public void didDismissInterstitial(String location) {
			Log.v("SkanwordsAds", "didDismissInterstitial");
			super.didDismissInterstitial(location);
		}

		@Override
		public void didDismissRewardedVideo(String location) {
			Log.v("SkanwordsAds", "didDismissRewardedVideo");
			super.didDismissRewardedVideo(location);
		}

		@Override
		public void didCompleteRewardedVideo(String location, int reward) {
			Log.v("SkanwordsAds", "didCompleteRewardedVideo");
			videoComplete(UserUpdatesManager.OFFER_NAME_CHARTBOOST_VIDEO);
			super.didCompleteRewardedVideo(location, reward);
		}

		@Override
		public void didDisplayInterstitial(String location) {
			showingInterstitial = true;
			super.didDisplayInterstitial(location);
		}

		@Override
		public void didDisplayRewardedVideo(String location) {
			showingInterstitial = true;
			SmappsScanwords
					.sendGAEvent(ActionType.APP_ACTION, "offervideo_"
							+ UserUpdatesManager.OFFER_NAME_CHARTBOOST_VIDEO
							+ "_enter"); // GA Event
			super.didDisplayRewardedVideo(location);
		}

		@Override
		public void didFailToLoadInterstitial(String location,
				CBImpressionError error) {
			Log.v("SkanwordsAds",
					" didFailToLoadInterstitial " + error.toString());
			super.didFailToLoadInterstitial(location, error);
		}

		@Override
		public void didFailToLoadRewardedVideo(String location,
				CBImpressionError error) {
			Log.v("SkanwordsAds", " didFailToLoadRewardedVideo " + error.name());
			super.didFailToLoadRewardedVideo(location, error);
		}

	};

	// ADMOB
	private boolean showAdmobInterstitial() {
		if (mAdmobInterstitial == null) {
			requestAdmobInterstitial(mCallerActivity);
		} else if (mAdmobInterstitial.isLoaded()) {
			try {
				mAdmobInterstitial.show();
				return true;
			} catch (Exception e) {
				
			}
		}
		return false;
	}

	private void requestAdmobInterstitial(Activity activity) {
		Log.v("SkanwordsAds", "requestAdmobInterstitial  " + activity);
		if (mAdmobInterstitial != null) {
			return;
		}
		mAdmobInterstitial = new InterstitialAd(activity);
		mAdmobInterstitial.setAdUnitId(ADMOB_INTERSTITIAL_UNIT_ID);
		AdRequest adRequest = new AdRequest.Builder().build();
		mAdmobInterstitial.loadAd(adRequest);
		mAdmobInterstitial.setAdListener(new AdListener() {

			@Override
			public void onAdFailedToLoad(int errorCode) {
				Log.v("SkanwordsAds", "onAdFailedToLoad  " + errorCode);
				mAdmobInterstitial = null;
				interstitialShowFailed();
				super.onAdFailedToLoad(errorCode);
			}

			@Override
			public void onAdLoaded() {
				super.onAdLoaded();
				Log.v("SkanwordsAds", "onAdLoaded "  + mAdmobInterstitial.isLoaded());
			}

			@Override
			public void onAdClosed() {
				showingInterstitial = false;
				mAdmobInterstitial = null;
				super.onAdClosed();
			}

			@Override
			public void onAdOpened() {
				showingInterstitial = true;
				super.onAdOpened();
			}
		});

	}

	// VUNGLE
	private boolean showVungleVideo(AdConfig config) {
		if (vunglePub.isAdPlayable()) {
			vunglePub.playAd(config);
			return true;
		}
		return false;
	}

	private EventListener mVungleEventsListener = new EventListener() {

		boolean playing = false;
		private Integer vungleMutex = 1;
		@Override
		public void onVideoView(boolean arg0, int arg1, int arg2) {
			synchronized (vungleMutex) {
				Log.v("SkanwordsAds", "onVideoView " + arg0 + "  " + arg1 + " " + arg2 + "  playing "+ playing);
				if (arg0) {
					videoComplete(UserUpdatesManager.OFFER_NAME_VUNGLE_VIDEO);
				} else {
					Log.v("SkanwordsAds", "vungle non complete ");
					if (!playing) {
						Log.v("SkanwordsAds", "vungle skipped " );
						videoSkipped("vungle");
					} 
				}
			}
		}

		@Override
		public void onAdUnavailable(String arg0) {
			videoShowFailed(UserUpdatesManager.OFFER_NAME_VUNGLE_VIDEO);
			Log.v("SkanwordsAds", "onAdUnavailable ");
		}

		@Override
		public void onAdStart() {
			synchronized (vungleMutex) {
				playing = true;
				showingInterstitial = true;
				Log.v("SkanwordsAds", "onAdStart ");
				SmappsScanwords.sendGAEvent(ActionType.APP_ACTION,
						"offervideo_"
								+ UserUpdatesManager.OFFER_NAME_VUNGLE_VIDEO
								+ "_enter"); // GA Event
			}
		}

		@Override
		public void onAdEnd(boolean arg0) {
			synchronized (vungleMutex) {
				playing = false;
				showingInterstitial = false;
				Log.v("SkanwordsAds", "onAdEnd " + arg0);
			}
		}

		@Override
		public void onAdPlayableChanged(boolean arg0) {
			// TODO Auto-generated method stub

		}

	};

	// ADCOLONY
	private boolean showAdcolonyVideo(int adType) {

		boolean available = false;
		if (adType == ADS_VIDEO_INTERSTITIAL)  {
			available = hasAdcolonyAdsForZoneKey(ADCOLONY_SKANWORDS_VIDINT_ZONE_ID);
		} else {
			available = videoForHints ? hasAdcolonyAdsForZoneKey(ADCOLONY_HINTS_ZONE_ID) : hasAdcolonyAdsForZoneKey(ADCOLONY_SKANWORDS_ZONE_ID);
		}
		if (!available) {
			videoShowFailed(UserUpdatesManager.OFFER_NAME_ADCOLONY_VIDEO);
			return false;
		}

		if (adType == ADS_REWARDED_VIDEO) {
			AdColonyV4VCAd ad = new AdColonyV4VCAd(
					!videoForHints ? ADCOLONY_SKANWORDS_ZONE_ID
							: ADCOLONY_HINTS_ZONE_ID)
					.withListener(adColonyAdListener);
			ad.show();
		} else if (adType == ADS_VIDEO_INTERSTITIAL) {
			AdColonyVideoAd ad = new AdColonyVideoAd().withListener(adColonyAdListener);
			ad.show();
		}
		return true;
	}

	private AdColonyAdAvailabilityListener adAvailabilityListener = new AdColonyAdAvailabilityListener() {

		@Override
		public void onAdColonyAdAvailabilityChange(boolean availabel,
				String zoneId) {
			Log.v("SkanwordsAds", "onAdColonyAdAvailabilityChange " + availabel + "  " + zoneId);
			adcolonyAdsAvailable.put(zoneId, availabel);
		}
	};

	private AdColonyV4VCListener adColonyV4VCListener = new AdColonyV4VCListener() {

		@Override
		public void onAdColonyV4VCReward(AdColonyV4VCReward reward) {
			Log.v("SkanwordsAds", "onAdColonyV4VCReward " + reward.success());
			if (reward.success()) {

				// TEST_BLOCK
			}
		}

	};
	private AdColonyAdListener adColonyAdListener = new AdColonyAdListener() {

		@Override
		public void onAdColonyAdStarted(AdColonyAd ad) {
			showingInterstitial = true;
			SmappsScanwords.sendGAEvent(ActionType.APP_ACTION, "offervideo_"
					+ UserUpdatesManager.OFFER_NAME_ADCOLONY_VIDEO + "_enter"); // GA
																				// Event
		}

		@Override
		public void onAdColonyAdAttemptFinished(AdColonyAd ad) {
			showingInterstitial = false;
			if (ad.shown() && !ad.canceled()) {
				videoComplete(UserUpdatesManager.OFFER_NAME_ADCOLONY_VIDEO);
			} else if (ad.noFill() || ad.notShown()) {
				videoShowFailed(UserUpdatesManager.OFFER_NAME_ADCOLONY_VIDEO);
			} else if (ad.canceled()) {
				videoSkipped("adcolony");
			}
			Log.v("SkanwordsAds",
					"onAdColonyAdAttemptFinished " + " ad.shown() "
							+ ad.shown() + " ad.canceled() " + ad.canceled()
							+ " ad.notShown() " + ad.notShown()
							+ " ad.skipped() " + ad.skipped() + " ad.noFill() "
							+ ad.noFill());
		}
	};

	// UNITY ADS

	private boolean showUnityVideo() {
		if (UnityAds.canShow()) {
			Log.v("SkanwordsAds", " showUnityVideo can show	");
			try {
				UnityAds.show();
			} catch (Exception e) {
				e.printStackTrace();
			}
			return true;
		} else {
			Log.v("SkanwordsAds", " showUnityVideo cant show	");
			videoShowFailed(UserUpdatesManager.OFFER_NAME_UNITY_VIDEO);
			return false;
		}
	}

	private IUnityAdsListener mUnityAdsListener = new IUnityAdsListener() {
		boolean skipped = false;
		@Override
		public void onVideoStarted() {
		}

		@Override
		public void onVideoCompleted(String arg0, boolean skipped) {
			this.skipped = skipped;
		}

		@Override
		public void onShow() {
			showingInterstitial = true;
		}

		@Override
		public void onHide() {
			showingInterstitial = false;
			if (!skipped) {
				videoComplete(UserUpdatesManager.OFFER_NAME_UNITY_VIDEO);
			} else {
				videoSkipped("unity");
			}
		}

		@Override
		public void onFetchFailed() {

		}

		@Override
		public void onFetchCompleted() {

		}
	};
	// FLURRY ADS

}
