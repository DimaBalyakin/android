package org.skanword.and.etc;

import java.lang.reflect.Field;

import org.skanword.and.SmappsScanwords;
import org.skanword.and.SmappsScanwords.TrackerName;

import android.support.v4.app.Fragment;
import android.util.Log;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

public class CustomFragment extends Fragment {
	private static final Field sChildFragmentManagerField;

    static {
        Field f = null;
        try {
            f = Fragment.class.getDeclaredField("mChildFragmentManager");
            f.setAccessible(true);
        } catch (NoSuchFieldException e) {
            Log.e("", "Error getting mChildFragmentManager field", e);
        }
        sChildFragmentManagerField = f;
    }
    
    public void fragmentOpened() {
    	
    }
    public void updateFragment() {
    	updateFragment(false);
    }
    public void updateFragment(boolean byUser) {
    	
    }
    @Override
    public void onResume() {
    	super.onResume();
    	final Tracker tracker = SmappsScanwords.instance.getTracker(
                        TrackerName.APP_TRACKER);
        if(tracker != null){

            tracker.setScreenName(getClass().getSimpleName());
            tracker.send(new HitBuilders.ScreenViewBuilder().build());
        }
    }
    @Override
    public void onDetach() {
        super.onDetach();

        if (sChildFragmentManagerField != null) {
            try {
                sChildFragmentManagerField.set(this, null);
            } catch (Exception e) {
                Log.e("", "Error setting mChildFragmentManager field", e);
            }
        }
    }
}
