package org.skanword.and.etc;


import java.util.HashMap;

import org.skanword.and.R;
import org.skanword.and.SmappsScanwords;
import org.skanword.and.datamanager.MainDataManager;

import android.app.Activity;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.media.SoundPool.OnLoadCompleteListener;

public class SoundsManager {


	public static final int SOUND_HINT_LETTER = R.raw.hint_letter;
	public static final int SOUND_WORD_CORRECT = R.raw.word_correct;
	public static final int SOUND_WIN = R.raw.win;
	public static final int SOUND_BONUS_SCRATCH = R.raw.hourly_bonus;
	public static final int SOUND_BONUS_SCRATCH_REST = R.raw.hourly_bonus_ding;

	
	public static void init(Activity activity) {


	}

	public static void playSound(Activity activity, final Integer soundID) {
		
		if (MainDataManager.getInstance().getOptions().isSounds()) {
			MediaPlayer mp = MediaPlayer.create(TopActivityManager.get().getTopActivity().getApplicationContext(), soundID);

			try {
				if (mp.isPlaying()) {
					mp.stop();
					mp.release();

					mp = MediaPlayer.create(TopActivityManager.get().getTopActivity().getApplicationContext(), soundID);
				}

				mp.start();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}