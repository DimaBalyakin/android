package org.skanword.and.etc;

import org.skanword.and.SmappsScanwords;
import org.skanword.and.SmappsScanwords.ActionType;
import org.skanword.and.datamanager.MainDataManager;

import android.app.Activity;
import android.util.Log;

public class AdBot {
	
	
	public static void makeStep(Activity callerActivity) {
		Log.v("SkanwordsAds", "adbot making step");

		boolean gamePlayed = SmappsScanwords.getAppSharedPreferences(MainDataManager.GENERAL_PREFERENCES_NAME).getBoolean("game_played", false);
		SmappsScanwords.sendGAEvent(ActionType.APP_ACTION, "adbot_step"); // GA Event
		if (hasPopupsToShow() || !gamePlayed) {
			
			return;
		}
		AdsManager.getInstance().showInterstitial(callerActivity);
	}
	
	public static boolean hasPopupsToShow() {
		return false;
	}
}
