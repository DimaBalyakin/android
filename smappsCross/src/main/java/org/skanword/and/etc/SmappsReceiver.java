package org.skanword.and.etc;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class SmappsReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
    	Log.v("" ,"onReceive  SmappsReceiver " + intent.getStringExtra(LocalNotificationsManager.EXTRA_NOTIFICATION_MESSAGE));
       Intent service1 = new Intent(context, SmappsAlarmService.class);
       service1.putExtras(intent.getExtras());
       context.startService(service1);
	}
}
