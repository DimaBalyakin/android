package org.skanword.and.etc;

import java.net.URLDecoder;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.google.android.gms.analytics.CampaignTrackingReceiver;

public class CustomReferralReceiver extends BroadcastReceiver {

	  public static String getReferrer(Context paramContext)
	  {
	    return paramContext.getSharedPreferences("referrer", 0).getString("referrer", null);
	  }
	@Override
	public void onReceive(Context paramContext, Intent paramIntent) {
	    new CampaignTrackingReceiver().onReceive(paramContext, paramIntent);
		if (paramIntent != null) {}
	    try
	    {
	      if (paramIntent.getAction().equals("com.android.vending.INSTALL_REFERRER"))
	      {
	        String str1 = paramIntent.getStringExtra("referrer");
	        if (str1 != null)
	        {
	          String str2 = URLDecoder.decode(str1, "UTF-8");
	          paramContext.getSharedPreferences("referrer", 0).edit().putString("referrer", str2).commit();
	        }
	      }
	    }
	    catch (Exception localException)
	    {
	    	
	    }
	}
}
