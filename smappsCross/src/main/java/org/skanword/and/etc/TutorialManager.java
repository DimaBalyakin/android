package org.skanword.and.etc;

import org.skanword.and.SmappsScanwords;
import org.skanword.and.datamanager.MainDataManager;

import com.musselwhizzle.dispatcher.events.SimpleEvent;


public class TutorialManager {

	public static final String TUTORIAL_STATE_CHANGED = "TUTORIAL_STATE_CHANGED";
	private static TutorialManager instance;

	public static TutorialManager getInstance() {
	   if(instance == null) {
	      instance = new TutorialManager();
	      
	      //SmappsScanwords.getAppSharedPreferencesEditor(MainDataManager.TUTORIAL_PREFERENCES_NAME).putInt("tutorial", TutorialStep.TUTORIAL_START.ordinal()).commit();
	   }
	   return instance;
	   
	   
	}


    public TutorialStep currentTutorialStep() {
        return TutorialStep.values()[SmappsScanwords.getAppSharedPreferences(MainDataManager.TUTORIAL_PREFERENCES_NAME).getInt("tutorial_step", TutorialStep.TUTORIAL_DISABLED.ordinal())];
    }
     
    public void setTutorialStep (TutorialStep step) {
        SmappsScanwords.getAppSharedPreferencesEditor(MainDataManager.TUTORIAL_PREFERENCES_NAME).putInt("tutorial_step", step.ordinal()).commit();
        SmappsScanwords.getEventsDispatcher().dispatchEvent(new SimpleEvent(TUTORIAL_STATE_CHANGED));
    }

    public enum TutorialStep {
        TUTORIAL_DISABLED,
        TUTORIAL_START,
        TUTORIAL_ISSUE_DOWNLOADING,
        TUTORIAL_ISSUE_DOWNLOADED,
        TUTORIAL_ISSUE_OPENED,
        TUTORIAL_SKANWORD_OPENED,
        TUTORIAL_WORD_SELECTED,
        TUTORIAL_LETTER_ENTERED,
        TUTORIAL_LETTER_ENTERED_BY_HINT,
        TUTORIAL_WORD_ENTERED,
        TUTORIAL_CELL_FOR_HINT_SELECTED,
        TUTORIAL_HINT_USED,
        TUTORIAL_SKANWORD_COMPLETE,
        TUTORIAL_COMPLETE,
    }
     
}
