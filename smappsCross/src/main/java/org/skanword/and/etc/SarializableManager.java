package org.skanword.and.etc;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

/** 
 * Usage sample serializing SomeClass instance 
 */
public class SarializableManager {

    /** Read the object from Base64 string. */
    @SuppressWarnings("unchecked")
	public static <T extends Serializable> T fromBlob( byte[] s, Object ob) throws IOException ,
                                                        ClassNotFoundException {
    	T o  = null;
       // byte [] data = Base64Coder.decode( s );
        ObjectInputStream ois = new ObjectInputStream( 
                                        new ByteArrayInputStream(  s ) );
        ois.close();        
        o = (T) ois.readObject();      
        return o;
    }

    /** Write the object to a Base64 string. */
    public static byte[] toBlobe( Serializable o ) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(baos);
        oos.writeObject(o);
        oos.close();
        return baos.toByteArray();
    }
}