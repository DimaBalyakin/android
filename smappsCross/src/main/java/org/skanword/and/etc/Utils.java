package org.skanword.and.etc;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Random;

import org.skanword.and.R;
import org.skanword.and.SmappsScanwords;

import android.app.AlertDialog;
import android.content.Context;
import android.content.res.Resources;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.view.View;

public class Utils {

	public static String md5(final String s) {
		String result = "";
		try {
			// Create MD5 Hash
			MessageDigest digest = java.security.MessageDigest
					.getInstance("MD5");
			digest.update(s.getBytes());
			byte messageDigest[] = digest.digest();

			// Create Hex String
			StringBuffer hexString = new StringBuffer();
			for (int i = 0; i < messageDigest.length; i++) {
				String h = Integer.toHexString(0xFF & messageDigest[i]);
				while (h.length() < 2)
					h = "0" + h;
				hexString.append(h);
			}
			result = hexString.toString();

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}

		return result;
	}
	public static int getResId(String resName, Class<?> c) {

	    try {
	        Field idField = c.getDeclaredField(resName);
	        return idField.getInt(idField);
	    } catch (Exception e) {
	        e.printStackTrace();
	        return -1;
	    }
	}
	public static int randInt(int min, int max) {

	    Random rand = new Random();
	    int randomNum = rand.nextInt((max - min) + 1) + min;

	    return randomNum;
	}
	public static void brandAlertDialog(AlertDialog dialog) {
	    try {
	        Resources resources = dialog.getContext().getResources();
	        int color = resources.getColor(R.color.theme_green); // your color here


	        int titleDividerId = resources.getIdentifier("titleDivider", "id", "android");
	        View titleDivider = dialog.getWindow().getDecorView().findViewById(titleDividerId);
	        titleDivider.setBackgroundColor(color); // change divider color
	    } catch (Exception ex) {
	        ex.printStackTrace();
	    }
	}

	public static String getAdvertiserId() {
		// TODO Auto-generated method stub
		return null;
	}

	public static String getAndroidId() {
		// TODO Auto-generated method stub
		return Settings.Secure.getString(SmappsScanwords.getContext().getContentResolver(),
				Settings.Secure.ANDROID_ID);
	}
	public static String getDeviceId() {
		TelephonyManager telephonyManager = (TelephonyManager) SmappsScanwords.getContext().getSystemService(Context.TELEPHONY_SERVICE);
		return telephonyManager.getDeviceId();
	}
	public static String getAndroidUniqueDeviceId() {

		StringBuffer idView = new StringBuffer();

		String imeistring = null;
		String imsistring = null;

		TelephonyManager telephonyManager = (TelephonyManager) SmappsScanwords.getContext().getSystemService(Context.TELEPHONY_SERVICE);

		/*
		 * getDeviceId() function Returns the unique device ID. for example,the
		 * IMEI for GSM and the MEID or ESN for CDMA phones.
		 */
		imeistring = telephonyManager.getDeviceId();
		idView.append("IMEI No : " + imeistring + "\n");

		/*
		 * getSubscriberId() function Returns the unique subscriber ID, for
		 * example, the IMSI for a GSM phone.
		 */
		imsistring = telephonyManager.getSubscriberId();
		idView.append("IMSI No : " + imsistring + "\n");

		/*
		 * System Property ro.serialno returns the serial number as unique
		 * number Works for Android 2.3 and above
		 */

		String serialnum = null;
		try {
			Class<?> c = Class.forName("android.os.SystemProperties");
			Method get = c.getMethod("get", String.class, String.class);
			serialnum = (String) (get.invoke(c, "ro.serialno", "unknown"));
			idView.append("serial : " + serialnum + "\n");
		} catch (Exception ignored) {
		}
		String serialnum2 = null;
		try {
			@SuppressWarnings("rawtypes")
			Class myclass = Class.forName("android.os.SystemProperties");
			Method[] methods = myclass.getMethods();
			Object[] params = new Object[] { new String("ro.serialno"),
					new String("Unknown") };
			serialnum2 = (String) (methods[2].invoke(myclass, params));
			idView.append("serial2 : " + serialnum2 + "\n");
		} catch (Exception ignored) {
		}
		/*
		 * Settings.Secure.ANDROID_ID returns the unique DeviceID Works for
		 * Android 2.2 and above
		 */
		String androidId = Settings.Secure.getString(SmappsScanwords.getContext().getContentResolver(),
				Settings.Secure.ANDROID_ID);
		idView.append("AndroidID : " + androidId + "\n");

		return md5(imeistring + imsistring + androidId + serialnum + serialnum2);
	}
	public static String spellVariantForNumber(int num, String first, String second, String third) {
		if (num % 100 > 10 && num % 100 < 20) {
	        return third;
	    }
		switch (num % 10) {
	        case 0: return third; 
	        case 1: return first; 
	        case 2: return second; 
	        case 3: return second; 
	        case 4: return second; 
	        case 5: return third; 
	        case 6: return third; 
	        case 7: return third; 
	        case 8: return third; 
	        case 9: return third; 
		}
		return first;
	}
	public static String secondsToTimerType(long time) {
		int totalSeconds = (int) time;
	    int seconds = totalSeconds % 60;
	    int minutes = (totalSeconds / 60) % 60;
	    int hours = totalSeconds / 3600;
	    return String.format("%02d:%02d:%02d",hours, minutes, seconds);
	}
	public static String secondsToMaxTimeType(long time) {
		int totalSeconds = (int) time;
	    int seconds = totalSeconds;
	    int minutes = totalSeconds / 60;
	    int hours = totalSeconds / 3600;
	    int days = totalSeconds / (3600 * 24);
	    if (days > 0) {
	    	return days + " " + spellVariantForNumber(days, "день", "дня", "дней");
	    } else if (hours > 0) {
	    	return hours + " " + spellVariantForNumber(hours, "час", "часа", "часов");
	    } else if (minutes > 0) {
	    	return minutes + " " + spellVariantForNumber(minutes, "минута", "минуты", "минут");
	    } else if (seconds > 0) {
	    	return seconds + " " + spellVariantForNumber(seconds, "секунда", "секунды", "секунд");
	    } else {
	    	return "unknown";
	    }
	}
	public static String join(List<Integer> list, String delim) {

	    StringBuilder sb = new StringBuilder();

	    String loopDelim = "";

	    for(Integer s : list) {

	        sb.append(loopDelim);
	        sb.append(s);            

	        loopDelim = delim;
	    }

	    return sb.toString();
	}
	public static <T> boolean compareLists(List<T> l1, List<T> l2) {
		return l1.equals(l2);
	}
}


