package org.skanword.and.etc;

import android.app.Activity;
import android.app.Application;
import android.app.Application.ActivityLifecycleCallbacks;
import android.content.Context;
import android.os.Bundle;

public class TopActivityManager implements ActivityLifecycleCallbacks {

	private Activity mTopActivity;
    private static TopActivityManager instance;
    public static TopActivityManager init(Application application){
        if (instance == null) {
            instance = new TopActivityManager();
            application.registerActivityLifecycleCallbacks(instance);
        }
        return instance;
    }
    public Activity getTopActivity() {
    	return mTopActivity;
    }
    public static TopActivityManager get(Application application){
        if (instance == null) {
            init(application);
        }
        return instance;
    }
    public static TopActivityManager get(Context ctx){
        if (instance == null) {
            Context appCtx = ctx.getApplicationContext();
            if (appCtx instanceof Application) {
                init((Application)appCtx);
            }
            throw new IllegalStateException(
                "TopActivityManager is not initialised and " +
                "cannot obtain the Application object");
        }
        return instance;
    }
    private TopActivity canBeTopActivity(Activity activity) {
    	TopActivity act;
    	try {
			act = (TopActivity)activity;
			return act;
		} catch (Exception e) {
			
		}
    	return null;
    }
    public static TopActivityManager get(){
        if (instance == null) {
            throw new IllegalStateException(
                "TopActivityManager is not initialised - invoke " +
                "at least once with parameterised init/get");
        }
        return instance;
    }
	@Override
	public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onActivityStarted(Activity activity) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onActivityResumed(Activity activity) {
		TopActivity act = canBeTopActivity(activity);
		if (act != null) {
			mTopActivity = activity;
		}
	}

	@Override
	public void onActivityPaused(Activity activity) {
		if (activity == mTopActivity)
			mTopActivity = null;
	}

	@Override
	public void onActivityStopped(Activity activity) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onActivityDestroyed(Activity activity) {
		
	}
	public static interface TopActivity {
		
	}
}
