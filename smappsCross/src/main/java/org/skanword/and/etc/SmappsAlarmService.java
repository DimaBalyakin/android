package org.skanword.and.etc;

import org.skanword.and.R;
import org.skanword.and.datamanager.MainDataManager;
import org.skanword.and.menu.MainMenuActivity;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

public class SmappsAlarmService extends Service {

	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}

    @Override
    public void onCreate() 
    {
       super.onCreate();
    }
 
 
    @Override
	public int onStartCommand(Intent intent, int flags, int startId) {
    	if (intent == null)
    		return super.onStartCommand(intent, flags, startId);
		NotificationCompat.Builder mBuilder =
			    new NotificationCompat.Builder(this)
				.setSmallIcon(R.drawable.icon)
			    .setContentTitle(intent.getExtras().getString(LocalNotificationsManager.EXTRA_NOTIFICATION_TITLE))
			    .setContentText(intent.getExtras().getString(LocalNotificationsManager.EXTRA_NOTIFICATION_MESSAGE));
        Intent intentToStart = new Intent(this.getApplicationContext(), MainMenuActivity.class);
        intentToStart.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intentToStart.putExtra("local_notification", true);
        intentToStart.putExtra("i", intent.getExtras().getString(LocalNotificationsManager.EXTRA_NOTIFICATION_ID));
        PendingIntent pendingNotificationIntent = PendingIntent.getActivity(this.getApplicationContext(), 0, intentToStart, PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(pendingNotificationIntent);
		NotificationManager mNotifyMgr = 
		        (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		Notification notification = mBuilder.build();
	    notification.flags |= Notification.FLAG_AUTO_CANCEL;
	    if (!MainDataManager.getInstance().getOptions().isDisabledNotificationsSounds())
	    	notification.defaults |= Notification.DEFAULT_SOUND;
        notification.defaults |= Notification.DEFAULT_VIBRATE;
		// Builds the notification and issues it.
		int mNotificationId = 001;
		mNotifyMgr.notify(mNotificationId, notification);
    	Log.v("SkanwordsNotif" ,"onCreate  SmappsAlarmService " + intent.getExtras().getString(LocalNotificationsManager.EXTRA_NOTIFICATION_MESSAGE) + " " +intent.getExtras().getString(LocalNotificationsManager.EXTRA_NOTIFICATION_ID));
		return super.onStartCommand(intent, flags, startId);
	}

	@Override
    public void onDestroy() 
    {
        // TODO Auto-generated method stub
        super.onDestroy();
    }
}
