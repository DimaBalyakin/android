package org.skanword.and.etc;

import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.TimeZone;

import org.skanword.and.SmappsScanwords;
import org.skanword.and.datamanager.MainDataManager;
import org.skanword.and.network.ConfigObject.NotificationRemind;
import org.skanword.and.network.DailyBonusObject;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class LocalNotificationsManager {

	public final static String EXTRA_NOTIFICATION_MESSAGE = "EXTRA_NOTIFICATION_MESSAGE";
	public final static String EXTRA_NOTIFICATION_TITLE = "EXTRA_NOTIFICATION_TITLE";
	public final static String EXTRA_NOTIFICATION_ID = "EXTRA_NOTIFICATION_ID";
	private static Integer bonusNotifsMutext = 1;

	public static void createLocalNotifications(Context context,
			boolean somethingChanged) {
		PendingIntent pendingIntent;
		AlarmManager alarmManager = (AlarmManager) context
				.getSystemService(Context.ALARM_SERVICE);
		int i = 0;
		if (MainDataManager.getInstance().getOptions() != null
				&& MainDataManager.getInstance().getLocalNotifications() != null) {
			Calendar today = Calendar.getInstance();
			for (NotificationRemind notifData : MainDataManager.getInstance()
					.getLocalNotifications().getRemindOn()) {
				if (notifData.getDays().size() < 1)
					continue;
				int hour = MainDataManager.getInstance().getOptions()
						.isDefaultNotificationsTime() ? 0 : MainDataManager
						.getInstance().getOptions()
						.getCustomNotificationTimeHour();
				int minute = MainDataManager.getInstance().getOptions()
						.isDefaultNotificationsTime() ? 0 : MainDataManager
						.getInstance().getOptions()
						.getCustomNotificationTimeMinute();
				Calendar date = Calendar.getInstance();
				date.setTimeZone(TimeZone.getTimeZone("UTC"));
				date.set(Calendar.HOUR_OF_DAY, hour);
				date.set(Calendar.MINUTE, minute);
				boolean nextDay = date.get(Calendar.HOUR_OF_DAY) <= today
						.get(Calendar.HOUR_OF_DAY)
						&& date.get(Calendar.MINUTE) <= today
								.get(Calendar.MINUTE)
						&& notifData.getDays().get(0) == 0;
				if (nextDay)
					continue;
				Intent intent = new Intent(context, SmappsReceiver.class);
				intent.putExtra(EXTRA_NOTIFICATION_MESSAGE, notifData.getText());
				intent.putExtra(EXTRA_NOTIFICATION_TITLE, "Сканворд дня!");
				Integer day = notifData.getDays().get(0);
				intent.putExtra(EXTRA_NOTIFICATION_ID,
						String.format("remind_on_%dd", day));
				pendingIntent = PendingIntent.getBroadcast(context, i, intent,
						Intent.FILL_IN_DATA);
				date.add(Calendar.DAY_OF_YEAR, day + (nextDay ? 1 : 0));
				Log.v("SkanwordsNotif", "set issue notification time  " + date.get(Calendar.DAY_OF_YEAR) + "  " + date.get(Calendar.HOUR_OF_DAY) + "  " + date.get(Calendar.MINUTE));
				if (MainDataManager.getInstance().getOptions() 
						.isInformNewIssues()
						&& ((date.getTimeInMillis() - TimeZone
								.getDefault().getRawOffset()) / 1000 - new Date()
								.getTime() / 1000) > 0){
					alarmManager.set(AlarmManager.RTC, date.getTimeInMillis()
							- TimeZone.getDefault().getRawOffset(),
							pendingIntent);
				}
				else {
					alarmManager.cancel(pendingIntent);
				}
				i++;
			}
			// NotificationRemind notifData =
			// MainDataManager.getInstance().getLocalNotifications().getRemindOn().get(0);
		}
	}
	
	public static void createDailyBonusNotifications(final DailyBonusObject dailyBonusObject, final Context context) {
		SmappsScanwords.getExecutorService().execute(new Runnable() {
			
			@Override
			public void run() {
				synchronized (bonusNotifsMutext) {
					Calendar datecont = Calendar.getInstance();
					datecont.setTimeInMillis(new Date().getTime());
					PendingIntent pendingIntent;
					AlarmManager alarmManager = (AlarmManager) context
							.getSystemService(Context.ALARM_SERVICE);
					int i = 0;
					Log.v("SkanwordsNotif", " createDailyBonusNotifications "+ dailyBonusObject.getNotification() + " " + dailyBonusObject.getTimeLeft());
					if (dailyBonusObject != null && dailyBonusObject.getNotification() != null && SmappsScanwords.getAppSharedPreferences(MainDataManager.GENERAL_PREFERENCES_NAME).getBoolean("daily_bonus_notify", true)) {
						Integer hour = 0;
						for (Map<String, Object> notifData : dailyBonusObject.getNotification()) {

							Calendar date = Calendar.getInstance();
							date.setTimeInMillis(new Date().getTime() + dailyBonusObject.getTimeLeft() * 1000);
							
							
							Intent intent = new Intent(context, SmappsReceiver.class);
							intent.putExtra(EXTRA_NOTIFICATION_MESSAGE, (String) notifData.get("text"));
							intent.putExtra(EXTRA_NOTIFICATION_TITLE, (String) notifData.get("title"));
							Integer titleId;
							Integer textId;
							if (notifData.get("title_id") == null) {
								titleId = textId = 0;
							} else  {
								titleId = ((Double) notifData.get("title_id")).intValue();
								textId = ((Double) notifData.get("text_id")).intValue();
							}
							hour += ((Double) notifData.get("hours")).intValue();
							String notifId = String.format("hh_%dh", ((Double) notifData.get("hours")).intValue()) + "_" + titleId + "_" + textId;
							intent.putExtra(EXTRA_NOTIFICATION_ID, notifId );
							pendingIntent = PendingIntent.getBroadcast(context, i + 100, intent,
									Intent.FILL_IN_DATA);
							
							date.add(Calendar.HOUR, hour);
							Log.v("SkanwordsNotif", " set bonus notification taime  " + date.get(Calendar.DAY_OF_YEAR) + "  " + date.get(Calendar.HOUR_OF_DAY) + "  " + date.get(Calendar.MINUTE) + "  " + notifId);
							if ((date.getTimeInMillis() / 1000 - new Date()
											.getTime() / 1000) > 0){
								alarmManager.set(AlarmManager.RTC, date.getTimeInMillis(),
										pendingIntent);
							}
							else {
								alarmManager.cancel(pendingIntent);
							}
							i++;
						}
					}
				}
			}
		});
	}

	public static void clearBonusNotifs() {
		SmappsScanwords.getExecutorService().execute(new Runnable() {
			@Override
			public void run() {
				synchronized (bonusNotifsMutext) {
					Log.v("SkanwordsNotif", " clearBonusNotifs ");
					PendingIntent pendingIntent;
					AlarmManager alarmManager = (AlarmManager) SmappsScanwords.getContext()
							.getSystemService(Context.ALARM_SERVICE);
					for (int j = 0; j < 15; j++) {
						Intent intent = new Intent(SmappsScanwords.getContext(), SmappsReceiver.class);
						pendingIntent = PendingIntent.getBroadcast(SmappsScanwords.getContext(), j + 100, intent,
								Intent.FILL_IN_DATA);
						alarmManager.cancel(pendingIntent);
					}
				}
			}
		});
	}
}
