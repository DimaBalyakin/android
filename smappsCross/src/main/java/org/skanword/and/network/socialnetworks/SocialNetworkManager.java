package org.skanword.and.network.socialnetworks;

import java.io.Serializable;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

public abstract class SocialNetworkManager {
	
	protected ISocialNetworkManagerDelegate delegate;
	protected boolean initted;
	protected Context appContext;
	protected SocialUser socialUser;
	protected final Gson gson = new Gson();
	
	public void initSocialNetworkManager(ISocialNetworkManagerDelegate delegate, Context appContext) {
		this.delegate = delegate;
		this.appContext = appContext;
	}
	public abstract void logout();
	public abstract void clearSocialNetworkManager();
	public abstract void authorise(Activity activity);
	public abstract void getCurrentUser();
	public abstract void getUserFriends();
	
	public static class SocialUser implements Serializable{
		
		/**
		 * 
		 */
		private static final long serialVersionUID = 7793063125969187103L;
		
		@SerializedName("updated_time")
		private long updatedTime = 0;
		private String socialid;  
		private String token;  
		@SerializedName("first_name")
		private String firstName; 
		@SerializedName("second_name")
		private String secondName;
		private String uid;  
		@SerializedName("avatar_url")
		private String avatarUrl; 
		
		public SocialUser(long updatedTime, String token, String socialid, String firstName, String secondName, String uid, String avatarUrl) {
			super();
			this.updatedTime = updatedTime;
			this.socialid = socialid;
			this.firstName = firstName;
			this.secondName = secondName;
			this.uid = uid;
			this.token = token;
			this.avatarUrl = avatarUrl;
		}
		public long getUpdatedTime() {
			return updatedTime;
		}
		public String getToken() {
			return token;
		}
		public String getSocialId() {
			return socialid;
		}
		public void setUpdatedTime(long updatedTime) {
			this.updatedTime = updatedTime;
		}
		public String getFirstName() {
			return firstName;
		}
		public String getSecondName() {
			return secondName;
		}
		public String getUid() {
			return uid;
		}
		public String getAvatarUrl() {
			return avatarUrl;
		}
		
		
		
	}
	public void onActivityResult(Activity activity, int requestCode, int resultCode, Intent data) {
		
	}
	public static SocialNetwork getSocialNetworkfromName(String name) {
		if (name.equals("cm")) {
			return SocialNetwork.SOCIAL_NETWORK_OK;
		} else if (name.equals("fb")) {
			return SocialNetwork.SOCIAL_NETWORK_FB;
		} else if (name.equals("vk")) {
			return SocialNetwork.SOCIAL_NETWORK_VK;
		}
		return SocialNetwork.SOCIAL_NETWORK_NO;
	}
	public static String getSocialNetworkStringId(SocialNetwork network) {
		switch (network) {
		case SOCIAL_NETWORK_OK:
			return "cm";
		case SOCIAL_NETWORK_FB:
			return "fb";
		case SOCIAL_NETWORK_VK:
			return "vk";

		default:
			break;
		}
		return "";
	}
	public static enum SocialNetwork {SOCIAL_NETWORK_NO, SOCIAL_NETWORK_OK, SOCIAL_NETWORK_FB, SOCIAL_NETWORK_VK }
}
