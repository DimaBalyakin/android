package org.skanword.and.network.socialnetworks;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.skanword.and.SmappsScanwords;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.Profile;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

public class FacebookManager extends SocialNetworkManager {
	CallbackManager callbackManager;
	
	private AccessTokenTracker accessTokenTracker;
	private AccessToken accessToken;

	@Override
	public void initSocialNetworkManager(ISocialNetworkManagerDelegate delegate, Context appContext) {
		super.initSocialNetworkManager(delegate, appContext);
		try {
	        PackageInfo info = SmappsScanwords.getContext().getPackageManager().getPackageInfo(
	                "org.skanword.and", 
	                PackageManager.GET_SIGNATURES);
	        for (Signature signature : info.signatures) {
	            MessageDigest md = MessageDigest.getInstance("SHA");
	            md.update(signature.toByteArray());
	            Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
	            }
	    } catch (NameNotFoundException e) {

	    } catch (NoSuchAlgorithmException e) {

	    }
		FacebookSdk.sdkInitialize(appContext);

		callbackManager = CallbackManager.Factory.create();

		LoginManager.getInstance().registerCallback(callbackManager,
				new FacebookCallback<LoginResult>() {
					@Override
					public void onSuccess(LoginResult loginResult) {
						getToken();
					}

					@Override
					public void onCancel() {
						FacebookManager.this.delegate.socialNetworkAuthoriseFailed(true);
					}

					@Override
					public void onError(FacebookException exception) {
						FacebookManager.this.delegate.socialNetworkAuthoriseFailed(false);
					}
				});
	    initted = true;
	}

	private void getToken() {
		accessTokenTracker = new AccessTokenTracker() {
			@Override
			protected void onCurrentAccessTokenChanged(
					AccessToken oldAccessToken,
					AccessToken currentAccessToken) {
				accessToken = currentAccessToken;
			}
		};
		accessToken = AccessToken.getCurrentAccessToken();
		if (accessToken != null)
			getCurrentUser();
	}

	@Override
	public void clearSocialNetworkManager() {
		// TODO Auto-generated method stub

	}

	@Override
	public void authorise(Activity activity) {
		Log.v("", "" + activity.toString());
		LoginManager.getInstance().logInWithReadPermissions(activity,
				Arrays.asList("public_profile", "user_friends", "user_about_me"));
	}

	@Override
	public void getCurrentUser() {
		GraphRequest request = GraphRequest.newMeRequest(accessToken, new GraphRequest.GraphJSONObjectCallback() {
			@Override
			public void onCompleted(JSONObject object, GraphResponse response) {
				if (delegate == null)
					return;
				String result = response.getRawResponse();
				Log.v("FacebookManager", "getCurrentUser " + result);
				if (result == null)
					return;
				JSONObject jsonObject = null;
				try {
					jsonObject = new JSONObject(result);
				} catch (JSONException e) {
					e.printStackTrace();
				}
				if (jsonObject != null) {
					try {
						String avatar = null;
						if (!jsonObject.getJSONObject("picture").getJSONObject("data").getBoolean("is_silhouette")) {
							avatar = jsonObject.getJSONObject("picture").getJSONObject("data").getString("url");
						}
						socialUser = new SocialUser(new Date().getTime(), accessToken.getToken(), SocialNetworkManager.getSocialNetworkStringId(SocialNetwork.SOCIAL_NETWORK_FB), jsonObject.getString("first_name"), jsonObject.getString("last_name"), jsonObject.getString("id"), avatar);
						getUserFriends();
					} catch (JSONException e) {
						e.printStackTrace();
					}
				} else {
					delegate.socialNetworkAuthoriseFailed(false);
				}
			}
		});
		Bundle parameters = new Bundle();
		parameters.putString("fields", "id,first_name,last_name,picture");
		request.setParameters(parameters);
		request.executeAsync();
	}

	@Override
	public void onActivityResult(Activity activity, int requestCode, int resultCode, Intent data) {
		Log.v("", "onActivityResult facebook");
		super.onActivityResult(activity, requestCode, resultCode, data);
		callbackManager.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	public void getUserFriends() {
		GraphRequest request = GraphRequest.newMyFriendsRequest(accessToken, new GraphRequest.GraphJSONArrayCallback() {
			@Override
			public void onCompleted(JSONArray objects, GraphResponse response) {
				String result = response.getRawResponse();
				if (result == null) {
					delegate.socialNetworkAuthorised(socialUser);
					return;
				}
				JSONArray jsonArray = null;
				try {
					jsonArray = new JSONObject(result).getJSONArray("data");
				} catch (JSONException e) {
					Log.v("", " " + result);
					e.printStackTrace();
					delegate.socialNetworkAuthorised(socialUser);
				}

				if (jsonArray != null) {
					try {
						List<String> friends = new ArrayList<String>();
						for (int i = 0; i < jsonArray.length(); i++) {
							JSONObject jsonObject = jsonArray.getJSONObject(i);
							friends.add(jsonObject.getString("id"));
						}
						delegate.socialNetworkFriendsGot(friends);
						delegate.socialNetworkAuthorised(socialUser);
					} catch (JSONException e) {
						e.printStackTrace();
					}
				} else {
					delegate.socialNetworkAuthorised(socialUser);
				}
			}
		});
		request.executeAsync();
	}

	@Override
	public void logout() {
		LoginManager.getInstance().logOut();
	}
	
}
