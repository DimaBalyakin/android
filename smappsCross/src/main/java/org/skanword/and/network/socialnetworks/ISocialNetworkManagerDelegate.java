package org.skanword.and.network.socialnetworks;

import java.util.List;

import org.skanword.and.network.socialnetworks.SocialNetworkManager.SocialUser;

public interface ISocialNetworkManagerDelegate {
	public void socialNetworkAuthorised(SocialUser user);
	public void socialNetworkAuthoriseFailed(boolean declined);
	public void socialNetworkFriendsGot(List<String> friends);
}
