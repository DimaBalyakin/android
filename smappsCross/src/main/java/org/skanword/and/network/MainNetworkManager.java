package org.skanword.and.network;

import java.io.IOException;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.HttpHostConnectException;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.skanword.and.R;
import org.skanword.and.SmappsScanwords;
import org.skanword.and.datamanager.MainDataManager;
import org.skanword.and.datamanager.NewsDataManager.News;
import org.skanword.and.datamanager.RatingDataManager.RatingInfo;
import org.skanword.and.datamanager.ShopDataManager.Shop;
import org.skanword.and.datamanager.SkanwordsDataManager;
import org.skanword.and.datamanager.SkanwordsDataManager.TaskListDirrectionType;
import org.skanword.and.datamanager.SkanwordsDataManager.TaskListType;
import org.skanword.and.datamanager.UserUpdatesManager;
import org.skanword.and.etc.TopActivityManager;
import org.skanword.and.etc.TutorialManager;
import org.skanword.and.etc.TutorialManager.TutorialStep;
import org.skanword.and.etc.Utils;
import org.skanword.and.inappbilling.InAppManager;
import org.skanword.and.menu.AuthorisationAcceptActivity;
import org.skanword.and.menu.MainMenuActivity;
import org.skanword.and.network.MainNetworkManager.RequestResponse.StatusType;
import org.skanword.and.network.socialnetworks.FacebookManager;
import org.skanword.and.network.socialnetworks.ISocialNetworkManagerDelegate;
import org.skanword.and.network.socialnetworks.OdnoklassnikiManager;
import org.skanword.and.network.socialnetworks.SocialNetworkManager;
import org.skanword.and.network.socialnetworks.SocialNetworkManager.SocialNetwork;
import org.skanword.and.network.socialnetworks.SocialNetworkManager.SocialUser;
import org.skanword.and.network.socialnetworks.VkontakteManager;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Looper;
import android.util.Base64;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import com.musselwhizzle.dispatcher.events.Event;
import com.musselwhizzle.dispatcher.events.EventListener;
import com.musselwhizzle.dispatcher.events.SimpleEvent;
import com.vk.sdk.VKSdk;

public class MainNetworkManager implements ISocialNetworkManagerDelegate {

	// Info bit masks
	public static final int INFO_NEWS = 1<<0; // 0x01
	public static final int INFO_SHOP = 1<<1; // 0x02
	public static final int INFO_RATING = 1<<2; // 0x04
	
	// Cross Data bit masks
	public static final int SKAN_DATA_PROGRESS = 1<<0; // 0x01
	public static final int SKAN_DATA_DATA = 1<<1; // 0x02
	
	public static final String SERVER_URL_PROD = "https://skanword.org/rest/";
	public static final String SERVER_URL_TEST = "https://skanword.smapps.net/rest/";
	public static final String USER_AUTH_PREFS_NAME = "UserAuthPrefs";
	public static final String VENDOR_KEY_PROD = "23599e3e23eb187798e2f6cedb44eb91";
	public static final String VENDOR_KEY_TEST = "d81b5f222115939769a88a690843d04c";
	public static final String EVENT_NETWORK_STATE_CHANGED = "EVENT_NETWORK_STATE_CHANGED";
	public static final String EVENT_NETWORK_STATE_CHANGED_ALL = "EVENT_NETWORK_STATE_CHANGED_ALL";
	

	private static MainNetworkManager instance = null;

	private final Gson gson = new Gson();
	
	private UserAuth mUserAuth = null;
	private SocialNetworkManager mSocialNetworkManager = null;
	private SocialUser mLastSocialUser = null;
	private ProgressDialog mWaitingProgressDialog;
	private Activity mCurrentAuthActivity = null;
	
	private String mStoreName = null;
	private boolean mInAuthProcess = false;
	private boolean mShowingConnectionAlert = false;
	private boolean mNetworkAvailable = false;
	private boolean mTrackPayment = false;
	private Map<String, HttpPost> mCurrentRequests = Collections.synchronizedMap(new HashMap<String, HttpPost>());
	static Integer mParsingMutex = 1;
	private boolean isInited = false;

	protected MainNetworkManager() {

		if (mNetworkStateChangedEventListener != null)
			SmappsScanwords.getEventsDispatcher().addListener(
					MainNetworkManager.EVENT_NETWORK_STATE_CHANGED,
					mNetworkStateChangedEventListener);
	}

	// Needed function because VKSdk can be initialized only in application onCreate
	public void onCreate(Context context) {
		try {
			VKSdk.initialize(context.getApplicationContext());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private EventListener  mNetworkStateChangedEventListener = new EventListener() {
		@Override
		public void onEvent(Event event) {
			updateNetworkConnectionAvailability();
		}
	}; 
	public static boolean isRelese() {
		return true;
	}

	public static String getVendorKey() { 
		if (isRelese()) {
			return VENDOR_KEY_PROD;
		} else 
			return VENDOR_KEY_TEST;
	}

	public static String getserverUrl() { 
		if (isRelese()) {
			return SERVER_URL_PROD;  
		} else
			return SERVER_URL_TEST;
	}

	public void clear() {
		if (mSocialNetworkManager != null) 
			mSocialNetworkManager.clearSocialNetworkManager();
	}

	public static MainNetworkManager getInstance() {
		if (instance == null) {
			instance = new MainNetworkManager();
		}
		if (!instance.isInited) {
			instance.init();
		}
		return instance;
	}

	public Activity getCurrentAuthActivity() {
		return mCurrentAuthActivity;
	}

	public void setCurrentAuthActivity(Activity currentAuthActivity) {
		this.mCurrentAuthActivity = currentAuthActivity;
	}

	private void init() {
		if (MainMenuActivity.instance != null && MainMenuActivity.instance.hasPermissions()) {
			isInited = true;
			mUserAuth = getLocalUserAuth();
			updateNetworkConnectionAvailability();
		}
	}

	public void acceptAuthorisation() {
		acceptAuthorisation(null);
	}

	public void showWaitingDialog(Activity activity, final String message) {
		activity = TopActivityManager.get().getTopActivity();
		if (activity == null || mWaitingProgressDialog != null)
			return;
		try {
			mWaitingProgressDialog = ProgressDialog.show(activity,
					"Подождите, пожалуйста...", message, false);
		} catch (Exception e) {
			
		}
	}

	public void hideWaitingDialog(Activity activity) {
		Runnable hideWaitingDialogRunnable = new Runnable() {

			@Override
			public void run() {
				if (mWaitingProgressDialog == null)
					return;
				mWaitingProgressDialog.dismiss();
				mWaitingProgressDialog = null;

			}
		};
		if (Looper.myLooper() == Looper.getMainLooper() || activity == null) {
			MainMenuActivity.instance.runOnUiThread(hideWaitingDialogRunnable);
		} else {
			activity.runOnUiThread(hideWaitingDialogRunnable);
		}
	}

	public boolean isUserAuthorised() {
		if (mUserAuth != null)
			return mUserAuth.getSocialNetwork() != SocialNetwork.SOCIAL_NETWORK_NO;
		return false;
	}

	public String getUserName() {
		if (mUserAuth == null) {
			return "Неизвестно";
		} else if (isUserAuthorised()) {
			return "Some Name";
		} else {
			return mUserAuth.getUserId() + "";
		}
	}

	public UserAuth getUserAuth() {
		return mUserAuth;
	}

	public void saveAuthData() {
		SharedPreferences.Editor editor = SmappsScanwords.getAppSharedPreferencesEditor(USER_AUTH_PREFS_NAME);
		editor.putString("user_id", mUserAuth.getUserId());
		editor.putString("user_auth_key", mUserAuth.getAuthKey());
		editor.putString("vendor_key", mUserAuth.getVendorId());
		editor.putString("user_social_id", mUserAuth.getUserSocialId());
		editor.putInt("social_network", mUserAuth.getSocialNetwork().ordinal());
		if (mLastSocialUser != null) {
			editor.putString("full_name", mLastSocialUser.getFirstName() + " "
					+ mLastSocialUser.getSecondName());
			editor.putString("avatar_url", mLastSocialUser.getAvatarUrl());
			mUserAuth.setAvatarUrl(mLastSocialUser.getAvatarUrl());
			mUserAuth.setFullName(mLastSocialUser.getFirstName() + " "
					+ mLastSocialUser.getSecondName());
		}
		editor.commit();
	}

	public UserAuth getLocalUserAuth() {
		SharedPreferences settings = SmappsScanwords.getAppSharedPreferences(USER_AUTH_PREFS_NAME);
		String userId = settings.getString("user_id", null);
		String userAuthKey = settings.getString("user_auth_key", null);
		String vendorKey = settings.getString("vendor_key", null);
		String fullName = settings.getString("full_name", null);
		String avatar = settings.getString("avatar_url", null);
		String socialId = settings.getString("user_social_id", null);
		SocialNetwork socialNetwork = SocialNetwork.values()[settings.getInt(
				"social_network", SocialNetwork.SOCIAL_NETWORK_NO.ordinal())];
		if (userId == null)
			return null;

		UserAuth userAuth = new UserAuth(vendorKey, userId, socialId,
				userAuthKey, socialNetwork);
		userAuth.setFullName(fullName);
		userAuth.setAvatarUrl(avatar);
		return userAuth;
	}


	public boolean checkAuth(Activity activity) {
		if (mUserAuth == null) {
			mUserAuth = getLocalUserAuth(); // check local storage
			if (mUserAuth == null) {
				mUserAuth = login(null, activity, null); // login through server
				if (mUserAuth == null)
					return false;
				saveAuthData();
			}
		}
		return true;
	}
	public boolean userLoggedIn() {
		return mUserAuth != null;
	}
	public abstract class RequestRunnable implements Runnable {
		protected RequestCompletionBlock mCompleteBlock;
		
		public RequestRunnable() {
			super();
			mCompleteBlock = null;
		}

		public RequestRunnable(RequestCompletionBlock completeBlock) {
			super();
			this.mCompleteBlock = completeBlock;
		}

	}
	public void requestConfigExecWithCompleteBlock(RequestCompletionBlock completeBlock) {
		SmappsScanwords.getExecutorService().execute(new RequestRunnable(completeBlock) {
			@Override
			public void run() {
				requestConfig(mCompleteBlock);
			}
		});
	}
	public void requestConfigExec() {
		requestConfigExecWithCompleteBlock(null);
	}
	public ConfigObject requestConfig(RequestCompletionBlock completeBlock) {
		ConfigObject config = null;
		boolean complete = false;
		Log.v("SkanwordsFunc", "requestConfig");
		if (checkAuth(null) && hasNetworkConnection()) {
			JsonElement o = makePostRequestForProtocol("config", addParamsWithUpdates(null)).getJsonResponse();
			if (o != null) {
				o =	o.getAsJsonObject().get("settings");
				if (o != null)
					config = gson.fromJson(o, ConfigObject.class);

				if (config != null) {
					mTrackPayment = config.paymentTrack();
				}
				
				MainDataManager.getInstance().configDataReceived(config);
				complete = true;
			}
		}
		if (completeBlock != null)
			completeBlock.complete(complete);
		return config;
	}
	
	public void loginInSocialNetwork(final SocialNetwork socialNetwork,
			final Activity activity, boolean start) {
		setCurrentAuthActivity(activity);
		if (start && mInAuthProcess) {
			return;
		}
		if (start) {
			if (!hasNetworkConnection(activity))
				return;
			mInAuthProcess = true;
		}
		if (mSocialNetworkManager != null) {
			mSocialNetworkManager.logout();
			mSocialNetworkManager.clearSocialNetworkManager();
		}
		mSocialNetworkManager = null;
		switch (socialNetwork) {
		case SOCIAL_NETWORK_OK:
			mSocialNetworkManager = new OdnoklassnikiManager();
			break;
		case SOCIAL_NETWORK_FB:
			mSocialNetworkManager = new FacebookManager();
			break;
		case SOCIAL_NETWORK_VK:
			mSocialNetworkManager = new VkontakteManager();
			break;
		case SOCIAL_NETWORK_NO:
			mLastSocialUser = null;
			break;

		default:
			break;
		}
		if (mSocialNetworkManager != null) {
			mSocialNetworkManager.initSocialNetworkManager(this,
					SmappsScanwords.getContext().getApplicationContext());
			mSocialNetworkManager.authorise(activity);
		} else {
			acceptAuthorisation(activity);
		}

		Log.v("", "need login to " + socialNetwork);
	}

	public void acceptAuthorisation(final Activity activity, final boolean throughWindow) {
		if (mWaitingProgressDialog == null) {
			showWaitingDialog(getCurrentAuthActivity(), "Идет авторизация...");
		}
		SmappsScanwords.getExecutorService().execute(new Runnable() {
			@Override
			public void run() {
				requestSync(new RequestCompletionBlock() {
					
					@Override
					public void complete(boolean success) {
						if (success) {
							UserAuth newAuth = login(mLastSocialUser, mCurrentAuthActivity, null);
							if (newAuth == null || (mUserAuth != null && newAuth.getUserId().equals(mUserAuth.getUserId()))) {
								socialNetworkAuthoriseFailed(false);
								hideWaitingDialog(activity);
								mInAuthProcess = false;
								return;
							}
							mUserAuth = newAuth;
							saveAuthData();
							hideWaitingDialog(activity);
							mInAuthProcess = false;
							MainDataManager.getInstance().userDataReceived(mUserAuth.getLoginUserState());
							requestConfig(null);
							SmappsScanwords.getEventsDispatcher().dispatchEvent(new SimpleEvent(SkanwordsDataManager.UPDATE_ISSUES_LIST));
						} else {
							socialNetworkAuthoriseFailed(false);
							hideWaitingDialog(activity);
							mInAuthProcess = false;
						}
					}
				});
				
			}
		});
	}
	public void acceptAuthorisation(final Activity activity) {
		acceptAuthorisation(activity, false);
	}

	public UserAuth login(SocialUser socialUser, Activity activity, RequestCompletionBlock completeBlock) { 
		UserAuth result = null;
		boolean complete = false;
		if (hasNetworkConnection(activity)) {
			List<NameValuePair> params = new ArrayList<NameValuePair>(4);
			params.add(new BasicNameValuePair("vendor_id",
					SmappsScanwords.getVendorId()));
			params.add(new BasicNameValuePair("google_advertising_id", SmappsScanwords.instance.getAdvertisingId()));
			params.add(new BasicNameValuePair("google_android_id", Utils.getAndroidId()));
			params.add(new BasicNameValuePair("google_device_id", Utils.getDeviceId()));
			
			params.add(new BasicNameValuePair("vendor_key", Utils
					.md5(getNetId() + "_" + SmappsScanwords.getVendorId() + "_"
							+ getVendorKey())));
			if (socialUser != null) {
				params.add(new BasicNameValuePair("suid", socialUser
						.getUid()));
				params.add(new BasicNameValuePair("access_token",
						socialUser.getToken()));
				params.add(new BasicNameValuePair("social_id",
						socialUser.getSocialId()));
			}
			if (mUserAuth == null) {
				SharedPreferences settings = SmappsScanwords.getContext()
						.getSharedPreferences("referrer", 0);
				String referrer = settings.getString("referrer", null);
				Log.v("SkanwordsFunc", "referrer - " + referrer);
				if (referrer != null) {
					params.add(new BasicNameValuePair("referrer",
							referrer));
				}
			}
			JsonElement o = makePostRequestForProtocol("login", params, false).getJsonResponse();
			if (o != null) {
				JsonObject authJsonObject = o.getAsJsonObject().get("auth").getAsJsonObject();
				result = new UserAuth(SmappsScanwords.getVendorId(),
						authJsonObject.get("user_id").getAsString(),
						socialUser != null ? socialUser.getUid() : null,
						authJsonObject.get("auth_key").getAsString(),
						socialUser == null ? SocialNetwork.SOCIAL_NETWORK_NO
								: SocialNetworkManager
										.getSocialNetworkfromName(socialUser
												.getSocialId()));
				
				o = o.getAsJsonObject()
						.get("user");
				if (o != null) {
					UserObject userObj = gson.fromJson(o, UserObject.class);
					result.setLoginUserState(userObj);
					if (userObj.getScoreScanwords() == 0 && TutorialManager.getInstance().currentTutorialStep() == TutorialStep.TUTORIAL_DISABLED) {
						TutorialManager.getInstance().setTutorialStep(TutorialStep.TUTORIAL_START);
					} else {
						TutorialManager.getInstance().setTutorialStep(TutorialStep.TUTORIAL_COMPLETE);
					}
				}
				
				complete = true;
			} else {
				hideWaitingDialog(MainMenuActivity.instance);
			}
		} else {
			hideWaitingDialog(MainMenuActivity.instance);
		}
		if (!complete) {
			requestError(activity);
		}

		if (completeBlock != null)
			completeBlock.complete(complete);
		return result;
		
	}
	public void requestSyncExecWithCompleteBlock(RequestCompletionBlock completeBlock) {
		SmappsScanwords.getExecutorService().execute(new RequestRunnable(completeBlock) {
			@Override
			public void run() {
				requestSync(mCompleteBlock);
			}
		});
	}
	
	public void requestSyncExec() {
		requestSyncExecWithCompleteBlock(null);
	}
	
	public void requestSync(RequestCompletionBlock completeBlock) {
		boolean complete = false;
		if (checkAuth(null) && hasNetworkConnection()) {
			JsonElement o = makePostRequestForProtocol("sync",
					addParamsWithUpdates(null)).getJsonResponse();
			if (o != null) {
				complete = true;
			}
		}
		if (completeBlock != null)
			completeBlock.complete(complete);
	}

	public void requestInfo(final int mask, final Activity activity) {
		requestInfoWithCompleteBlock(mask, activity, null);
	}
	public void requestInfoWithCompleteBlock(final int mask, final Activity activity, RequestCompletionBlock completeBlock) {
		SmappsScanwords.getExecutorService().execute(new RequestRunnable(completeBlock) {
			@Override
			public void run() {
				requestInfo(mCompleteBlock, mask, activity);
			}
		});
	}
	public void requestInfo(RequestCompletionBlock completeBlock, int mask, final Activity activity) {
		boolean complete = false;
		if (checkAuth(activity) && hasNetworkConnection()) {
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			
			params.add(new BasicNameValuePair("mask", mask + ""));
			
			params.add(new BasicNameValuePair("news_hash", MainDataManager.getInstance().getNewsDataManager().getNewsHash()));
			params.add(new BasicNameValuePair("shop_hash", MainDataManager.getInstance().getShopDataManager().getShopHash()));
			params.add(new BasicNameValuePair("rating_hash", MainDataManager.getInstance().getRatingDataManager().getRatingHash()));
			
			if (MainDataManager.getInstance().getUserFriends() != null
					&& MainDataManager.getInstance().getUserFriends().size() > 0) {
				String delim = "";
				StringBuilder sb = new StringBuilder();
				for (String i : MainDataManager.getInstance().getUserFriends()) {
					sb.append(delim).append(i);
					delim = ",";
				}
				params.add(new BasicNameValuePair("friends", sb
						.toString()));
			}
			JsonElement o = makePostRequestForProtocol("info",
					addParamsWithUpdates(params)).getJsonResponse();
			if (o != null && o.getAsJsonObject().get("info") != null) {
				o = o.getAsJsonObject().get("info");
				JsonElement shopData =	o.getAsJsonObject().get("shop");
				if (shopData != null) {
					String hash = shopData.getAsJsonObject().get("hash").getAsString();
					Shop shop = gson.fromJson(shopData.getAsJsonObject().get("data"), Shop.class);
					MainDataManager.getInstance().updateShopData(shop, hash);
				}
				JsonElement newsData = o.getAsJsonObject().get("news");
				if (newsData != null) {
					String hash = newsData.getAsJsonObject().get("hash").getAsString();
					List<News> news = gson.fromJson(newsData.getAsJsonObject().get("data"), new TypeToken<List<News>>() {
					}.getType());
					MainDataManager.getInstance().updateNewsData(news, hash);
				}
				JsonElement ratingData = o.getAsJsonObject().get("rating");
				if (ratingData != null) {
					String hash = ratingData.getAsJsonObject().get("rating_hash").getAsString();
					RatingInfo rating = gson.fromJson(ratingData.getAsJsonObject().get("data"), RatingInfo.class);
					MainDataManager.getInstance().updateRatingData(rating, hash);
				}
			}
			complete = (o != null);
		}
		if (!complete) {
			requestError(activity);
		}
		if (completeBlock != null)
			completeBlock.complete(complete);
	}

	public void requestIssuesListExec(final TaskListType listType, final TaskListDirrectionType dirrectionType, final Activity activity) {
		requestIssuesListExecWithCompleteBlock(listType, dirrectionType, null, activity);
	}
	public void requestIssuesListExecWithCompleteBlock(final TaskListType listType, final TaskListDirrectionType dirrectionType, final RequestCompletionBlock completeBlock, final Activity activity) {
		SmappsScanwords.getExecutorService().execute(new RequestRunnable(completeBlock) {
			@Override
			public void run() {
				requestIssuesList(listType, dirrectionType, mCompleteBlock, activity);
			}
		});
	}
	public void requestIssuesList(TaskListType listType, TaskListDirrectionType dirrectionType, RequestCompletionBlock completeBlock, Activity activity) {
		boolean complete = false;
		RequestResponse response = null;
		if (userLoggedIn() && hasNetworkConnection()) {
			List<NameValuePair> params = addParamsWithUpdates(null);
			params.add(new BasicNameValuePair("page", SkanwordsDataManager.dirrectionStringRepresentation(dirrectionType)));
			params.add(new BasicNameValuePair("type", SkanwordsDataManager.listTypeStringRepresentation(listType)));
			if (dirrectionType != TaskListDirrectionType.TOP_DIRRECTION) {
				params.add(new BasicNameValuePair("set_id_first", MainDataManager.getInstance().getSkanwordsDataManager().getLatesIssueIdOfType(listType) + ""));
				params.add(new BasicNameValuePair("set_id_last", MainDataManager.getInstance().getSkanwordsDataManager().getOldestIssueIdOfType(listType) + ""));
			}
			Log.v("SkanwordsSpeed", "start requestIssuesList " + listType);
			response = makePostRequestForProtocol("cross_list_data", params);
			Log.v("SkanwordsSpeed", "start parse requestIssuesList " + listType);
			synchronized (mParsingMutex) {
				if (response != null) {
					SetsListObject listObject = gson.fromJson(response.getJsonResponse(), SetsListObject.class);
					if (listObject != null) {
						listObject.setListType(listType);
						listObject.setDirrectionType(dirrectionType);
					}
					MainDataManager.getInstance().getSkanwordsDataManager().setsListReceived(listObject);
				}
			}
			Log.v("SkanwordsSpeed", "finish requestIssuesList " + listType);
			complete = response.getStatusType() == StatusType.COMPLETE || response.getStatusType() == StatusType.ABORTED;
		}
		Log.v("SkanwordsFunc", "MainNetworkManager requestIssuesList " + complete);
		if (!complete) {
			requestError(activity);
		}
		if (completeBlock != null)
			completeBlock.complete(complete);
	}
	
	public SkanwordsSetDataObject skanwordSetGet(Activity activity, List<Map<String, Integer>> setIds) {
		SkanwordsSetDataObject result = null;
		if (checkAuth(activity) && hasNetworkConnection(activity)) {
			String json = gson.toJson(setIds);
			Log.v("", "json cross set data  "+ json);
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("set_ids",Base64
					.encodeToString(json.getBytes(), Base64.DEFAULT)));
			JsonElement o = makePostRequestForProtocol("cross_set_data", params).getJsonResponse();
			
			
			if (o != null) {
				result = gson.fromJson(o, SkanwordsSetDataObject.class);
				result.setRequestdSetIds(setIds);
			} else {
				requestError();
			}
		}
		return result;
	}

	public void sendPushToken(String token) {
		if (checkAuth(null) && hasNetworkConnection()) {
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("push_token", token));
			makePostRequestForProtocol("push_token_set", params);
		}
	}

	private UserObject purchaseSend(boolean restore, Activity activity) {
		UserObject userResult = null;
		List<String> purchases = new ArrayList<String>(
				!restore ? MainDataManager.getInstance().getPurchasesData()
						.getPurchases() : MainDataManager.getInstance()
						.getPurchasesData().getPurchasesRestored());
		if (checkAuth(activity) && hasNetworkConnection(activity) && purchases.size() > 0) {
			
			List<NameValuePair> params = addParamsWithUpdates(null);
			
			params.add(new BasicNameValuePair("restored", "" + Integer.valueOf(restore ? 1 : 0)));
			params.add(new BasicNameValuePair("receipts", gson
					.toJson(purchases)));
			
			JsonElement parsedResult = makePostRequestForProtocol("payment", params, true).getJsonResponse();
			
			JsonElement o = null;
			if (parsedResult != null)
				o = parsedResult.getAsJsonObject()
				.get("user");
			
			if (o != null)
				userResult = gson.fromJson(o, UserObject.class);
			
			MainDataManager.getInstance().getPurchasesData()
					.clearPurcheses(purchases);
			MainDataManager.getInstance().storePurchases();
		}
		return userResult;
	}

	public String getNetId() {
		if (mStoreName != null) {
			return mStoreName;
		}
		String storeName = SmappsScanwords.getAppSharedPreferences(USER_AUTH_PREFS_NAME).getString("store_name", null);
		if (storeName != null) {
			Log.v("", "store name restored " + storeName);
			mStoreName = storeName;
			return mStoreName;
		}

		String name = InAppManager.getInstance().getStoreName();
		Log.v("", "  storename name " + name);
		String[] parts = name.split("\\.");
		if (parts.length > 1) {
			storeName = parts[1];
		} else if (parts.length > 0) {
			storeName = parts[0];
		}
		if (storeName != null) {
			Log.v("", "store name saved " + storeName);
			SmappsScanwords.getAppSharedPreferencesEditor(USER_AUTH_PREFS_NAME).putString("store_name", storeName).commit();
			mStoreName = storeName;
		}

		return mStoreName;
	}

	public RequestResponse makePostRequestForProtocol(String protocol,  List<NameValuePair> params) {
		return makePostRequestForProtocol(protocol, params, true);
	}
	
	public RequestResponse makePostRequestForProtocol(String protocol,  List<NameValuePair> params, boolean addCommonParams) {
		for (NameValuePair nameValuePair : params) {
			Log.v("SkanwordsNetwork", "key - " + nameValuePair.getName() + "  value - " + nameValuePair.getValue());
		}
		String jsonStringResponse = null;
		JsonElement o = null;
		StatusType status = StatusType.FAILED;
		List<NameValuePair> requestParams;
		if (addCommonParams) {
			requestParams = genCommonRequestParams();
			if (params != null)
				requestParams.addAll(params);
		} else {
			requestParams = params;
		}
		try {
			jsonStringResponse = makePostRequestForSuffix(protocol, requestParams);
			Log.v("SkanwordsNetwork" ,"makePostRequestForProtocol - " + protocol + " \nresponse - " + jsonStringResponse);
			o = parseRequestResult(jsonStringResponse, addCommonParams);
			status = StatusType.COMPLETE;
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		}catch (SocketException e) {
			e.printStackTrace();
			status = StatusType.ABORTED;
		} catch (OutOfMemoryError e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JsonSyntaxException e) {
			status = StatusType.NOT_PARSED;
			illegalJSON(jsonStringResponse);
		}
		return new RequestResponse(status, o);
	}
	
	public String makePostRequestForSuffix(String suffix,
			List<NameValuePair> requestParams)
			throws ClientProtocolException, IOException, OutOfMemoryError, HttpHostConnectException {
		Log.v("SkanwordsFunc", "make request for url suffix " + suffix);
		String response = null;
		final String targetURL = getserverUrl() + suffix + "/" + getNetId();
		HttpPost httppost = new HttpPost(targetURL);
		httppost.setEntity(new UrlEncodedFormEntity(requestParams));
		
		DefaultHttpClient client = MainNetworkManager.getThreadSafeClient();
		if (mCurrentRequests.containsKey(targetURL)) {
			Log.v("SkanwordsFunc", "aborting previous request for url suffix " + suffix);
			try {

				mCurrentRequests.get(targetURL).abort();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		if (!suffix.equals("cross_set_data")) {
			mCurrentRequests.put(targetURL, httppost);
		}

		try {
			Log.v("SkanwordsSpeed", "start request for url suffix " + suffix);
			HttpResponse response1 = client.execute(httppost);
			Log.v("SkanwordsSpeed", "finish request for url suffix " + suffix);
			response = EntityUtils.toString(response1.getEntity());
		} catch (Exception e) {
			Log.v("Error", e.toString());
		} finally {
			mCurrentRequests.remove(targetURL);
		}

		return response;
	}
	
	public static DefaultHttpClient getThreadSafeClient() {

		DefaultHttpClient client = new DefaultHttpClient();

		ClientConnectionManager mgr = client.getConnectionManager();

		HttpParams params = client.getParams();

		client = new DefaultHttpClient(new ThreadSafeClientConnManager(params,

		mgr.getSchemeRegistry()), params);

		return client;

	}
	public List<NameValuePair> genCommonRequestParams() {
		PackageInfo pInfo = null;
		try {
			pInfo = SmappsScanwords
					.getContext()
					.getPackageManager()
					.getPackageInfo(
							SmappsScanwords.getContext().getPackageName(), 0);
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		} catch (NullPointerException e) {

		}
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		if (mUserAuth != null) {
			nameValuePairs.add(new BasicNameValuePair("auth_key", mUserAuth
					.getAuthKey()));
			nameValuePairs.add(new BasicNameValuePair("user_id", mUserAuth
					.getUserId()));
			nameValuePairs.add(new BasicNameValuePair("vendor_id", mUserAuth
					.getVendorId()));
			nameValuePairs.add(new BasicNameValuePair("last_sync_type",
					connectionType()));
			nameValuePairs.add(new BasicNameValuePair("vendor_key", Utils
					.md5(getNetId() + "_" + mUserAuth.getVendorId() + "_"
							+ getVendorKey())));
		}
		HashMap<String, String> deviceInfo = new HashMap<String, String>();
		deviceInfo.put("device_system_version",
				System.getProperty("os.version"));
		deviceInfo.put("device", System.getProperty(android.os.Build.DEVICE));
		deviceInfo.put("device_model",
				System.getProperty(android.os.Build.MODEL));
		deviceInfo.put("device_product",
				System.getProperty(android.os.Build.PRODUCT));

		if (pInfo != null) {
			String version = pInfo.versionName;
			int versionCode = pInfo.versionCode;
			deviceInfo.put("app_version", version);
			deviceInfo.put("app_build", "" + versionCode);
		}

		nameValuePairs.add(new BasicNameValuePair("google_advertising_id", SmappsScanwords.instance.getAdvertisingId()));
		nameValuePairs.add(new BasicNameValuePair("google_android_id", Utils.getAndroidId()));
		nameValuePairs.add(new BasicNameValuePair("google_device_id", Utils.getDeviceId()));
		nameValuePairs.add(new BasicNameValuePair("device_info", Base64
				.encodeToString(gson.toJson(deviceInfo).getBytes(),
						Base64.DEFAULT)));
		return nameValuePairs;
	}
	private void illegalJSON(String string) {
		Log.v("", "illegal json " + string);

		final Activity callerActivity = TopActivityManager.get().getTopActivity();
		if (callerActivity == null)
			return;
		callerActivity.runOnUiThread(new Runnable() {
			public void run() {
				AlertDialog.Builder builder = new AlertDialog.Builder(
						callerActivity,
						R.style.Theme_Scanword_style_dialog);
				builder.setMessage("Не удалось расшифровать ответ от сервера.")
						.setPositiveButton("Ок",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int id) {
									}
								}).setTitle("Ошибка");
				AlertDialog dialog = builder.create();
				dialog.show();
			}
		});
	}

	public String connectionType() {
		ConnectivityManager cm = (ConnectivityManager) SmappsScanwords
				.getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo[] netInfo = cm.getAllNetworkInfo();
		for (NetworkInfo ni : netInfo) {
			if (ni.getTypeName().equalsIgnoreCase("WIFI"))
				if (ni.isConnected())
					return "wi-fi";
			if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
				if (ni.isConnected())
					return "mobile_internet";
		}
		return "no_sync_type";
	}

	public boolean hasNetworkConnection() {
		return hasNetworkConnection(null);
	}
	public boolean hasNetworkConnection(final Activity alertActivity) {
		if (!mNetworkAvailable && alertActivity != null) {
			alertActivity.runOnUiThread(new Runnable() {
				public void run() {
					if (mShowingConnectionAlert)
						return;
					mShowingConnectionAlert = true;
					Log.v("", "show network availability error");
					AlertDialog.Builder builder = new AlertDialog.Builder(
							alertActivity, R.style.Theme_Scanword_style_dialog);
					builder.setMessage("Отсутствует соединение к сети Интернет.")
							.setPositiveButton("Ок",
									new DialogInterface.OnClickListener() {
										public void onClick(
												DialogInterface dialog, int id) {
											mShowingConnectionAlert = false;
										}
									})
							.setOnCancelListener(new OnCancelListener() {

								@Override
								public void onCancel(DialogInterface dialog) {
									mShowingConnectionAlert = false;
								}
							}).setTitle("Ошибка");
					// Create the AlertDialog object and return it
					AlertDialog dialog = builder.create();
					dialog.show();
				}
			});
		}
		return mNetworkAvailable;
	}
	public void updateNetworkConnectionAvailability() {
		boolean haveConnectedWifi = false;
		boolean haveConnectedMobile = false;

		ConnectivityManager cm = (ConnectivityManager) SmappsScanwords
				.getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo[] netInfo = cm.getAllNetworkInfo();
		for (NetworkInfo ni : netInfo) {
			if (ni.getTypeName().equalsIgnoreCase("WIFI"))
				if (ni.isConnected())
					haveConnectedWifi = true;
			if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
				if (ni.isConnected())
					haveConnectedMobile = true;
		}
		boolean result = false;
		
		if (MainDataManager.getInstance().getOptions().isOnlyWifi()) {
			result = haveConnectedWifi;
		} else
			result = haveConnectedWifi || haveConnectedMobile;
		
		mNetworkAvailable = result;
	}

	private void requestError(final Activity activity) {
		if (activity == null)
			return;
		activity.runOnUiThread(new Runnable() {
			public void run() {
				AlertDialog.Builder builder = new AlertDialog.Builder(
						activity,
						R.style.Theme_Scanword_style_dialog);
				builder.setMessage(
						"Не удалось поключится к серверу. Попробуйте еще раз.")
						.setPositiveButton("Ок",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int id) {
										
									}
								}).setTitle("Ошибка");
				// Create the AlertDialog object and return it
				AlertDialog dialog = builder.create();
				dialog.show();
			}
		});
	}
	private void requestError() {
		requestError(TopActivityManager.get().getTopActivity());
	}
	
	
	synchronized private JsonElement parseRequestResult(String stringJSONResult, boolean checkResponse) {
		JsonParser parser = new JsonParser();
		JsonElement parsedResult;
		if (stringJSONResult != null) {
			parsedResult = parser.parse(stringJSONResult);
		} else {
			parsedResult = null;
		}
		
		if (checkResponse && parsedResult != null)
			checkServerResponse(parsedResult);
		
		return parsedResult;
	}
	private void checkServerResponse(JsonElement parsedResult) {
		checkResponseForPaymentMessage(parsedResult);
		checkResponseForServerMessage(parsedResult);
		checkResponseForUpdatesHashes(parsedResult);
		checkResponseForUserData(parsedResult);
		checkResponseForDailyBonus(parsedResult);
	}
	private void checkResponseForDailyBonus(JsonElement parsedResult) {
		parsedResult = parsedResult.getAsJsonObject()
				.get("bonus_hourly");
		if (parsedResult != null)
			MainDataManager.getInstance().dailyBonusObjectRecieved(gson.fromJson(parsedResult, DailyBonusObject.class));
	}
	private void checkResponseForUserData(JsonElement parsedResult) {
		parsedResult = parsedResult.getAsJsonObject()
				.get("user");
		if (parsedResult != null)
			MainDataManager.getInstance().userDataReceived(gson.fromJson(parsedResult, UserObject.class));
	}
	private void checkResponseForUpdatesHashes(JsonElement parsedResult) {
		List<String> updates = null;
		JsonElement o = parsedResult.getAsJsonObject()
				.get("updates");
		if (o != null)
			updates = gson.fromJson(o, new TypeToken<List<String>>() {
			}.getType());

		UserUpdatesManager.getInstance().clearUpdateDataForHashes(updates);
	}

	private void checkResponseForPaymentMessage(JsonElement parsedResult) {
		final JsonElement o = parsedResult.getAsJsonObject()
				.get("payment_message");
		if (o != null && TopActivityManager.get().getTopActivity() != null) {
			TopActivityManager.get().getTopActivity().runOnUiThread(new Runnable() {
				public void run() {
					AlertDialog.Builder builder = new AlertDialog.Builder(
							TopActivityManager.get().getTopActivity(),
							R.style.Theme_Scanword_style_dialog);
					builder.setMessage(o.getAsString())
							.setPositiveButton("Ок",
									new DialogInterface.OnClickListener() {
										public void onClick(
												DialogInterface dialog, int id) {
										}
									}).setTitle("Поздравляем!");
					// Create the AlertDialog object and return it
					AlertDialog dialog = builder.create();
					dialog.show();
				}
			});
		}
	}

	private void checkResponseForServerMessage(JsonElement parsedResult)
			throws JsonSyntaxException {
		checkResponseForServerMessage(parsedResult, TopActivityManager.get().getTopActivity());
	}

	private void checkResponseForServerMessage(JsonElement parsedResult, final
			Activity callerActivity) throws JsonSyntaxException {
		final JsonObject o = parsedResult.getAsJsonObject()
				.get("message") != null ? parsedResult
				.getAsJsonObject().get("message").getAsJsonObject() : null;
		if (o != null && callerActivity != null) {
			callerActivity.runOnUiThread(new Runnable() {
				public void run() {
					AlertDialog.Builder builder = new AlertDialog.Builder(
							callerActivity,
							R.style.Theme_Scanword_style_dialog);
					builder.setMessage(o.get("text").getAsString())
							.setPositiveButton("Ок",
									new DialogInterface.OnClickListener() {
										public void onClick(
												DialogInterface dialog, int id) {
										}
									}).setTitle(o.get("title").getAsString());
					AlertDialog dialog = builder.create();
					dialog.show();
				}
			});
		}
	}

	public void purchaseComplete(final Activity activity, boolean completition) {

		if (MainDataManager.getInstance().getPurchasesData().getPurchases()
				.size() < 1
				&& MainDataManager.getInstance().getPurchasesData()
						.getPurchasesRestored().size() < 1) {
			
			if (activity != null) {
				activity.runOnUiThread(new Runnable() {
					public void run() {
						AlertDialog.Builder builder = new AlertDialog.Builder(
								activity, R.style.Theme_Scanword_style_dialog);
						builder.setMessage(
								"Вы не совершали покупок, которые можно восстановить.")
								.setPositiveButton("Ок",
										new DialogInterface.OnClickListener() {
											public void onClick(
													DialogInterface dialog,
													int id) {

											}
										});
						AlertDialog dialog = builder.create();
						dialog.show();
					}
				});
			}
			return;
		}
		if (activity != null)
			activity.runOnUiThread(new Runnable() {
				public void run() {
					showWaitingDialog(activity, "Идет синхронизация...");
				}
			});
		if (completition) {
			MainNetworkManager.getInstance().serverLog("payment_track", "5. purchaseComplete has data to send");
		}
		SmappsScanwords.getExecutorService().execute(
				new PurchaseCompleteRunnable(activity));
	}

	public void serverLog(String type, String log) {
		if (type.equals("payment_track") && mTrackPayment)
			SmappsScanwords.getExecutorService().execute(new ServerLogRunnable(type, log));
		
	}
	public class ServerLogRunnable implements Runnable {
		private final String mType;
		private final String mLog; 
		
		public ServerLogRunnable(String mType, String mLog) {
			super();
			this.mType = mType;
			this.mLog = mLog;
		}

		public void run() {
			try {
				List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
				nameValuePairs.add(new BasicNameValuePair("log", mLog));
				makePostRequestForSuffix(mType,
						nameValuePairs);
			} catch (Exception e) {
				
			}
		}
	}
	public void serverErrorLog(String type, String log) {
		SmappsScanwords.getExecutorService().execute(new ServerErrorLogRunnable(type, log));
		
	}
	public class ServerErrorLogRunnable implements Runnable {
		private final String mType;
		private final String mLog; 
		
		public ServerErrorLogRunnable(String mType, String mLog) {
			super();
			this.mType = mType;
			this.mLog = mLog;
		}

		public void run() {
			try {
				Log.v("", "send error log of type " + mType + "  with log :" + mLog);
				List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
				nameValuePairs.add(new BasicNameValuePair("error", mLog));
				nameValuePairs.add(new BasicNameValuePair("protocol", mType));
				makePostRequestForSuffix("app_error",
						nameValuePairs);
			} catch (Exception e) {
				
			}
		}
	}
	public class PurchaseCompleteRunnable implements Runnable {
		private Activity mCallerActivity = null;
 
		public PurchaseCompleteRunnable(Activity activity) {
			super();
			this.mCallerActivity = activity;
		}

		public void run() {

			UserObject user = purchaseSend(false, mCallerActivity);
			UserObject userRestoredData = purchaseSend(true, mCallerActivity);
			if (user != null || userRestoredData != null) {
				MainDataManager.getInstance().getShopDataManager().setShopHash("");
			}
			hideWaitingDialog(MainMenuActivity.instance);
		}
	}


	
	private List<NameValuePair> addParamsWithUpdates(List<NameValuePair> params) {
		if (params == null)
			params = new ArrayList<NameValuePair>();
		String upadetsString = gson.toJson(UserUpdatesManager.getInstance()
				.saveCurrentData());
		Log.v("SkanwordsNetwork", "addParamsWithUpdates  " + upadetsString);
		params.add(new BasicNameValuePair("updates", Base64
				.encodeToString(upadetsString.getBytes(), Base64.DEFAULT)));
		return params;
	}

	// / SOCIAL NETWORK METHODS

	public SocialUser getLastSocialUser() {
		return mLastSocialUser;
	}

	@Override
	public void socialNetworkAuthorised(final SocialUser user) {
		this.mLastSocialUser = user;
		showWaitingDialog(getCurrentAuthActivity(), "Идет синхронизация...");
		Runnable runnable = new Runnable() {

			@Override
			public void run() {
				List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
				nameValuePairs.add(new BasicNameValuePair("social_id", user
						.getSocialId() + "-" + user.getUid()));
				try {
					if (mUserAuth != null) {
						nameValuePairs.addAll(genCommonRequestParams());
						String userInfo = makePostRequestForSuffix("user_info",
								nameValuePairs);
						JSONObject json = new JSONObject(userInfo);
						if (json.getInt("bind") == 1) {
							showAuthorisationAcceptActivity(userInfo, user);
						} else {
							acceptAuthorisation(MainMenuActivity.instance);
						}
					} else {
						acceptAuthorisation(MainMenuActivity.instance);
					}
				} catch (ClientProtocolException e) {
					hideWaitingDialog(getCurrentAuthActivity());
					e.printStackTrace();
				} catch (IOException e) {
					hideWaitingDialog(getCurrentAuthActivity());
					e.printStackTrace();
				} catch (JSONException e) {
					hideWaitingDialog(getCurrentAuthActivity());
					e.printStackTrace();
				}

			}
		};
		SmappsScanwords.getExecutorService().execute(runnable);

	}

	public void onActivityResult(Activity activity, int requestCode,
			int resultCode, Intent data) {
		Log.v("", " main network onActivityResult  " + requestCode + "  "
				+ resultCode);
		if (requestCode == 102) {
			if (resultCode == Activity.RESULT_CANCELED) {
				mInAuthProcess = false;
			}
			hideWaitingDialog(activity);
		}
		if (mSocialNetworkManager != null) {
			mSocialNetworkManager.onActivityResult(activity, requestCode,
					resultCode, data);
		}
	}

	public void showAuthorisationAcceptActivity(final String userInfoJson,
			final SocialUser user) {

		getCurrentAuthActivity().runOnUiThread(new Runnable() {
			public void run() {
				Intent intent = new Intent(Intent.ACTION_EDIT, null,
						getCurrentAuthActivity(),
						AuthorisationAcceptActivity.class);
				intent.putExtra("user_info", userInfoJson);
				intent.putExtra("user_social", user);
				getCurrentAuthActivity().startActivityForResult(intent, 102);
			}
		});
	}

	@Override
	public void socialNetworkAuthoriseFailed(boolean declined) {
		Log.v("", " socialNetworkAuthoriseFailed  ");
		mInAuthProcess = false;
		if (declined)
			return;
		getCurrentAuthActivity().runOnUiThread(new Runnable() {
			public void run() {
				AlertDialog.Builder builder = new AlertDialog.Builder(
						getCurrentAuthActivity(),
						R.style.Theme_Scanword_style_dialog);
				builder.setMessage(
						"Авторизация не удалась, попробуйте еще раз.")
						.setPositiveButton("Ок",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int id) {

									}
								});
				AlertDialog dialog = builder.create();
				dialog.show();
			}
		});
	}

	@Override
	public void socialNetworkFriendsGot(List<String> friends) {
		MainDataManager.getInstance().setUserFriends(friends);
		// Log.v("", "  user friends  " + friends);
	}
	
	public interface RequestCompletionBlock {
		public void complete(boolean success);
	}
	public static class RequestResponse {
		
		private final StatusType mStatusType;
		private final JsonElement mJsonResponse;
		
		

		public RequestResponse(StatusType mStatusType, JsonElement mJsonResponse) {
			super();
			this.mStatusType = mStatusType;
			this.mJsonResponse = mJsonResponse;
		}

		

		public JsonElement getJsonResponse() {
			return mJsonResponse;
		}



		public StatusType getStatusType() {
			return mStatusType;
		}



		public enum StatusType {
			COMPLETE, ABORTED, NOT_PARSED, FAILED
		}
	}
}