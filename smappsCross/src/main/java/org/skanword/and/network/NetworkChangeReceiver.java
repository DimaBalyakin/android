package org.skanword.and.network;

import org.skanword.and.SmappsScanwords;
import org.skanword.and.menu.MainMenuActivity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.musselwhizzle.dispatcher.events.SimpleEvent;

public class NetworkChangeReceiver extends BroadcastReceiver {
	private static boolean firstTime = true;
	private static boolean hasConnection = false;
	@Override
	public void onReceive(Context context, Intent intent) {
		if (MainMenuActivity.instance.hasPermissions()) {
			if (hasConnection != isOnline(context) || firstTime) {
				firstTime = false;
				hasConnection = isOnline(context);
				SmappsScanwords.getEventsDispatcher().dispatchEvent(
						new SimpleEvent(MainNetworkManager.EVENT_NETWORK_STATE_CHANGED));
				SmappsScanwords.getEventsDispatcher().dispatchEvent(
						new SimpleEvent(MainNetworkManager.EVENT_NETWORK_STATE_CHANGED_ALL));
				Log.v("SkanwordsFunc", "NetworkChangeReceiver onReceive  " + isOnline(context));
			}
		}
	}
	public boolean isOnline(Context context) {

	    ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
	    NetworkInfo netInfo = cm.getActiveNetworkInfo();
	    return (netInfo != null && netInfo.isConnected());

	}
}
