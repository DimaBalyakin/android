package org.skanword.and.network;

import java.io.Serializable;
import java.util.Date;

import com.google.gson.annotations.SerializedName;

public class UserObject implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -1797044308341728223L;
	private int hints;
	@SerializedName("ads_off")
	private boolean adsOff;
	@SerializedName("vip_id")
	private String vipId;
	public void setVipId(String vipId) {
		this.vipId = vipId;
	}
	private long vip;
	@SerializedName("cross_user_score")
	private final int scoreScanwords;
	@SerializedName("cross_user_place")
	private final int placeScanwords;
	@SerializedName("show_hints_video_offer")
	private final boolean showHintsVideoOffer;
	@SerializedName("cross_save_hash")
	private final String scanwordsProgressHash;
	@SerializedName("cross_save")
	private final String scanwordsProgress;
	@SerializedName("updated_time")
	private long updatedTime = 0;
	@SerializedName("hint_video_count")
	private int hintsForVideo;
	
	public UserObject(int hints, boolean adsOff, String vipId, long vip,
			int scoreScanwords, int placeScanwords, boolean showHintsVideoOffer, String scanwordsProgressHash, String scanwordsProgress, long updatedTime, int hintsForVideo) {
		super();
		this.hints = hints;
		this.adsOff = adsOff;
		this.vipId = vipId;
		this.vip = vip;
		this.scoreScanwords = scoreScanwords;
		this.placeScanwords = placeScanwords;
		this.showHintsVideoOffer = showHintsVideoOffer;
		this.scanwordsProgressHash = scanwordsProgressHash;
		this.scanwordsProgress = scanwordsProgress;
		this.updatedTime = updatedTime;
		this.hintsForVideo = hintsForVideo;
	}
	
	public UserObject() {
		super();
		hints = 0;
		adsOff = false;
		vipId = "";
		vip = 0;
		scoreScanwords = 0;
		scanwordsProgress = "";
		placeScanwords = 0;
		showHintsVideoOffer = false;
		scanwordsProgressHash = "";
		updatedTime = 0;
		hintsForVideo = 0;
	}
	public void setHintsForVideoCount(int hintsForVideo) {
		this.hintsForVideo = hintsForVideo;
	}
	public int getHintsForVideoCount() {
		return hintsForVideo;
	}

	public boolean isShowHintsVideoOffer() {
		return showHintsVideoOffer;
	}
	public int getHints() {
		return hints;
	}
	public void setAdsOff(boolean adsOff) {
		this.adsOff = adsOff;
	}
	public boolean isAdsOff() {
		return adsOff;
	}
	public String getVipId() {
		return vipId;
	}
	public long getVip() {
		return vip;
	}
	public long lastVip() {
		return vip - (new Date().getTime() / 1000 - getUpdatedTime());
	}
	public boolean isVip() {
		return lastVip() > 0;
	}
	public long getUpdatedTime() {
		return updatedTime;
	}

	public void setUpdatedTime(long updatedTime) {
		this.updatedTime = updatedTime;
	}
	public String getScanwordsProgress() {
		return scanwordsProgress;
	}
	public String getScanwordsProgressHash() {
		return scanwordsProgressHash;
	}
	public int getPlaceScanwords() {
		return placeScanwords;
	}
	public int getScoreScanwords() {
		return scoreScanwords;
	}
	public void setHints(int hints) {
		this.hints = hints;
	}
	public void setVip(long vip) {
		this.vip = vip;
	}
	
}