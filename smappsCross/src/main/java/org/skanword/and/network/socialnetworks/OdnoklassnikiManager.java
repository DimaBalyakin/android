package org.skanword.and.network.socialnetworks;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import ru.ok.android.sdk.Odnoklassniki;
import ru.ok.android.sdk.OkTokenRequestListener;
import ru.ok.android.sdk.util.OkScope;
import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

public class OdnoklassnikiManager extends SocialNetworkManager implements OkTokenRequestListener {

	public static final String APP_ID = "184762112";
	public static final String APP_SECRET_KEY = "2D530094866EA5CA4658B6C8";
	public static final String APP_PUBLIC_KEY = "CBAQDEAMABABABABA";
	private Odnoklassniki mOdnoklassniki = null;
	
	@Override
	public void initSocialNetworkManager(ISocialNetworkManagerDelegate delegate, Context appContext) {
		super.initSocialNetworkManager(delegate, appContext);
	    mOdnoklassniki = Odnoklassniki.createInstance(appContext, APP_ID, APP_SECRET_KEY, APP_PUBLIC_KEY);
	    mOdnoklassniki.setTokenRequestListener(this);
	    initted = true;
	}

	@Override
	public void authorise(Activity activity) {
		mOdnoklassniki.requestAuthorization(activity, false, OkScope.VALUABLE_ACCESS);
	}
	@Override
	public void clearSocialNetworkManager() {
		if (initted) {
		    mOdnoklassniki.removeTokenRequestListener();
		}
	}

	@Override
	public void onSuccess(String accessToken) {
	    Log.v("APIOK", "Success");
		getCurrentUser();
	}

	@Override
	public void onError() {
	    Log.v("APIOK", "Error");
	    delegate.socialNetworkAuthoriseFailed(false);
	}

	@Override
	public void onCancel() {
	    Log.v("APIOK", "Cancel");
	    delegate.socialNetworkAuthoriseFailed(true);
	}

	@Override
	public void getCurrentUser() {
		new GetCurrentUserTask().execute(new Void[0]);
	}
	
	protected final class GetCurrentUserTask extends AsyncTask<Void, Void, String> {

		@Override
		protected String doInBackground(final Void... params) {
			try {
				Map<String, String> paramsRequest = new HashMap<String, String>();
				paramsRequest.put("emptyPictures", "true");
				paramsRequest.put("fields", "uid,first_name,last_name,gender,pic_1,pic_3");
				return mOdnoklassniki.request("users.getCurrentUser", paramsRequest, "get");
			} catch (Exception exc) {
				Log.e("Odnoklassniki", "Failed to get current user info", exc);
				if (delegate != null) {
					delegate.socialNetworkAuthoriseFailed(false);
				}
			}
			return null;
		}
		
		@Override
		protected void onPostExecute(final String result) {
			if (delegate == null)
				return;
			if (result != null) {
			    Log.v("APIOK", "user - " + result);
				JSONObject jsonObject = null;
				try {
					jsonObject = new JSONObject(result);
				} catch (JSONException e) {
					e.printStackTrace();
				}
				if (jsonObject != null) {
					try {
						String avatar = jsonObject.getString("pic_3");
						if (avatar.contains("stub_")) {
							avatar = null;
						}
						socialUser = new SocialUser(new Date().getTime(), mOdnoklassniki.getCurrentAccessToken(), SocialNetworkManager.getSocialNetworkStringId(SocialNetwork.SOCIAL_NETWORK_OK), jsonObject.getString("first_name"), jsonObject.getString("last_name"), jsonObject.getString("uid"), avatar);
						getUserFriends();
					} catch (JSONException e) {
						e.printStackTrace();
					}
				} else {
					delegate.socialNetworkAuthoriseFailed(false);
				}
			} else {
				delegate.socialNetworkAuthoriseFailed(false);
			}
		}
	}

	@Override
	public void getUserFriends() {
		new GetUserFriendsTask().execute(new Void[0]);
	}
	
	protected final class GetUserFriendsTask extends AsyncTask<Void, Void, String> {

		@Override
		protected String doInBackground(final Void... params) {
			try {
				Map<String, String> paramsRequest = new HashMap<String, String>();
				paramsRequest.put("emptyPictures", "true");
				return mOdnoklassniki.request("friends.get", paramsRequest, "get");
			} catch (Exception exc) {
				delegate.socialNetworkAuthorised(socialUser);
			}
			return null;
		}
		
		@Override
		protected void onPostExecute(final String result) {
			if (result != null) {
				JSONArray jsonArray = null;
				try {
					jsonArray = new JSONArray(result);
				} catch (JSONException e) {
					e.printStackTrace();
				}
				if (jsonArray != null) {
					try {
						List<String> friends = new ArrayList<String>(); 
						for (int i = 0; i < jsonArray.length(); i++) {
							String uid = jsonArray.getString(i);
							friends.add(uid);
						}
						delegate.socialNetworkFriendsGot(friends);
					} catch (JSONException e) {
						
					}
				}
			}
			delegate.socialNetworkAuthorised(socialUser);
		}
	}

	@Override
	public void logout() {
		
	}

}
