package org.skanword.and.network;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.skanword.and.SmappsScanwords;
import org.skanword.and.datamanager.MainDataManager.IDowloadSetProgress;

import android.util.Log;


public class DownloaderArhiveFile{

	private String zipFilePath;
	private String fileName;
	private String unZipPath;

	private IDowloadSetProgress progress;
	private int startProgress;
	private int endProgressDownload = 90 ;
	
	
	public void dowload(URL url, String savePath) throws Exception{
		if (savePath == null) {
			Log.v("", " dowload  " + savePath);
			throw new Exception();
		}
		int count;
		fileName = getFileName(savePath);
		unZipPath = getUnzipPath(savePath);
		File file = new File(unZipPath);
		if(!file.exists() || true){
			Log.v("ANDRO_ASYNC", "fileName of file: " + url);
			URLConnection conexion = url.openConnection();
			conexion.connect();

			int lenghtOfFile = conexion.getContentLength();
			Log.v("ANDRO_ASYNC", "Lenght of file: " + lenghtOfFile);

			InputStream input = new BufferedInputStream(url.openStream());
			Log.v("ANDRO_ASYNC", "input " + input);
			zipFilePath = SmappsScanwords.CROSSWORDS_DIR.getPath() + "/" + fileName;
			Log.v("ANDRO_ASYNC", "zipFilePath " + zipFilePath);
			OutputStream output = new FileOutputStream(zipFilePath);

			Log.v("ANDRO_ASYNC", "input file: " + input + " output file = " + output);
			byte data[] = new byte[1024];

			long total = 0;

			while ((count = input.read(data)) != -1) {
				total += count;
				if (progress !=  null){
					progress.onProgress(startProgress + Math.round(total*100)/lenghtOfFile * (endProgressDownload - startProgress) /100 );
				}
				//publishProgress(""+(int)((total*100)/lenghtOfFile));
				
				output.write(data, 0, count);
			}
			output.flush();
			output.close();
			input.close();
			unzip();
			if (progress !=  null){
				progress.onProgress(100);
			}
		} 
	}
	
	public static String getFileName(String stringPath){
		String[] strings = stringPath.split("/");
		
		return strings[strings.length -1];
	}
	
	public static String getUnzipPath(String stringPath){
		return SmappsScanwords.CROSSWORDS_DIR.getPath() + "/" + getFolderName(stringPath);		
	}
	
	public static String getFolderName(String stringPath){
		return getFileName(stringPath).split(".zip")[0];
	}

	private void unzip() { 
		Log.d("ANDRO_ASYNC", "unzip");
		File file = new File(unZipPath);
		if((!file.exists() && file.mkdir()) || true){
			InputStream is;
			ZipInputStream zis;
			try {
				is = new FileInputStream(zipFilePath);
				zis = new ZipInputStream(new BufferedInputStream(is));          
				ZipEntry ze;

				while((ze = zis.getNextEntry()) != null) {
					ByteArrayOutputStream baos = new ByteArrayOutputStream();
					byte[] buffer = new byte[1024];
					int count;
					String filename = ze.getName();
					Log.d("ANDRO_ASYNC", "unzip " + unZipPath + "/" + filename);
					FileOutputStream fout = new FileOutputStream(unZipPath + "/" + filename);

					// reading and writing
				while((count = zis.read(buffer)) != -1){
	                 baos.write(buffer, 0, count);
	                 byte[] bytes = baos.toByteArray();
	                 fout.write(bytes);             
	                 baos.reset();
	             }
					fout.close();               
					zis.closeEntry();
				}				
				zis.close();
				
			}catch(Exception e){
				if (progress !=  null){
					progress.onProgress(IDowloadSetProgress.ERROR_PROGRESS);
				}
				File dir = new File(unZipPath );
				if (dir.exists()){
					dir.delete();
				}
				
	
			}finally{
				File zipFile = new File(zipFilePath);
				zipFile.delete();
			}
		}

	}


	/*protected void onProgressUpdate(String... progress) {
		Log.d("ANDRO_ASYNC"," progress " + progress[0]);
		if (progress[0].equals("100")){
			unzip();
		}
	}*/


	public void setProgress(IDowloadSetProgress progress, int startProgress) {
		this.progress = progress;
		this.startProgress = startProgress;
	}


}