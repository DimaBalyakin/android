package org.skanword.and.network;

import org.skanword.and.network.socialnetworks.SocialNetworkManager.SocialNetwork;




public class UserAuth {
	private final String vendorId;
	private final String userId;
	private final String userSocialId;
	private final String authKey;
	private SocialNetwork socialNetwork;
	private String avatarUrl = null;
	private String fullName = null;
	private UserObject loginUserState = null;

	public UserAuth(String vendorId, String userId, String userSocialId, String authKey, SocialNetwork socialNetwork) {
		super();
		this.userSocialId = userSocialId;
		this.vendorId = vendorId;
		this.userId = userId;
		this.authKey = authKey;
		this.socialNetwork = socialNetwork;
	}
	
	public UserObject getLoginUserState() {
		return loginUserState;
	}

	public void setLoginUserState(UserObject loginUserState) {
		this.loginUserState = loginUserState;
	}

	public String getAvatarUrl() {
		return avatarUrl;
	}

	public void setAvatarUrl(String avatarUrl) {
		this.avatarUrl = avatarUrl;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public SocialNetwork getSocialNetwork() {
		return socialNetwork;
	}

	public void setSocialNetwork(SocialNetwork socialNetwork) {
		this.socialNetwork = socialNetwork;
	}

	public String getUserSocialId() {
		return userSocialId;
	}
	public String getUserId() {
		return userId;
	}

	public String getAuthKey() {
		return authKey;
	}

	public String getVendorId() {
		return vendorId;
	}
	
	
}
