package org.skanword.and.network;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import android.util.Log;

import com.google.gson.annotations.SerializedName;

public class DailyBonusObject implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 231081669275352021L;
	private int win;
	@SerializedName("timeout")
	private int timeLeft;
	
	private List<Integer> variants;
	
	private List<Map<String, Object>> notifications;
	
	private int mScratchedPosition = 0;
	
	private long downloadedTime;

	public DailyBonusObject(int win, int timeLeft, List<Integer> variants,
			List<Map<String, Object>> notification) {
		super();
		this.win = win;
		this.timeLeft = timeLeft;
		this.variants = variants;
		this.notifications = notification;
	}

	
	public long getDownloadedTime() {
		return downloadedTime;
	}
	
	public void setWin(int win) {
		this.win = win;
	}
	
	public void setVariants(List<Integer> variants) {
		this.variants = variants;
	}
	
	public void setDownloadedTime(long downloadedTime) {
		this.downloadedTime = downloadedTime;
	}


	public int getWin() {
		return win;
	}

	public int getTimeLeft() {
		return timeLeft;
	}

	public List<Integer> getVariants() {
		return variants;
	}
	public int getOtherVariant(int num) {
		List<Integer> vars = getVariants();
		Log.v("SkanwordsFunc", "vars  " + vars + "  " + getWin());
		for (Integer variant : vars) {
			if (variant == getWin()) {
				vars.remove(variant);
				break;
			}
		}
		try {
			return vars.get(num - 1);
		} catch (Exception e) {
		}
		return 0;
	}
	public List<Map<String, Object>> getNotification() {
		return notifications;
	}

	public void setTimeLeft(int time) {
		this.timeLeft = time;
	}
	
	public void setScratchedPosition(int position) {
		this.mScratchedPosition = position;
	}
	public int getScratchedPosition() {
		return mScratchedPosition;
	}
	
	
}
