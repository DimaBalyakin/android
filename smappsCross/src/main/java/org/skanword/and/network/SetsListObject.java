package org.skanword.and.network;

import java.io.Serializable;
import java.util.List;

import org.skanword.and.datamanager.SkanwordsDataManager.TaskListDirrectionType;
import org.skanword.and.datamanager.SkanwordsDataManager.TaskListType;
import org.skanword.and.datamanager.SkanwordsSetInfo;

import com.google.gson.annotations.SerializedName;

public class SetsListObject implements Serializable {

	private static final long serialVersionUID = -6152007683222991068L;


	@SerializedName("page_previous")
	private final boolean pagePreviousAvailable;
	@SerializedName("page_next")
	private final boolean pageNextAvailable;

	@SerializedName("set_id_first")
	private final int latestSetId;
	@SerializedName("set_id_last")
	private final int oldestSetId;

	@SerializedName("list")
	private final List<SkanwordsSetInfo> setsList;

	@SerializedName("set_tomorrow")
	private final SkanwordsSetInfo tomorrowSet;
	
	private TaskListType listType;
	private TaskListDirrectionType dirrectionType;

	public SetsListObject(boolean pagePreviousAvailable,
			boolean pageNextAvailable, int latestSetId, int oldestSetId,
			List<SkanwordsSetInfo> setsList, SkanwordsSetInfo tomorrowSet) {
		super();
		this.pagePreviousAvailable = pagePreviousAvailable;
		this.pageNextAvailable = pageNextAvailable;
		this.latestSetId = latestSetId;
		this.oldestSetId = oldestSetId;
		this.setsList = setsList;
		this.tomorrowSet = tomorrowSet;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public boolean isPagePreviousAvailable() {
		return pagePreviousAvailable;
	}

	public boolean isPageNextAvailable() {
		return pageNextAvailable;
	}

	public int getLatestSetId() {
		return latestSetId;
	}

	public int getOldestSetId() {
		return oldestSetId;
	}

	public List<SkanwordsSetInfo> getSetsList() {
		return setsList;
	}

	public SkanwordsSetInfo getTomorrowSet() {
		return tomorrowSet;
	}

	public TaskListType getListType() {
		return listType;
	}

	public void setListType(TaskListType listType) {
		this.listType = listType;
	}

	public TaskListDirrectionType getDirrectionType() {
		return dirrectionType;
	}

	public void setDirrectionType(TaskListDirrectionType dirrectionType) {
		this.dirrectionType = dirrectionType;
	}
	
	
	
}
