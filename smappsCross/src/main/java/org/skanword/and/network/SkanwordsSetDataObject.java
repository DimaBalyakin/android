package org.skanword.and.network;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.skanword.and.datamanager.SkanwordSet;

import com.google.gson.annotations.SerializedName;

public class SkanwordsSetDataObject implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8630112335338511777L;

	@SerializedName("set_list")
	private final List<SkanwordSet> setsList;
	
	@SerializedName("solved")
	private final List<List<String>> solvedSkanwords;
	
	@SerializedName("save")
	private final List<List<String>> saveSkanwords;
	
	private List<Map<String, Integer>> requestdSetIds;
	
	public SkanwordsSetDataObject(List<SkanwordSet> setsList,
			List<List<String>> solvedSkanwords,
			List<List<String>> saveSkanwords) {
		super();
		this.setsList = setsList;
		this.solvedSkanwords = solvedSkanwords;
		this.saveSkanwords = saveSkanwords;
		
	}

	public List<SkanwordSet> getSetsList() {
		return setsList;
	}
	
	public SkanwordSet getFristScanwordSet() {
		if (setsList != null) {
			return setsList.get(0);
		}
		return null;
	}

	public List<List<String>> getSolvedSkanwords() {
		return solvedSkanwords != null ? solvedSkanwords : (new ArrayList<List<String>>());
	}

	public List<List<String>> getSaveSkanwords() {
		return saveSkanwords != null ? saveSkanwords : (new ArrayList<List<String>>());
	}

	public List<Map<String, Integer>> getRequestdSetIds() {
		return requestdSetIds;
	}

	public void setRequestdSetIds(List<Map<String, Integer>> requestdSetIds) {
		this.requestdSetIds = requestdSetIds;
	}
	
}
