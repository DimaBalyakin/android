package org.skanword.and.network;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.skanword.and.etc.Utils;
import org.skanword.and.network.ConfigObject.AppAds.AppAdsObject;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

public class ConfigObject implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2846031599572231609L;

	
	@SerializedName("local_notifications")
	private final LocalNotification localNotifications;
	@SerializedName("push_token_exist")
	private final boolean pushTokenExist;
	@SerializedName("app_ads")
	private final AppAds appAds;
	@SerializedName("app_offers")
	private final AppOffers appOffers;
	@SerializedName("payment_track")
	private final boolean paymentTrack;
	@SerializedName("news_hash")
	private final String newsHash;
	@SerializedName("shop_hash")
	private final String shopHash;

	@SerializedName("version")
	private final Versions versions;
	
	
	
	
	public ConfigObject(LocalNotification localNotifications,
			boolean pushTokenExist, AppAds appAds, AppOffers appOffers,
			boolean paymentTrack, String newsHash, String shopHash, Versions versions) {
		super();
		this.localNotifications = localNotifications;
		this.pushTokenExist = pushTokenExist;
		this.appAds = appAds;
		this.appOffers = appOffers;
		this.paymentTrack = paymentTrack;
		this.newsHash = newsHash;
		this.shopHash = shopHash;
		this.versions = versions;
	}
	
	public String getNewsHash() {
		return newsHash;
	}

	public String getShopHash() {
		return shopHash;
	}

	public boolean paymentTrack() {
		return this.paymentTrack;
	}
	public AppOffers getAppOffers() {
		return appOffers;
	}
	
	public Versions getVersions () {
		return versions;
	}
	
	
	public AppAds getAppAds() {
		return appAds;
	}
	public LocalNotification getLocalNotifications() {
		return localNotifications;
	}

	public boolean isPushTokenExist() {
		return pushTokenExist;
	}
	
	public static class AppOffers implements Serializable {

		/**
		 * 
		 */
		private static final long serialVersionUID = 8736484659189829995L;
		@SerializedName("video_hints") 
		private final List<AppAds.AppAdsObject> videoHints;
		private final List<AppAds.AppAdsObject> video;
		@SerializedName("video_interstitial") 
		private final List<AppAds.AppAdsObject> videoInterstitial;
		public AppOffers(List<AppAdsObject> videoHints, List<AppAdsObject> video, List<AppAds.AppAdsObject> videoInterstitial) {
			super();
			this.videoHints = videoHints;
			this.video = video;
			this.videoInterstitial = videoInterstitial;
		}
		
		public AppOffers() {
			super();
			videoHints = new ArrayList<AppAds.AppAdsObject>();
			video = new ArrayList<AppAds.AppAdsObject>();
			videoInterstitial = new ArrayList<ConfigObject.AppAds.AppAdsObject>();
		}

		public List<AppAds.AppAdsObject> getVideoHints() {
			return videoHints;
		}
		public List<AppAds.AppAdsObject> getVideo() {
			return video;
		}

		public List<AppAds.AppAdsObject> getVideoInterstitial() {
			return videoInterstitial;
		}
	}
	public static class AppAds implements Serializable{
		/**
		 * 
		 */
		private static final long serialVersionUID = -8690415063978369826L;
		@SerializedName("main") 
		private final List<AppAdsObject> mainShows;
		@SerializedName("questions") 
		private final List<AppAdsObject> questionsShows;
		@SerializedName("cross") 
		private final List<AppAdsObject> crossBanners;
		@SerializedName("show_popup") 
		private final boolean isShowPopUp;
		@SerializedName("show_delay") 
		private final int showDelay;
		public AppAds(List<AppAdsObject> mainShows, List<AppAdsObject> questionsShows, List<AppAdsObject> crossBanners,
				boolean isShowPopUp, int showDelay) {
			super();
			this.crossBanners = crossBanners;
			this.mainShows = mainShows;
			this.questionsShows = questionsShows;
			this.isShowPopUp = isShowPopUp;
			this.showDelay = showDelay;
		}
		
		public AppAds() {
			super();
			mainShows = new ArrayList<AppAds.AppAdsObject>();
			questionsShows = new ArrayList<AppAds.AppAdsObject>();
			crossBanners = new ArrayList<AppAds.AppAdsObject>();
			showDelay = 0;
			isShowPopUp = false;
			// TODO Auto-generated constructor stub
		}

		public List<AppAdsObject> getMainShows() {
			return mainShows;
		}
		public List<AppAdsObject> getQuestionsShows() {
			return questionsShows;
		}
		public List<AppAdsObject> getCrossBanners() {
			return crossBanners;
		}
		public boolean isShowPopUp() {
			return isShowPopUp;
		}
		public int getShowDelay() {
			return showDelay;
		}
		
		
		public static class AppAdsObject implements Serializable {

			private static final long serialVersionUID = -8159363141808903804L;
			private final String name;
			@SerializedName("app_signature") 
			private final String appSignature;
			@SerializedName("app_id") 
			private final String appId;
			private final int coefficient;
			public AppAdsObject(String name, String appSignature, String appId,
					int coefficient) {
				super();
				this.name = name;
				this.appSignature = appSignature;
				this.appId = appId;
				this.coefficient = coefficient;
			}
			public static long getSerialversionuid() {
				return serialVersionUID;
			}
			public String getName() {
				return name;
			}
			public String getAppSignature() {
				return appSignature;
			}
			public String getAppId() {
				return appId;
			}
			public int getCoefficient() {
				return coefficient;
			}
			
			
			
		}
	}

	public static class LocalNotification implements Serializable{
		/**
		 * 
		 */
		private static final long serialVersionUID = -2370775632301098549L;
		@SerializedName("remind_off")
		private final List<NotificationRemind> remindOff;
		@SerializedName("remind_on")
		private final List<NotificationRemind> remindOn;
		public LocalNotification(List<NotificationRemind> remindOff,
				List<NotificationRemind> remindOn) {
			super();
			this.remindOff = remindOff;
			this.remindOn = remindOn;
		}
		
		public LocalNotification() {
			super();
			remindOn = new ArrayList<NotificationRemind>();
			remindOff = new ArrayList<NotificationRemind>();
		}

		public List<NotificationRemind> getRemindOff() {
			return remindOff;
		}
		public List<NotificationRemind> getRemindOn() {
			return remindOn;
		}	
		
		public boolean compareWithNotifications(LocalNotification notifications) {
			Gson gson = new Gson();
			String newNotifs = gson.toJson(notifications);
			String oldNotifs = gson.toJson(this);
			
			return Utils.md5(newNotifs).equals(Utils.md5(oldNotifs));
		}
	}
	
	public static class NotificationRemind implements Serializable{
		/**
		 * 
		 */
		private static final long serialVersionUID = 155091005774545474L;
		private final List<Integer> days;
		private final String text;
		public NotificationRemind(List<Integer> days, String text) {
			super();
			this.days = days;
			this.text = text;
		}
		public List<Integer> getDays() {
			return days;
		}
		public String getText() {
			return text;
		}
		
	}
	
	public static class Versions implements Serializable {
		/**
		 * 
		 */
		private static final long serialVersionUID = 2627868444307832476L;
		private final Version inform;
		private final Version lock;
		public Versions(Version inform, Version lock) {
			super();
			this.inform = inform;
			this.lock = lock;
		}
		public Version getInform() {
			if (inform == null) {
				return new Version(0, "");
			}
			return inform;
		}
		public Version getLock() {
			if (lock == null) {
				return new Version(0, "");
			}
			return lock;
		}
		
		@Override
		public boolean equals(Object obj) {
		    if (this == obj)
		        return true;
		    if (obj == null)
		        return false;
		    if (getClass() != obj.getClass())
		        return false;
		    Versions v = (Versions) obj;
			return getInform().equals(v.getInform()) && getInform().equals(v.getLock());
		}
		
	}
	public static class Version implements Serializable {
		
		/**
		 * 
		 */
		private static final long serialVersionUID = -2006107567450886910L;
		private final int build;
		private final String text;
		
		
		

		public Version(int build, String text) {
			super();
			this.build = build;
			this.text = text;
		}
		
		


		public int getBuild() {
			return build;
		}




		public String getText() {
			if (text == null)
				return "";
			return text;
		}




		@Override
		public boolean equals(Object obj) {
		    if (this == obj)
		        return true;
		    if (obj == null)
		        return false;
		    if (getClass() != obj.getClass())
		        return false;
		    Version v = (Version) obj;
		    
		    return getBuild() == v.getBuild() && getText().equals(v.getText());
		}
	}
}
