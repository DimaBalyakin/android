package org.skanword.and.network.socialnetworks;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKAccessTokenTracker;
import com.vk.sdk.VKCallback;
import com.vk.sdk.VKScope;
import com.vk.sdk.VKSdk;
import com.vk.sdk.VKUIHelper;
import com.vk.sdk.api.VKApi;
import com.vk.sdk.api.VKApiConst;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKRequest.VKRequestListener;
import com.vk.sdk.api.VKResponse;
import com.vk.sdk.dialogs.VKCaptchaDialog;
import com.vk.sdk.util.VKUtil;

public class VkontakteManager extends SocialNetworkManager {

    private static String sTokenKey = "VK_ACCESS_TOKEN";
	private static String[] sMyScope = new String[]{VKScope.OFFLINE};
	VKAccessTokenTracker vkAccessTokenTracker = new VKAccessTokenTracker() {
		@Override
		public void onVKAccessTokenChanged(VKAccessToken oldToken, VKAccessToken newToken) {
			if (newToken == null) {
				getCurrentUser();
			}
		}
	};
	@Override
	public void initSocialNetworkManager(
			ISocialNetworkManagerDelegate delegate, Context appContext) {
		// TODO Auto-generated method stub
		super.initSocialNetworkManager(delegate, appContext);
		vkAccessTokenTracker.startTracking();

        String[] fingerprint = VKUtil.getCertificateFingerprint(appContext, appContext.getPackageName());
		Log.v("", "initSocialNetworkManager  " + fingerprint[0]);
		 
	}
	@Override
	public void onActivityResult(Activity activity, int requestCode,
			int resultCode, Intent data) {
		// TODO Auto-generated method stub
	    if(!VKSdk.onActivityResult(requestCode, resultCode, data, new VKCallback<VKAccessToken>() {
			@Override
			public void onResult(VKAccessToken res) {
				getCurrentUser();
			}

			@Override
			public void onError(VKError error) {
				delegate.socialNetworkAuthoriseFailed(true);
			}
		})) {
			super.onActivityResult(activity, requestCode, resultCode, data);

		}
	}

	@Override
	public void clearSocialNetworkManager() {
		// TODO Auto-generated method stub

	}

	@Override
	public void authorise(Activity activity) {
        VKSdk.login(activity, sMyScope);
	}

	@Override
	public void getCurrentUser() {
		VKApi.users().get(VKParameters.from(VKApiConst.FIELDS, "photo_100")).executeWithListener(new VKRequestListener() {
		    @Override
		    public void onComplete(VKResponse response) {
				JSONObject jsonObject = null;
				try {
					jsonObject = response.json.getJSONArray("response").getJSONObject(0);
					Log.v("", "" + jsonObject.toString());
				} catch (JSONException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				if (jsonObject != null) {
					try {
						String avatar = null;
						if (jsonObject.getString("photo_100") != null) {
							avatar = jsonObject.getString("photo_100");
						}
						socialUser = new SocialUser(new Date().getTime(), VKSdk.getAccessToken().accessToken, SocialNetworkManager.getSocialNetworkStringId(SocialNetwork.SOCIAL_NETWORK_VK), jsonObject.getString("first_name"), jsonObject.getString("last_name"), jsonObject.getString("id"), avatar);
						getUserFriends();
					} catch (JSONException e) {
						e.printStackTrace();
					}
				} else {
					delegate.socialNetworkAuthoriseFailed(false);
				}
		    }
		    @Override
		    public void onError(VKError error) {
				delegate.socialNetworkAuthoriseFailed(false);
		    }
		    @Override
		    public void onProgress(VKRequest.VKProgressType progressType,
		                                     long bytesLoaded,
		                                     long bytesTotal)
		    {
		        //I don't really believe in progress
		    }
		    @Override
		    public void attemptFailed(VKRequest request, int attemptNumber, int totalAttempts) {
				delegate.socialNetworkAuthoriseFailed(false);
		    }
		});
	}
	@Override
	public void getUserFriends() {
		VKApi.friends().get(VKParameters.from(VKApiConst.FIELDS, "photo_100")).executeWithListener(new VKRequestListener() {
		    @Override
		    public void onComplete(VKResponse response) {
				JSONArray jsonArray = null;
				try {
					jsonArray = response.json.getJSONArray("items");
				} catch (JSONException e1) {
					e1.printStackTrace();
				}
				if (jsonArray != null) {
					try {
						List<String> friends = new ArrayList<String>(); 
						for (int i = 0; i < jsonArray.length(); i++) {
							JSONObject jsonObject = jsonArray.getJSONObject(i);
							friends.add(jsonObject.getString("id"));
						}
						delegate.socialNetworkFriendsGot(friends);
						delegate.socialNetworkAuthorised(socialUser);
					} catch (JSONException e) {
						e.printStackTrace();
					}
				} else {
					delegate.socialNetworkAuthorised(socialUser);
				}
		    }
		    @Override
		    public void onError(VKError error) {
		    	delegate.socialNetworkAuthorised(socialUser);
		    }
		    @Override
		    public void onProgress(VKRequest.VKProgressType progressType,
		                                     long bytesLoaded,
		                                     long bytesTotal)
		    {
		    }
		    @Override
		    public void attemptFailed(VKRequest request, int attemptNumber, int totalAttempts) {
		    	delegate.socialNetworkAuthorised(socialUser);
		    }
		});
	}
	@Override
	public void logout() {
		
	}
	
}
