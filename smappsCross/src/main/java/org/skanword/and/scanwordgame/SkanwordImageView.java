/**
 * This file is part of Words With Crosses.
 *
 * Copyright (C) 2013 Adam Rosenfield
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.skanword.and.scanwordgame;

import java.util.logging.Logger;

import org.skanword.and.datamanager.MainDataManager;
import org.skanword.and.etc.InputConnectionAccomodatingLatinIMETypeNullIssues;
import org.skanword.and.scanwordgame.SkanwordBoard.Position;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.text.InputType;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;

public class SkanwordImageView extends TouchImageView {
	private SkanwordBoard board;
	private int boardWidth = 1;
	private int boardHeight = 1;

	private float renderScale = 1.0f;
	private float minRenderScale = 1.0f;
	private float maxRenderScale = 1.0f;
	private float displayScale = 1.0f;
	private float overScale = 0f;
	private boolean created = false;

	private ClickListener clickListener;
	private RenderScaleListener renderScaleListener;

	private static final Logger LOG = Logger
			.getLogger("com.adamrosenfield.wordswithcrosses");
	private float tx;
	private float ty;
	private boolean isNowRefresh;
	private float zoomScale;

	public SkanwordImageView(Context context, AttributeSet attrs) {
		super(context, attrs);
		setFocusable(true);
		setFocusableInTouchMode(true);
	}
	
	
	
	public void setBoard(SkanwordBoard board) {
		this.board = board;
	}
	public void setBoard(SkanwordBoard board, DisplayMetrics metrics,
			Integer maxTextureSize) {
		if (this.board == null) {
			this.board = board;
			boardWidth = board.getWidth();
			boardHeight = board.getHeight();
			TypedValue tv = new TypedValue();
			float actionBarHeight = 0;
			if (getContext().getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true))
			{
				int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
				int statusBarHeight = 0;
			    if (resourceId > 0) {
			    	statusBarHeight = getResources().getDimensionPixelSize(resourceId);
			    } 
			    actionBarHeight = TypedValue.complexToDimensionPixelSize(tv.data,getResources().getDisplayMetrics()) + (statusBarHeight);
			}
			
			minRenderScale = Math.max(Math.max(0.5f,(metrics.heightPixels - actionBarHeight)
					/ (float) (boardHeight * SkanwordBox.BOX_SIZE)), Math.max(0.5f,(metrics.widthPixels)
							/ (float) (boardWidth * SkanwordBox.BOX_SIZE)));
			Log.v("", " min render scales   " + " " + metrics.widthPixels
					/ (float) (boardWidth * SkanwordBox.BOX_SIZE) + " " + (metrics.heightPixels - actionBarHeight)
					/ (float) (boardHeight * SkanwordBox.BOX_SIZE));
			maxRenderScale = Math.min((float) maxTextureSize
					/ (Math.max(boardWidth, boardHeight) * SkanwordBox.BOX_SIZE), 4);
			displayScale = MainDataManager.getInstance().getOptions().isIncreasedRatio() ? (maxRenderScale * 1.4f) : maxRenderScale * 1.2f;
			// maxRenderScale = minRenderScale > maxRenderScale ? minRenderScale
			// : maxRenderScale;
			renderScale = Math.min(Math.max(minRenderScale, renderScale),
					displayScale);

			Log.v("SkanwordsGameFunc", maxTextureSize + "  setBoard " + maxRenderScale + "   "
					+ renderScale + "   " + boardWidth * renderScale
					* SkanwordBox.BOX_SIZE + "   " + boardHeight * renderScale
					* SkanwordBox.BOX_SIZE);
		}
	}

	public float zoomIn() {
		Log.v("SkanwordsGame", "Zoom in");
		float width = (float) getWidth();
		if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
			width = (float) getHeight();
		}

		float tempZoomScale = width / 6f / SkanwordBox.BOX_SIZE;
		if (tempZoomScale * SkanwordBox.BOX_SIZE > 128f) {
			tempZoomScale = Math.max(128f / SkanwordBox.BOX_SIZE,
					minRenderScale);
		} else if (tempZoomScale * SkanwordBox.BOX_SIZE < 60f) {
			tempZoomScale = Math
					.min(60f / SkanwordBox.BOX_SIZE, displayScale);
		} else {
			tempZoomScale = Math.min(Math.max(minRenderScale, tempZoomScale),
					displayScale);
		}
		if (tempZoomScale == zoomScale)
			return 0;
		zoomScale = tempZoomScale;

		return setRenderScale(zoomScale);
	}

	public float zoomOut() {
		Log.v("SkanwordsGame", "Zoom out");
		return setRenderScale(renderScale / 1.5f);
	}

	public float fitToScreen() {
		Log.v("SkanwordsGame", "Fit to screen");
		float horzScale = (float) getWidth()

		/ (boardWidth * SkanwordBox.BOX_SIZE);
		float vertScale = (float) getHeight()
				/ (boardHeight * SkanwordBox.BOX_SIZE);
		return setRenderScale(Math.min(horzScale, vertScale));
	}

	public float setRenderScale(float newScale) {
		Log.v("SkanwordsGame", "set render scale " + newScale);
		float oldBoxSize = SkanwordBox.BOX_SIZE * renderScale;
		Position highlight = board.getHighlightLetter();
		if (highlight == null) {
			highlight = new Position(0, 0);
		}
		float pivotX = ((float) highlight.across + 0.5f) * oldBoxSize;
		float pivotY = ((float) highlight.down + 0.5f) * oldBoxSize;

		return setRenderScale(newScale, pivotX, pivotY);
	}

	private float setRenderScale(float newScale, float pivotX, float pivotY) {
		// Clamp the scale to our range. If it didn't change appreciably, don't
		// do anything
		newScale = Math.min(Math.max(newScale, minRenderScale), displayScale);

		PointF bmPivot = pixelToBitmapPos(pivotX, pivotY);

		float relScale = newScale / renderScale;

		if (newScale > maxRenderScale) {
			overScale = newScale / maxRenderScale;
			newScale = maxRenderScale;
		} else {
			overScale = 0;
		}
		if (Math.abs(newScale - renderScale) < 0.0001f && created) {
			return renderScale;
		} else {
			tx = pivotX - relScale * bmPivot.x;
			ty = pivotY - relScale * bmPivot.y;
			renderScale = newScale;
			render(false);
		}
		
		
		created = true;
		Log.v("", " setRenderScale " +  Math.abs(newScale - renderScale)  + "   " + minRenderScale / renderScale + "   " + displayScale / renderScale + "   "  + (minRenderScale < displayScale));
		if (minRenderScale < displayScale) {
			setMinScale(minRenderScale / renderScale);
			setMaxScale(displayScale / renderScale);
			if (overScale > 0) {
				setScaleAndTranslate(overScale, tx, ty);
			} else 
				setScaleAndTranslate(1.0f, tx, ty);
		} else {
			setMinScale(minRenderScale / renderScale + 0.005f);
			setMaxScale(minRenderScale / renderScale + 0.005f);
			setScaleAndTranslate(minRenderScale / displayScale + 0.005f, tx,
					ty);
		}

		if (renderScaleListener != null) {
			renderScaleListener.onRenderScaleChanged(renderScale);
		}

		return renderScale;
	}

	@Override
	public InputConnection onCreateInputConnection(EditorInfo outAttrs) {
		// Passing FALSE as the SECOND ARGUMENT (fullEditor) to the constructor
		// will result in the key events continuing to be passed in to this
		// view. Use our special BaseInputConnection-derived view
		InputConnectionAccomodatingLatinIMETypeNullIssues baseInputConnection = 
	    new InputConnectionAccomodatingLatinIMETypeNullIssues(this, false);

	   //In some cases an IME may be able to display an arbitrary label for a 
	   // command the user can perform, which you can specify here.  A null value
	   // here asks for the default for this key, which is usually something 
	   // like Done.
	   outAttrs.actionLabel = null;

	   //Special content type for when no explicit type has been specified. 
	   // This should be interpreted (by the IME that invoked 
	   // onCreateInputConnection())to mean that the target InputConnection 
	   // is not rich, it can not process and show things like candidate text 
	   // nor retrieve the current text, so the input method will need to run 
	   // in a limited "generate key events" mode.  This disables the more 
	   // sophisticated kinds of editing that use a text buffer.
	   outAttrs.inputType = InputType.TYPE_NULL;

	   //This creates a Done key on the IME keyboard if you need one
	   outAttrs.imeOptions = EditorInfo.IME_ACTION_DONE;
		return baseInputConnection;
	}

	public void refresh() {
		Log.v("SkanwordsGame", "refresh Scanword Image View");
		if (!isNowRefresh) {
			isNowRefresh = true;
			render(false);
			if (minRenderScale < displayScale) {
				setScaleAndTranslate(1.0f, tx, ty);
			} else {
				setScaleAndTranslate(minRenderScale / displayScale + 0.005f,
						tx, ty);
			}
			if (renderScaleListener != null) {
				renderScaleListener.onRenderScaleChanged(renderScale);
			}
			isNowRefresh = false;
		}
	}

	public float getRenderScale() {
		return renderScale;
	}

	public void setClickListener(ClickListener listener) {
		clickListener = listener;
	}

	public void setRenderScaleListener(RenderScaleListener listener) {
		renderScaleListener = listener;
	}

	public void ensureVisible(Position pos) {
		Log.v("SkanwordsGame", "ensure visible");
		Matrix m = getImageMatrix();
		float scale = overScale > 0 ? overScale * renderScale : renderScale;
		float boxSize = SkanwordBox.BOX_SIZE * scale;
		float x = (float) ((pos.across + 0.5) * boxSize);
		float y = (float) ((pos.down + 0.5) * boxSize);
		
		float centerWidth = (boxSize * boardWidth) / 2;
		float centerHeight = (boxSize * boardHeight) / 2;
		float[] values = new float[9];
		matrix.getValues(values);
		float globalX = values[Matrix.MTRANS_X];
		float globalY = values[Matrix.MTRANS_Y];
		Log.v("SkanwordsGame" , " size  renderscale - " + renderScale + "  display scale - " + displayScale + "  max render scale - " + maxRenderScale + "  zoom scale - " + zoomScale + "  over scale - " + overScale + "  x - " + globalX + "  y - " + globalY);
		
		float[] p = new float[] { x, y };
		//m.mapPoints(p);

		float dx = -x + getWidth() / 2 - globalX, dy = -y + getHeight() / 2 - globalY;
		Log.v("SkanwordsGame" ,boxSize + " ensureVisible  dx - " + dx + "  dy - " + dy);
		translate(dx, dy);
	}

	private enum ClickEvent {
		CLICK, DOUBLE_CLICK, LONG_CLICK
	};

	@Override
	protected void onClick(PointF pos) {
		onClickEvent(ClickEvent.CLICK, pos);
	}

	@Override
	protected void onDoubleClick(PointF pos) {
		onClickEvent(ClickEvent.DOUBLE_CLICK, pos);
	}

	@Override
	protected void onLongClick(PointF pos) {
		onClickEvent(ClickEvent.LONG_CLICK, pos);
	}

	private void onClickEvent(ClickEvent event, PointF pos) {
		if (clickListener == null) {
			return;
		}

		float boxSize = SkanwordBox.BOX_SIZE * renderScale;
		int x = (int) (pos.x / boxSize);
		int y = (int) (pos.y / boxSize);
		Position crosswordPos = null;
		if (x >= 0 && x < boardWidth && y >= 0 && y < boardHeight) {
			crosswordPos = new Position(x, y);
		}

		switch (event) {
		case CLICK:
			clickListener.onClick(crosswordPos);
			break;

		case DOUBLE_CLICK:
			clickListener.onDoubleClick(crosswordPos);
			break;

		case LONG_CLICK:
			clickListener.onLongClick(crosswordPos);
		}
	}

	@Override
	protected void onScaleEnd(float newScale) {
		// Re-render at the new scale and set the TouchImageView scale back to
		// 1.0
		if (Math.abs((renderScale * newScale) - renderScale) < 0.0001f) {
			// return renderScale;
		}
		setRenderScale(renderScale * newScale);
	}

	public void render(boolean forceRenderAll) {
		Bitmap bitmap = board.drawSkanword(renderScale, forceRenderAll);
		setImageBitmap(bitmap);

	}

	@Override
	public boolean onCheckIsTextEditor() {
		return true;
	}

	public interface ClickListener {
		public void onClick(Position pos);

		public void onDoubleClick(Position pos);

		public void onLongClick(Position pos);
	}

	public interface RenderScaleListener {
		public void onRenderScaleChanged(float renderScale);
	}
}
