package org.skanword.and.scanwordgame;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

public class PlayLayout extends RelativeLayout {
	private IChandeKeyboardViewListener listener;
	private boolean isViewKeyBoard;


	public PlayLayout(Context context) {
		super(context);
	}

	public PlayLayout(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}
 
	public PlayLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
	} 
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		final int proposedheight = MeasureSpec.getSize(heightMeasureSpec);
		final int actualHeight = getHeight();
		isViewKeyBoard = (actualHeight > proposedheight);
		if (listener != null){
			listener.onChange(isViewKeyBoard);
		}
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
	}

	public void setOnChangeKeyboadListener(IChandeKeyboardViewListener listener){
		this.listener = listener;
	}

	public static interface IChandeKeyboardViewListener{
		void onChange(boolean isVisible);
	}
}


