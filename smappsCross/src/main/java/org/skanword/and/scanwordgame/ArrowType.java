package org.skanword.and.scanwordgame;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;

public enum ArrowType{


	RIGHT(0),
	BOTTOM(1),
	RIGHT_BOTTOM(2),
	LEFT_BOTTOM(3),
	TOP_RIGHT(4),
	BOTTOM_RIGHT(5),
	TOP_LEFT_BOTTOM(6),
	TOP_RIGHT_BOTTOM(7),
	LEFT_BOTTOM_RIGHT(8),
	TOP_LEFT_RIGHT(9),
	SMALL_TOP_RIGHT(10),
	SMALL_BOTTOM_RIGHT(11),
	SMALL_LEFT_BOTTOM(12),
	SMALL_RIGHT_BOTTOM(13);
	
	
	
	
	//constants
	private static final int ARROW_SIZE = 10;
	private static final float START_FEATHERING_RATIO = 0.5f;
	private static final float END_FEATHERING_RATIO = 0.65f;		
	private static final float VERTICAL_SHIFT_FEATHERING_RATIO = 0.25f;
	private static final float STICK_HEIGHT_RATIO = 0.1f;
	private static final float STICK_SMALL_RATIO = 0.3f;

	private static final int COLOR = Color.BLACK;



	private final int id;
	private final Paint arrowPaint;

	private static final int verticesColors[] = {
		COLOR,  COLOR,  COLOR,  COLOR,  COLOR,  COLOR
	};
	
	public static ArrowType create(int inputId){
		//inputId = 13;
		ArrowType result = RIGHT;
		for (ArrowType type : values()) {
			if (type.id == (inputId - 1)){
				result = type;
				break;
			}
		}
		
		return result;
	}


	private ArrowType(Integer id){
		this.id = id;
		arrowPaint = new Paint();
		arrowPaint.setColor(COLOR);

	}

	protected void draw(Canvas canvas, int x, int y, float scale){
		int arrowSize = (int)(ARROW_SIZE * scale);		
		int boxSize = (int)(SkanwordBox.BOX_SIZE * scale); 
		int startX = 0;
		int startY = 0;
		int stopX = 0;
		int stopY = 0;
		int middleX = 0;
		int middleY = 0;

		switch (id) {
		case 0:			
			//draw rectangle
			startX = x;
			startY = y + Math.round((arrowSize * STICK_HEIGHT_RATIO  +  boxSize) / 2);
			stopX = startX + (Math.round(arrowSize * END_FEATHERING_RATIO));
			stopY = startY;				
			arrowPaint.setStrokeWidth(arrowSize * STICK_HEIGHT_RATIO);
			canvas.drawLine(startX, startY, stopX, stopY, arrowPaint);	

			drawFeathering(canvas, new Point(stopX, stopY), false, scale);
			break;
		case 1:			
			//draw line
			startX = x + Math.round((arrowSize * STICK_HEIGHT_RATIO  +  boxSize) / 2);
			startY = y;				
			stopX = startX;
			stopY = (int) (startY + Math.round(arrowSize * END_FEATHERING_RATIO));			
			arrowPaint.setStrokeWidth(arrowSize * STICK_HEIGHT_RATIO);
			canvas.drawLine(startX, startY, stopX, stopY, arrowPaint);		

			drawFeathering(canvas, new Point(stopX, stopY), true, scale);
			break;
		case 2:			
			startX = x + Math.round(arrowSize * STICK_HEIGHT_RATIO / 2);
			startY = y + Math.round((arrowSize * STICK_HEIGHT_RATIO  +  boxSize) / 2);

			middleX = startX + Math.round(arrowSize * END_FEATHERING_RATIO);
			middleY = startY;

			stopX = startX + Math.round(arrowSize * END_FEATHERING_RATIO) ;
			stopY = startY + Math.round(arrowSize * END_FEATHERING_RATIO);

			drawCurvedLine(canvas, new Point(startX,  startY), new Point(middleX,  middleY), new Point(stopX,  stopY), arrowPaint, scale);
			drawFeathering(canvas, new Point(stopX, stopY), stopY != middleY, scale);		

			break;
		case 3:			
			startX = x + boxSize;
			startY = y + Math.round((arrowSize * STICK_HEIGHT_RATIO + boxSize ) / 2);

			middleX = startX - Math.round(arrowSize * END_FEATHERING_RATIO);
			middleY = startY;

			stopX = startX - Math.round(arrowSize * END_FEATHERING_RATIO) ;
			stopY = startY + Math.round(arrowSize * END_FEATHERING_RATIO);

			drawCurvedLine(canvas, new Point(startX,  startY), new Point(middleX,  middleY), new Point(stopX,  stopY), arrowPaint, scale);
			drawFeathering(canvas, new Point(stopX, stopY), stopY != middleY, scale);		

			break;

		case 4:			
			startX = x + Math.round((arrowSize * STICK_HEIGHT_RATIO + boxSize) / 2);
			startY = y + boxSize;

			middleX = startX ;
			middleY = startY - Math.round(arrowSize * END_FEATHERING_RATIO);

			stopX = startX + Math.round(arrowSize * END_FEATHERING_RATIO) ;
			stopY = startY - Math.round(arrowSize * END_FEATHERING_RATIO);

			drawCurvedLine(canvas, new Point(startX,  startY), new Point(middleX,  middleY), new Point(stopX,  stopY), arrowPaint, scale);
			drawFeathering(canvas, new Point(stopX, stopY), stopY != middleY, scale);		

			break;


		case 5:			
			startX = x + Math.round((arrowSize * STICK_HEIGHT_RATIO + boxSize) / 2);
			startY = y + Math.round(arrowSize * STICK_HEIGHT_RATIO / 2);

			middleX = startX ;
			middleY = startY + Math.round(arrowSize * END_FEATHERING_RATIO);

			stopX = startX + Math.round(arrowSize * END_FEATHERING_RATIO) ;
			stopY = startY + Math.round(arrowSize * END_FEATHERING_RATIO);

			drawCurvedLine(canvas, new Point(startX,  startY), new Point(middleX,  middleY), new Point(stopX,  stopY), arrowPaint, scale);
			drawFeathering(canvas, new Point(stopX, stopY), stopY != middleY, scale);		

			break;

		case 6:			
			startX = x + boxSize;
			startY = y + boxSize;

			middleX = startX - arrowSize;
			middleY = startY - arrowSize;

			stopX = middleX;
			stopY = middleY + Math.round(arrowSize * END_FEATHERING_RATIO / 1.5f);

			drawCurvedLine(canvas, new Point(startX,  startY), new Point(middleX,  middleY), new Point(stopX,  stopY), arrowPaint, scale);
			drawFeathering(canvas, new Point(stopX, stopY), stopY != middleY, scale);		

			break;

		case 7:			
			startX = x + Math.round(arrowSize * STICK_HEIGHT_RATIO / 2);;
			startY = y + boxSize;

			middleX = startX + arrowSize;
			middleY = startY - arrowSize;

			stopX = middleX;
			stopY = middleY + Math.round(arrowSize * END_FEATHERING_RATIO / 1.5f);

			drawCurvedLine(canvas, new Point(startX,  startY), new Point(middleX,  middleY), new Point(stopX,  stopY), arrowPaint, scale);
			drawFeathering(canvas, new Point(stopX, stopY), stopY != middleY, scale);		

			break;

		case 8:			
			startX = x + boxSize;
			startY = y + Math.round(arrowSize * STICK_HEIGHT_RATIO / 2);

			middleX = startX - arrowSize;
			middleY = startY + arrowSize;

			stopX = middleX + Math.round(arrowSize * END_FEATHERING_RATIO / 1.5f);
			stopY = middleY;

			drawCurvedLine(canvas, new Point(startX,  startY), new Point(middleX,  middleY), new Point(stopX,  stopY), arrowPaint, scale);
			drawFeathering(canvas, new Point(stopX, stopY), stopY != middleY, scale);		

			break;

		case 9:			
			startX = x + boxSize;
			startY = y + boxSize;

			middleX = startX - arrowSize;
			middleY = startY - arrowSize;

			stopX = middleX + Math.round(arrowSize * END_FEATHERING_RATIO / 1.5f);
			stopY = middleY;

			drawCurvedLine(canvas, new Point(startX,  startY), new Point(middleX,  middleY), new Point(stopX,  stopY), arrowPaint, scale);
			drawFeathering(canvas, new Point(stopX, stopY), stopY != middleY, scale);		


			break;
			
		case 10:			
			startX = x;
			startY = y + boxSize;

			middleX = startX + Math.round(arrowSize * STICK_SMALL_RATIO);
			middleY = startY - Math.round(arrowSize * STICK_SMALL_RATIO);

			stopX = middleX + Math.round(arrowSize * END_FEATHERING_RATIO);
			stopY = middleY;

			drawCurvedLine(canvas, new Point(startX,  startY), new Point(middleX,  middleY), new Point(stopX,  stopY), arrowPaint, scale);
			drawFeathering(canvas, new Point(stopX, stopY), stopY != middleY, scale);		

			break;
			
		case 11:			
			startX = x;
			startY = y;

			middleX = startX + Math.round(arrowSize * STICK_SMALL_RATIO);
			middleY = startY + Math.round(arrowSize * STICK_SMALL_RATIO);

			stopX = middleX + Math.round(arrowSize * END_FEATHERING_RATIO);
			stopY = middleY;

			drawCurvedLine(canvas, new Point(startX,  startY), new Point(middleX,  middleY), new Point(stopX,  stopY), arrowPaint, scale);
			drawFeathering(canvas, new Point(stopX, stopY), stopY != middleY, scale);		

			break;
			
		case 12:			
			startX = x + boxSize;
			startY = y;

			middleX = startX - Math.round(arrowSize * STICK_SMALL_RATIO);
			middleY = startY + Math.round(arrowSize * STICK_SMALL_RATIO);

			stopX = middleX ;
			stopY = middleY + Math.round(arrowSize * END_FEATHERING_RATIO);

			drawCurvedLine(canvas, new Point(startX,  startY), new Point(middleX,  middleY), new Point(stopX,  stopY), arrowPaint, scale);
			drawFeathering(canvas, new Point(stopX, stopY), stopY != middleY, scale);		

			break;
			
		case 13:			
			startX = x;
			startY = y;

			middleX = startX + Math.round(arrowSize * STICK_SMALL_RATIO);
			middleY = startY + Math.round(arrowSize * STICK_SMALL_RATIO);

			stopX = middleX ;
			stopY = middleY + Math.round(arrowSize * END_FEATHERING_RATIO);

			drawCurvedLine(canvas, new Point(startX,  startY), new Point(middleX,  middleY), new Point(stopX,  stopY), arrowPaint, scale);
			drawFeathering(canvas, new Point(stopX, stopY), stopY != middleY, scale);		

			break;

		}

	}

	private void drawTriangle(Canvas canvas, Point a, Point b, Point c){

		float verts[] = {
				a.x, a.y, b.x, b.y, c.x, c.y
		};

		canvas.drawVertices(Canvas.VertexMode.TRIANGLES, verts.length, verts, 0, null, 0, verticesColors,   0, null, 0, 0, new Paint());

	}

	private void drawFeathering(Canvas canvas, Point startPoint, Boolean isVertical, float scale){
		int arrowSize = (int)(ARROW_SIZE * scale);		

		Point a = startPoint;
		Point b = null;
		Point c = null;

		if(isVertical){
			//top feathering
			b = new Point(a.x, a.y + Math.round((1 - END_FEATHERING_RATIO)  * arrowSize));
			c = new Point(a.x +  Math.round(arrowSize * (0.5f - VERTICAL_SHIFT_FEATHERING_RATIO)), a.y -  Math.round(arrowSize * (END_FEATHERING_RATIO - START_FEATHERING_RATIO)));
			drawTriangle(canvas, a, b, c);
			//bottom feathering
			c = new Point(a.x -  Math.round(arrowSize * (0.5f - VERTICAL_SHIFT_FEATHERING_RATIO)), a.y -  Math.round(arrowSize * (END_FEATHERING_RATIO - START_FEATHERING_RATIO)));
			drawTriangle(canvas, a, b, c);
		}else{
			//top feathering
			b = new Point(a.x + Math.round((1 - END_FEATHERING_RATIO)  * arrowSize), a.y);
			c = new Point(a.x -  Math.round(arrowSize * (END_FEATHERING_RATIO - START_FEATHERING_RATIO)), a.y - Math.round(arrowSize * (0.5f - VERTICAL_SHIFT_FEATHERING_RATIO)));
			drawTriangle(canvas, a, b, c);
			//bottom feathering
			c = new Point(a.x -  Math.round(arrowSize * (END_FEATHERING_RATIO - START_FEATHERING_RATIO)), a.y + Math.round(arrowSize * (0.5f - VERTICAL_SHIFT_FEATHERING_RATIO)));
			drawTriangle(canvas, a, b, c);
		}
	}

	private void drawCurvedLine(Canvas canvas, Point startPoint, Point middPoint, Point stopPoint, Paint arrowPaint , float scale){
		int arrowSize = (int)(ARROW_SIZE * scale);		
		arrowPaint.setStrokeWidth(arrowSize * STICK_HEIGHT_RATIO);
		arrowPaint.setStyle(Paint.Style.STROKE);
		arrowPaint.setStrokeCap(Paint.Cap.ROUND);
		Path path = new Path();
		path.moveTo(startPoint.x ,startPoint.y);
		//path.cubicTo(startX, startY, middleX, middleY, stopX, stopY);
		path.lineTo(middPoint.x, middPoint.y);
		path.moveTo(middPoint.x, middPoint.y);
		path.lineTo(stopPoint.x, stopPoint.y);
		path.close();				
		canvas.drawPath(path, arrowPaint);
	}
}