package org.skanword.and.scanwordgame;

import java.util.logging.Logger;

import org.skanword.and.datamanager.MainDataManager;
import org.skanword.and.datamanager.Skanword;
import org.skanword.and.datamanager.Skanword.Keyword.KeywordCell;
import org.skanword.and.datamanager.Skanword.Question;
import org.skanword.and.datamanager.Skanword.QuestionData;
import org.skanword.and.datamanager.Skanword.Replace;
import org.skanword.and.datamanager.SkanwordsDataManager;
import org.skanword.and.datamanager.UserUpdatesManager;
import org.skanword.and.etc.TutorialManager;
import org.skanword.and.etc.TutorialManager.TutorialStep;
import org.skanword.and.scanwordgame.SkanwordBox.BoxType;

import android.content.res.AssetManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Point;
import android.util.Log;

public class SkanwordBoard {
	private final Skanword mSkanword;
	private final SkanwordBox[][] mBoxes;
	private static boolean isLandscape;
	private Position highlightPosition;
	private Position highlightPositionPrev;
	private Question highlighQuestion;
	private float bitmapScale;
	private Bitmap cacheEmptyBitmap;
	private Bitmap cacheBitmap;

	private Bitmap selectedLayerBitmap;

	private static final Logger LOG = Logger
			.getLogger("ua.smapssproject.scanword");

	private CrosswordActions action = CrosswordActions.NONE;
	private boolean isUserHighlightLetterChange;
	private boolean mProgressChanged = false;

	public SkanwordBoard(Skanword skanword, int orientation,
			AssetManager assetManager) {
		super();
		mSkanword = skanword;
		isLandscape = (orientation == Configuration.ORIENTATION_LANDSCAPE);

		mBoxes = new SkanwordBox[skanword.getSettings().getWidth()][skanword
				.getSettings().getHeight()];
		// create empty cells
		for (int c = 0; c < skanword.getSettings().getWidth(); c++) {
			for (int r = 0; r < skanword.getSettings().getHeight(); r++) {
				mBoxes[c][r] = new SkanwordBox(c, r, skanword.getImageDir(),
						assetManager);

			}
		}
		if (skanword.getKeyword() != null)
			for (KeywordCell keywordCell : skanword.getKeyword().getCells()) {
				SkanwordBox box = mBoxes[keywordCell.getX()][keywordCell.getY()];
				box.setKeywordLetterNumber(keywordCell.getNumber());
			}
		// set questions
		QuestionData data = null;
		for (Question question : skanword.getQuestions()) {
			data = question.getQuestionData();
			mBoxes[data.getX()][data.getY()].addQuestionBox(question, true);
			int tempX;
			int tempY;
			// add empty box around image
			if (!data.getQuestionValue().isTextQuestion()) {

				for (int i = 0; i < data.getQuestionValue().getWidth(); i++) {
					for (int j = 0; j < data.getQuestionValue().getHeight(); j++) {
						if (!(i == 0 && j == 0)) {
							// add question
							tempX = data.getX() + i;
							tempY = data.getY() + j;
							if (mBoxes.length > tempX
									&& mBoxes[tempX].length > tempY) {
								mBoxes[tempX][tempY].addQuestionBox(question,
										false);
							}
						}
					}
				}
			}

			// add arrow
			tempX = question.getAnswer().getX();
			tempY = question.getAnswer().getY();
			if (mBoxes.length > tempX && mBoxes[tempX].length > tempY) {
				mBoxes[tempX][tempY].addArrow(ArrowType.create(question
						.getArrowData().getType()));
			}

			// add question relations to letter

			for (int i = 0; i < question.getAnswer().getLength(); i++) {
				if (question.getQuestionData().isVerticalOrientation()) {
					tempX = question.getAnswer().getX();
					tempY = question.getAnswer().getY() + i;
				} else {
					tempX = question.getAnswer().getX() + i;
					tempY = question.getAnswer().getY();
				}
				mBoxes[tempX][tempY].addQuestionRelation(question);
			}

		}
		highlightPosition = null;

		setAnswers();
		isUserHighlightLetterChange = false;
	}
	public boolean isProgressChanged() {
		return mProgressChanged;
	}
	public void setProgressChanged(boolean changed) {
		mProgressChanged = changed;
	}
	public SkanwordBox[][] getBoxes() {
		return mBoxes;
	}

	public Bitmap drawSkanword(float scale, boolean forceRenderAll) {
		Log.v("SkanwordsTime", "drawSkanword start");
		int boxSize = (int) (SkanwordBox.BOX_SIZE * scale);
		boolean renderAll = (scale != bitmapScale || cacheEmptyBitmap == null || forceRenderAll);
		if (renderAll) {
			Log.v("SkanwordsTime", "drawSkanword render all");
			action = CrosswordActions.CHANGE_SCALE;
			cacheEmptyBitmap = Bitmap.createBitmap(
					(int) (getWidth() * boxSize),
					(int) (getHeight() * boxSize), Bitmap.Config.RGB_565);
			cacheEmptyBitmap.eraseColor(Color.BLACK);
			Log.v("SkanwordsTime", "drawSkanword render all end");
		}
		bitmapScale = scale;
		Canvas canvas = new Canvas(cacheEmptyBitmap);
		int x = 0;
		int y = 0;
		boolean isSelected = false;
		boolean isActive = false;
		boolean changedPosition = highlightPositionPrev == null
				|| !highlightPosition.equals(highlightPositionPrev);
		SkanwordBox box = null;
		Log.v("SkanwordsTime", "drawSkanword draw boxes start");
		for (int c = 0; c < getWidth(); c++) {
			for (int r = 0; r < getHeight(); r++) {

				box = mBoxes[c][r];
				isSelected = (highlightPosition != null
						&& c == highlightPosition.across && r == highlightPosition.down);
				isActive = isAcriveWord(c, r);
 
				if (renderAll || box.isSelected() != isSelected
						|| box.isActiveWorld() != isActive || (box.isActiveWorld() && changedPosition)) {
					x = (int) (c * boxSize);
					y = (int) (r * boxSize);
					
					
					box.setHighlightLetter(isSelected);
					box.setHighlightdWord(isActive);
					
					box.drawBackground(canvas, x, y, scale);
					 
					
					
					if (box.getKeywordLetterNumber() > 0)
						box.drawKeywordFrame(canvas, x, y, scale, box.getKeywordLetterNumber());
					
					box.drawResultLetters(canvas, x, y, scale); 
					
					box.drawArrow(canvas, x, y, scale);
					
					
				}
			}
		}
		Log.v("SkanwordsTime", "drawSkanword draw boxes end");
		renderAll = renderAll || changedPosition;
		selectedLayerBitmap = null;

		cacheBitmap = cacheEmptyBitmap;
		if (selectedLayerBitmap != null) {
			Log.v("SkanwordsTime", "drawSkanword selected layer start");
			Canvas resultCanvas = new Canvas(cacheBitmap);
			resultCanvas.drawBitmap(selectedLayerBitmap, x, y, null);
			Log.v("SkanwordsTime", "drawSkanword selected layer end");
		}

		Log.v("SkanwordsTime", "drawSkanword end");
		return cacheBitmap;
	}

	public int getWidth() {
		return mSkanword.getSettings().getWidth();
	}

	public int getHeight() {
		return mSkanword.getSettings().getHeight();
	}

	public Position getHighlightLetter() {
		return highlightPosition;
	}
	public Question getHightlightQuestion() {
		return highlighQuestion;
	}
	public void setHighlightLetter(Position pos) {
		if (pos != null
				&& (highlightPosition == null || (pos.across < mBoxes.length && pos.down < mBoxes[pos.across].length))) {
			highlighQuestion = selectHighlighQuestion(pos);
			if (!pos.equals(highlightPosition)) {
				isUserHighlightLetterChange = true;
				SkanwordBox box = mBoxes[pos.across][pos.down];
				highlightPosition = pos;
				if (box.getBoxType() == BoxType.LETTER) {
					highlightPosition = pos;
				} else {
					highlightPosition = getStartInputAnswer(box.getQuestions()
							.get(0));
				}
			}
			SkanwordBox box = mBoxes[highlightPosition.across][highlightPosition.down];
			if (!box.isComplete() && !box.isHintOpen() && TutorialManager.getInstance().currentTutorialStep() == TutorialStep.TUTORIAL_WORD_ENTERED) {
				TutorialManager.getInstance().setTutorialStep(TutorialStep.TUTORIAL_CELL_FOR_HINT_SELECTED);
			}
			
			action = CrosswordActions.CHANGE_SELECT;
		}
	}

	public boolean isHighlightedBoxGuessed() {
		if (getHighlightedBox() == null || highlighQuestion == null)
			return false;
		return getHighlightedBox().isHintOpen()
				|| getHighlightedBox().isComplete();
	}

	public SkanwordBox getHighlightedBox() {
		if (getHighlightLetter() == null)
			return null;
		return mBoxes[highlightPosition.across][highlightPosition.down];
	}

	public int inputLetter(String letter, boolean byHint) {
		int result = 0;
		if (highlighQuestion != null && highlightPosition != null) {
			SkanwordBox b = getHighlightedBox();
			if (b != null) {

				action = CrosswordActions.CHANGE_INPUT;
				if (!(b.isHintOpen() || b.isComplete()) && letter != null) {
					for (Replace replace : mSkanword.getReplaces()) {
						if (b.getX() == replace.getCellX()
								&& b.getY() == replace.getCellY()
								&& letter.toLowerCase().equals(
										replace.getLetterSource()
												.toLowerCase())) {
							letter = replace.getLetterTarget().toUpperCase();
						}
					}
					mSkanword.setLetter(highlightPosition, new Point(
							getWidth(), getHeight()), letter, byHint);
					mProgressChanged = true;
					b.setResponse(letter);
					b.setHintOpen(byHint);
					result = checkIsCompleteQuestion(b, 1);

				} else if (letter == null)  {
					if (!isUserHighlightLetterChange) {
						b = getPrevLetterBox(highlightPosition.across, highlightPosition.down);
					}
					
					if (!b.isHintOpen() && !b.isComplete()) {
						b.setResponse(" ");
						b.setHintOpen(byHint);
						mSkanword.setLetter(highlightPosition, new Point(
								getWidth(), getHeight()), " ", byHint);
						mProgressChanged = true;
					}
				} 
				if (letter != null && (result < 1 || !highlighQuestion.isComplete()))
					nextLetter();
			}
			isUserHighlightLetterChange = false;
		}

		return result;
	}

	public Bitmap getActiveQuestion() {
		QuestionData questionData = null;
		Bitmap bitmap = null;
		int i = 0;
		while (bitmap == null) {
			questionData = mSkanword.getQuestions().get(i).getQuestionData();
			bitmap = mBoxes[questionData.getX()][questionData.getY()]
					.getTempBitmap();
			Log.i("  ", "bitmap =  " + bitmap);
			i++;
		}

		return bitmap;
	}

	synchronized public Skanword getCurrentSkanword() {
		return mSkanword;

	}
	public int openAll() { // TEST_BLOCK
		for (int c = 0; c < getWidth(); c++) {
			for (int r = 0; r < getHeight(); r++) {
				String mask = mSkanword.getMask()[c][r];
				if (mask != null && !mask.equals("false")) {
					String letter;
					for (int i = 0; i < SkanwordGameActivity.ALPHA.length(); i++) {
						letter = SkanwordGameActivity.ALPHA.substring(i, i + 1);
						if (SkanwordsDataManager.md5Answer(String.valueOf(letter), mSkanword.getId()).equals(mask)) {
							SkanwordBox box = mBoxes[c][r];
							for (Replace replace : mSkanword.getReplaces()) {
								if (box.getX() == replace.getCellX()
										&& box.getY() == replace.getCellY()
										&& letter.toLowerCase().equals(
												replace.getLetterSource()
														.toLowerCase())) {
									letter = replace.getLetterTarget().toUpperCase();
								}
							}
							box.setResponse(letter.toUpperCase());
							box.setHintOpen(true);
							checkIsCompleteQuestion(box, 0);
							mSkanword.setLetter(new Position(c, r), new Point(
									getWidth(), getHeight()), letter, true, false);
							mProgressChanged = true;
						}
					}
				}
				
			}
		}
		mSkanword.calculateFinishedQuestions();
		UserUpdatesManager.getInstance().updateScanwordMasks(mSkanword);
		return mSkanword.getQuestions().size();
	}
	public int openLetter() {
		int result = 0;
		if (highlightPosition != null) {
			String mask = mSkanword.getMask()[highlightPosition.across][highlightPosition.down];
			if (mask != null && !mask.equals("false")) {
				String letter;
				for (int i = 0; i < SkanwordGameActivity.ALPHA.length(); i++) {
					letter = SkanwordGameActivity.ALPHA.substring(i, i + 1);

					if (SkanwordsDataManager.md5Answer(String.valueOf(letter), mSkanword.getId()).equals(mask)) {
						result = inputLetter(letter.toUpperCase(), true);
						return result;
					}
				}
			}
		}
		return result;
	}

	private Question getQuestionForId(int questionId) {
		Question result = null;
		if (mSkanword != null && mSkanword.getQuestions() != null) {
			for (Question question : mSkanword.getQuestions()) {
				if (question.getId() == questionId) {
					result = question;
					break;
				}
			}
		}

		return result;
	}

	private void setAnswers() {
		if (mSkanword == null)
			return;
		if (mSkanword.getAnswers() != null
				&& mSkanword.getAnswers().length() > 0) {
			int rowId = 0;
			int colId = 0;
			String letter;
			for (int i = 0; i < mSkanword.getAnswers().length(); i++) {
				colId = (int) Math.floor(i / getHeight());
				rowId = i - colId * getHeight();
				if (colId < mBoxes.length && rowId < mBoxes[colId].length) {
					letter = mSkanword.getAnswers().substring(i, i + 1);
					if (letter.equals(" "))
						continue;

					boolean byHint = (mSkanword.getHints()
							.substring(i, i + 1)).equals(".");
					mBoxes[colId][rowId].setResponse(letter.toUpperCase());
					mBoxes[colId][rowId].setHintOpen(byHint);
					checkIsCompleteQuestion(mBoxes[colId][rowId], 0);
				}
			}
		}
	}

	private int checkIsCompleteQuestion(SkanwordBox inputBox, int tag) {
		int result = 0;
		if (inputBox != null && inputBox.getBoxType() == BoxType.LETTER
				&& inputBox.getQuestions() != null
				&& inputBox.getQuestions().size() > 0) {
			StringBuilder stringBuilder = null;
			int start = 0;
			int stop = 0;
			for (Question question : inputBox.getQuestions()) {
				stringBuilder = new StringBuilder();
				start = question.getQuestionData().isVerticalOrientation() ? question
						.getAnswer().getY() : question.getAnswer().getX();
				stop = start + question.getAnswer().getLength();
				SkanwordBox box = null;
				String latter = " ";
				boolean fullWord = true;
				for (int i = start; i < stop; i++) {
					box = question.getQuestionData().isVerticalOrientation() ? mBoxes[question
							.getAnswer().getX()][i] : mBoxes[i][question
							.getAnswer().getY()];
					if (box.getLatterResponse() != " ") {
						latter = box.getLatterResponse();
						// Replace some letters
						if (mSkanword.getReplaces() != null
								&& mSkanword.getReplaces().size() > 0) {
							for (Replace replace : mSkanword.getReplaces()) {
								if (box.getX() == replace.getCellX()
										&& box.getY() == replace.getCellY()
										&& latter.toLowerCase().equals(
												replace.getLetterSource()
														.toLowerCase())) {
									latter = replace.getLetterTarget();
								}
							}
						}
						stringBuilder.append(latter);
					} else {
						fullWord = false;
						break;
					}
				}
				boolean wasComplete = question.isComplete();
				question.setComplete(stringBuilder.length() == question
						.getAnswer().getLength()
						&& SkanwordsDataManager.md5Answer(stringBuilder.toString(), mSkanword.getId()).equals(
								question.getAnswer().getHash()));
				if (fullWord) {
					if (TutorialManager.getInstance().currentTutorialStep() == TutorialStep.TUTORIAL_LETTER_ENTERED)
						TutorialManager.getInstance().setTutorialStep(TutorialStep.TUTORIAL_WORD_ENTERED);
					else if (TutorialManager.getInstance().currentTutorialStep() == TutorialStep.TUTORIAL_LETTER_ENTERED_BY_HINT)
							TutorialManager.getInstance().setTutorialStep(TutorialStep.TUTORIAL_SKANWORD_COMPLETE);
				}

				result = question.isComplete() ? result + 1 : result;
			}
		}
		if (tag == 1 && result > 0) {
			mSkanword.calculateFinishedQuestions();
		}
		return result;
	}
	public boolean isKeywordGuessed() {
		if (mSkanword.getKeyword() == null)
			return false;
		if (!mSkanword.getKeyword().isGuessed()) {
			mSkanword.checkKeyword();
		}
		

		for (KeywordCell keywordCell : mSkanword.getKeyword().getCells()) {
			SkanwordBox box = mBoxes[keywordCell.getX()][keywordCell.getY()];
			if (keywordCell.isPersist())
				box.setCompletition();
		}
		
		return mSkanword.getKeyword().isGuessed();
	}
	public String getKeywordString() {
        StringBuilder sb = new StringBuilder();
        for (KeywordCell keywordCell : mSkanword.getKeyword().getCells()) {
			SkanwordBox box = mBoxes[keywordCell.getX()][keywordCell.getY()];
			sb.append(box.getLatterResponse());
		}
		return sb.toString();
	}
	private int checkIsCompleteQuestion(SkanwordBox inputBox) {
		return checkIsCompleteQuestion(inputBox, 0);
	}


	private Position getStartInputAnswer(Question question) {
		Position result = null;
		if (question != null) {
			boolean isFind = false;
			SkanwordBox box = null;
			int startAnswer = question.getQuestionData()
					.isVerticalOrientation() ? question.getAnswer().getY()
					: question.getAnswer().getX();
			int stopAnswer = startAnswer + question.getAnswer().getLength();
			LOG.info("START INDEX = " + startAnswer + " STOP INDEX = "
					+ stopAnswer + "vertical "
					+ question.getQuestionData().isVerticalOrientation());
			for (int i = startAnswer; i < stopAnswer; i++) {
				if (!isFind) {
					box = question.getQuestionData().isVerticalOrientation() ? mBoxes[question
							.getAnswer().getX()][i] : mBoxes[i][question
							.getAnswer().getY()];
					LOG.info(box.toString());
					if (box.getBoxType() == BoxType.LETTER
							&& box.getLatterResponse() == " ") {
						isFind = true;
						break;
					}
				} else {
					break;
				}

			}
			if (!isFind || !MainDataManager.getInstance().getOptions().isSkipGuessedLetters()) {
				box = mBoxes[question.getAnswer().getX()][question.getAnswer()
						.getY()];
			}

			result = new Position(box.getX(), box.getY());
		}
		LOG.info("X = " + result.across + " Y = " + result.down);

		return result;
	}
	
	@Override
	protected void finalize() throws Throwable {
		super.finalize();
	}

	private void nextLetter() {
		/*if (highlighQuestion.isComplete())
			return;*/
		int tempX = highlightPosition.across;
		int tempY = highlightPosition.down;
		nextLetter(tempX, tempY);

	}
	private void nextLetter(int cellX, int cellY) {
		int tempX = cellX;
		int tempY = cellY;
		if (highlighQuestion.getQuestionData().isVerticalOrientation()) {
			if (highlighQuestion.getAnswer().getY()
					+ highlighQuestion.getAnswer().getLength() > (tempY + 1)) {
				tempY++;
			} else {
				tempY = highlighQuestion.getAnswer().getY();
			}
		} else {
			if (highlighQuestion.getAnswer().getX()
					+ highlighQuestion.getAnswer().getLength() > (tempX + 1)) {
				tempX++;
			} else {
				tempX = highlighQuestion.getAnswer().getX();
			}
		}
		SkanwordBox box = mBoxes[tempX][tempY];

		if ((box.isComplete() || box.isHintOpen()) && MainDataManager.getInstance().getOptions().isSkipGuessedLetters() && !highlighQuestion.isComplete()) {
			//Log.v("", tempX + " -tempX " + tempY + "  - tempY " + box.isComplete() +" - isComplete " + box.isHintOpen() + " - isHintOpen " + highlighQuestion.isComplete() + " highlighQuestion.isComplete() ");
			nextLetter(tempX, tempY);
			return;
		}
		highlightPosition = new Position(tempX, tempY);
	}

	private SkanwordBox getPrevLetterBox(int cellX, int cellY) {
		int tempX = cellX;
		int tempY = cellY;
		if (highlighQuestion.getQuestionData().isVerticalOrientation()) {
			if (highlighQuestion.getAnswer().getY() < tempY) {
				tempY--;
			} else {
				tempY = highlighQuestion.getAnswer().getY()
						+ highlighQuestion.getAnswer().getLength() - 1;
			}
		} else {
			if (highlighQuestion.getAnswer().getX() < tempX) {
				tempX--;
			} else {
				tempX = highlighQuestion.getAnswer().getX()
						+ highlighQuestion.getAnswer().getLength() - 1;
			}
		}

		SkanwordBox box = mBoxes[tempX][tempY];

		if ((box.isComplete() || box.isHintOpen()) && MainDataManager.getInstance().getOptions().isSkipGuessedLetters() && !highlighQuestion.isComplete()) {
			 box = getPrevLetterBox(tempX, tempY);
			return box;
		}

		highlightPosition = new Position(tempX, tempY);
		return box;
	}

	private Question selectHighlighQuestion(Position pos) {
		Question result = mBoxes[pos.across][pos.down].getQuestions().get(0);
		SkanwordBox box = mBoxes[pos.across][pos.down];
		if (box.getQuestions().size() == 1) {
			result = box.getQuestions().get(0);
		} else if (box.getQuestions().size() == 2) {
			// second click
			if (highlighQuestion != null && highlightPosition.equals(pos)) {
				if (box.getQuestions().get(0).equals(highlighQuestion)) {
					result = box.getQuestions().get(1);
				} else {
					result = box.getQuestions().get(0);
				}

			} else if (highlighQuestion != null
					&& isAcriveWord(pos.across, pos.down)) {
				result = highlighQuestion;

				// start answer
			} else if (box.getQuestions().get(0).getAnswer().getX() == pos.across
					&& box.getQuestions().get(0).getAnswer().getY() == pos.down) {
				result = box.getQuestions().get(0);
			} else if (box.getQuestions().get(1).getAnswer().getX() == pos.across
					&& box.getQuestions().get(1).getAnswer().getY() == pos.down) {
				result = box.getQuestions().get(1);

			} else if ((result.getQuestionData().isVerticalOrientation() && isLandscape)
					|| (!result.getQuestionData().isVerticalOrientation() && !isLandscape)) {
				result = mBoxes[pos.across][pos.down].getQuestions().get(1);
			}
		}

		return result;
	}

	private boolean isAcriveWord(int x, int y) {
		boolean result = false;
		if (highlighQuestion != null && x < mBoxes.length
				&& y < mBoxes[x].length) {
			if (highlighQuestion.getQuestionData().isVerticalOrientation()) {
				result = (x == highlighQuestion.getAnswer().getX()
						&& y >= highlighQuestion.getAnswer().getY() && y < highlighQuestion
						.getAnswer().getY()
						+ highlighQuestion.getAnswer().getLength());
			} else {
				result = (y == highlighQuestion.getAnswer().getY()
						&& x >= highlighQuestion.getAnswer().getX() && x < highlighQuestion
						.getAnswer().getX()
						+ highlighQuestion.getAnswer().getLength());
			}
		}

		return result;
	}

    public static class Position {

        public int across;
        public int down;

        protected Position(){

        }

        public Position(int across, int down) {
            this.down = down;
            this.across = across;
        }

        public Position(Position p) {
            across = p.across;
            down = p.down;
        }

        @Override
        public boolean equals(Object o) {
            if ((o == null) || (o.getClass() != this.getClass())) {
                return false;
            }

            Position p = (Position) o;

            return ((p.down == this.down) && (p.across == this.across));
        }

        @Override
        public int hashCode() {
            return this.across ^ this.down;
        }

        @Override
        public String toString() {
            return "[" + this.across + " x " + this.down + "]";
        }
    }
	private static enum CrosswordActions {
		NONE, CHANGE_SCALE, CHANGE_SELECT, CHANGE_INPUT;
	}
	
}
