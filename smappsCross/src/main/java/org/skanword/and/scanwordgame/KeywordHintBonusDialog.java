package org.skanword.and.scanwordgame;

import org.skanword.and.R;
import org.skanword.and.SmappsScanwords;
import org.skanword.and.etc.AdsManager;
import org.skanword.and.etc.Utils;
import org.skanword.and.menu.HintsBonusDialog;
import org.skanword.and.menu.HintsBonusDialog.HintsBonusDialogDelegate;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.qustom.dialog.QustomDialogBuilder;

public class KeywordHintBonusDialog extends QustomDialogBuilder {

    private static HintsBonusDialogDelegate mDelegate;
	private static AlertDialog mDialog = null;
	private boolean mHadRewardedVideo = false;


	public KeywordHintBonusDialog(Context context, int theme) {
		super(context, theme);
	}

	public KeywordHintBonusDialog(Context context) {
        super(context);
	}
	
	@Override
	protected int getLayoutresId() {
    	layoutResId = R.layout.custom_dialog_layout;
		return super.getLayoutresId();
	} 
	
	private View mDialogView = null;
	
	public void setDialog(AlertDialog dialog) {
		mDialog = dialog;
	}
	
	public static AlertDialog getDialog() {
		return mDialog;
	}
	
	public View getDialogView() {
		return mDialogView;
	}

	public void setDialogView(View mDialog) {
		this.mDialogView = mDialog;
	}
	
	
	
	public boolean isHadRewardedVideo() {
		return mHadRewardedVideo;
	}

	public void setHadRewardedVideo(boolean mHadRewardedVideo) {
		this.mHadRewardedVideo = mHadRewardedVideo;
	}

	public static void showDialogInContext(Context context, HintsBonusDialogDelegate delegate, String keyword) {

		
		mDelegate = delegate;
		LayoutInflater li = LayoutInflater.from(context);
		final View view = li.inflate(R.layout.keyword_bonus_dialog_view, null);
        
        final KeywordHintBonusDialog qustomDialogBuilder = new KeywordHintBonusDialog(context, R.style.Theme_Scanword_style_dialog);
        
        qustomDialogBuilder.setCustomView(view);
        final AlertDialog dialog = qustomDialogBuilder.show();
        qustomDialogBuilder.setDialogView(view);
        dialog.setCancelable(false);
        qustomDialogBuilder.setDialog(dialog);
        qustomDialogBuilder.setHadRewardedVideo(AdsManager.getInstance().hasRewardedVideo(true));
        if (!qustomDialogBuilder.isHadRewardedVideo()) {
        	view.findViewById(R.id.closePanel).setVisibility(View.VISIBLE);
    		view.findViewById(R.id.closeButton).setOnClickListener(qustomDialogBuilder.takeClickListener);
        } else {
        	view.findViewById(R.id.bonusPanel).setVisibility(View.VISIBLE);
    		view.findViewById(R.id.getButton).setOnClickListener(qustomDialogBuilder.takeClickListener);
    		view.findViewById(R.id.doubleButton).setOnClickListener(qustomDialogBuilder.doubleClickListener);
        }
        
        

		qustomDialogBuilder.buildKeyword((LinearLayout)view.findViewById(R.id.keywordLayout), keyword);
	}

	public void buildKeyword(LinearLayout keywordLayout, String keyword) {
		LayoutInflater inflater = LayoutInflater.from(SmappsScanwords.getContext());		
		keywordLayout.removeAllViewsInLayout();

		for (int i = 0;  i < keyword.length(); i++ ) {

			String letter = keyword.substring(i, i+1).toUpperCase();
			
			final View letterView = inflater.inflate(R.layout.question_letter, null);
			keywordLayout.addView(letterView);
			LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) letterView.getLayoutParams();
			params.rightMargin = 15;
			params.bottomMargin = 2;
			letterView.setLayoutParams(params);
			letterView.findViewById(R.id.letter_view).setBackgroundResource(R.drawable.guessed_keyword_letter_cell);
			if (!letter.equalsIgnoreCase(" ")) {
				((TextView)letterView.findViewById(R.id.letter_text)).setText(letter);
			}
		}
	}
	public static boolean isShowing() {
		return getDialog() != null && getDialog().isShowing();
	}

	public static void showCompleteBonusDialogInContext(Context context, boolean doubled, OnCancelListener onCancelListener) {
		LayoutInflater li = LayoutInflater.from(context);
		View view = li.inflate(R.layout.bonus_complete_dialog_view, null);
        
        final KeywordHintBonusDialog qustomDialogBuilder = new KeywordHintBonusDialog(context, R.style.Theme_Scanword_style_dialog);
        qustomDialogBuilder.setCustomView(view);
        final AlertDialog dialog = qustomDialogBuilder.show();
        qustomDialogBuilder.setDialogView(view);
        qustomDialogBuilder.setDialog(dialog);
        
        boolean hasRewardedVideo = AdsManager.getInstance().hasRewardedVideo(true);
        TextView headLabel = (TextView) view.findViewById(R.id.headLabel); 
        int hintsCount = 1 * (doubled ? 2 : 1);
        if (!hasRewardedVideo) {
        	headLabel.setText("Поздравляем!");
        	view.findViewById(R.id.simpleHintsCountsArea).setVisibility(View.VISIBLE);
        	((TextView)view.findViewById(R.id.hintsReceivedLabel)).setText("Вам начислено " + hintsCount 
    				+ " " + Utils.spellVariantForNumber(hintsCount, "подсказку", "подсказки", "подсказок"));
        } else {
        	view.findViewById(R.id.videoHintsCountsArea).setVisibility(View.VISIBLE);
        	headLabel.setText("Начислено " + hintsCount 
            				+ " " + Utils.spellVariantForNumber(hintsCount, "подсказку", "подсказки", "подсказок") + ":");
        	((TextView)view.findViewById(R.id.hintsCountLabel)).setText("1");
        	((TextView)view.findViewById(R.id.doubledLabel)).setText(doubled ? "да" : " нет");
        	((TextView)view.findViewById(R.id.doubledLabel)).setTextColor(doubled ? Color.parseColor("#99cc00") : Color.parseColor("#ff4444"));
        }

    	view.findViewById(R.id.dev1).setVisibility(View.GONE);
    	view.findViewById(R.id.notificationArea).setVisibility(View.GONE);
        

		view.findViewById(R.id.closeButton).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				mDialog = null;
				dialog.cancel();
			}
		});
		dialog.setOnCancelListener(onCancelListener);
        
	}
	public View.OnClickListener takeClickListener = new View.OnClickListener() {

		@Override
		public void onClick(View v) {
			AlertDialog a = mDialog;
			mDialog = null;
			if (mDelegate == null)
				return;
			mDelegate.onTake(mHadRewardedVideo);
			if (a != null)
				a.dismiss();
		}
	};
	public View.OnClickListener doubleClickListener = new View.OnClickListener() {

		@Override
		public void onClick(View v) {
			AlertDialog a = mDialog;
			mDialog = null;
			mDelegate.onDouble();
			if (a != null)
				a.dismiss();
		}
	};
	
	public OnCancelListener mCompleteDialogCancelListener = new OnCancelListener() {
		@Override
		public void onCancel(DialogInterface dialog) {
			
		}
	};
}
