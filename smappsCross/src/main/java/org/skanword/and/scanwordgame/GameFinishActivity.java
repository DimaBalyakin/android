package org.skanword.and.scanwordgame;

import org.skanword.and.R;
import org.skanword.and.SmappsScanwords;
import org.skanword.and.SmappsScanwords.ActionType;
import org.skanword.and.datamanager.MainDataManager;
import org.skanword.and.datamanager.Skanword;
import org.skanword.and.etc.Utils;
import org.skanword.and.network.MainNetworkManager;
import org.skanword.and.network.socialnetworks.SocialNetworkManager.SocialNetwork;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.musselwhizzle.dispatcher.events.Event;
import com.musselwhizzle.dispatcher.events.EventListener;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.qustom.dialog.QustomDialogBuilder;

public class GameFinishActivity extends Activity {
	private Skanword mScanword;
	private EventListener mUserInfoEventListener;
	private EventListener mProgressUpdateListener;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.game_finish_activity);
	    ActionBar actionBar = getActionBar();
	    actionBar.setDisplayHomeAsUpEnabled(true);
	    setTitle("Поздравляем!");
		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
	    
		mScanword = (Skanword) getIntent().getSerializableExtra(SkanwordGameActivity.EXTRA_SKANWORD_OBJECT);
		
		int totalPoints = 0;
		if (mScanword.getLevel() == 1) {
	         totalPoints = 150;
	    }else if (mScanword.getLevel() == 2) {
	         totalPoints = 200;
	    } else if (mScanword.getLevel() == 3) {
	         totalPoints = 250;
	    }
		TextView diffPointsTextView = (TextView) findViewById(R.id.difficultyEarnedPointsLabel);
		diffPointsTextView.setText(totalPoints + " " + Utils.spellVariantForNumber(totalPoints, "очко", "очка", "очков") + " " + getResources().getString(R.string.difficalty_points_label));
		TextView solvationPointsTextView = (TextView) findViewById(R.id.answersEarnedPointsLabel);
		solvationPointsTextView.setText(mScanword.getQuestions().size() + " " + Utils.spellVariantForNumber(mScanword.getQuestions().size(), "очко", "очка", "очков") + " " + getResources().getString(R.string.answers_points_label));
		TextView totalPointsTextView = (TextView) findViewById(R.id.totalEarnedRatingPoints);
		totalPoints += mScanword.getQuestions().size();
		totalPointsTextView.setText(totalPoints + " " + Utils.spellVariantForNumber(totalPoints, "очко", "очка", "очков") + " " + getResources().getString(R.string.rating_points_label));
		
		View authLayout = findViewById(R.id.authLayout);
		authLayout.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				showAuthDialog();
				SmappsScanwords.sendGAEvent(ActionType.USER_ACTION,	"click_gamefinish_login"); // GA Event
			}
		});
		View noNetworkView = findViewById(R.id.noNetworkLabel);
		noNetworkView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				
			}
		});
		
		((Button)findViewById(R.id.newScanwordButton)).setOnClickListener(new OnClickListener() {					
			public void onClick(View v) {
				setResult(RESULT_OK, null);
			    finish();	
				SmappsScanwords.sendGAEvent(ActionType.USER_ACTION, "click_gamefinish_chooseanother"); // GA Event						
			}
		});
		mUserInfoEventListener = new EventListener() {
			public void onEvent(Event event) {
				runOnUiThread(new Runnable() {
					public void run() {
						updateUserInfo();
					}
				});
			}
		};
		
		SmappsScanwords.getEventsDispatcher().addListener(
					MainDataManager.EVENT_USER_INFO_UPDATE, mUserInfoEventListener);
		mProgressUpdateListener = new EventListener() {
			public void onEvent(Event event) {
				runOnUiThread(new Runnable() {
					public void run() {
						setResult(555, null);
					    finish();	
					}
				});
			}
		};
		
		SmappsScanwords.getEventsDispatcher().addListener(
					MainDataManager.EVENT_PROGRESS_UPDATE, mProgressUpdateListener);
		updateUserInfo();
	}
	private void updateUserInfo() {

		View notAuthedLayout = findViewById(R.id.authLayout);
		View authedLayout = findViewById(R.id.authedLayout);
		View noNetworkView = findViewById(R.id.noNetworkLabel);
		if (!MainNetworkManager.getInstance().hasNetworkConnection()) {
			notAuthedLayout.setVisibility(View.GONE);
			authedLayout.setVisibility(View.GONE);
			noNetworkView.setVisibility(View.VISIBLE);
			return;
		}
		noNetworkView.setVisibility(View.GONE);
		if (MainNetworkManager.getInstance().isUserAuthorised()) {
			notAuthedLayout.setVisibility(View.GONE);
			authedLayout.setVisibility(View.VISIBLE);

    		if (MainNetworkManager.getInstance().getUserAuth().getAvatarUrl() != null) {
    			DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
    					.showImageForEmptyUri(R.drawable.avatar)
    					.showImageOnFail(R.drawable.avatar).build();
    			ImageLoader.getInstance().displayImage(MainNetworkManager.getInstance().getUserAuth().getAvatarUrl(), (ImageView) findViewById(R.id.profile_image), options);
    		}
        	TextView userName = (TextView) findViewById(R.id.userNameLabel);
        	userName.setText(MainNetworkManager.getInstance().getUserAuth().getFullName());
        	TextView placeLabel = (TextView) findViewById(R.id.placeLabel);
        	placeLabel.setText(MainDataManager.getInstance().getUserData().getPlaceScanwords() + " " + getResources().getString(R.string.place_label));
        	TextView pointsLabel = (TextView) findViewById(R.id.pointsLabel);
        	pointsLabel.setText(MainDataManager.getInstance().getUserData().getScoreScanwords() + " " + Utils.spellVariantForNumber(MainDataManager.getInstance().getUserData().getScoreScanwords(), "очко", "очка", "очков"));
		} else {
			notAuthedLayout.setVisibility(View.VISIBLE);
			authedLayout.setVisibility(View.GONE);
		}
	}
	private void showAuthDialog() {
		LayoutInflater li = LayoutInflater.from(this);
		View view = li.inflate(R.layout.auth_dialog_layout, null);
        
        QustomDialogBuilder qustomDialogBuilder = new QustomDialogBuilder(this, R.style.Theme_Scanword_style_dialog). 
                setDividerColor("#44000000").setTitle("Авторизироваться сейчас:").setTitleColor("#000000");
        qustomDialogBuilder.setPositiveButton("Отмена", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    	
                    }
                });
        
        qustomDialogBuilder.setCustomView(view);
        final AlertDialog dialog = qustomDialogBuilder.show();
        View.OnClickListener authListener = new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (v.getId() == R.id.fbAuthButton) {
					MainNetworkManager.getInstance().loginInSocialNetwork(SocialNetwork.SOCIAL_NETWORK_FB, GameFinishActivity.this, true);
				} else if (v.getId() == R.id.okAuthButton) {
					MainNetworkManager.getInstance().loginInSocialNetwork(SocialNetwork.SOCIAL_NETWORK_OK, GameFinishActivity.this, true);
				} else if (v.getId() == R.id.vkAuthButton) {
					MainNetworkManager.getInstance().loginInSocialNetwork(SocialNetwork.SOCIAL_NETWORK_VK, GameFinishActivity.this, true);
				}
				dialog.dismiss();
			}
		};
		Button authButton = (Button) view.findViewById(R.id.okAuthButton);
		authButton.setOnClickListener(authListener);
		authButton = (Button) view.findViewById(R.id.fbAuthButton);
		authButton.setOnClickListener(authListener);
		authButton = (Button) view.findViewById(R.id.vkAuthButton);
		authButton.setOnClickListener(authListener);
	}
	
	@Override
    public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
	    case android.R.id.home:
			setResult(RESULT_CANCELED, null);
	        this.finish();
	        return true;
	    }
        return super.onOptionsItemSelected(item);
    }

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		MainNetworkManager.getInstance().onActivityResult(this, requestCode, resultCode, data);
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		SmappsScanwords.getEventsDispatcher().removeListener(
				MainDataManager.EVENT_USER_INFO_UPDATE, mUserInfoEventListener);
		SmappsScanwords.getEventsDispatcher().removeListener(
				MainDataManager.EVENT_PROGRESS_UPDATE, mProgressUpdateListener);
	}
	
	
	
}
