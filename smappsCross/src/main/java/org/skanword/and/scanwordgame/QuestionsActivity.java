package org.skanword.and.scanwordgame;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.skanword.and.R;
import org.skanword.and.SmappsScanwords;
import org.skanword.and.datamanager.Skanword;
import org.skanword.and.datamanager.Skanword.Keyword.KeywordCell;
import org.skanword.and.datamanager.Skanword.Question;
import org.skanword.and.etc.TopActivityManager.TopActivity;

import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.astuetz.PagerSlidingTabStrip;

public class QuestionsActivity extends FragmentActivity implements ActionBar.TabListener, TopActivity{

	public static final String EXTRA_CROSS = "cross";
	public static final String EXTRA_QUESTIONS = "EXTRA_QUESTIONS";
	public static final String EXTRA_SOLVE_STATE = "EXTRA_SOLVE_STATE";
	private Skanword mSkanword;
	private PagerSlidingTabStrip tabs;
    ViewPager mViewPager;
    AppSectionsPagerAdapter mAppSectionsPagerAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.questions);
		mSkanword = (Skanword) getIntent().getSerializableExtra(SkanwordGameActivity.EXTRA_SKANWORD_OBJECT);
		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

		mAppSectionsPagerAdapter = new AppSectionsPagerAdapter(getSupportFragmentManager(),mSkanword);

		tabs = (PagerSlidingTabStrip) findViewById(R.id.tabs_questions);

        mViewPager = (ViewPager) findViewById(R.id.pager_questions);
        mViewPager.setAdapter(mAppSectionsPagerAdapter);
        mViewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                // When swiping between different app sections, select the corresponding tab.
                // We can also use ActionBar.Tab#select() to do this if we have a reference to the
                // Tab.
            	Log.v("","onPageSelected " + position);
            }
        });
		tabs.setViewPager(mViewPager);
		mAppSectionsPagerAdapter.notifyDataSetChanged();
	    ActionBar actionBar = getActionBar();
	    actionBar.setDisplayHomeAsUpEnabled(true);
	    setTitle("Список вопросов");
	    buildKeyword((LinearLayout)findViewById(R.id.keywordLayout));
	}
	
	public void buildKeyword(LinearLayout keywordLayout) {
		if (mSkanword.getKeyword() == null || mSkanword.getKeyword().getCells().size() < 1) {
			keywordLayout.setVisibility(View.GONE);
			return;
		}
		LayoutInflater inflater = LayoutInflater.from(SmappsScanwords.getContext());		
		keywordLayout.removeAllViewsInLayout();

		SkanwordBox[][] boxes = getBoxes(); 
		for (KeywordCell keywordCell : mSkanword.getKeyword().getCells()) {
			SkanwordBox box = boxes != null ? boxes[keywordCell.getX()][keywordCell.getY()] : null;
			if (box == null) {
				finish();
				return;
			}
			String letter = box.getLatterResponse();
			boolean hint = box != null ? box.isHintOpen() || box.isComplete() : false;
			
			final View letterView = inflater.inflate(R.layout.question_letter, null);
			keywordLayout.addView(letterView);
			LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) letterView.getLayoutParams();
			params.rightMargin = 10;
			params.bottomMargin = 2; 
			letterView.setLayoutParams(params);
			letterView.findViewById(R.id.letter_view).setBackgroundResource(R.drawable.unguessed_keyword_letter_cell);
			
			if (hint) {
				letterView.findViewById(R.id.letter_view).setBackgroundResource(R.drawable.guessed_keyword_letter_cell);
			}
			if (!letter.equalsIgnoreCase(" ")) {
				((TextView)letterView.findViewById(R.id.letter_text)).setText(letter);
			}
		}
	}
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	public static SkanwordBox[][] getBoxes() {
		if (SkanwordGameActivity.sGetSkanwordBoard() != null)
			return SkanwordGameActivity.sGetSkanwordBoard().getBoxes();
		return null;
	}
	@Override
    public boolean onOptionsItemSelected(MenuItem item) {switch (item.getItemId()) {
	    case android.R.id.home:
	        this.finish();
	        return true;
	    }
        return super.onOptionsItemSelected(item);
    }

	public static class AppSectionsPagerAdapter extends FragmentStatePagerAdapter {

		private Skanword mScanword;
        public AppSectionsPagerAdapter(FragmentManager fm, Skanword scanword) {
            super(fm);
            mScanword = scanword;
        }

        @Override
        public Fragment getItem(int i) {
        	QuestionsFragment fragment = new QuestionsFragment();
            Bundle args = new Bundle();
            args.putSerializable(EXTRA_QUESTIONS, (Serializable) getQuestionsForList(i));
            args.putSerializable(EXTRA_CROSS, mScanword);
            fragment.setArguments(args);
            switch (i) {
			case 0:
				fragment.setEmptyLabelText("Ура! Все решено!");
				break;
			case 1:
				fragment.setEmptyLabelText("У вас нет ни одного решенного вопроса");
				break;

			default:
				break;
			}
            return fragment;
        }

	    @Override
	    public int getItemPosition(Object object){
	        return PagerAdapter.POSITION_NONE;
	    }
        private List<Question> getQuestionsForList(int i) {
        	List<Question> list = new ArrayList<Question>();
        	for (Question question : mScanword.getQuestions()) {
				if (i == 0 && !question.isComplete()) {
					list.add(question);
				} else if (i == 1 && question.isComplete()) {
					list.add(question);
				}
			}
        	return list;
        }
		@Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
        	int finishedCount = mScanword.isFinished() ? mScanword.getQuestions().size() : mScanword.getFinishedQuestionsCount();
        	switch (position) {
			case 0:
				return "НЕРЕШЁННЫЕ" + "(" + (mScanword.getQuestions().size() - finishedCount) + ")";
			case 1:
				return "РЕШЁННЫЕ" + "(" + finishedCount + ")";

			default:
				break;
			}
            return "Section " + (position + 1);
        }
    }

	public static class QuestionsFragment extends Fragment {

		@Override
		public void onSaveInstanceState(Bundle outState) {
			// TODO Auto-generated method stub
			super.onSaveInstanceState(outState);
			Log.v("", "  onSaveInstanceState ");
		}

		private Skanword mScanword;
		private ListView mListView = null;
		private QuestionsListAdapter mAdapter = null;
		private List<Question> mQuestions = null;
		private ItemAction mActions = null;
		private TextView mEmptyListLabel = null;
		private String mEmptyText = "";
		private String mScanwordImagesDir = "" ;
		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			// TODO Auto-generated method stub
			Bundle bundle = getArguments();
			final View view = inflater.inflate(R.layout.questions_list, container, false);
			mListView = (ListView) view.findViewById(R.id.questions_list);
		    mListView.setScrollBarStyle(ListView.SCROLLBARS_OUTSIDE_OVERLAY);
		    mQuestions =  (List<Question>) bundle.getSerializable(EXTRA_QUESTIONS);
			Log.v("SkanwordsTime", "question sort  1");
		    for (Question question : mQuestions) {
	    		question.setUnguessedCellsCount(0);
		    	if (question.isComplete()) {
		    		continue;
		    	}
		    	boolean vertical = question.getQuestionData().isVerticalOrientation();
				for (int i = 0; i < question.getAnswer().getLength(); i++) {
					int x = question.getAnswer().getX() + (vertical ? 0 : i);
					int y = question.getAnswer().getY() + (vertical ? i : 0);
					SkanwordBox box = QuestionsActivity.getBoxes() != null ? QuestionsActivity.getBoxes()[x][y] : null;
					boolean hint = box != null ? box.isHintOpen() || box.isComplete() : false;
					if (!hint)
						question.setUnguessedCellsCount(question.getUnguessedCellsCount() + 1);
				}
			}
		    Collections.sort(mQuestions, new Comparator<Question>() {

				@Override
				public int compare(Question lhs, Question rhs) {
					if (lhs.hasKeywordCell() && !rhs.hasKeywordCell()) {
						return -1;
					} else  if (!lhs.hasKeywordCell() && rhs.hasKeywordCell()) {
						return 1;
					} else {
						if (lhs.getUnguessedCellsCount() < rhs.getUnguessedCellsCount()) {
							return -1;
						} else if (rhs.getUnguessedCellsCount() < lhs.getUnguessedCellsCount()) {
							return 1;
						} else {
							if (lhs.getGuessedCellsCount() > rhs.getGuessedCellsCount()) {
								return -1;
							} else if (rhs.getGuessedCellsCount() > lhs.getGuessedCellsCount()) {
								return 1;
							}
						}
					}
					return 0;
				}
		    	
			});
			Log.v("SkanwordsTime", "question sort  2");
		    mScanword = (Skanword) bundle.getSerializable(EXTRA_CROSS);
		    mScanwordImagesDir = mScanword.getImageDir();
		    mEmptyListLabel = (TextView) view.findViewById(R.id.empty_quesstions_list);
			Log.v("",mAdapter + "onCreateView questionsActivity " + mScanword);
			mAdapter = new QuestionsListAdapter(getActivity(), R.layout.question_row_item, mQuestions, mScanwordImagesDir, mScanword, mActions);
			mListView.setAdapter(mAdapter);	
			if (mQuestions.size() > 0) {
				mEmptyListLabel.setVisibility(View.INVISIBLE);
			} else {
				mEmptyListLabel.setVisibility(View.VISIBLE);
			}
			mEmptyListLabel.setText(mEmptyText);
			
			mListView.setOnItemClickListener(new AdapterView.OnItemClickListener()  {

				public void onItemClick(AdapterView<?> parent, View view,
						int position, long id) {
					openQuestion(mQuestions.get(position));
				}

			});
			return view;
			
		}
		private void openQuestion(Question question) {
			Intent intent = new Intent(Intent.ACTION_EDIT, null, getActivity(), SkanwordGameActivity.class);
	        intent.putExtra(SkanwordGameActivity.EXTRA_SKAN_ID, mScanword.getId());
	        intent.putExtra(SkanwordGameActivity.EXTRA_SELETED_QUESTION, question);
	        this.getActivity().setResult(RESULT_OK, intent);
	        this.getActivity().finish();
	        //this.startActivity(intent);
		}
		public void setEmptyLabelText(String text) {
			mEmptyText = text;
		}
	}
	public static class QuestionsListAdapter extends ArrayAdapter<Question> {
		private final int mResourceId;
		private final String mScanwordImagesDir;
		private Skanword mScanword;
		private SkanwordBox[][] mBoxes;

		public QuestionsListAdapter(Context context, int resource,
				List<Question> objects,String scanwordImagesDir, Skanword scanword, ItemAction itemAction) {
			super(context, resource, objects);
			this.mScanwordImagesDir = scanwordImagesDir;
			this.mResourceId = resource;
			this.mScanword = scanword;
			mBoxes = QuestionsActivity.getBoxes();
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			if (convertView != null) {
				rebuildView(convertView, getItem(position));
			} else {
				convertView = createRowView(parent);
				rebuildView(convertView, getItem(position));
			}
			return convertView;
		}
		private void rebuildView(View view, Question question) {
			TextView questionText = (TextView)view.findViewById(R.id.question_text);
			questionText.setText(question.getQuestionData().getQuestionValue().getOriginal());
			if (!question.getQuestionData().getQuestionValue().isTextQuestion()) {
				Bitmap tempBitmap = BitmapFactory.decodeFile(mScanwordImagesDir +"/"+ question.getQuestionData().getQuestionValue().getUrl());
				Log.v("",mScanwordImagesDir +"/"+ question.getQuestionData().getQuestionValue().getUrl());
				ImageView myImage = (ImageView) view.findViewById(R.id.question_image);
			    myImage.setImageBitmap(tempBitmap);
				view.findViewById(R.id.question_image).setVisibility(View.VISIBLE);
				view.findViewById(R.id.question_text).setVisibility(View.GONE);
			} else {
				view.findViewById(R.id.question_image).setVisibility(View.GONE);
				view.findViewById(R.id.question_text).setVisibility(View.VISIBLE);
			}
			LayoutInflater inflater = LayoutInflater.from(getContext());		
			LinearLayout letters = (LinearLayout)view.findViewById(R.id.letters);
			letters.removeAllViewsInLayout();
			
			boolean vertical = question.getQuestionData().isVerticalOrientation();
			for (int i = 0; i < question.getAnswer().getLength(); i++) {
				int x = question.getAnswer().getX() + (vertical ? 0 : i);
				int y = question.getAnswer().getY() + (vertical ? i : 0);
				SkanwordBox box = mBoxes != null ? mBoxes[x][y] : null;
				int start = x * mScanword.getSettings().getHeight() + y;
				String letter = mScanword.getAnswers().substring(start, start + 1);
				boolean hint = box != null ? box.isHintOpen() || box.isComplete() : false;

				final View letterView = inflater.inflate(R.layout.question_letter, null);
				letters.addView(letterView);
				LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) letterView.getLayoutParams();
				params.rightMargin = 10;
				params.bottomMargin = 2; 
				letterView.setLayoutParams(params);
				if (question.isComplete() || hint) {
					letterView.findViewById(R.id.letter_view).setBackgroundResource(box.getKeywordLetterNumber() > 0 ? R.drawable.guessed_keyword_letter_cell : R.drawable.guessed_simple_letter_cell);
				} else {
					letterView.findViewById(R.id.letter_view).setBackgroundResource(box.getKeywordLetterNumber() > 0 ? R.drawable.unguessed_keyword_letter_cell : R.drawable.unguessed_simple_letter_cell);
				}
				if (!letter.equalsIgnoreCase(" ")) {
					((TextView)letterView.findViewById(R.id.letter_text)).setText(letter);
				}
			}
		}
		private View createRowView(ViewGroup parent) {
			LayoutInflater inflater = LayoutInflater.from(getContext());			 
			final View view = inflater.inflate(mResourceId, parent, false);
			return view;
		}
	}

	private static interface ItemAction {
		void clickItem(Question question);
	}
	public void onTabSelected(Tab tab, FragmentTransaction ft) {
		// TODO Auto-generated method stub
        mViewPager.setCurrentItem(tab.getPosition());
		
	}

	public void onTabUnselected(Tab tab, FragmentTransaction ft) {
		// TODO Auto-generated method stub
		
	}

	public void onTabReselected(Tab tab, FragmentTransaction ft) {
		// TODO Auto-generated method stub
		
	}



}
