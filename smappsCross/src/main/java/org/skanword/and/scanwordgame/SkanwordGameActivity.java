/**
 * This file is part of Words With Crosses.
 *
 * Copyright (C) 2009-2010 Robert Cooper
 * Copyright (C) 2013 Adam Rosenfield
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.skanword.and.scanwordgame; 

import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import javax.microedition.khronos.opengles.GL10;

import me.grantland.widget.AutofitTextView;

import org.skanword.and.R;
import org.skanword.and.SmappsScanwords;
import org.skanword.and.SmappsScanwords.ActionType;
import org.skanword.and.datamanager.MainDataManager;
import org.skanword.and.datamanager.ShopDataManager.Shop.ShopItemsSet.ShopItem;
import org.skanword.and.datamanager.ShopDataManager.ShopProductType;
import org.skanword.and.datamanager.Skanword;
import org.skanword.and.datamanager.Skanword.Question;
import org.skanword.and.datamanager.UserUpdatesManager;
import org.skanword.and.etc.AdsManager;
import org.skanword.and.etc.AdsManager.AdsEventsListener;
import org.skanword.and.etc.SoundsManager;
import org.skanword.and.etc.TopActivityManager.TopActivity;
import org.skanword.and.etc.TutorialManager;
import org.skanword.and.etc.TutorialManager.TutorialStep;
import org.skanword.and.etc.Utils;
import org.skanword.and.etc.versions.AndroidVersionUtils;
import org.skanword.and.inappbilling.InAppManager;
import org.skanword.and.inappbilling.InAppManager.PurchaseSource;
import org.skanword.and.menu.ArraySectionedListAdapter;
import org.skanword.and.menu.HintForVideoDialog;
import org.skanword.and.menu.HintForVideoDialog.HintForVideoDialogDelegate;
import org.skanword.and.menu.HintsBonusDialog.HintsBonusDialogDelegate;
import org.skanword.and.menu.shopmenu.MenuShop;
import org.skanword.and.network.MainNetworkManager;
import org.skanword.and.scanwordgame.PlayLayout.IChandeKeyboardViewListener;
import org.skanword.and.scanwordgame.SkanwordBoard.Position;
import org.skanword.and.scanwordgame.SkanwordImageView.ClickListener;
import org.skanword.and.scanwordgame.TouchImageView.ImageViewDelegate;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Rect;
import android.media.AudioManager;
import android.opengl.GLES11;
import android.opengl.GLES20;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.musselwhizzle.dispatcher.events.Event;
import com.musselwhizzle.dispatcher.events.EventListener;
import com.qustom.dialog.QustomDialogBuilder;

public class SkanwordGameActivity extends Activity implements TopActivity, ImageViewDelegate {

	/** Extra data tag required by this activity */
	public static final String EXTRA_SKAN_ID = "cross_id";
	public static final String EXTRA_SKANWORD_OBJECT = "EXTRA_SCANWORD_OBJECT";
	public static final String EXTRA_SCANWORD_BOXES_OBJECT = "EXTRA_SCANWORD_BOXES_OBJECT";
	public static final String EXTRA_SOLVE_STATE_OBJECT = "EXTRA_SOLVE_STATE_OBJECT";
	public static final String EXTRA_SET_ID = "set_id";
	public static final String EXTRA_SELETED_QUESTION = "sel_question";

	public static final String ALPHA = "йцукенгшщзхъфывапролджэёячсмитьбю";

	private static final Logger LOG = Logger
			.getLogger("com.adamrosenfield.wordswithcrosses");
	private TextView mHintsCountTextView = null;
	private TextView mQuestionsLeftCountTextView = null;

	private ProgressDialog loadProgressDialog;

	private int mSkanwordId;
	private SkanwordImageView mBoardView = null;
	private Toast mHintToast = null;

	private boolean adbotStep = true;

	private DisplayMetrics metrics;

	private MinishopListAdapter mMinishopListAdapter;

	private boolean hasSetInitialZoom = false;
	private View keyboardView;
	private PlayLayout playLayout;
	private EventListener mTimerEventListener;

	public static boolean isClickOpenKeyboard;
	private int heightKeyboard = 0;
	private static int[] maxTextureSizes;
	private static SkanwordBoard mBoard;
	private Skanword mSkanword;
	private Question mQuestionToOpen = null;
	private boolean mScanwordFinished = false;

	protected AndroidVersionUtils utils = AndroidVersionUtils.Factory
			.getInstance();
	protected SharedPreferences prefs;
	private EventListener mUserInfoEventListener;
	private View mActionBarView;
	private boolean mKeywordGuessed = false;
	
	private boolean mBannerLoaded = false;
	private EventListener mTutorialStateListener;

	public SkanwordGameActivity() {
		super();
		Log.v("", "ScanwordGameActivity  constructor");
	}

	/** Called when the activity is first created. */
	@SuppressLint("NewApi")
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		if (TutorialManager.getInstance().currentTutorialStep() == TutorialStep.TUTORIAL_ISSUE_OPENED)
			TutorialManager.getInstance().setTutorialStep(TutorialStep.TUTORIAL_SKANWORD_OPENED);
		
		this.prefs = PreferenceManager.getDefaultSharedPreferences(this);
		
		mSkanwordId = getIntent().getIntExtra(EXTRA_SKAN_ID, -1);
		mSkanword = (Skanword) getIntent().getSerializableExtra(
				EXTRA_SKANWORD_OBJECT);
		if (mSkanword == null) {
			mSkanword = SmappsScanwords.getDatabaseHelper().getFullskanwordData(mSkanwordId);
		}
		mScanwordFinished = mSkanword.isFinished(); 
		if (!mSkanword.hasFiles()) {
			setResult(556);
			finish();
			return;
		}
		
		this.setVolumeControlStream(AudioManager.STREAM_MUSIC);
		if (mSkanwordId == -1)
			throw new RuntimeException("WRONG ID crosswordId = " + mSkanwordId);
		for (Question question : mSkanword.getQuestions()) {
			question.setComplete(false);
		}
		// get max texture size of OPEN_GL
		if (maxTextureSizes == null) {
			maxTextureSizes = new int[1];
			GLES20.glGetIntegerv(GLES20.GL_MAX_TEXTURE_SIZE, maxTextureSizes, 0);

			if (maxTextureSizes[0] == 0) {
				GLES11.glGetIntegerv(GLES11.GL_MAX_TEXTURE_SIZE,
						maxTextureSizes, 0);
			}
			if (maxTextureSizes[0] == 0) {
				maxTextureSizes[0] = GL10.GL_MAX_TEXTURE_SIZE;
			}
		}
		
		getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
		metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);

		ActionBar actionBar = getActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setDisplayShowCustomEnabled(true);
		LayoutInflater linflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
		mActionBarView = linflater.inflate(R.layout.scanwords_game_action_bar, null);
		ActionBar.LayoutParams lp = new ActionBar.LayoutParams(
				ActionBar.LayoutParams.MATCH_PARENT,
				ActionBar.LayoutParams.MATCH_PARENT);
		mActionBarView.setLayoutParams(lp);
		actionBar.setCustomView(mActionBarView);
		mActionBarView.findViewById(R.id.use_hint_button).setOnClickListener(
				new View.OnClickListener() {

					public void onClick(View v) {
						useHint();
						SmappsScanwords.sendGAEvent(ActionType.USER_ACTION,
								"click_playscan_hintsbtn"); // GA Event
					}
				});
		mHintsCountTextView = (TextView) mActionBarView
				.findViewById(R.id.hints_count_label);
		mActionBarView.findViewById(R.id.use_hint_button).setOnLongClickListener(
				new OnLongClickListener() {
					public boolean onLongClick(View v) {
						showViewToast(v);
						return false;
					}
				});
		mActionBarView.findViewById(R.id.watchVideoButton).setOnClickListener(
				new OnClickListener() {
					public void onClick(View v) {
						SkanwordGameActivity.this.showVideoForHintDialog();
					}
				});
		mActionBarView.findViewById(R.id.watchVideoButton).setOnLongClickListener(
				new OnLongClickListener() {
					public boolean onLongClick(View v) {
						showViewToast(v);
						return false;
					}
				});
		mActionBarView.findViewById(R.id.questions_list_button).setOnClickListener(
				new View.OnClickListener() {

					public void onClick(View v) {
						goToQuestionsList();
						SmappsScanwords.sendGAEvent(ActionType.USER_ACTION,
								"click_playscan_questionsbtn"); // GA Event
					}
				});
		mActionBarView.findViewById(R.id.questions_list_button).setOnLongClickListener(
				new OnLongClickListener() {

					public boolean onLongClick(View v) {
						showViewToast(v);
						return false;
					}
				});
		mQuestionsLeftCountTextView = (TextView) mActionBarView
				.findViewById(R.id.questions_count_label);
		mActionBarView.findViewById(R.id.open_shop_button).setOnClickListener(
				new View.OnClickListener() {

					public void onClick(View v) {
						openShop(false);
						SmappsScanwords.sendGAEvent(ActionType.USER_ACTION,
								"click_playscan_openshop"); // GA Event
					}
				});
		mActionBarView.findViewById(R.id.open_shop_button).setOnLongClickListener(
				new OnLongClickListener() {

					public boolean onLongClick(View v) {
						showViewToast(v);
						return false;
					}
				});
		checkVideoOfferAvailability();
		// Must happen after all calls to requestWindowFeature()
		setContentView(R.layout.play);

		utils.holographic(this);
		setTitle("");

		playLayout = (PlayLayout) findViewById(R.id.gameLayout);

		postPuzzleLoaded();

		mTimerEventListener = new EventListener() {
			public void onEvent(Event event) {
				runOnUiThread(new Runnable() {
					public void run() {
						if (MainDataManager.getInstance().getShopDataManager()
								.shopItemsTypeWithSale(ShopProductType.HINTS))
							updateTimers();
						SkanwordGameActivity.this.checkVideoOfferAvailability();
					}
				});
			}
		};
		if (mTimerEventListener != null)
			SmappsScanwords.getEventsDispatcher().addListener(
					SmappsScanwords.APPLICATION_TIMER_EVENT,
					mTimerEventListener);

		updateHintsCountLabel();

		mUserInfoEventListener = new EventListener() {
			public void onEvent(Event event) {
				runOnUiThread(new Runnable() {
					public void run() {
						updateHintsCountLabel();
					}
				});
			}
		};
		
		SmappsScanwords.getEventsDispatcher().addListener(
					MainDataManager.EVENT_USER_INFO_UPDATE, mUserInfoEventListener);
		

		mTutorialStateListener = new EventListener() {
			
			@Override
			public void onEvent(Event event) {
				checkTutorialState();
			}
		};
		SmappsScanwords.getEventsDispatcher().addListener(TutorialManager.TUTORIAL_STATE_CHANGED, mTutorialStateListener);
		
		
		// AdmobBanner
		if (MainDataManager.getInstance().getAppAds().getCrossBanners() != null && MainDataManager.getInstance().getAppAds().getCrossBanners().size() > 0 
				&& MainDataManager.getInstance().getAppAds().getCrossBanners().get(0).getName().equals("admob") && MainDataManager.getInstance().getAppAds().getCrossBanners().get(0).getCoefficient() > 0) {

			AdView mAdView = (AdView) findViewById(R.id.adView);
			mAdView.setAdListener(new AdListener() {
				@Override
				public void onAdLoaded() {
					if (keyboardView.getVisibility() == View.GONE)
						findViewById(R.id.adView).setVisibility(View.VISIBLE);
					mBannerLoaded = true;
					super.onAdLoaded();
				}
			});
	        AdRequest adRequest = new AdRequest.Builder().build();
	        mAdView.loadAd(adRequest);
		}
	}

	protected void checkTutorialState() {
		
		boolean showBigTutorial = false;
		boolean showSmallTutorial = false;
		if (TutorialManager.getInstance().currentTutorialStep() == TutorialStep.TUTORIAL_SKANWORD_OPENED) {
			showBigTutorial = true;
		}
		if (TutorialManager.getInstance().currentTutorialStep() == TutorialStep.TUTORIAL_WORD_SELECTED
				|| TutorialManager.getInstance().currentTutorialStep() == TutorialStep.TUTORIAL_LETTER_ENTERED
				|| TutorialManager.getInstance().currentTutorialStep() == TutorialStep.TUTORIAL_LETTER_ENTERED_BY_HINT
				|| TutorialManager.getInstance().currentTutorialStep() == TutorialStep.TUTORIAL_WORD_ENTERED
				|| TutorialManager.getInstance().currentTutorialStep() == TutorialStep.TUTORIAL_CELL_FOR_HINT_SELECTED
				|| TutorialManager.getInstance().currentTutorialStep() == TutorialStep.TUTORIAL_SKANWORD_COMPLETE  ) {
			
			showSmallTutorial = true;
		}
		if (TutorialManager.getInstance().currentTutorialStep() == TutorialStep.TUTORIAL_SKANWORD_COMPLETE) {
			tutorialComplete();
		}
		final boolean fShowBig = showBigTutorial; 
		final boolean fShowSmall = showSmallTutorial; 
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				findViewById(R.id.tutorialView).setVisibility(fShowBig ? View.VISIBLE : View.GONE);
				findViewById(R.id.tutorialViewSmall).setVisibility(fShowSmall ? View.VISIBLE : View.GONE);
				findViewById(R.id.tutorialImageSmall).setVisibility(TutorialManager.getInstance().currentTutorialStep() == TutorialStep.TUTORIAL_WORD_SELECTED ? View.GONE : View.VISIBLE);
				
				if (TutorialManager.getInstance().currentTutorialStep() == TutorialStep.TUTORIAL_LETTER_ENTERED
						|| TutorialManager.getInstance().currentTutorialStep() == TutorialStep.TUTORIAL_LETTER_ENTERED_BY_HINT) {
					((TextView)findViewById(R.id.tutorialTextSmall)).setText("Правильный ответ будет выделен зеленым.");
				} else if (TutorialManager.getInstance().currentTutorialStep() == TutorialStep.TUTORIAL_WORD_ENTERED) {
					((ImageView)findViewById(R.id.tutorialImageSmall)).setImageResource(R.drawable.tutorial_img_cell_select);
					((TextView)findViewById(R.id.tutorialTextSmall)).setText("В трудной ситуации выручат\nподсказки. Выберите клетку.");
				} else if (TutorialManager.getInstance().currentTutorialStep() == TutorialStep.TUTORIAL_CELL_FOR_HINT_SELECTED) {
					((ImageView)findViewById(R.id.tutorialImageSmall)).setImageResource(R.drawable.tutorial_img_hint_use);
					((TextView)findViewById(R.id.tutorialTextSmall)).setText("Теперь нажмите на иконку с подсказками.");
				} else if (TutorialManager.getInstance().currentTutorialStep() == TutorialStep.TUTORIAL_SKANWORD_COMPLETE) {
					((ImageView)findViewById(R.id.tutorialImageSmall)).setImageResource(R.drawable.tutorial_img_complete);
					((TextView)findViewById(R.id.tutorialTextSmall)).setText("Теперь разгадывать сканворд не составит труда!");
				}
			}
		});
		
	}
	private void tutorialComplete() {
		SmappsScanwords.getScheduleExecutorService().schedule(new Runnable() {
			
			@Override
			public void run() {
				if (TutorialManager.getInstance().currentTutorialStep() == TutorialStep.TUTORIAL_SKANWORD_COMPLETE) {
					TutorialManager.getInstance().setTutorialStep(TutorialStep.TUTORIAL_COMPLETE);
				}
			}
		} , 3, TimeUnit.SECONDS);
	}
	private void letterEntered(boolean byHint) {
		if (TutorialManager.getInstance().currentTutorialStep() == TutorialStep.TUTORIAL_WORD_SELECTED || (TutorialManager.getInstance().currentTutorialStep() == TutorialStep.TUTORIAL_LETTER_ENTERED && byHint)) {
			TutorialManager.getInstance().setTutorialStep(byHint ? TutorialStep.TUTORIAL_LETTER_ENTERED_BY_HINT : TutorialStep.TUTORIAL_LETTER_ENTERED);
		}
	}

	private void hintTutorialStep() {
		if (TutorialManager.getInstance().currentTutorialStep() == TutorialStep.TUTORIAL_CELL_FOR_HINT_SELECTED) {
			TutorialManager.getInstance().setTutorialStep(TutorialStep.TUTORIAL_SKANWORD_COMPLETE);
		}
	}

	
	
	protected void showVideoForHintDialog() {
		boolean hasRewardedVideo = AdsManager.getInstance().hasRewardedVideo(false) && MainDataManager.getInstance().getUserData().getHintsForVideoCount() != 0;
		if (!MainNetworkManager.getInstance().hasNetworkConnection()) {
			AlertDialog.Builder builder = new AlertDialog.Builder(
					this, R.style.Theme_Scanword_style_dialog);
			builder.setMessage(
					"Для получения подсказки необходимо подключение к сети Интернет.")
					.setPositiveButton("Ок",null);
			AlertDialog dialog = builder.create();
			dialog.show();
			SmappsScanwords.sendGAEvent(ActionType.APP_ACTION, "hfv_dialog_no_network_click"); // GA Event
		} else if (hasRewardedVideo) {

			if (TutorialManager.getInstance().currentTutorialStep() == TutorialStep.TUTORIAL_CELL_FOR_HINT_SELECTED) {
				TutorialManager.getInstance().setTutorialStep(TutorialStep.TUTORIAL_COMPLETE);
			}
			
			HintForVideoDialog.showsDialogInContext(this, new HintForVideoDialogDelegate() {
				@Override
				public void onWatch() {
					AdsManager.getInstance().showVideoOffer(SkanwordGameActivity.this,
							new AdsEventsListener() {
								@Override
								public void onComplete() {
									super.onComplete();
									MainDataManager.getInstance().addUserHints(1, true);
									HintForVideoDialog.showCompleteWatchDialogInContext(SkanwordGameActivity.this);
								}

								@Override
								public void onNoAds() {
									super.onNoAds();
								}
							}, "hc" + mSkanword.getId(), false);
				}
				
				@Override
				public void onCancel() {
				}
			});
		} else {
			AlertDialog.Builder builder = new AlertDialog.Builder(
					this, R.style.Theme_Scanword_style_dialog);
			builder.setMessage("В данный момент, к сожалению, нет видео. Попробуйте позже.")
					.setPositiveButton("Ок",null);
			AlertDialog dialog = builder.create();
			dialog.show();
			SmappsScanwords.sendGAEvent(ActionType.APP_ACTION, "hfv_dialog_no_offer_click"); // GA Event
		}
	}

	protected void checkVideoOfferAvailability() {
		boolean hasRewardedVideo = AdsManager.getInstance().hasRewardedVideo(false) && MainDataManager.getInstance().getUserData().getHintsForVideoCount() != 0;
		boolean hasNetworkConenction = MainNetworkManager.getInstance().hasNetworkConnection();
		mActionBarView.findViewById(R.id.newVideoIcon).setVisibility(View.INVISIBLE);
		mActionBarView.findViewById(R.id.faqIcon).setVisibility(View.INVISIBLE);
		if (!hasNetworkConenction) {
			mActionBarView.findViewById(R.id.faqIcon).setVisibility(View.VISIBLE);
		} else if (hasRewardedVideo) {
			mActionBarView.findViewById(R.id.newVideoIcon).setVisibility(View.VISIBLE);
		}
	}

	private void updateTimers() {
		if (mMinishopListAdapter != null)
			mMinishopListAdapter.updateTimerView();
	}

	public void openCellForOffer() {
		hideKeyboard();
		SmappsScanwords.sendGAEvent(ActionType.USER_ACTION,
				"playscan_hints4video_video_enter"); // GA Event
		AdsManager.getInstance().showVideoOffer(this,
				new AdsEventsListener() {
					@Override
					public void onComplete() {
						super.onComplete();
						openLetter();
						if (mBoard != null) {
							Log.v("SkanwordsFunc", "openCellForOffer " + mBoard.isProgressChanged());
							saveScanwordState(mBoard.isProgressChanged());
						}
						SmappsScanwords.sendGAEvent(ActionType.USER_ACTION,
								"playscan_hints4video_video_shown"); // GA Event
					}

					@Override
					public void onNoAds() {
						super.onNoAds();
						SmappsScanwords.sendGAEvent(ActionType.USER_ACTION,
								"playscan_hints4video_video_nowshown"); // GA
																		// Event

						AlertDialog.Builder builder = new AlertDialog.Builder(
								SkanwordGameActivity.this,
								R.style.Theme_Scanword_style_dialog);
						builder.setMessage(
                                "Извините, сейчас у нас нет предложений для вас. Пожалуйста, попробуйте позже.")
								.setPositiveButton("Ок",
										new DialogInterface.OnClickListener() {
											public void onClick(
													DialogInterface dialog,
													int id) {

											}
										});
						AlertDialog dialog = builder.create();
						dialog.show();
					}
				}, "hc" + mSkanword.getId(), false);
	}

	private void showViewToast(View v) {
		String text = null;
		switch (v.getId()) {
		case R.id.use_hint_button:
			text = "Подсказка";
			break;
		case R.id.questions_list_button:
			text = "Список вопросов";
			break;
		case R.id.open_shop_button:
			text = "Магазин";
			break;
		case R.id.watchVideoButton:
			text = "Бесплатная подсказка";
			break;

		default:
			break;
		}
		if (text == null)
			return;
		Context context = getApplicationContext();
		int duration = Toast.LENGTH_SHORT;

		Toast toast = Toast.makeText(context, text, duration);
		final float scale = getApplicationContext().getResources()
				.getDisplayMetrics().density;
		int offset = (int) (48 * scale + 0.5f);
		toast.setGravity(Gravity.TOP | Gravity.LEFT,
				(int) (v.getLeft() * scale + 0.5f), offset);
		toast.show();

	}

	private boolean checkScanwordFinished() {
		if (mScanwordFinished)
			return mScanwordFinished;
		mScanwordFinished = mBoard.getCurrentSkanword().isFinished();
		if (mScanwordFinished) {
			Intent intent = new Intent(Intent.ACTION_EDIT, null, this,
					GameFinishActivity.class);
			intent.putExtra(SkanwordGameActivity.EXTRA_SKAN_ID,
					mSkanword.getId());
			intent.putExtra(SkanwordGameActivity.EXTRA_SKANWORD_OBJECT,
					mSkanword);
			this.startActivityForResult(intent, 99);
			SmappsScanwords.sendGAEvent(ActionType.USER_ACTION,
					"playscan_victory_enter"); // GA Event
			MainNetworkManager.getInstance().requestSyncExec();
		}
		return mScanwordFinished;
	}

	private void openShop(boolean hintUse) {

		boolean showHintsOffers = SmappsScanwords.getAppSharedPreferences(MainDataManager.GENERAL_PREFERENCES_NAME).getBoolean("show_hints_offers",
				false);
		boolean enableOffers = mBoard.getHighlightLetter() != null
				&& !mBoard.isHighlightedBoxGuessed() && showHintsOffers
				&& MainDataManager.getInstance().getUserData().getHints() < 1;
		
		if (AdsManager.getInstance().hasRewardedVideo(false) && hintUse && enableOffers) {
			SmappsScanwords.sendGAEvent(ActionType.USER_ACTION,
					"playscan_hints4video_hintuse_show"); // GA
															// Event

			AlertDialog.Builder builder = new AlertDialog.Builder(
					SkanwordGameActivity.this,
					R.style.Theme_Scanword_style_dialog);
			builder.setMessage(
                    "Открыть букву за просмотр видео?")
					.setPositiveButton("Ок",
							new DialogInterface.OnClickListener() {
								public void onClick(
										DialogInterface dialog,
										int id) {
									SmappsScanwords.sendGAEvent(ActionType.USER_ACTION,
											"playscan_hints4video_hintuse_approve"); // GA
																					// Event
									openCellForOffer();
								}
							}).setNegativeButton("Отмена", null).setOnCancelListener(new OnCancelListener() {
								
								@Override
								public void onCancel(DialogInterface dialog) {
									SmappsScanwords.sendGAEvent(ActionType.USER_ACTION,
											"playscan_hints4video_hintuse_cancel"); // GA
																					// Event
								}
							});
			AlertDialog dialog = builder.create();
			dialog.show();
			
			return;
		}
		
		LayoutInflater li = LayoutInflater.from(this);

		QustomDialogBuilder qustomDialogBuilder = new QustomDialogBuilder(this,
				R.style.Theme_Scanword_style_dialog).setDividerColor("#8fb815")
				.setTitle("Покупка подсказок").setTitleColor("#000000");
		qustomDialogBuilder.setPositiveButton("Закрыть",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						SmappsScanwords.sendGAEvent(ActionType.USER_ACTION,
								"click_minishop_close"); // GA Event
					}
				});
		View view = li.inflate(R.layout.mini_shop_layout, null);
		ListView lView = (ListView) view.findViewById(R.id.miniShopList);
		List<ShopItem> items = MainDataManager.getInstance()
				.getShopDataManager()
				.getShopItemsOfType(ShopProductType.MINI_SHOP);
		qustomDialogBuilder.setCustomView(view);
		Log.v("SkanwordsFunc", "openShop  " + showHintsOffers);
		mMinishopListAdapter = new MinishopListAdapter(this,
				R.layout.shop_item_row, items, this, enableOffers,
				qustomDialogBuilder.show());
		lView.setAdapter(mMinishopListAdapter);
		SmappsScanwords.sendGAEvent(ActionType.USER_ACTION,
				"playscan_minishop_enter"); // GA Event
	}

	public static SkanwordBoard sGetSkanwordBoard() {
		return mBoard;
	}
	public static boolean isGameActive() {
		return sGetSkanwordBoard() != null;
	}

	public void useHint() {
		if (MainDataManager.getInstance().getUserData().getHints() <= 0) {
			openShop(true);
			return;
		}

		if (mBoard.getHighlightLetter() == null) {
			AlertDialog.Builder builder = new AlertDialog.Builder(this,
					R.style.Theme_Scanword_style_dialog);
			builder.setMessage(
					"Выберите клетку сканворда, чтобы использовать подсказку.")
					.setPositiveButton("Ок",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {
									// SmappsScanwords.getExecutorService().execute(new
									// RestoreProgressRunnable());
								}
							});
			// Create the AlertDialog object and return it
			AlertDialog dialog = builder.create();
			dialog.show();
			return;
		}

		if (!mBoard.isHighlightedBoxGuessed() ) {
			MainDataManager.getInstance().addUserHints(-1, false);
			Context context = getApplicationContext();
			CharSequence text = "-1 подсказка";
			if (mHintToast != null) {
				mHintToast.cancel();
			}
			mHintToast = Toast.makeText(context, text, Toast.LENGTH_SHORT);
			mHintToast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
			mHintToast.show();
			updateHintsCountLabel();
			UserUpdatesManager.getInstance().usedHint(1);
			
			hintTutorialStep();
			
		}
		openLetter();
	}

	private void openLetter() {
		letterEntered(true);
		int result = mBoard.openLetter();
		updateQuestionCountLable(result);
		boolean keywordGuessed = checkKeywordComplete();
		render(result > 0 || keywordGuessed);
		mBoardView.ensureVisible(mBoard.getHighlightLetter());
		this.setVolumeControlStream(AudioManager.STREAM_MUSIC);
		updateQuestionTextView();
		if (keywordGuessed) {
			showKeywordBonus();
			return;
		}
		
		if (checkScanwordFinished()) {
			SoundsManager.playSound(this, SoundsManager.SOUND_WIN);
		} else {
			if (result > 0) {
				SoundsManager.playSound(this, SoundsManager.SOUND_WORD_CORRECT);
			} else {
				SoundsManager.playSound(this, SoundsManager.SOUND_HINT_LETTER);
			}
		}
	}

	private void showKeywordBonus() {
		KeywordHintBonusDialog.showDialogInContext(this, new HintsBonusDialogDelegate() {

        	private void showCompl(final boolean doubled, final boolean hadRewardedVideo) {
        		SkanwordGameActivity.this.runOnUiThread(new Runnable() {
					@Override
					public void run() {
						MainDataManager.getInstance().addUserHints(1, false);
						UserUpdatesManager.getInstance().bonusRecieved(doubled, hadRewardedVideo, false);
						KeywordHintBonusDialog.showCompleteBonusDialogInContext(SkanwordGameActivity.this, doubled, new OnCancelListener() {
							@Override
							public void onCancel(DialogInterface dialog) {
								checkScanwordFinished();
							}
						});
						MainNetworkManager.getInstance().requestSyncExec();
					}
				});
        	}
        	
			@Override
			public void onTake(boolean hadRewardedVideo) {
				showCompl(false, hadRewardedVideo); 
			}
			
			@Override
			public void onDouble() {
				AdsManager.getInstance().showVideoOffer(SkanwordGameActivity.this,
						new AdsEventsListener() {
							@Override
							public void onComplete() {
								super.onComplete();
								Log.v("SkanwordsAds", "onComplete listener");
								showCompl(true, true);
							}
							@Override
							public void onSkipped() {
								super.onSkipped();
								Log.v("SkanwordsAds", "videoSkipped listener");
								showCompl(false, true);
							}
							@Override
							public void onNoAds() {
								showCompl(false, false);
								Log.v("SkanwordsAds", "onNoAds listener");
							}
						}, "ck" + mSkanword.getId(), false);
			}
			
			@Override
			public void onCancel() {
				Log.v("SkanwordsFunc", "showKeywordBonus onCancel");
				checkScanwordFinished();
			}
		}, mBoard.getKeywordString());
	}

	private boolean checkKeywordComplete() {
		if (mSkanword.getKeyword() == null || mKeywordGuessed)
			return false;
		mKeywordGuessed = mBoard.isKeywordGuessed();
		return mKeywordGuessed;
	}

	public void updateQuestionCountLable(int count) {
		if (count > 0) {
			Context context = getApplicationContext();
			CharSequence text = "+"
					+ count
					+ " "
					+ Utils.spellVariantForNumber(count, "очко", "очка",
							"очков");
			if (mHintToast != null) {
				mHintToast.cancel();
			}
			mHintToast = Toast.makeText(context, text, Toast.LENGTH_SHORT);
			mHintToast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
			mHintToast.show();
		}
		if (count != 0)
			mQuestionsLeftCountTextView.setText(""
					+ (mSkanword.getQuestions().size() - mBoard
							.getCurrentSkanword().getFinishedQuestionsCount()));
	}

	public void updateHintsCountLabel() {
		mHintsCountTextView.setText(""
				+ MainDataManager.getInstance().getUserData().getHints());
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			hideKeyboard();
			this.finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private void saveScanwordState(boolean notify) {
		if (mBoard != null) {
			MainDataManager.getInstance().getSkanwordsDataManager()
				.saveSkanwordState(mBoard.getCurrentSkanword(), notify);
		}
	}

	private void postPuzzleLoaded() {
		initPlayboard();
		initKeyboard();
		// set selected question if opened from questions list
		int questionId = getIntent().getIntExtra(EXTRA_SELETED_QUESTION, -1);
		if (questionId >= 0 && questionId < mSkanword.getQuestions().size()) {

			final Question question = mSkanword.getQuestions().get(questionId);

			if (question != null) {
				Position pos = new Position(question.getQuestionData().getX(),
						question.getQuestionData().getY());
				mBoard.setHighlightLetter(pos);
				delayshowKeyboard();
			}
		}
	}

	public void cellClicked(Position pos) {
		if (mBoard == null || mBoardView == null) {
			finish();
			return;
		}
		boolean posChanged = !pos.equals(mBoard.getHighlightLetter());
		if (pos != null && !posChanged
				&& !isClickOpenKeyboard) {
			showKeyboard();
		} else {
			boolean keyboardClosed = !isClickOpenKeyboard;
			if (keyboardClosed) {
				showKeyboard();
			}
			if (pos != null) {
				mBoard.setHighlightLetter(pos);
			}
			mBoardView.zoomIn();
			render(false);
			if (!keyboardClosed || posChanged) {
				mBoardView.ensureVisible(mBoard.getHighlightLetter());
			}
		}
		updateQuestionTextView();
	}

	private void initPlayboard() {
		mBoardView = (SkanwordImageView) findViewById(R.id.board);
		mBoardView.setDelegate(this);
		Log.v("", mSkanword + "  " + mSkanword.getImageDir());
		mBoard = new SkanwordBoard(mSkanword,
				getResources().getConfiguration().orientation, getAssets());
		mKeywordGuessed = mBoard.isKeywordGuessed();
		mBoardView.setBoard(mBoard, metrics, maxTextureSizes[0]);
		this.registerForContextMenu(mBoardView);

		mBoardView.setClickListener(new ClickListener() {
			public void onClick(Position pos) {
				cellClicked(pos);
			}

			public void onDoubleClick(Position pos) {
			}

			public void onLongClick(Position pos) {
			}
		});

		playLayout
				.setOnChangeKeyboadListener(new IChandeKeyboardViewListener() {

					public void onChange(boolean isVisible) {
						if (mBoardView != null && mBoard != null
								&& mBoard.getHighlightLetter() != null) {
							isClickOpenKeyboard = isVisible;
							// boardView.refresh();
							// boardView.ensureVisible(board.getHighlightLetter());
						}

					}
				});

		playLayout.getViewTreeObserver().addOnGlobalLayoutListener(
				new OnGlobalLayoutListener() {
					private boolean isChangedKeyboard = true;

					public void onGlobalLayout() {
						if (mBoard == null || mBoardView == null)
							return;
						Log.v("", "onGlobalLayout");
						if (isChangedKeyboard
								&& mBoard.getHighlightLetter() != null) {
							isChangedKeyboard = false;
							mBoardView.ensureVisible(mBoard
									.getHighlightLetter());
						}
						Rect r = new Rect();
						((View) keyboardView.getParent())
								.getWindowVisibleDisplayFrame(r);

						int screenHeight = ((View) mBoardView.getParent())
								.getRootView().getHeight();
						int heightDifference = screenHeight
								- (r.bottom - r.top);
						LOG.info("heightDifference2 =" + heightDifference);
						// boardView.refresh();
						// add keyboard custom background
						if (heightDifference > 0
								&& heightKeyboard != heightDifference) {
							// scroll for hide keyboard background
							heightKeyboard = heightDifference;

							if (mBoardView != null && mBoard != null
									&& mBoard.getHighlightLetter() != null) {
								mBoardView.refresh();
								mBoardView.ensureVisible(mBoard
										.getHighlightLetter());
								// boardView.refresh();
							}
							isChangedKeyboard = true;
						}
						// remove keyboard custom background
						if (heightDifference == 0
								&& heightKeyboard != heightDifference) {
							heightKeyboard = heightDifference;

							if (mBoardView != null && mBoard != null) {
								mBoardView.refresh();
								if (mBoard.getHighlightLetter() != null) {
									mBoardView.ensureVisible(mBoard
											.getHighlightLetter());
								}
							}
							isChangedKeyboard = true;
						}
					}
				});

		updateQuestionCountLable(-1);

	}

	private void goToQuestionsList() {
		if (TutorialManager.getInstance().currentTutorialStep() == TutorialStep.TUTORIAL_CELL_FOR_HINT_SELECTED) {
			TutorialManager.getInstance().setTutorialStep(TutorialStep.TUTORIAL_COMPLETE);
		}
		Intent intent = new Intent(Intent.ACTION_EDIT, null, this,
				QuestionsActivity.class);
		intent.putExtra(SkanwordGameActivity.EXTRA_SKAN_ID, mSkanwordId);
		if (mSkanword != null) {
			intent.putExtra(SkanwordGameActivity.EXTRA_SKANWORD_OBJECT,
					mSkanword);
		}
		isClickOpenKeyboard = false;
		this.startActivityForResult(intent, 1);
		SmappsScanwords.sendGAEvent(ActionType.USER_ACTION,
				"playscan_questions_enter"); // GA Event
	}

	private void initKeyboard() {
		updateKeyboardFromPrefs();
	}

	private void updateKeyboardFromPrefs() {
		keyboardView = (View) findViewById(R.id.playKeyboard);
		keyboardView.setVisibility(View.GONE);
	}
	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		recalcField();
	}
	private void recalcField() {
		if (!hasSetInitialZoom) {
			float zoom;
			String zoomStr = prefs.getString("initialZoom", "0");
			try {
				zoom = Float.parseFloat(zoomStr);
			} catch (NumberFormatException e) {
				zoom = 0.0f;
			}

			if (Math.abs(zoom) < 0.01f) {
				mBoardView.fitToScreen();
			} else {
				mBoardView.setRenderScale(zoom);
			}

			hasSetInitialZoom = true;
			render(false);
		}
	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// this.render();
		Log.v("", requestCode + " ---------onActivityResult---------  "
				+ resultCode);
		InAppManager.getInstance().onActivityResult(requestCode, resultCode,
				data);
		if (requestCode == 99 && resultCode == RESULT_OK) {
			this.finish();
			return;
		} else if (requestCode == 99 && resultCode == 555) {
			setResult(resultCode);
			this.finish();
			return;
		} else if (data == null || resultCode == RESULT_CANCELED)
			return;
		Bundle extras = data.getExtras();
		Question question = (Question) extras
				.getSerializable(SkanwordGameActivity.EXTRA_SELETED_QUESTION);
		mQuestionToOpen = question;
		// Log.v("","  open question with id " + pos);
	}

	@Override
	protected void onPause() {
		Log.v("", " ------------------   onPause");
		if (mBoard != null) {
			Log.v("SkanwordsFunc", "onPause saveScanwordState " + mBoard.isProgressChanged());
			saveScanwordState(mBoard.isProgressChanged());
		}
		boolean tempvalue = isClickOpenKeyboard;
		hideKeyboard();
		if (mHintToast != null) {
			mHintToast.cancel();
		}
		isClickOpenKeyboard = tempvalue;
		super.onPause();
	}

	@Override
	protected void onRestart() {
		super.onRestart();
		Log.v("", " ------------------   onRestart");
	}

	@Override
	protected void onResume() {
		// getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_UNCHANGED);
		super.onResume();
		AdsManager.getInstance().activityResumed(this);
		isClickOpenKeyboard = false;
		if (mQuestionToOpen != null) {
			Position pos = new Position(mQuestionToOpen.getQuestionData().getX(),
					mQuestionToOpen.getQuestionData().getY());
			cellClicked(pos);
			mQuestionToOpen = null;
		}
		Log.v("", " ------------------   onResume");
		// getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

	}

	@Override
	protected void onStart() {
		super.onStart();
		checkTutorialState();
		if (adbotStep) {
			if (!mSkanword.isOpened()) {
				
				AdsManager.getInstance().showAds(this, new AdsEventsListener(), AdsManager.ADS_INTERSTITIAL | AdsManager.ADS_VIDEO_INTERSTITIAL | AdsManager.ADS_VIDEO);
				mSkanword.setOpened(true);
				mSkanword.setChanged(true);
				mBoard.setProgressChanged(true);
			} else {
				AdsManager.getInstance().showInterstitial(this);
			}

			//AdBot.makeStep(this);
			adbotStep = false;
		}
	}

	@Override
	protected void onStop() {

		super.onStop();
		Log.v("", " ------------------   onStop");
	}

	@Override
	protected void onDestroy() {
		Log.v("", "onDestroy ----");
		if (mBoard != null)
			mBoard.getCurrentSkanword();
		mBoard = null;
		if (mBoardView != null) {
			mBoardView.setDelegate(null);
			mBoardView.setBoard(null);
			mBoardView.setImageBitmap(null);
		}
		mBoardView = null;
		
		if (TutorialManager.getInstance().currentTutorialStep() == TutorialStep.TUTORIAL_CELL_FOR_HINT_SELECTED) {
			TutorialManager.getInstance().setTutorialStep(TutorialStep.TUTORIAL_COMPLETE);
		}
		
		SmappsScanwords.getAppSharedPreferencesEditor(MainDataManager.GENERAL_PREFERENCES_NAME).putBoolean("game_played", true).commit();


		if (mTimerEventListener != null)
			SmappsScanwords.getEventsDispatcher().removeListener(
					SmappsScanwords.APPLICATION_TIMER_EVENT,
					mTimerEventListener);
		if (mUserInfoEventListener != null) {
			SmappsScanwords.getEventsDispatcher().removeListener(
						MainDataManager.EVENT_USER_INFO_UPDATE, mUserInfoEventListener);
		}

		SmappsScanwords.getEventsDispatcher().removeListener(
					MainDataManager.EVENT_USER_INFO_UPDATE, mUserInfoEventListener);
		
		if (loadProgressDialog != null) {
			loadProgressDialog.dismiss();
			loadProgressDialog = null;
		}
		super.onDestroy();
	}

	private void render(boolean forceRenderAll) {
		Log.v("", "render  Play crossword activity");
		this.mBoardView.render(forceRenderAll);
		this.mBoardView.requestFocus();

	}

	@Override
	public void onBackPressed() {
		// close or hide keyboard
		if (keyboardView.getVisibility() != View.GONE) {
			hideKeyboard();
		} else {
			super.onBackPressed();
		}
		;
	}
	@Override
	public void scrollStarted() {
		hideKeyboard();
	} 
	@Override
	public void scaleStarted() {
		hideKeyboard();
	} 
	private void hideKeyboard() {
		LOG.info("Keyboard hide");
		isClickOpenKeyboard = false;
		if (keyboardView.getVisibility() != View.GONE) {
			keyboardView.setVisibility(View.GONE);
			if (mBannerLoaded)
				findViewById(R.id.adView).setVisibility(View.VISIBLE);
			updateBoardViewPosition();
			Log.v("SkanwordsGame", "hidekeyboard");
		}
	}

	private void showKeyboard() {
		Log.v("SkanwordsFunc", "Keyboard show");
		isClickOpenKeyboard = true;

		if (TutorialManager.getInstance().currentTutorialStep() == TutorialStep.TUTORIAL_SKANWORD_OPENED) {
			TutorialManager.getInstance().setTutorialStep(TutorialStep.TUTORIAL_WORD_SELECTED);
		}
		
		if (keyboardView.getVisibility() != View.VISIBLE) {
			keyboardView.setVisibility(View.VISIBLE);
			findViewById(R.id.adView).setVisibility(View.GONE);
			updateBoardViewPosition();
			Log.v("SkanwordsGame", "showkeyboard");
		}

	}
	private void updateBoardViewPosition() { // action to update view position on next loop
		new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
			@Override
			public void run() {
				mBoardView.ensureVisible(mBoard
						.getHighlightLetter());
			}
		}, 1);
	}
	private void delayshowKeyboard() {
		new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
			@Override
			public void run() {
				showKeyboard();
				mBoardView.ensureVisible(mBoard
						.getHighlightLetter());
			}
		}, 250);
	}
	private void updateQuestionTextView() {
		Log.v("SkanwordsGame", "updateQuestionTextView " + mBoard.getHightlightQuestion().getQuestionData().getQuestionValue().getOriginal());
		if (mBoard == null || mBoard.getHightlightQuestion() == null) {
			return;
		}
		String question = "Картинка";
		if (mBoard.getHightlightQuestion().getQuestionData().getQuestionValue().isTextQuestion()) {
			question = mBoard.getHightlightQuestion().getQuestionData().getQuestionValue().getOriginal();
		}
		((AutofitTextView)findViewById(R.id.questionText)).setText(question);
	}
	public void questionPreviousButtonClicked(View view) {
		if (mBoard.getHightlightQuestion() == null)
			return;
		Question q = mSkanword.getPreviousToQuestion(mBoard.getHightlightQuestion());

		if (q == null)
			return;
		Position pos = new Position(q.getQuestionData().getX(),
				q.getQuestionData().getY());
		cellClicked(pos);
		
	}
	public void questionNextButtonClicked(View view) {
		if (mBoard.getHightlightQuestion() == null)
			return;
		Question q = mSkanword.getNextToQuestion(mBoard.getHightlightQuestion());

		if (q == null)
			return;
		Position pos = new Position(q.getQuestionData().getX(),
				q.getQuestionData().getY());
		cellClicked(pos);
	}
	public void keyboardHideButtonClicked(View view) {
		hideKeyboard();
	}
	public void keyboardDeleteButtonClicked(View view) {
		enterLetter(null);
	}
	public void keyboardButtonClicked(View view) {
		enterLetter(((Button)view).getText().toString().toUpperCase());
	
	}
	public void keyboardSpaceButtonClicked(View view) {
		enterLetter(" ");
	}
	private void enterLetter(String c) {
		letterEntered(false);
		int inputResult = mBoard.inputLetter(c, false);
		updateQuestionCountLable(inputResult);
		boolean keywordGuessed = checkKeywordComplete();
		render(inputResult > 0 || keywordGuessed);
		if (mBoard.getHighlightLetter() != null)
			mBoardView.ensureVisible(mBoard.getHighlightLetter());
		
		updateQuestionTextView();
		
		if (keywordGuessed) {
			showKeywordBonus();
			return;
		}
		if (checkScanwordFinished()) {
			SoundsManager.playSound(this, SoundsManager.SOUND_WIN);
		} else {
			if (inputResult > 0) {
				SoundsManager.playSound(this, SoundsManager.SOUND_WORD_CORRECT);
			} else {
				SoundsManager.playSound(this, SoundsManager.SOUND_HINT_LETTER);
			}
		}
	}
	public static class MinishopListAdapter extends
			ArraySectionedListAdapter<ShopItem> {

		private int mResourceId;
		private final Activity mActivity;
		private List<ShopItem> mObjectsList;
		private View mSectionHeaderView;
		private boolean mOffersEnabled;
		private AlertDialog miniShopDialog;

		public MinishopListAdapter(Context context, int mResourceId,
				List<ShopItem> mObjectsList, Activity mActivity,
				boolean offersEnabled, AlertDialog qustomDialogBuilder) {
			super(context, mResourceId, mObjectsList);
			this.mResourceId = mResourceId;
			this.mActivity = mActivity;
			this.mObjectsList = mObjectsList;
			this.mOffersEnabled = offersEnabled;
			miniShopDialog = qustomDialogBuilder;
		}

		private void updateTimerView() {

			mActivity.runOnUiThread(new Runnable() {
				@Override
				public void run() {
					if (mSectionHeaderView != null) {
						String text = "Акция! Осталось: "
								+ Utils.secondsToTimerType(MainDataManager
										.getInstance().getShopDataManager()
										.getSaleTimeLeft());
						((TextView) mSectionHeaderView
								.findViewById(R.id.setLabel)).setText(text);
						mSectionHeaderView.invalidate();
					}
				}
			});
		}

		@Override
		protected View newRowViewInSection(Context context, int section,
				int row, ViewGroup parent) {
			View view = createRowView(parent);
			return view;
		}

		@Override
		protected void bindRowViewInSection(Context context, int section,
				int row, View convertView) {
			rebuildView(convertView, getItemInSectionAndRow(section, row));
		}

		@Override
		protected View newSectionView(Context context, int section,
				ViewGroup parent) {
			if (!MainDataManager.getInstance().getShopDataManager()
					.shopItemsTypeWithSale(ShopProductType.HINTS)) {
				View v = new View(getContext());
				return v;
			}
			if (mSectionHeaderView == null)
				mSectionHeaderView = ((LayoutInflater) context
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE))
						.inflate(R.layout.header, parent, false);
			return mSectionHeaderView;
		}

		@Override
		protected void bindSectionView(Context context, int section,
				View convertView) {
			Log.v("", " bindSectionView");
			convertView.setClickable(true);
			updateTimerView();
		}

		private void rebuildView(View view, final ShopItem shopItem) {
			Button buyButton = (Button) view.findViewById(R.id.buyButton);
			TextView label = (TextView) view.findViewById(R.id.itemNameLabel);
			view.setClickable(true);
			label.setTextColor(0xff000000);
			if (shopItem == null && mOffersEnabled) {

				label.setText("Открыть\nза видео");
				((LinearLayout) view.findViewById(R.id.sale_label))
						.setVisibility(View.GONE);
				((ImageView) view.findViewById(R.id.itemTypeIcon))
						.setBackgroundResource(R.drawable.icon_open_for_video);
				buyButton.setText("Открыть");
				buyButton.setBackgroundResource(R.drawable.orange_btn_d);
				buyButton.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						miniShopDialog.dismiss();
						((SkanwordGameActivity) mActivity).openCellForOffer();
						SmappsScanwords.sendGAEvent(ActionType.USER_ACTION,
								"click_minishop_hint4video"); // GA Event
					}
				});
				return;
			}

			label.setText(shopItem.getName());
			((LinearLayout) view.findViewById(R.id.sale_label))
					.setVisibility(shopItem.isSale()
							&& MainDataManager.getInstance()
									.getShopDataManager().saleEnabled() ? View.VISIBLE
							: View.GONE);
			((ImageView) view.findViewById(R.id.itemTypeIcon))
					.setBackgroundResource(MenuShop.ShopItemsAdapter.getIconId(
							shopItem, ShopProductType.HINTS));
			buyButton.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					InAppManager.getInstance().purchaseProduct(
							shopItem.getId(), mActivity,
							PurchaseSource.MINISHOP);
					SmappsScanwords.sendGAEvent(ActionType.USER_ACTION,
							"click_minishop_buy_" + shopItem.getId()); // GA
																		// Event
				}
			});
			String price = InAppManager.getInstance().getPriceForProductId(
					shopItem.getId());
			price = price.equals("") ? "купить" : price;
			if (price != null)
				buyButton.setText(price);
		}

		private View createRowView(ViewGroup parent) {
			LayoutInflater inflater = LayoutInflater.from(getContext());
			final View view = inflater.inflate(mResourceId, parent, false);
			return view;
		}

		@Override
		protected int getNumberOfSections() {
			// TODO Auto-generated method stub
			return 1;
		}

		@Override
		protected int getNumberOfRowsInSection(int section) {
			// TODO Auto-generated method stub
			return mObjectsList.size() + (mOffersEnabled ? 1 : 0);
		}

		@Override
		public ShopItem getItemInSectionAndRow(int section, int row) {
			// TODO Auto-generated method stub
			Log.v("", mOffersEnabled + " getItemInSectionAndRow " + row);
			if (mOffersEnabled) {
				return row == 0 ? null : mObjectsList.get(row - 1);
			}
			return mObjectsList.get(row);
		}

	}

}
