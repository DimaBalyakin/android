package org.skanword.and.scanwordgame;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.skanword.and.datamanager.Skanword.Question;

import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.Shader.TileMode;
import android.graphics.Typeface;
import android.util.Log;

public class SkanwordBox implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static final float SELECTED_SCALE = 1.1f;

	private final int x;
	private final int y;
	private static  AssetManager assetManager;
	private final String skanwordDirName;
	private final List<Question> questions;
	private String response;
	private Paint questionText;
	private BoxType boxType;
	public static final int BOX_SIZE = 30;
	private Bitmap tempBitmap;
	private boolean isAddedArrow;
	private List<ArrowType> arrowTypes;
	private boolean isSelected;
	private boolean isActiveWorld;
	private boolean isHintOpen;

	private boolean isComplete;
	
	private int keywordLetterNumber = 0;

	
	public SkanwordBox(int x, int y, String skanwordDirName,  AssetManager assetManager) {
		super();
		this.x = x;
		this.y = y;
		this.skanwordDirName = skanwordDirName;
		if (SkanwordBox.assetManager == null){
			SkanwordBox.assetManager = assetManager;
		}
		boxType = BoxType.LETTER;
		questions = new ArrayList<Question>(2);
		questionText = new Paint();
		questionText.setTextAlign(Align.CENTER);
		questionText.setColor(Color.BLACK);
		questionText.setAntiAlias(true);
		questionText.setTypeface(boxType.getQuestionFont());
		
		isAddedArrow = false;
		arrowTypes = new ArrayList<ArrowType>(8);
		isSelected = false;
		isActiveWorld = false;
		response = " ";
	}
	

	public int getKeywordLetterNumber() {
		return keywordLetterNumber;
	}


	public void setKeywordLetterNumber(int keywordLetterNumber) {
		this.keywordLetterNumber = keywordLetterNumber;
	}


	public void drawLetterBackground(Canvas canvas, int x, int y, float scale){
		
		isCompleteCheck();
		switch (boxType) {
			case LETTER:
				if (isSelected()){
					if (!isComplete && !isHintOpen ){
						boxType.drawLetterSelected(canvas, x, y, scale);
					}else{
						boxType.drawCompleteLetterSelected(canvas, x, y, scale);
					}
				}else if(isActiveWorld()){
					if (!isComplete  && !isHintOpen){
						boxType.drawLetterWordSelected(canvas, x, y, scale);
					}else{
						boxType.drawCompleteLetterWordSelected(canvas, x, y, scale);
					}
				} else if (isComplete || isHintOpen){
					boxType.drawCompleteBackground(canvas, x, y, scale);
				}else{
					boxType.drawBackground(canvas, x, y, scale);
				}
				break;
			default:
				break;

		
		}
	}

	public void drawResultLetters(Canvas canvas, int x, int y, float scale){
		boxType.drawResponseLettter(canvas, x, y, response, isComplete || isHintOpen, scale);
	}

	public void drawArrow(Canvas canvas, int x, int y, float scale){
		if (isAddedArrow){
			for (ArrowType arrow : arrowTypes) {
				arrow.draw(canvas, x, y, scale);
			}		
		}
	}


	public void drawKeywordFrame(Canvas canvas, int x, int y, float scale,
			int num) {
		boxType.drawKeywordFrame (canvas, x, y, scale, num);
	}

	public void drawBackground(Canvas canvas, int x, int y, float scale){
		if (canvas == null)
			return;
		int boxSize = (int)(BOX_SIZE * scale);

		switch (boxType) {
		case LETTER:
			drawLetterBackground(canvas, x, y, scale);
			break;
		case MAIN_QUESTION:
			if (getQuestions().get(0).getQuestionData().getQuestionValue().isTextQuestion()){
				boxType.drawTextBackground(canvas, x, y, scale);
				final float textSize = scale * 5F;
				questionText.setTextSize(textSize);
				final int numberBeetweenLineSize = (int)(textSize * 1.2);		
				
				if (getQuestions().size() == 1){
					String[] strings = getQuestions().get(0).getQuestionData().getQuestionValue().getFormatted().split("_");
					// centering the text vertically
					int textLineY = (int) (y + Math.round(boxSize / 2 - textSize ));
					if(strings.length % 2 == 0){
						textLineY -= numberBeetweenLineSize / 2;
					}		
					for (int i = 0; i < strings.length; i++) {									
						canvas.drawText(strings[i], x + Math.round(boxSize / 2), textLineY , questionText);
						textLineY  = (int) (textLineY + numberBeetweenLineSize);
					}
				}else{
					throw new RuntimeException("Wrong Question data in Box");
				}
			} else {	
				String fileName = skanwordDirName +"/"+ getQuestions().get(0).getQuestionData().getQuestionValue().getUrl();
				if (getTempBitmap() == null){
					tempBitmap = BitmapFactory.decodeFile(fileName);
				}
				Log.v("SkanwordsFunc", skanwordDirName + " tempBitmap " + tempBitmap);
				float borderSize = getBorderSize(scale);
				int tempWidth = (int)Math.round(boxSize * getQuestions().get(0).getQuestionData().getQuestionValue().getWidth()- borderSize);
				int tempHeight = (int) Math.round(boxSize * getQuestions().get(0).getQuestionData().getQuestionValue().getHeight()- borderSize);
				if (getTempBitmap() != null)
					canvas.drawBitmap(Bitmap.createScaledBitmap(getTempBitmap(), tempWidth, tempHeight, false), Math.round(x + borderSize) , Math.round(y + borderSize), null);
			}
			break;
		default:
			break;

		}
	}
	
	public float getBorderSize(float scale){
		return BoxType.getBorderSize(scale);
	}




	public void addQuestionRelation(Question question){
		if (getQuestions().size() >= 2){
			throw new RuntimeException("full questions List");  
		}else{
			getQuestions().add(question);
		}
	}

	public void addQuestionBox(Question question , Boolean isMainQuestionBox){
		boxType =  isMainQuestionBox ? BoxType.MAIN_QUESTION : BoxType.SLAVE_QUESTION;
		addQuestionRelation(question);
	}

	public void addArrow(ArrowType arrowType){
		isAddedArrow = true;
		arrowTypes.add(arrowType);
	}

	public void setHighlightLetter(boolean isSelected){
		this.isSelected = isSelected;
	}

	public BoxType getBoxType() {
		return boxType;
	}

	public List<Question> getQuestions() {
		return questions;
	}

	public void setHighlightdWord(boolean isActiveWorld) {
		this.isActiveWorld = isActiveWorld;		
	}

	public void setResponse(String letter) {
		if (!isComplete && !isHintOpen ){
			response  = letter; 
		}
	} 

	public String getLatterResponse(){
		return response;
	}



	public Bitmap getTempBitmap() { 
		return tempBitmap;
	}

	public boolean isSelected() {
		return isSelected;
	}


	public boolean isActiveWorld() {
		return isActiveWorld;
	}


	public int getX() {
		return x;
	}


	public int getY() {
		return y;
	}


	@Override
	public String toString() {
		StringBuilder strResult = new StringBuilder();
		strResult.append("BOX[").append(x).append(", ").append(y).append("] type =").append(boxType).append("  letter =").append(response);
		return strResult.toString();
	}




	private boolean isCompleteCheck(){
		boolean result = isComplete;
		if(questions != null && !isComplete){
			for (Question question : questions) {
				if (question.isComplete()){
					result = true;
					isComplete = result;
					break;
				}
			}
		}
		return result;
	}

	public boolean isHintOpen() {
		return isHintOpen;
	}
	
	public void setCompletition() {
		isComplete = true;
	}
	
	public boolean isComplete() {
		return isComplete;
	}


	public void setHintOpen(boolean isHintOpen) {
		this.isHintOpen = isHintOpen;
	}




	public static enum BoxType{
		// input  
		LETTER(0),
		//question render 
		MAIN_QUESTION(1), 
		//question not render 
		SLAVE_QUESTION(2);

		private static final float BORDER_SIZE = 0.75f;
		private static final int[][] GRARIENTS_COLORS = new int[][]{
			{Color.parseColor("#c2e0f4"), Color.parseColor("#6eb0dc")}, // 0 text question 
			{Color.parseColor("#d3f958"), Color.parseColor("#9cbc35")}, // 1 complete question selected
			{Color.parseColor("#e9fcaf"), Color.parseColor("#c5e169")}, // 2 complete question active word
			{Color.parseColor("#bce296"), Color.parseColor("#5ea659")}, // 3 complete question
			{Color.parseColor("#fff4d3"), Color.parseColor("#ffeeba")}, // 4 selected question active word
		};
		
		
		private final Paint whiteBox;
		private final Rect boxRect;
		private final int id;
		private final Paint selecteLetterdBox;
		private final Paint completeLetterdBox;
		private final Paint selecteLetterWordBox;
		private final Paint responsePaint;
		private final Paint completeSelecteLetterdBox;
		private final Paint completeSelecteLetterWordBox;
		private final Paint textBox;
		private final Typeface[] fonts;

		private BoxType(Integer id){
			this.id = id;
			whiteBox = new Paint();
			whiteBox.setColor(Color.WHITE);
			
			textBox = new Paint();
			textBox.setDither(true);
			
			selecteLetterdBox = new Paint();
			selecteLetterdBox.setColor(Color.parseColor("#ffea94"));
			
			completeLetterdBox = new Paint();
			completeLetterdBox.setDither(true);
			
			completeSelecteLetterdBox = new Paint();
			completeSelecteLetterdBox.setDither(true);
			
			completeSelecteLetterWordBox = new Paint();
			completeSelecteLetterWordBox.setDither(true);
			
			boxRect = new Rect();
			
			selecteLetterWordBox = new Paint();
			selecteLetterWordBox.setDither(true);
			
			if (id  == 0){
			responsePaint = new Paint();
			responsePaint.setTextAlign(Align.CENTER);
			responsePaint.setColor(Color.BLACK);
			responsePaint.setAntiAlias(true);
			
			fonts = new Typeface[]{Typeface.createFromAsset(assetManager, "fonts/SEGOEUIB.TTF"), Typeface.createFromAsset(assetManager, "fonts/SEGOEUI.TTF")} ;
			}else{
				responsePaint = null;
				fonts = null;
			}
			
		}

		public void drawKeywordFrame(Canvas canvas, int x, int y, float scale,
				int num) {
			float border = 2.0f;
			
			int boxSize = (int)(BOX_SIZE * scale);		
			int borderSize = Math.round(border * scale);
			//Log.i("", "borderSize=  " + borderSize);
			Rect rect = new Rect(x + borderSize / 2, y + borderSize / 2, x + boxSize, y + boxSize);
			
			Paint myPaint = new Paint();
			myPaint.setColor(Color.parseColor("#f38d02"));
			myPaint.setStrokeWidth(borderSize);
			myPaint.setStyle(Paint.Style.STROKE);
			canvas.drawRect(rect, myPaint);

			myPaint.setStyle(Paint.Style.FILL);
			Path path = new Path();
			path.moveTo(x + boxSize, y);
			path.lineTo(x + boxSize, y + boxSize * 0.4f);
			path.lineTo(x + boxSize - boxSize * 0.4f, y);
			path.close();
			canvas.drawPath(path, myPaint);
			whiteBox.setTextSize(scale * 5F);
			whiteBox.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));
			canvas.drawText("" + num, x + boxSize * 0.85f, y + boxSize * 0.2f, whiteBox);
		}

		public void drawTextBackground(Canvas canvas, int x, int y, float scale) {		
			drawLetterBox(createGradient(textBox, 0, x, y, scale), canvas, x, y, scale);		
		}
		
		public void drawCompleteBackground(Canvas canvas, int x, int y, float scale) {
			if (id  == 0){			
				drawLetterBox(createGradient(completeLetterdBox, 3, x, y, scale), canvas, x, y, scale);
			}
		} 

		protected void drawBackground(Canvas canvas, int x, int y, float scale){
			drawLetterBox(whiteBox, canvas, x, y, scale);
		}

		protected void drawLetterSelected(Canvas canvas, int x, int y, float scale){
			if (id  == 0){
				drawLetterBox(selecteLetterdBox, canvas, x, y, scale);
			}
		}

		protected void drawLetterWordSelected(Canvas canvas, int x, int y, float scale){
			if (id  == 0){
				drawLetterBox(createGradient(selecteLetterWordBox, 4, x, y, scale), canvas, x, y, scale);
			}
		}
		
		protected void drawCompleteLetterSelected(Canvas canvas, int x, int y, float scale){
			if (id  == 0){
				drawLetterBox(createGradient(completeSelecteLetterdBox, 1, x, y, scale), canvas, x, y, scale);
			}
		}

		protected void drawCompleteLetterWordSelected(Canvas canvas, int x, int y, float scale){
			if (id  == 0){
				drawLetterBox(createGradient(completeSelecteLetterWordBox, 2, x, y, scale), canvas, x, y ,scale);
			}
		}

		private void drawLetterBox(Paint paint, Canvas canvas, int x, int y, float scale){
			int boxSize = (int)(BOX_SIZE * scale);		
			int borderSize = Math.round(getBorderSize(scale));
			//Log.i("", "borderSize=  " + borderSize);
			boxRect.set(x + borderSize, y + borderSize, (x + boxSize), (y + boxSize));
			canvas.drawRect(boxRect, paint);
		}

		protected void drawResponseLettter(Canvas canvas, int x, int y, String letter, boolean isComplete, float scale){
			int boxSize = (int)(BOX_SIZE * scale);		
			if (id == 0 && letter != " "){
				char[] text = letter.toCharArray();
				responsePaint.setTextSize(scale * 15F);
				
				responsePaint.setTypeface(fonts[isComplete ? 0 : 1]);
				canvas.drawText(text, 0, text.length, x + Math.round(boxSize / 2), y + Math.round((boxSize + responsePaint.getTextSize()) / 2)  , responsePaint);
			}
		}
		
		protected Typeface getQuestionFont(){ 
			return fonts[1];
		}
		
		private Paint createGradient(Paint paint, int index, int x, int y, float scale){
			if (paint !=  null && index >=0 && index < GRARIENTS_COLORS.length){
				int boxSize = (int)(BOX_SIZE * scale);		
				LinearGradient gradient = new LinearGradient(x, y, x + boxSize, y + boxSize, GRARIENTS_COLORS[index][0], GRARIENTS_COLORS[index][1], TileMode.CLAMP);
				paint.setShader(gradient);
			}
			
			return paint;
		}
		
		private static float getBorderSize(float scale){
			return Math.max(1f, BORDER_SIZE * scale);
		}
		

	}



}
