package org.skanword.and.datamanager;

import java.io.File;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.Queue;

import org.skanword.and.datamanager.Skanword.Keyword.KeywordCell;
import org.skanword.and.etc.Utils;
import org.skanword.and.network.MainNetworkManager;
import org.skanword.and.scanwordgame.SkanwordBoard.Position;

import android.graphics.Point;
import android.util.Log;

import com.google.gson.annotations.SerializedName;

public class Skanword implements Serializable {
	private static final long serialVersionUID = 346611261613055968L;
	private final int id;
	private int setId;
	private final int level;
	@SerializedName("view_id")
	private final int viewId;
	private final Settings settings;
	private final List<Question> questions;
	@SerializedName("replace")
	private final List<Replace> replaces;
	private final String[][] mask;
	private final String hash;
	private final Keyword keyword;
	private String imageDir;
	private String answers;
	private boolean started;
	private boolean finished;
	private int finishedQuestionsCount;
	private long updateTime;
	private String hints;
	private boolean changed;
	
	private String cover;
	private int questionsCount;
	private boolean opened;

	public Skanword(int id, int level, int viewId, Settings settings,
			List<Question> questions, List<Replace> replaces, String[][] mask,
			String hash, Keyword keyword, String imageDir) {
		super();
		this.id = id;
		this.level = level;
		this.viewId = viewId;
		this.settings = settings;
		this.questions = questions;
		this.replaces = replaces;
		this.mask = mask;
		this.hash = hash;
		this.keyword = keyword;
		this.imageDir = imageDir;
	}
	
	
	
	public String getCover() {
		if (cover == null && getQuestions() != null) {
			for (Question question : getQuestions()) {
				if (!question.getQuestionData().getQuestionValue().isTextQuestion()) {
					cover = "/"+question.getQuestionData().getQuestionValue().getUrl();
					break;
				}
			}
		}
		return cover;
	}



	public void setCover(String cover) {
		this.cover = cover;
	}
	
	public Keyword getKeyword() {
		return this.keyword;
	}

	public boolean isOpened() {
		return opened;
	}
	public void clearKeyword() {
		if (getKeyword() == null)
			return;
		for (KeywordCell keywordCell : getKeyword().getCells()) {
			keywordCell.setCharacter(" ", false);
		}
	}
	public void checkKeyword() {
		if (getQuestions() == null || getKeyword() == null)
			return;
		boolean allWord = true;
		for (KeywordCell keywordCell : getKeyword().getCells()) {
			if (keywordCell.isPersist())
				continue;
			String letter = getLetter(keywordCell.getX(),keywordCell.getY());
			boolean byHint = openedByHint(keywordCell.getX(),keywordCell.getY());
			boolean letterPersist = wordGuessedForCell(keywordCell.getX(),keywordCell.getY()) || byHint;
			String mask = getMask()[keywordCell.getX()][keywordCell.getY()];
			boolean correctLetter = mask != null && !mask.equals("false") && SkanwordsDataManager.md5Answer(String.valueOf(letter), getId()).equals(mask);
			keywordCell.setCharacter(letter, correctLetter && letterPersist);
			if (!correctLetter)
				allWord = false;
		}
		if (allWord) {
			for (KeywordCell keywordCell : getKeyword().getCells()) {
				String letter = getLetter(keywordCell.getX(),keywordCell.getY());
				keywordCell.setCharacter(letter, true);
			}
		}
	}
	private boolean wordGuessedForCell(int cellX, int cellY) {
		for (Question question : getQuestions()) {
			if (question.getAnswer().getX() != cellX && question.getAnswer().getY() != cellY) {
				continue;
			}
			
			boolean vertical = question.getQuestionData()
					.isVerticalOrientation();
			StringBuilder stringBuilder = new StringBuilder();

			stringBuilder = new StringBuilder();
			boolean cellInAnswer = false;
			for (int i = 0; i < question.getAnswer().getLength(); i++) {
				int x = question.getAnswer().getX() + (vertical ? 0 : i);
				int y = question.getAnswer().getY() + (vertical ? i : 0);
				if (x == cellX && y == cellY) {
					question.setHasKeywordCell(true);
					cellInAnswer = true;
				}
				int start = x * getSettings().getHeight() + y;
				String letter = getAnswers().substring(start, start + 1)
						.toLowerCase();
				if (letter.equals(" ")) {
					continue;
				}
				if (getReplaces() != null && getReplaces().size() > 0) {
					for (Replace replace : getReplaces()) {
						if (x == replace.getCellX()
								&& y == replace.getCellY()
								&& letter.toLowerCase()
										.equals(replace.getLetterSource()
												.toLowerCase())) {
							letter = replace.getLetterTarget();
						}
					}
				}
				stringBuilder.append(letter);
			}
			if (!cellInAnswer)
				continue;
			String answer = stringBuilder.toString();
			StringBuilder builder = new StringBuilder();
			if (question
					.getAnswer()
					.getHash()
					.equals(Utils
							.md5(builder
									.append(MainNetworkManager.getInstance()
											.getNetId()).append('_')
									.append(getId()).append('_')
									.append(answer.toLowerCase()).append('_')
									.append("ce9d660ffb810b5e59b444b84a712ca1")
									.toString()))) {
				return true;
			}
		}
		return false;
	}
	private String getLetter(int x, int y) {
		if (getSettings() == null)
			return " "; 
		int start = x * getSettings().getHeight() + y;
		String letter = getAnswers().substring(start, start + 1)
				.toLowerCase();
		return letter;
	}
	private boolean openedByHint(int x, int y) {
		if (getSettings() == null)
			return false; 
		int start = x * getSettings().getHeight() + y;
		String letter = getHints().substring(start, start + 1)
				.toLowerCase();
		return letter.equals(".");
	}
	
	public Question getPreviousToQuestion(Question question) {
		if (getQuestionsCount() < 1) {
			return null;
		}
		int index = questions.indexOf(question);
		if (index == questionsCount - 1) {
			return questions.get(0);
		} else {
			return questions.get(index + 1);
		}
	}
	public Question getNextToQuestion(Question question) {
		if (getQuestionsCount() < 1) {
			return null;
		}
		int index = questions.indexOf(question);
		if (index == 0) {
			return questions.get(questionsCount - 1);
		} else {
			return questions.get(index - 1);
		}

	}

	public void setOpened(boolean opened) {
		this.opened = opened;
	}



	public int getQuestionsCount() {
		if (questionsCount < 1 && getQuestions() != null) {
			questionsCount = getQuestions().size();
		}
		return questionsCount;
	}



	public void setQuestionsCount(int questionsCount) {
		this.questionsCount = questionsCount;
	}



	public void reset() {
		for (Question question : questions) {
			question.setComplete(false);
		}
	}

	public boolean hasFiles() {
		String fileName = getImageDir();
		Log.v("", "file name " + fileName);
		File dir = new File(fileName);
		String[] files = dir.list();
		if (files == null || files.length < 1) {
			return false;
		}
		return true;
	}

	public int getSetId() {
		return setId;
	}

	public void setSetId(int setId) {
		this.setId = setId;
	}

	public int getViewId() {
		return viewId;
	}

	public String[][] getMask() {
		return mask;
	}

	public String getHash() {
		return hash;
	}

	public int getId() {
		return id;
	}

	public Settings getSettings() {
		return settings;
	}

	public List<Question> getQuestions() {
		return questions;
	}

	public int getLevel() {
		return level;
	}

	public List<Replace> getReplaces() {
		return replaces;
	}

	@Override
	public String toString() {
		return "№" + id;
	}

	public String getImageDir() {
		return imageDir;
	}

	public void setImageDir(String immageDir) {
		this.imageDir = immageDir;
	}

	public String getAnswers() {
		if (answers == null || answers.equals("")) {
			char[] charArray = new char[getSettings().getWidth()
					* getSettings().getHeight()];
			Arrays.fill(charArray, ' ');
			answers = new String(charArray);
		}
		return answers;
	}

	public void setAnswers(String answers) {
		this.answers = answers;
	}

	public String getHints() {
		if (hints == null || hints.equals("")) {
			char[] charArray = new char[getSettings().getWidth()
					* getSettings().getHeight()];
			Arrays.fill(charArray, ' ');
			hints = new String(charArray);
		}
		return hints;
	}

	public void setHints(String hints) {
		this.hints = hints;
	}

	public boolean isStarted() {
		return started;
	}

	public void setStarted(boolean started) {
		this.started = started;
	}

	public boolean isFinished() {
		return finished;
	}

	public void setFinished(boolean finished) {
		this.finished = finished;
	}

	public int getFinishedQuestionsCount() {
		return finishedQuestionsCount;
	}

	public void setFinishedQuestionsCount(int finishedQuestionsCount) {
		this.finishedQuestionsCount = finishedQuestionsCount;
	}

	public long getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(long updateTime) {
		this.updateTime = updateTime;
	}

	public boolean isChanged() {
		return changed;
	}

	public void setChanged(boolean changed) {
		this.changed = changed;
	}

	public void setLetter(final Position pos, final Point scanwordSize,
			final String letter, final boolean byHint) {
		Log.v("SkanwordsTime", "setLetter  1");
		setLetter(pos, scanwordSize, letter, byHint, true);
		Log.v("SkanwordsTime", "setLetter  2");
	}

	public void setLetter(Position pos, Point scanwordSize, String letter,
			boolean byHint, boolean withUpdate) {
		getAnswers();
		getHints();
		int position = pos.down + scanwordSize.y * pos.across;
		if (!letter.equals(" ")) {
			letter = letter.toLowerCase();
		}
		answers = answers.substring(0, position) + letter
				+ answers.substring(position + 1);
		if (byHint) {
			hints = hints.substring(0, position) + "."
					+ hints.substring(position + 1);
		}
		if (letter.equals(" ") && this.finishedQuestionsCount == 0) {
			checkAnswerForStarted();
		} else {
			started = true;
		}
		if (withUpdate) {
			UserUpdatesManager.getInstance().updateScanwordMasks(this);
		}
		setChanged(true);
	}

	public void clear() {
		char[] charArray = new char[getSettings().getWidth()
				* getSettings().getHeight()];
		Arrays.fill(charArray, ' ');
		answers = new String(charArray);
		hints = new String(charArray);
	}

	private void checkAnswerForStarted() {
		char[] charArray = new char[getSettings().getWidth()
				* getSettings().getHeight()];
		Arrays.fill(charArray, ' ');
		String str = new String(charArray);
		started = !str.equals(answers);
	}

	public void calculateFinishedQuestions() {
		this.finishedQuestionsCount = 0;
		for (Question question : getQuestions()) {
			if (question.isComplete()) {
				this.finishedQuestionsCount++;
				continue;
			}
			boolean vertical = question.getQuestionData()
					.isVerticalOrientation();
			StringBuilder stringBuilder = new StringBuilder();

			boolean notComplete = false;
			stringBuilder = new StringBuilder();
			for (int i = 0; i < question.getAnswer().getLength(); i++) {
				int x = question.getAnswer().getX() + (vertical ? 0 : i);
				int y = question.getAnswer().getY() + (vertical ? i : 0);
				int start = x * getSettings().getHeight() + y;
				String letter = answers.substring(start, start + 1)
						.toLowerCase();
				if (letter.equals(" ")) {
					notComplete = true;
					break;
				}
				if (getReplaces() != null && getReplaces().size() > 0) {
					for (Replace replace : getReplaces()) {
						if (x == replace.getCellX()
								&& y == replace.getCellY()
								&& letter.toLowerCase()
										.equals(replace.getLetterSource()
												.toLowerCase())) {
							letter = replace.getLetterTarget();
						}
					}
				}
				stringBuilder.append(letter);
			}
			if (notComplete) {
				continue;
			}

			String answer = stringBuilder.toString();
			StringBuilder builder = new StringBuilder();
			if (question
					.getAnswer()
					.getHash()
					.equals(Utils
							.md5(builder
									.append(MainNetworkManager.getInstance()
											.getNetId()).append('_')
									.append(getId()).append('_')
									.append(answer.toLowerCase()).append('_')
									.append("ce9d660ffb810b5e59b444b84a712ca1")
									.toString()))) {
				question.setComplete(true);
				this.finishedQuestionsCount++;

			}
		}
		if (!isFinished())
			setFinished(this.finishedQuestionsCount == getQuestions().size());
	}
	
	public static class Keyword implements Serializable {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1003502434278371538L;
		
		private final List<KeywordCell> cells;

		public Keyword(List<KeywordCell> cells) {
			super();
			this.cells = cells;
		}

		public List<KeywordCell> getCells() {
			return cells;
		}
		
		public static class KeywordCell implements Serializable {

			/**
			 * 
			 */
			private static final long serialVersionUID = -3246729032383784033L;
			private final int x;
			private final int y;
			private final int number;
			private String character = " ";
			private boolean persist = false;
			public KeywordCell(int x, int y, int number, String character, boolean persist) {
				super();
				this.x = x;
				this.y = y;
				this.number = number;
				this.character = character;
				this.persist = persist;
			}
			public int getX() {
				return x;
			}
			public int getY() {
				return y;
			}
			public int getNumber() {
				return number;
			}
			
			public String getCharacter() {
				return character;
			}
			
			public boolean isPersist() {
				if (character == null || character.equals(" "))
					return false;
				return persist;
			}
			
			public void setCharacter(String character, boolean persist) {
				this.character = character;
				this.persist = persist;
			}
			
		}

		public boolean isGuessed() {
			for (KeywordCell keywordCell : this.cells) {
				if (!keywordCell.isPersist()) {
					return false;
				}
			}
			Log.v("SkanwordsFunc", "isGuessed " + true);
			return true;
		}
	}
	
	public static class Settings implements Serializable {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		private final int width;
		private final int height;

		public Settings(int width, int height) {
			super();
			this.width = width;
			this.height = height;
		}

		public int getWidth() {
			return width;
		}

		public int getHeight() {
			return height;
		}

	}

	public static class Question implements Serializable {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		private final int id;
		@SerializedName("question")
		private final QuestionData questionData;
		@SerializedName("arrow")
		private final ArrowData arrowData;
		private final Answer answer;
		private boolean hasKeywordCell;
		
		
		private boolean isComplete;
		private int unguessedCellsCount;

		public Question(int id, QuestionData questionData, ArrowData arrowData,
				Answer answer) {
			super();
			this.id = id;
			this.questionData = questionData;
			this.arrowData = arrowData;
			this.answer = answer;
			setComplete(false);
		}

		public int getUnguessedCellsCount() {
			return unguessedCellsCount;
		}

		public void setUnguessedCellsCount(int count) {
			this.unguessedCellsCount = count;
		}
		
		public boolean hasKeywordCell() {
			return hasKeywordCell;
		}

		public void setHasKeywordCell(boolean hasKeywordCell) {
			this.hasKeywordCell = hasKeywordCell;
		}

		public ArrowData getArrowData() {
			return arrowData;
		}

		public QuestionData getQuestionData() {
			return questionData;
		}

		public int getId() {
			return id;
		}

		public Answer getAnswer() {
			return answer;
		}

		@Override
		public boolean equals(Object o) {
			boolean result = !((o == null) || (o.getClass() != this.getClass()));
			if (result) {
				Question q = (Question) o;
				result = (q.id == id);
			}

			return result;
		}

		@Override
		public int hashCode() {
			return (int) id;
		}

		public boolean isComplete() {
			return isComplete;
		}

		public void setComplete(boolean isComplete) {
			this.isComplete = isComplete;
		}

		public int getGuessedCellsCount() {
			// TODO Auto-generated method stub
			return getAnswer().length - getUnguessedCellsCount();
		}

	}

	public static class QuestionData implements Serializable {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		private static final String VERTICAL_ORIENTATION = "vertical";
		private final int x;
		private final int y;
		private final String orientation;
		@SerializedName("value")
		private final QuestionValue questionValue;

		public QuestionData(int x, int y, String orientation,
				QuestionValue value) {
			super();
			this.x = x;
			this.y = y;
			this.orientation = orientation;
			this.questionValue = value;
		}

		public int getX() {
			return x;
		}

		public int getY() {
			return y;
		}

		public QuestionValue getQuestionValue() {
			return questionValue;
		}

		public boolean isVerticalOrientation() {
			return VERTICAL_ORIENTATION.equals(orientation);
		}
	}

	public static class QuestionValue implements Serializable {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		@SerializedName("type")
		private final String questionType;
		private final String formatted;
		private final String original;
		private final String url;
		private final int width;
		private final int height;

		private static String STRING_TEXT_TYPE = "text";

		public QuestionValue(String questionType, String formatted,
				String original, String url, int width, int height) {
			super();
			this.questionType = questionType;
			this.formatted = formatted;
			this.original = original;
			this.url = url;
			this.width = width;
			this.height = height;
		}

		public String getQuestionType() {
			return questionType;
		}

		public String getFormatted() {
			return formatted;
		}

		public String getOriginal() {
			return original;
		}

		public String getUrl() {
			return url;
		}

		public int getWidth() {
			return (width <= 0 ? 1 : width);
		}

		public int getHeight() {
			return (height <= 0 ? 1 : height);
		}

		public boolean isTextQuestion() {
			return STRING_TEXT_TYPE.equals(questionType);
		}
	}

	public static class ArrowData implements Serializable {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		private final int type;

		public ArrowData(int type) {
			super();
			this.type = type;
		}

		public int getType() {
			return type;
		}

	}

	public static class Answer implements Serializable {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		private final int x;
		private final int y;
		private final int length;
		private final String hash;

		public Answer(int x, int y, int length, String hash) {
			super();
			this.x = x;
			this.y = y;
			this.length = length;
			this.hash = hash;
		}

		public int getX() {
			return x;
		}

		public int getY() {
			return y;
		}

		public int getLength() {
			return length;
		}

		public String getHash() {
			return hash;
		}
	}

	public static class Replace implements Serializable {
		/**
		 * 
		 */
		private static final long serialVersionUID = 2864107489648774608L;
		@SerializedName("cell_x")
		private final int cellX;
		@SerializedName("cell_y")
		private final int cellY;
		@SerializedName("letter_source")
		private final String letterSource;
		@SerializedName("letter_target")
		private final String letterTarget;

		public Replace(int cellX, int cellY, String letterSource,
				String letterTarget) {
			super();
			this.cellX = cellX;
			this.cellY = cellY;
			this.letterSource = letterSource;
			this.letterTarget = letterTarget;
		}

		public int getCellX() {
			return cellX;
		}

		public int getCellY() {
			return cellY;
		}

		public String getLetterSource() {
			return letterSource;
		}

		public String getLetterTarget() {
			return letterTarget;
		}

	}

}
