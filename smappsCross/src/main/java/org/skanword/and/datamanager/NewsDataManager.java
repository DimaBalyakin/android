package org.skanword.and.datamanager;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.skanword.and.SmappsScanwords;

import android.util.Log;

import com.musselwhizzle.dispatcher.events.SimpleEvent;

public class NewsDataManager {
	private List<News> mNews = null;
	private Set<Integer> mReadNews = null;
	private String mNewsHash = "";

	public void retrieveData () {
		mNewsHash = SmappsScanwords.getDatabaseHelper().<String>retrieveGeneralData(DataBaseManager.NEWS_HASH_ID);
		if (mNewsHash == null)
			mNewsHash = "";
		Log.v("", "mNewsHash retrieve " + mNewsHash);
		updateNews(SmappsScanwords.getDatabaseHelper().<List<News>>retrieveGeneralData(DataBaseManager.NEWS_DATA_ID), false);
		Set<Integer> readNews = SmappsScanwords.getDatabaseHelper().<Set<Integer>>retrieveGeneralData(DataBaseManager.READ_NEWS_DATA_ID);
		if (readNews == null) {
			this.mReadNews = Collections.synchronizedSet(new HashSet<Integer>());
		} else {
			this.mReadNews = Collections.synchronizedSet(readNews);
		}  
		
 	}
	
	public void updateNews(List<News> news, boolean total) {
		if (news == null)
			return;
		mNews = Collections.synchronizedList(news);
		if (total) {
			SmappsScanwords.getDatabaseHelper().storeGeneralData(new ArrayList<News>(news), DataBaseManager.NEWS_DATA_ID);
		}
		SmappsScanwords.getEventsDispatcher().dispatchEvent(new SimpleEvent(MainDataManager.EVENT_RATING_UPDATE));
	}
	public int getNotReadNewsCount() {
		int count = 0;
		if (mNews != null)
			for (News news : mNews) {
				if (!mReadNews.contains(news.getId()))
					count ++;
			}
		return count;
	}
	public void updateReadedNews() {
		if (mNews == null || mReadNews == null)
			return;
		for (News news : mNews) {
			mReadNews.add(news.getId());
		}
		SmappsScanwords.getDatabaseHelper().storeGeneralData(new HashSet<Integer>(mReadNews), DataBaseManager.READ_NEWS_DATA_ID);
	}
	public List<News> getNews() {
		return mNews;
	}

	public String getNewsHash() {
		return mNewsHash;
	}

	public void setNewsHash(String hash) {
		this.mNewsHash = hash;
		SmappsScanwords.getDatabaseHelper().storeGeneralData(this.mNewsHash, DataBaseManager.NEWS_HASH_ID);
		Log.v("", "mNewsHash update " + mNewsHash);
	}
	public static class News implements Serializable{

		/**
		 * 
		 */
		private static final long serialVersionUID = 4256668074486644081L;
		private final int id;
		private final String title;
		private final String text;
		private final String image;
		private final String date;
		private final String action;
		private final String link;
		private String newsHash;
		public News(int id, String title, String text, String image,
				String date, String action, String link, String newsHash) {
			super();
			this.id = id;
			this.title = title;
			this.text = text;
			this.image = image;
			this.date = date;
			this.action = action;
			this.link = link;
			this.newsHash = newsHash;
		}
		public void setNewsHash(String newsHash) {
			this.newsHash = newsHash;
		}
		public String getNewsHash() {
			return newsHash;
		}
		public int getId() {
			return id;
		}
		public String getTitle() {
			return title;
		}
		public String getText() {
			return text;
		}
		public String getImage() {
			return image;
		}
		public String getDate() {
			return date;
		}
		public String getAction() {
			return action;
		}
		public String getLink() {
			return link;
		}
		
		
	}
}
