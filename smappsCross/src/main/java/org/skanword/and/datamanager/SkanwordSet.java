package org.skanword.and.datamanager;

import java.io.File;
import java.io.Serializable;
import java.util.List;

import org.skanword.and.network.DownloaderArhiveFile;

import com.google.gson.annotations.SerializedName;

public class SkanwordSet implements Serializable {

	private static final long serialVersionUID = 1L;
	@SerializedName("set_id") 
	private final Integer setId;
	@SerializedName("media_url") 
	private final String mediaUrl;
	@SerializedName("list") 
	private final List<Skanword> scanwords;
	


	public SkanwordSet(Integer setId, String mediaUrl, List<Skanword> scanwords) {
		super();
		this.setId = setId;
		this.mediaUrl = mediaUrl;
		this.scanwords = scanwords;
	}

	public boolean hasUnzipedFiles () {
		String fileName = DownloaderArhiveFile.getUnzipPath(setId + ".zip");
		File dir = new File(fileName);
		String[] files = dir.list();
		if (files == null || files.length < 1) {
			return false;
		}
		return true;
	}

	public Integer getId() {
		return setId;
	}



	public String getMediaUrl() {
		return mediaUrl;
	}



	public List<Skanword> getSkanwords() {
		return scanwords;
	}

	
	
}
