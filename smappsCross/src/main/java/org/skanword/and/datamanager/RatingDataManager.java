package org.skanword.and.datamanager;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

import org.skanword.and.SmappsScanwords;

import android.util.Log;

import com.google.gson.annotations.SerializedName;
import com.musselwhizzle.dispatcher.events.SimpleEvent;


public class RatingDataManager {

	private HashMap<String, List<UsersRating>> ratings = null;
	private String mRatingHash = "";
	
	public void retrieveData () {
		mRatingHash = SmappsScanwords.getDatabaseHelper().<String>retrieveGeneralData(DataBaseManager.RATING_HASH_ID);
		if (mRatingHash == null)
			mRatingHash = "";
		Log.v("", "mRatingHash retrieve " + mRatingHash);
		upadateUsersRating(SmappsScanwords.getDatabaseHelper().<HashMap<String, List<UsersRating>>>retrieveGeneralData(DataBaseManager.RATING_DATA_ID), false);
	}
	public void upadateUsersRating(HashMap<String, List<UsersRating>> rating, boolean total) {
		if (rating == null)
			return;
		ratings = rating;
		if (total) {
			SmappsScanwords.getDatabaseHelper().storeGeneralData(ratings, DataBaseManager.RATING_DATA_ID);
		}
		SmappsScanwords.getEventsDispatcher().dispatchEvent(new SimpleEvent(MainDataManager.EVENT_RATING_UPDATE));
	}
	public List<UsersRating> getFriendsRating() {
		if (ratings != null) {
			return ratings.get("friends_skanword");
		}
		return null;
	}

	public List<UsersRating> getCommonRating() {
		if (ratings != null) {
			return ratings.get("common_skanword");
		}
		return null;
	}
	public String getRatingHash() {
		return mRatingHash;
	}

	public void setRatingHash(String rating) {
		this.mRatingHash = rating;
		SmappsScanwords.getDatabaseHelper().storeGeneralData(this.mRatingHash, DataBaseManager.RATING_HASH_ID);
		Log.v("", "mRatingHash update " + mRatingHash);
	}
	public static class RatingInfo implements Serializable{
		/**
		 * 
		 */
		private static final long serialVersionUID = 9098701843974191552L;
		private final HashMap<String, String> leaderboards;
		@SerializedName("total") 
		private final HashMap<String, List<UsersRating>> totalRatings;

		
		
		public RatingInfo(HashMap<String, String> leaderboards,
				HashMap<String, List<UsersRating>> totalRatings) {
			super();
			this.leaderboards = leaderboards;
			this.totalRatings = totalRatings;
		}



		public HashMap<String, List<UsersRating>> getTotalRatings() {
			return totalRatings;
		}

		public HashMap<String, String> getLeaderboards() {
			return leaderboards;
		}

		
	}
	
	public static class UsersRating implements Serializable{
		/**
		 * 
		 */
		private static final long serialVersionUID = -8243320369198349000L;
		@SerializedName("user_place") 
		private final int userPlace;
		@SerializedName("user_id") 
		private final String userId;
		@SerializedName("user_score") 
		private final String userScore;
		@SerializedName("user_social") 
		private final SocialUser socialUser;
		public UsersRating(int userPlace, String userId, String userScore,
				SocialUser socialUser) {
			super();
			this.userPlace = userPlace;
			this.userId = userId;
			this.userScore = userScore;
			this.socialUser = socialUser;
		}
		public int getUserPlace() {
			return userPlace;
		}
		public String getUserId() {
			return userId;
		}
		public String getUserScore() {
			return userScore;
		}
		public SocialUser getSocialUser() {
			return socialUser;
		}
	}
	
	public static class SocialUser implements Serializable{
		/**
		 * 
		 */
		private static final long serialVersionUID = 2348805317929820015L;
		@SerializedName("user_id") 
		private final String userId;
		@SerializedName("social_id") 
		private final String socialId;
		@SerializedName("user_name") 
		private final String userName;
		@SerializedName("user_photo") 
		private final String userPhoto;
		public SocialUser(String userId, String socialId, String userName,
				String userPhoto) {
			super();
			this.userId = userId;
			this.socialId = socialId;
			this.userName = userName;
			this.userPhoto = userPhoto;
		}
		public String getUserId() {
			return userId;
		}
		public String getSocialId() {
			return socialId;
		}
		public String getUserName() {
			return userName;
		}
		public String getUserPhoto() {
			return userPhoto;
		}

		
	}
	public static enum RatingType {SCANWORD_COMMON, SCANWORD_FRIENDS}
}
