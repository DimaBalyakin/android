package org.skanword.and.datamanager;

import java.io.File;
import java.io.Serializable;
import java.util.Comparator;
import java.util.List;

import org.skanword.and.network.DownloaderArhiveFile;

import com.google.gson.annotations.SerializedName;

public class SkanwordsSetInfo implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private final Integer id;

	private final String name;

	@SerializedName("display_name")
	private String displayName;

	@SerializedName("cross_count")
	private final int skanwordsCount;

	@SerializedName("data_hash")
	private final String dataHash;
	
	@SerializedName("save_hash")
	private String saveHash;

	@SerializedName("download_time_left")
	private int downloadTimeLeft;

	@SerializedName("free_ids")
	private List<Integer> freeIds;

	@SerializedName("offer_ids")
	private List<Integer> offerIds;

	@SerializedName("downloaded_time")
	private long downloadedTime = 0;

	@SerializedName("save_count")
	private int startedCount;

	@SerializedName("solved_count")
	private int finishedCount;
	
	private String cover;
	
	private boolean today = false;


	private boolean progressChanged = false;
	private boolean isLocalData;

	public SkanwordsSetInfo(Integer id, String name, String displayName,
			int skansCount, String dataHash, String saveHash, int downloadTimeLeft,
			List<Integer> freeIds, List<Integer> offerIds, long downloadedTime, int startedCount, int finishedCount, String cover) {
		super();
		this.id = id;
		this.name = name;
		this.displayName = displayName;
		this.skanwordsCount = skansCount;
		this.dataHash = dataHash;
		this.saveHash = saveHash;
		this.downloadTimeLeft = downloadTimeLeft;
		this.freeIds = freeIds;
		this.offerIds = offerIds;
		this.startedCount = startedCount;
		this.finishedCount = finishedCount;
		this.cover = cover;
		this.downloadedTime = downloadedTime;
	}

	public int getSkanwordsStartedCount() {
		return startedCount;
	}

	public int getSkanwordsFinishedCount() {
		return finishedCount;
	}
	
	public String getCover() {
		return this.cover;
	}
	
	public void setStartedCount(Integer startedCount) {
		this.startedCount = startedCount;
	}

	public void setFinishedCount(Integer finishedCount) {
		this.finishedCount = finishedCount;
	}

	public boolean isToday() {
		return today;
	}

	public void setToday(boolean today) {
		this.today = today;
	}

	public long getDownloadedTime() {
		return downloadedTime;
	}

	public void setDownloadedTime(long downloadedTime) {
		this.downloadedTime = downloadedTime;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public void setFreeIds(List<Integer> freeIds) {
		this.freeIds = freeIds;
	}

	public void setOfferIds(List<Integer> offerIds) {
		this.offerIds = offerIds;
	}

	public List<Integer> getOfferIds() {
		return offerIds;
	}

	public int getSkanwordsCount() {
		return skanwordsCount;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Integer getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getDisplayName() {
		return displayName;
	}

	public String getSaveHash() {
		return saveHash;
	}
	public void setSaveHash(String saveHash) {
		this.saveHash = saveHash;
	}
	public String getDataHash() {
		return dataHash;
	}

	public void setDownloadTimeLeft(int downloadTimeLeft) {
		this.downloadTimeLeft = downloadTimeLeft;
	}

	public int getDownloadTimeLeft() {
		return downloadTimeLeft;
	}

	public List<Integer> getFreeIds() {
		return freeIds;
	}

	@Override
	public String toString() {

		return getDisplayName();
	}

	public boolean isLocalData() {
		return isLocalData;
	}

	public void setLocalData(boolean isLocalData) {
		this.isLocalData = isLocalData;
	}

	public boolean hasNew() {
		return this.skanwordsCount - this.getSkanwordsStartedCount() - this.getSkanwordsFinishedCount() > 0;
	}


	public boolean isStarted() {
		return getSkanwordsStartedCount() > 0;
	}


	public boolean isFinished() {
		return getSkanwordsFinishedCount() > 0;
	}


	public String getMediaUrl() {
		return id + ".zip";
	}


	public boolean hasDownloadedFiles() {
		if (getMediaUrl() == null)
			return false;
		String fileName = DownloaderArhiveFile.getUnzipPath(getMediaUrl());
		File dir = new File(fileName);
		String[] files = dir.list();
		if (files == null || files.length < 1) {
			return false;
		}
		return true;
	}

	public boolean isProgressChanged() {
		return progressChanged;
	}

	public void setProgressChanged(boolean progressChanged) {
		this.progressChanged = progressChanged;
	}

	public static class SkanwordSetDate implements Serializable {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		@SerializedName("visible_from")
		private final int visibleFrom;
		private final int year;
		private final int mounth;
		private final int day;
		@SerializedName("today")
		private boolean isTooday;

		public SkanwordSetDate(int visibleFrom, int year, int mounth, int day,
				boolean isTooday) {
			super();
			this.visibleFrom = visibleFrom;
			this.year = year;
			this.mounth = mounth;
			this.day = day;
			this.isTooday = isTooday;
		}

		public int getVisibleFrom() {
			return visibleFrom;
		}

		public int getYear() {
			return year;
		}

		public int getMounth() {
			return mounth;
		}

		public int getDay() {
			return day;
		}

		public boolean isTooday() {
			return isTooday;
		}

		public void setToday(boolean today) {
			isTooday = today;
		}
	}

	public static class Comparators {
		public static Comparator<SkanwordsSetInfo> ID = new Comparator<SkanwordsSetInfo>() {

			public int compare(SkanwordsSetInfo lhs, SkanwordsSetInfo rhs) {
				long delta = rhs.getId() - lhs.getId();

				return delta > 0 ? 1 : (delta == 0 ? 0 : -1);
			}

		};
	}
}
