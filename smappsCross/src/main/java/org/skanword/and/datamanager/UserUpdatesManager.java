package org.skanword.and.datamanager;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.skanword.and.SmappsScanwords;
import org.skanword.and.etc.Utils;
import org.skanword.and.scanwordgame.SkanwordGameActivity;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

public class UserUpdatesManager {

	public static final String UPDATES_SKAN_MASK = "ucm";
	public static final String UPDATES_USER_OFFERS_USED = "uou";
	public static final String UPDATES_USER_HINTS_BONUS = "uhb";
	public static final String UPDATES_USER_KEYWORD_BONUS = "uhkb";
	public static final String UPDATES_OBJECT_TIME = "_uot";
	public static final String UPDATES_USER_STAT = "uus";
	public static final String USER_STATS_FIELD_DOUBLE_BONUS = "x2use";
	
	public static final String OFFER_NAME_ADCOLONY_VIDEO_HASH = "acvh";
	public static final String OFFER_NAME_ADCOLONY_VIDEO = "acv";
	public static final String OFFER_NAME_CHARTBOOST_VIDEO_HASH = "cbvh";
	public static final String OFFER_NAME_CHARTBOOST_VIDEO = "cbv";
	public static final String OFFER_NAME_VUNGLE_VIDEO_HASH = "vgvh";
	public static final String OFFER_NAME_VUNGLE_VIDEO = "vgv";
	public static final String OFFER_NAME_FLURRY_VIDEO_HASH = "flvh";
	public static final String OFFER_NAME_FLURRY_VIDEO = "flv";
	public static final String OFFER_NAME_UNITY_VIDEO_HASH = "unvh";
	public static final String OFFER_NAME_UNITY_VIDEO = "unv";
	public static final String OFFER_NAME_HEYZAP_VIDEO_HASH = "hzvh";
	public static final String OFFER_NAME_HEYZAP_VIDEO = "hzv";
	public static final String OFFER_SECRET_KEY = "cbc69bba7dcf617944325eaa0538d3a3";

	
	private static UserUpdatesManager instance = null;
	private Map<String, UserUpdateData> userDatas;
	private UserUpdateData currentData;
	
	protected UserUpdatesManager() {
		
	}
	public static UserUpdatesManager getInstance() {
	   if(instance == null) {
	      instance = new UserUpdatesManager();
	      instance.retrieveLocalData();
	   }
	   return instance;
	}
	
	private void  retrieveLocalData() {
		DataBaseManager database = SmappsScanwords.getDatabaseHelper();
		if (database != null) {
			HashMap<String, UserUpdateData> map = database.<HashMap<String, UserUpdateData>>retrieveGeneralData(DataBaseManager.UPDATES_DATA_ID);
			if (map == null) {
				userDatas = Collections.synchronizedMap(new HashMap<String, UserUpdateData>());
			} else {
				userDatas = Collections.synchronizedMap(map);
			}

			currentData = SmappsScanwords.getDatabaseHelper().<UserUpdateData>retrieveGeneralData(DataBaseManager.UPDATE_LAST_DATA_ID);
			if (currentData == null)
				currentData = new UserUpdateData();
		}
	}
	
	private void saveData() {
		SmappsScanwords.getDatabaseHelper().storeGeneralData(currentData, DataBaseManager.UPDATE_LAST_DATA_ID);
	}
	
	 public Map<String, UserUpdateData> saveCurrentData() {
		 Log.v("SkanwordsFunc","  saveTime" + (currentData != null ? currentData.hasData() : " no data"));
		if (currentData != null && (currentData.hasData() || SmappsScanwords.getTime(false, false) + SmappsScanwords.getTime(true, false) >= 5)) {
			long time = new Date().getTime();
			synchronized (currentData) {
				saveTime();
				currentData.saveTimeHash(time / 1000);
				userDatas.put(Utils.md5("" + time), currentData);
				SmappsScanwords.getDatabaseHelper().storeUserUpdateData(new HashMap<String, UserUpdateData>(userDatas));
				currentData = new UserUpdateData();
				saveData();
			}
		}
		return new HashMap<String, UserUpdatesManager.UserUpdateData>(userDatas);
	}
	public void saveTime() {
		synchronized (currentData) {
			boolean ingame = SkanwordGameActivity.isGameActive();
			if (ingame) {
				currentData.getUserStats().setTimeCrossOffline(currentData.getUserStats().getTimeCrossOffline() + SmappsScanwords.getTime(false, true));
				currentData.getUserStats().setTimeCrossOnline(currentData.getUserStats().getTimeCrossOnline() + SmappsScanwords.getTime(true, true));
			} else {
				currentData.getUserStats().setTimeMainOffline(currentData.getUserStats().getTimeMainOffline() + SmappsScanwords.getTime(false, true));
				currentData.getUserStats().setTimeMainOnline(currentData.getUserStats().getTimeMainOnline() + SmappsScanwords.getTime(true, true));
			}
			Gson gson = new Gson();
			Log.v("SkanwordsFunc",gson.toJson(currentData) + "  saveTime");
		}
		
	}
	public void bonusRecieved(boolean doubled, boolean hadRewardedVideo, boolean hourly) {
		synchronized (currentData) {
			Log.v("SkanwordsFunc","  bonusRecieved   " + doubled);
			currentData.bonusRecieved(doubled, hadRewardedVideo, hourly);
		}
		saveData();
	}
	public void offerUsed(String offerTarget, String offerService) {
		synchronized (currentData) {
			currentData.userOfferUsed(offerTarget, offerService);
		}
		saveData();
	}
	public void usedHint(int count) {
		synchronized (currentData) {
			currentData.useHints(count);
		}
		saveData();
	}
	public void loggedIn(LoginType type, String suffix) {
		if (currentData != null) {
			String loginTypeString = "";
			switch (type) {
				case LOGGIN_APP_FOREGROUND:
					loginTypeString = "background";
					break;
				case LOGGIN_LOCAL_NOTIFICATION:
					loginTypeString = "nl";
					break;
				case LOGGIN_APP_START:
					loginTypeString = "desktop";
					break;
				case LOGGIN_PUSH_NOTIFICATION:
					loginTypeString = "notification_remote";
					break;

				default:
					break;
			}
			if (suffix != null) {
				Log.v("SkanwordsNotif", "loggedIn notif suffix " + suffix);
				loginTypeString = String.format("%s%s", loginTypeString, suffix);
			}
			int lastDay = SmappsScanwords.getAppSharedPreferences(MainDataManager.GENERAL_PREFERENCES_NAME).getInt("login_day", 0);
			int currentDay = Calendar.getInstance().get(Calendar.DAY_OF_YEAR);
			SmappsScanwords.getAppSharedPreferencesEditor(MainDataManager.GENERAL_PREFERENCES_NAME).putInt("login_day", currentDay).commit();
			synchronized (currentData) {
				currentData.logginUser(loginTypeString, lastDay != currentDay);
			}
			saveData();
		}
	}
	public Map<String, UserUpdateData> getUserDatas() {
		return userDatas;
	}
	public void clearUpdateDataForHashes(List<String> hashes) {
		if (hashes == null || hashes.size() < 1)
			return;
		synchronized (userDatas) {
			for (String hash : hashes) {
				Iterator<Map.Entry<String,UserUpdateData>> iter = userDatas.entrySet().iterator();
				while (iter.hasNext()) {
				    Map.Entry<String,UserUpdateData> entry = iter.next();
				    if(hash.equals(entry.getKey())){
				        iter.remove();
				    }
				}
			}
			SmappsScanwords.getDatabaseHelper().storeUserUpdateData(new HashMap<String, UserUpdateData>(userDatas));
		}
	}
	public void updateScanwordMasks (Skanword scanword) {

		synchronized (currentData) {
			currentData.placeMask("c"+scanword.getId(), scanword.getAnswers(), scanword.getHints());
		}
		saveData();
	}
	public static class UserUpdateData implements Serializable {

		private static final long serialVersionUID = 346611263252355968L;
		@SerializedName(UPDATES_SKAN_MASK) 
		private HashMap<String, SkanwordMask> skanwordsMasks;
		@SerializedName(UPDATES_USER_OFFERS_USED) 
		private HashMap<String, String> userOffersUsed;
		@SerializedName(UPDATES_USER_HINTS_BONUS) 
		private HashMap<String, Object> userHintsBonus;
		@SerializedName(UPDATES_USER_KEYWORD_BONUS) 
		private HashMap<String, Object> userKeywordBonus;
		@SerializedName(UPDATES_USER_STAT) 
		private UserStats userStats;
		@SerializedName(UPDATES_OBJECT_TIME) 
		private Long time;

		private boolean keyword = true;
		
		public UserUpdateData() {
			super();
		}
		
		public void bonusRecieved(boolean doubled, boolean hadRewardedVideo, boolean hourly) {
			HashMap<String, Object> bonus = hourly ? getUserHintsBonus() : getUserKeywordBonus();
			if (!hadRewardedVideo)
				bonus.put(USER_STATS_FIELD_DOUBLE_BONUS, 0);
			else if (doubled) 
				bonus.put(USER_STATS_FIELD_DOUBLE_BONUS, 1);
			else
				bonus.put(USER_STATS_FIELD_DOUBLE_BONUS, 2);
		}

		public boolean hasData() {
			return getSkanwordsMasks().size() > 0 || getUserOffersUsed().size() > 0 || getUserStats().hasData() || getUserHintsBonus().size() > 0;
		}

		public UserUpdateData(HashMap<String, SkanwordMask> skanwordsMasks, HashMap<String, String> userOfferUsed, UserStats userStats, HashMap<String, Object> userHintsBonus) {
			super();
			this.skanwordsMasks = skanwordsMasks;
			this.userOffersUsed = userOfferUsed;
			this.userStats = userStats;
			this.userHintsBonus = userHintsBonus;
		}
		public void saveTimeHash(Long time) {
			this.time = time;
		}
		public UserStats getUserStats() {
			if (userStats == null)
				userStats = new UserStats();
			return userStats;
		}
		public HashMap<String, Object> getUserKeywordBonus() {
			if (userKeywordBonus == null)
				userKeywordBonus = new HashMap<String, Object>();
			return userKeywordBonus;
		}
		public HashMap<String, Object> getUserHintsBonus() {
			if (userHintsBonus == null)
				userHintsBonus = new HashMap<String, Object>();
			return userHintsBonus;
		}
		public HashMap<String, String> getUserOffersUsed() {
			if (userOffersUsed == null)
				userOffersUsed = new HashMap<String, String>();
			return userOffersUsed;
		}

		public HashMap<String, SkanwordMask> getSkanwordsMasks() {
			if (skanwordsMasks == null)
				skanwordsMasks = new HashMap<String, SkanwordMask>();
			return skanwordsMasks;
		}

		public void placeMask(String scan, String answers, String hints) {
			
			getSkanwordsMasks().put(scan, new SkanwordMask(answers, hints));
		}
		public void logginUser(String from, boolean uniq) {
			UserStats stats = getUserStats();
			stats.setLoginsCount(stats.getLoginsCount() + 1);
			if (uniq) {
				stats.setLoginsCountUniq(stats.getLoginsCountUniq()  +1);
			}
			HashMap<String,  Integer> froms = stats.getLoginsFrom(); 
			Integer loginsFromCount = froms.get(from);
			if (loginsFromCount == null)
				loginsFromCount = 0;
			loginsFromCount ++;
			stats.getLoginsFrom().put(from, loginsFromCount);
		}
		public void useHints(int count) {
			UserStats stats = getUserStats();
			stats.hintsCellsOpened ++;
			stats.hintsLettersUsed ++;
		}
		public void userOfferUsed(String offerTarget, String offerService) {
			if (offerTarget == null)
				return;
			Log.v("", "user offer used withtarget - " + offerTarget + " for service " + offerService);
			String offers = getUserOffersUsed().get(offerService);
			Log.v("", "offers of service  " + offers);
			String offerHashName = null;
			if (offers == null) {
				offers = offerTarget;
			} else {
				offers = offers + "," + offerTarget;
			}
			if (offerService.equals(OFFER_NAME_VUNGLE_VIDEO))
				offerHashName = OFFER_NAME_VUNGLE_VIDEO_HASH;
			else if (offerService.equals(OFFER_NAME_ADCOLONY_VIDEO))
				offerHashName = OFFER_NAME_ADCOLONY_VIDEO_HASH;
			else if (offerService.equals(OFFER_NAME_CHARTBOOST_VIDEO))
				offerHashName = OFFER_NAME_CHARTBOOST_VIDEO_HASH;
			else if (offerService.equals(OFFER_NAME_UNITY_VIDEO))
				offerHashName = OFFER_NAME_UNITY_VIDEO_HASH; 
			else if (offerService.equals(OFFER_NAME_FLURRY_VIDEO))
				offerHashName = OFFER_NAME_FLURRY_VIDEO_HASH;
			else if (offerService.equals(OFFER_NAME_HEYZAP_VIDEO))
				offerHashName = OFFER_NAME_HEYZAP_VIDEO_HASH;
			if (offerHashName == null)
				return;
			userOffersUsed.put(offerHashName, Utils.md5(String.format("%s_%s", offers, OFFER_SECRET_KEY)));
			userOffersUsed.put(offerService, offers);
		}
		public static class SkanwordMask implements Serializable {
			private static final long serialVersionUID = 1L;
			@SerializedName("a") 
			private final String answerMask;
			@SerializedName("h") 
			private final String hintsMask;
			public SkanwordMask(String answerMask, String hintsMask) {
				super();
				this.answerMask = answerMask;
				this.hintsMask = hintsMask;
			}
		}
		public static class UserStats implements Serializable {
			private static final long serialVersionUID = 8870257552488368218L;
			@SerializedName("lc") 
			private int loginsCount = 0;
			@SerializedName("lu") 
			private int LoginsCountUniq = 0;
			@SerializedName("hco") 
			private int hintsCellsOpened = 0;
			@SerializedName("hlu") 
			private int hintsLettersUsed = 0;
			@SerializedName("lf") 
			private HashMap<String, Integer> loginsFrom;
			
			@SerializedName("tc_on") 
			private int timeCrossOnline = 0;
			@SerializedName("tc_off") 
			private int timeCrossOffline = 0;
			@SerializedName("tm_on") 
			private int timeMainOnline = 0;
			@SerializedName("tm_off") 
			private int timeMainOffline = 0;
			
			
			public UserStats(int loginsCount, int loginsCountUniq,
					HashMap<String, Integer> loginsFrom, int hintsCellsOpened, int hintsLettersUsed, int timeCrossOnline, int timeCrossOffline, int timeMainOnline, int timeMainOffline) {
				super();
				this.loginsCount = loginsCount;
				LoginsCountUniq = loginsCountUniq;
				this.loginsFrom = loginsFrom;
				this.hintsCellsOpened = hintsCellsOpened;
				this.hintsLettersUsed = hintsLettersUsed;
				
				this.timeCrossOffline = timeCrossOffline;
				this.timeCrossOnline = timeCrossOnline;
				this.timeMainOffline = timeMainOffline;
				this.timeMainOnline = timeMainOnline;
			}
			public boolean hasData() {
				return ((loginsCount + hintsCellsOpened) > 0) || ((timeCrossOffline + timeCrossOnline + timeMainOffline + timeMainOnline) >= 5);
			}
			public UserStats() {
				super();
				loginsFrom = new HashMap<String, Integer>();
			}
			
			
			
			public int getTimeCrossOnline() {
				return timeCrossOnline;
			}
			public void setTimeCrossOnline(int timeCrossOnline) {
				this.timeCrossOnline = timeCrossOnline;
			}
			public int getTimeCrossOffline() {
				return timeCrossOffline;
			}
			public void setTimeCrossOffline(int timeCrossOffline) {
				this.timeCrossOffline = timeCrossOffline;
			}
			public int getTimeMainOnline() {
				return timeMainOnline;
			}
			public void setTimeMainOnline(int timeMainOnline) {
				this.timeMainOnline = timeMainOnline;
			}
			public int getTimeMainOffline() {
				return timeMainOffline;
			}
			public void setTimeMainOffline(int timeMainOffline) {
				this.timeMainOffline = timeMainOffline;
			}
			public int getLoginsCount() {
				return loginsCount;
			}
			public void setLoginsCount(int loginsCount) {
				this.loginsCount = loginsCount;
			}
			public int getLoginsCountUniq() {
				return LoginsCountUniq;
			}
			public void setLoginsCountUniq(int loginsCountUniq) {
				LoginsCountUniq = loginsCountUniq;
			}
			public HashMap<String, Integer> getLoginsFrom() {
				if (loginsFrom == null) {
					loginsFrom = new HashMap<String, Integer>();
				}
				return loginsFrom;
			}
			public int getHintsCellsOpened() {
				return hintsCellsOpened;
			}
			public void setHintsCellsOpened(int hintsCellsOpened) {
				this.hintsCellsOpened = hintsCellsOpened;
			}
			public int getHintsLettersUsed() {
				return hintsLettersUsed;
			}
			public void setHintsLettersUsed(int hintsLettersUsed) {
				this.hintsLettersUsed = hintsLettersUsed;
			}
			
			
		}
	}
	public static enum LoginType {LOGGIN_LOCAL_NOTIFICATION, LOGGIN_PUSH_NOTIFICATION , LOGGIN_APP_START, LOGGIN_APP_FOREGROUND}
}
