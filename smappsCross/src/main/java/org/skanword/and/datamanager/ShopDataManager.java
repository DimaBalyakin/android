package org.skanword.and.datamanager;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.skanword.and.SmappsScanwords;
import org.skanword.and.datamanager.ShopDataManager.Shop.ShopItemsSet.ShopItem;
import org.skanword.and.inappbilling.InAppManager;

import android.util.Log;

import com.google.gson.annotations.SerializedName;
import com.musselwhizzle.dispatcher.events.SimpleEvent;

public class ShopDataManager {
	private Shop mShop = null;
	private String mShopHash = null;
	
	public void retrieveData () {
		mShopHash = SmappsScanwords.getDatabaseHelper().<String>retrieveGeneralData(DataBaseManager.SHOP_HASH_ID);
		if (mShopHash == null)
			mShopHash = "";
		updateShopData(SmappsScanwords.getDatabaseHelper().<Shop>retrieveGeneralData(DataBaseManager.SHOP_DATA_ID), false);
		//Log.v("" ," retrieved shop data " + mShop);
	}
	
	public void updateShopData(Shop shop, boolean total) {
		if (shop == null)
			return;
		if (mShop != null) {
			shop.setUpdatedTime(new Date().getTime() / 1000);
		}
		mShop = shop;
		List<String> skus = new ArrayList<String>();
		for (ShopItem item : mShop.getItemsCommon().getHints()) {
			skus.add(item.getId());
		}
		for (ShopItem item : mShop.getItemsCommon().getVip()) {
			skus.add(item.getId());
		}
		InAppManager.getInstance().getProductsPrices(skus);
		if (total)
			SmappsScanwords.getDatabaseHelper().storeShopData(shop);
		SmappsScanwords.getEventsDispatcher().dispatchEvent(new SimpleEvent(MainDataManager.EVENT_SHOP_DATA_UPDATE));
	}
	
	public long getSaleTimeLeft() {
		if (mShop == null || mShop.getSale() == null)
			return 0;
		return mShop.getSale().getSaleTimeLeft() - (new Date().getTime() / 1000 - mShop.getUpdatedTime());
	}
	public String getShopHash() {
		return mShopHash ;
	}

	public void setShopHash(String hash) {
		this.mShopHash = hash;
		SmappsScanwords.getDatabaseHelper().storeGeneralData(this.mShopHash, DataBaseManager.SHOP_HASH_ID);
		Log.v("", "mShopHash update " + mShopHash);
	}

	public boolean saleEnabled() {
		return getSaleTimeLeft() > 0;
	}
	
	public boolean shopItemsTypeWithSale(ShopProductType type) {
		if (!saleEnabled())
			return false;
		List<ShopItem> list = null;
		if (mShop == null)
			return false;
		switch (type) {
		case SUBSCRIPTION:
			list = mShop.getItemsCommon().getVip();
			break;
		case HINTS:
			list = mShop.getItemsCommon().getHints();
			break;

		default:
			break;
		}
		for (ShopItem shopItem : list) {
			if (shopItem.isSale())
				return true;
		}
		return false;
	}
	
	public ShopItem getDefaultShopItemOfType(ShopProductType type) {
		List<ShopItem> list = null;
		if (mShop == null)
			return null;
		switch (type) {
		case SUBSCRIPTION:
			list = mShop.getItemsCommon().getVip();
			break;
		case HINTS:
			list = mShop.getItemsCommon().getHints();
			break;

		default:
			break;
		}
		for (ShopItem shopItem : list) {
			if (shopItem.isDefaultItem())
				return shopItem;
		}
		return null;
	}
	public List<ShopItem> getShopItemsOfType(ShopProductType type) {
		List<ShopItem> list = null;
		if (mShop == null)
			return list;
		switch (type) {
		case SUBSCRIPTION:
			list = mShop.getItemsCommon().getVip();
			break;
		case HINTS:
			list = mShop.getItemsCommon().getHints();
			break;
		case ADS_OFF:
			list = mShop.getItemsCommon().getAdsOff();
			break;
		case MINI_SHOP:
			list = new ArrayList<ShopItem>();
			for (ShopItem shopItem : mShop.getItemsCommon().getHints()) {
				if (shopItem.isMiniShop()) {
					list.add(shopItem);
				}
			}
			break;

		default:
			break;
		}
		return list;
	}
	public static class Shop implements Serializable{

		/**
		 * 
		 */
		private static final long serialVersionUID = -1176298582371777763L;
		@SerializedName("items_common")
		private final ShopItemsSet itemsCommon;
		@SerializedName("items_sale")
		private final ShopItemsSet itemsSale;
		private final SaleData sale;
		@SerializedName("updated_time")
		private long updatedTime = 0;
		
		
		public Shop(ShopItemsSet itemsCommon, ShopItemsSet itemsSale, SaleData sale) {
			super();
			this.itemsCommon = itemsCommon;
			this.itemsSale = itemsSale;
			this.sale = sale;
		}
		public SaleData getSale() {
			return sale;
		}

		public ShopItemsSet getItemsCommon() {
			return itemsCommon;
		}
		public ShopItemsSet getItemsSale() {
			return itemsSale;
		}
		

		public long getUpdatedTime() {
			return updatedTime;
		}

		public void setUpdatedTime(long updatedTime) {
			this.updatedTime = updatedTime;
		}

		public static class SaleData implements Serializable {

			/**
			 * 
			 */
			private static final long serialVersionUID = 5925859866820118892L;

			@SerializedName("time_left")
			private final long saleTimeLeft;
			
			public SaleData(Long saleTimeLeft) {
				super();
				this.saleTimeLeft = saleTimeLeft;
			}

			public long getSaleTimeLeft() {
				return saleTimeLeft;
			}
			
		}

		public static class ShopItemsSet implements Serializable {
			/**
			 * 
			 */
			private static final long serialVersionUID = -2770942828674933010L;
			private final List<ShopItem> hints;
			private final List<ShopItem> vip;
			@SerializedName("ads_off")
			private final List<ShopItem> adsOff;
			
			public ShopItemsSet(List<ShopItem> hints, List<ShopItem> vip,
					List<ShopItem> adsOff) {
				super();
				this.hints = hints;
				this.vip = vip;
				this.adsOff = adsOff;
			}

			public List<ShopItem> getHints() {
				return hints;
			}

			public List<ShopItem> getVip() {
				return vip;
			}

			public List<ShopItem> getAdsOff() {
				return adsOff;
			}

			public static class ShopItem implements Serializable{
				/**
				 * 
				 */
				private static final long serialVersionUID = -1697317095487755238L;
				private final String id;
				private final String name;
				private final String description;
				@SerializedName("success_message")
				private final String successMessage;
				@SerializedName("success_message_restored")
				private final String successMessageRestored;
				@SerializedName("default")
				private final boolean defaultItem;
				private final boolean visible;
				private final boolean sale;
				@SerializedName("mini_shop")
				private final boolean miniShop;
				private final int sort;
				@SerializedName("vip_cross_new")
				private final int vipScanwordsNew;
				@SerializedName("vip_cross_archive")
				private final int vipScanwordsArchive;
				@SerializedName("icon_name")
				private final String iconName;
				public ShopItem(String id, String name, String description,
						String successMessage,
						String successMessageRestored, boolean defaultItem, boolean visible,
						boolean sale, boolean miniShop, int sort,
						int vipScanwordsNew, int vipScanwordsArchive, String iconName) {
					super();
					this.id = id;
					this.name = name;
					this.description = description;
					this.successMessage = successMessage;
					this.successMessageRestored = successMessageRestored;
					this.defaultItem = defaultItem;
					this.visible = visible;
					this.sale = sale;
					this.miniShop = miniShop;
					this.sort = sort;
					this.iconName = iconName;
					this.vipScanwordsNew = vipScanwordsNew;
					this.vipScanwordsArchive = vipScanwordsArchive;
				}
				
				public String getIconName() {
					return iconName;
				}

				public boolean isDefaultItem() {
					return defaultItem;
				}
				public String getId() {
					return id;
				}
				public String getName() {
					return name;
				}
				public String getDescription() {
					return description;
				}
				public String getSuccessMessage() {
					return successMessage;
				}
				public String getSuccessMessageRestored() {
					return successMessageRestored;
				}
				public boolean isVisible() {
					return visible;
				}
				public boolean isSale() {
					return sale;
				}
				public boolean isMiniShop() {
					return miniShop;
				}
				public int getSort() {
					return sort;
				}
				public int getVipScanwordsNew() {
					return vipScanwordsNew;
				}
				public int getVipScanwordsArchive() {
					return vipScanwordsArchive;
				}
				
				
				
			}
		}
	}

	public static enum ShopProductType {SUBSCRIPTION, HINTS, ADS_OFF, MINI_SHOP }
}
