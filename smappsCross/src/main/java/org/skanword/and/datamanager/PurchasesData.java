package org.skanword.and.datamanager;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import android.util.Log;

public class PurchasesData implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1457480924490174550L;
	private List<String> purchasesMade;
	private List<String> purchasesRestored;
	
	public PurchasesData(List<String> purchases, List<String> purchasesRestored) {
		super();
		this.purchasesMade = purchases;
		this.purchasesRestored = purchases;
		Log.v("", " purchases  restored " + purchases);
	}
	public PurchasesData() {
		super();
		purchasesMade = new ArrayList<String>();
		purchasesRestored = new ArrayList<String>();
	}
	public List<String> getPurchases() {
		return purchasesMade;
	}
	public List<String> getPurchasesRestored() {
		return purchasesRestored;
	}
	public void addPurchase(String purchase, Long time, boolean restored) {
		if (restored) {
			purchasesRestored.add(purchase);
		} else
			purchasesMade.add(purchase);
	}
	public void clearPurcheses(List<String> purchases) {
		for (String purchase : purchases) {
			purchasesMade.remove(purchase);
			purchasesRestored.remove(purchase);
		}
	}
}
