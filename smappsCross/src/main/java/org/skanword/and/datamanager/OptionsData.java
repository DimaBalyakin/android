package org.skanword.and.datamanager;

import java.io.Serializable;

public class OptionsData implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9096395339197790134L;
	
	private boolean sounds = true;
	private boolean skipGuessedLetters = false;
	private boolean increasedRatio = false;
	private boolean autoDownload = false;
	private boolean onlyWifi = false;
	private boolean informNewIssues = true;
	private boolean defaultNotificationsTime = true;
	private boolean disabledNotificationsSounds = false;
	private int customNotificationTimeHour = 0;
	private int customNotificationTimeMinute = 0;
	public OptionsData(boolean sounds, boolean skipGuessedLetters,
			boolean increasedRatio, boolean autoDownload, boolean onlyWifi,
			boolean informNewIssues, boolean defaultNotificationsTime, boolean disabledNotificationsSounds,int customNotificationTimeHour, int customNotificationTimeMinute) {
		super();
		this.sounds = sounds;
		this.skipGuessedLetters = skipGuessedLetters;
		this.increasedRatio = increasedRatio;
		this.autoDownload = autoDownload;
		this.onlyWifi = onlyWifi;
		this.informNewIssues = informNewIssues;
		this.defaultNotificationsTime = defaultNotificationsTime;
		this.disabledNotificationsSounds = disabledNotificationsSounds;
		this.customNotificationTimeHour = customNotificationTimeHour;
		this.customNotificationTimeMinute = customNotificationTimeMinute;
	}
	public OptionsData() {
		super();
		// TODO Auto-generated constructor stub
	}
	public boolean isSounds() {
		return sounds;
	}
	public void setSounds(boolean sounds) {
		this.sounds = sounds;
	}
	public boolean isSkipGuessedLetters() {
		return skipGuessedLetters;
	}
	public void setSkipGuessedLetters(boolean skipGuessedLetters) {
		this.skipGuessedLetters = skipGuessedLetters;
	}
	public boolean isIncreasedRatio() {
		return increasedRatio;
	}
	public void setIncreasedRatio(boolean increasedRatio) {
		this.increasedRatio = increasedRatio;
	}
	public boolean isAutoDownload() {
		return autoDownload;
	}
	public void setAutoDownload(boolean autoDownload) {
		this.autoDownload = autoDownload;
	}
	public boolean isOnlyWifi() {
		return onlyWifi;
	}
	public void setOnlyWifi(boolean onlyWifi) {
		this.onlyWifi = onlyWifi;
	}
	public boolean isInformNewIssues() {
		return informNewIssues;
	}
	public void setInformNewIssues(boolean informNewIssues) {
		this.informNewIssues = informNewIssues;
	}
	public boolean isDefaultNotificationsTime() {
		return defaultNotificationsTime;
	}
	public void setDefaultNotificationsTime(boolean defaultNotificationsTime) {
		this.defaultNotificationsTime = defaultNotificationsTime;
	}
	
	
	public int getCustomNotificationTimeHour() {
		return customNotificationTimeHour;
	}
	public void setCustomNotificationTimeHour(int customNotificationTimeHour) {
		this.customNotificationTimeHour = customNotificationTimeHour;
	}
	public int getCustomNotificationTimeMinute() {
		return customNotificationTimeMinute;
	}
	public void setCustomNotificationTimeMinute(int customNotificationTimeMinute) {
		this.customNotificationTimeMinute = customNotificationTimeMinute;
	}
	public boolean isDisabledNotificationsSounds() {
		return disabledNotificationsSounds;
	}
	public void setDisabledNotificationsSounds(boolean disabledNotificationsSounds) {
		this.disabledNotificationsSounds = disabledNotificationsSounds;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
