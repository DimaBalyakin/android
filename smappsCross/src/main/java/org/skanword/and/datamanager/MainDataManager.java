package org.skanword.and.datamanager;

import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.skanword.and.SmappsScanwords;
import org.skanword.and.datamanager.NewsDataManager.News;
import org.skanword.and.datamanager.RatingDataManager.RatingInfo;
import org.skanword.and.datamanager.ShopDataManager.Shop;
import org.skanword.and.etc.LocalNotificationsManager;
import org.skanword.and.etc.TutorialManager;
import org.skanword.and.network.ConfigObject;
import org.skanword.and.network.ConfigObject.AppAds;
import org.skanword.and.network.ConfigObject.AppOffers;
import org.skanword.and.network.ConfigObject.LocalNotification;
import org.skanword.and.network.ConfigObject.Versions;
import org.skanword.and.network.DailyBonusObject;
import org.skanword.and.network.DownloaderArhiveFile;
import org.skanword.and.network.MainNetworkManager;
import org.skanword.and.network.SkanwordsSetDataObject;
import org.skanword.and.network.UserObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.util.Log;

import com.musselwhizzle.dispatcher.events.SimpleEvent;

@SuppressLint("UseSparseArrays")
public class MainDataManager {

    public static final String USER_DATA_PREFS_NAME = "UserDataPrefs";
    public static final String EVENT_SKANWORDS_SETS_INFOS_UPDATE = "EVENT_SCANWORDS_SETS_INFOS_UPDATE";
    public static final String EVENT_PROGRESS_UPDATE = "EVENT_PROGRESS_UPDATE";
    public static final String EVENT_RATING_UPDATE = "EVENT_RATING_UPDATE";
    public static final String EVENT_NEWS_UPDATE = "EVENT_NEWS_UPDATE";
    public static final String EVENT_SHOP_DATA_UPDATE = "EVENT_SHOP_DATA_UPDATE";
    public static final String EVENT_SKANWORDS_TASKS_UPDATE = "EVENT_SCANWORDS_TASKS_UPDATE";
    public static final String EVENT_USER_INFO_UPDATE = "EVENT_USER_INFO_UPDATE";
    public static final String EVENT_OPTIONS_UPDATE = "EVENT_OPTIONS_UPDATE";
    public static final String EVENT_NOTIFICATIONS_UPDATE = "EVENT_NOTIFICATIONS_UPDATE";
    public static final String EVENT_NO_TOKEN = "EVENT_NO_TOKEN";
    public static final String EVENT_DOWNLOAD_TODAY_ISSUE = "EVENT_DOWNLOAD_TODAY_ISSUE";
    public static final String GENERAL_PREFERENCES_NAME = "GENERAL_PREFERENCES_NAME";
    public static final String TUTORIAL_PREFERENCES_NAME = "tutorial";
    public static final String DAILY_BONUS_READY = "DAILY_BONUS_READY";
	
	private UserObject mUserData;
	private static MainDataManager instance = null;
	private final SkanwordsDataManager mSkanwordsDataManager;
	private final ShopDataManager mShopDataManager;
	private final RatingDataManager mRatingDataManager;
	private final NewsDataManager mNewsDataManager;
	private OptionsData mOptionsData;
	private LocalNotification mLocalNotifications;
	private PurchasesData mPurchasesData;
	private DailyBonusObject mDailyBonus;
	private List<String> mUserFriends = null;
	private volatile Map<Integer, Integer> setsProgresses = new HashMap<Integer, Integer>();
	private AppAds mAppAds;
	private AppOffers mAppOffers;
	private Versions mVersions;

	protected MainDataManager() {
		mSkanwordsDataManager = new SkanwordsDataManager();
		mShopDataManager = new ShopDataManager();
		mRatingDataManager = new RatingDataManager();
		mNewsDataManager = new NewsDataManager();
	}
	
	public LocalNotification getLocalNotifications() {
		return mLocalNotifications;
	}

	public void setLocalNotifications(LocalNotification mLocalNotifications) {
		this.mLocalNotifications = mLocalNotifications;
	}

	public PurchasesData getPurchasesData() {
		return mPurchasesData;
	}

	public RatingDataManager getRatingDataManager() {
		return mRatingDataManager;
	}
	public NewsDataManager getNewsDataManager() {
		return mNewsDataManager;
	}
	public UserObject getUserData() {
		return mUserData;
	}
	public SkanwordsDataManager getSkanwordsDataManager () {
		return mSkanwordsDataManager;
	}
	
	public ShopDataManager getShopDataManager() {
		return mShopDataManager;
	}
	
	
	
	public List<String> getUserFriends() {
		return mUserFriends;
	}

	public void setUserFriends(List<String> mUserFriends) {
		this.mUserFriends = mUserFriends;
		ArrayList<String> a = new ArrayList<String>(mUserFriends);
		SmappsScanwords.getDatabaseHelper().storeGeneralData(a, DataBaseManager.FRIENDS_DATA_ID);
	}

	public static MainDataManager getInstance() {
	   if(instance == null) {
	      instance = new MainDataManager();
	      instance.retrieveLocalData();
	   }
	   return instance;
	}
	
	public void configDataReceived(ConfigObject config) {
		if (config == null)
			return;
		
		boolean versionsUpdate = false;
		if (config.getVersions() != null) {
			Log.v("SkanwordsFunc","config recieved versions");
			if (mVersions == null || !mVersions.equals(config.getVersions())) {
				Log.v("SkanwordsFunc","versions not the same");
				versionsUpdate = true;
			}
		}
		mVersions = config.getVersions();
		SmappsScanwords.getDatabaseHelper().storeGeneralData(mVersions, DataBaseManager.VERSIONS_DATA_ID);
		if (versionsUpdate) {
			Log.v("SkanwordsFunc","check versions after config received");
			SmappsScanwords.instance.checkVersions();
		}
		
		if (config.getLocalNotifications() != null) {
			mLocalNotifications = config.getLocalNotifications();
			SmappsScanwords.getDatabaseHelper().storeGeneralData(mLocalNotifications, DataBaseManager.NOTIFICATIONS_DATA_ID);
			SmappsScanwords.getEventsDispatcher().dispatchEvent(new SimpleEvent(MainDataManager.EVENT_NOTIFICATIONS_UPDATE));
		}
		
		if (config.getAppAds() != null) {
			mAppAds = config.getAppAds();
			SmappsScanwords.getDatabaseHelper().storeGeneralData(mAppAds, DataBaseManager.ADS_DATA_ID);
		}
		if (config.getAppOffers() != null) {
			mAppOffers = config.getAppOffers();
			SmappsScanwords.getDatabaseHelper().storeGeneralData(mAppOffers, DataBaseManager.OFFERS_DATA_ID);
			
		}
		if (!config.isPushTokenExist()) {
			SmappsScanwords.getEventsDispatcher().dispatchEvent(new SimpleEvent(MainDataManager.EVENT_NO_TOKEN));
		}
		int infoMask = 0;
		if (!config.getShopHash().equals(mShopDataManager.getShopHash())) {
			Log.v("",config.getShopHash() +  " - config shop hash | stored shop hash - " + mShopDataManager.getShopHash());
			infoMask |= MainNetworkManager.INFO_SHOP;
		}
		if (!config.getNewsHash().equals(mNewsDataManager.getNewsHash())) {
			Log.v("",config.getNewsHash() +  " - config news hash | stored news hash - " + mNewsDataManager.getNewsHash());
			infoMask |= MainNetworkManager.INFO_NEWS;
		}
		if (infoMask > 0) {
			Log.v("", "request info after config got for mask " + infoMask);
			MainNetworkManager.getInstance().requestInfo(infoMask, null);
		}
	}
	public DailyBonusObject getDailyBonus() {
		return mDailyBonus;
	}
	public void rewriteDailyBonusObjectRecieved(DailyBonusObject dailyBonusObject) {
		Log.v("SkanwordsFunc", "rewriteDailyBonusObjectRecieved " +dailyBonusObject);
		if (dailyBonusObject == null) {
			LocalNotificationsManager.clearBonusNotifs();
		}
		mDailyBonus = dailyBonusObject;
		SmappsScanwords.getDatabaseHelper().storeGeneralData(mDailyBonus, DataBaseManager.DAILY_BONUS_DATA_ID);
	}
	public void dailyBonusObjectRecieved(DailyBonusObject dailyBonusObject) {
		Log.v("SkanwordsFunc", "1dailyBonusObjectRecieved " + dailyBonusObject);
		LocalNotificationsManager.createDailyBonusNotifications(dailyBonusObject, SmappsScanwords.getContext());
		if (mDailyBonus == null) {
			mDailyBonus = dailyBonusObject;
		} else {
			mDailyBonus.setScratchedPosition(0);
			mDailyBonus.setTimeLeft(dailyBonusObject.getTimeLeft());
			mDailyBonus.setVariants(dailyBonusObject.getVariants());
			mDailyBonus.setWin(dailyBonusObject.getWin());
		}
		mDailyBonus.setDownloadedTime(new Date().getTime());
		SmappsScanwords.getDatabaseHelper().storeGeneralData(mDailyBonus, DataBaseManager.DAILY_BONUS_DATA_ID);
		
		if (mDailyBonus.getTimeLeft() < 1)  {
			SmappsScanwords.getEventsDispatcher().dispatchEvent(new SimpleEvent(MainDataManager.DAILY_BONUS_READY));
		}
		
	}
	public void addUserHints(int i, boolean videoOfferUsed) {
		MainDataManager
				.getInstance()
				.getUserData()
				.setHints(
						MainDataManager.getInstance().getUserData()
								.getHints() + i);
		if (videoOfferUsed)
			MainDataManager.getInstance().getUserData().setHintsForVideoCount(MainDataManager.getInstance().getUserData().getHintsForVideoCount() - 1);
		MainDataManager.getInstance().storeUserData();
	}
	public void userDataReceived(UserObject user) {
		if (user != null)
			mUserData = user;
		mUserData.setUpdatedTime(new Date().getTime() / 1000);
		storeUserData();
		SmappsScanwords.getAppSharedPreferencesEditor(MainDataManager.GENERAL_PREFERENCES_NAME).putBoolean("show_hints_offers", user.isShowHintsVideoOffer()).commit();
	}
	public void updateShopData(Shop shop, String hash) {
		if (shop == null) 
			return;
		mShopDataManager.updateShopData(shop, true);
		mShopDataManager.setShopHash(hash);
	}
	public void updateNewsData(List<News> news, String hash) {
		if (news == null) 
			return;
		mNewsDataManager.updateNews(news, true);
		mNewsDataManager.setNewsHash(hash);
	}
	public void updateRatingData(RatingInfo rating, String hash) {
		if (rating == null) 
			return;
		mRatingDataManager.upadateUsersRating(rating.getTotalRatings(), true);
		mRatingDataManager.setRatingHash(hash);
	}
	private void retrieveLocalData() {
		
		//SharedPreferences settings = SmappsScanwords.getContext().getSharedPreferences(MainDataManager.GENERAL_PREFERENCES_NAME, 0);
		
		mSkanwordsDataManager.retrieveData();
		mShopDataManager.retrieveData();
		mRatingDataManager.retrieveData();
		mNewsDataManager.retrieveData();
		mVersions = SmappsScanwords.getDatabaseHelper().<Versions>retrieveGeneralData(DataBaseManager.VERSIONS_DATA_ID);
		
		mUserData = SmappsScanwords.getDatabaseHelper().<UserObject>retrieveGeneralData(DataBaseManager.USER_DATA_ID);
		if (mUserData == null) {
			mUserData = new UserObject();
		}
		mOptionsData = SmappsScanwords.getDatabaseHelper().<OptionsData>retrieveGeneralData(DataBaseManager.OPTIONS_DATA_ID);
		if (mOptionsData == null) {
			mOptionsData = new OptionsData();
		}
		mPurchasesData = SmappsScanwords.getDatabaseHelper().<PurchasesData>retrieveGeneralData(DataBaseManager.PURCHASES_DATA_ID);
		if (mPurchasesData == null) {
			mPurchasesData = new PurchasesData();
		}
		mLocalNotifications = SmappsScanwords.getDatabaseHelper().<LocalNotification>retrieveGeneralData(DataBaseManager.NOTIFICATIONS_DATA_ID);
		if (mLocalNotifications == null) {
			mLocalNotifications = new LocalNotification();
		}
		mAppAds = SmappsScanwords.getDatabaseHelper().<AppAds>retrieveGeneralData(DataBaseManager.ADS_DATA_ID);
		if (mAppAds == null) {
			mAppAds = new AppAds();
		}
		mAppOffers = SmappsScanwords.getDatabaseHelper().<AppOffers>retrieveGeneralData(DataBaseManager.OFFERS_DATA_ID);
		if (mAppOffers == null) {
			mAppOffers = new AppOffers();
		}
		mLocalNotifications = SmappsScanwords.getDatabaseHelper().<LocalNotification>retrieveGeneralData(DataBaseManager.NOTIFICATIONS_DATA_ID);
		if (mLocalNotifications == null) {
			mLocalNotifications = new LocalNotification();
		}
		mDailyBonus = SmappsScanwords.getDatabaseHelper().<DailyBonusObject>retrieveGeneralData(DataBaseManager.DAILY_BONUS_DATA_ID);
		
		mUserFriends = SmappsScanwords.getDatabaseHelper().<List<String>>retrieveGeneralData(DataBaseManager.FRIENDS_DATA_ID);
		SmappsScanwords.getEventsDispatcher().dispatchEvent(new SimpleEvent(MainDataManager.EVENT_USER_INFO_UPDATE));
	}
	public void storeOptions() {
		if (mOptionsData == null)
			return;
		SmappsScanwords.getEventsDispatcher().dispatchEvent(new SimpleEvent(MainDataManager.EVENT_OPTIONS_UPDATE));
		SmappsScanwords.getDatabaseHelper().storeGeneralData(mOptionsData, DataBaseManager.OPTIONS_DATA_ID);
	}
	public Versions getVersions() {
		return mVersions;
	}
	public OptionsData getOptions() {
		return mOptionsData;
	}
	
	public AppAds getAppAds() {
		return mAppAds;
	}

	public AppOffers getAppOffers() {
		return mAppOffers;
	}

	public void storeUserData() {
		if (mUserData == null)
			return;
		SmappsScanwords.getEventsDispatcher().dispatchEvent(new SimpleEvent(MainDataManager.EVENT_USER_INFO_UPDATE));
		SmappsScanwords.getDatabaseHelper().storeGeneralData(mUserData, DataBaseManager.USER_DATA_ID);
	}
	public void storePurchases() {
		if (mPurchasesData == null)
			return;
		SmappsScanwords.getDatabaseHelper().storeGeneralData(mPurchasesData, DataBaseManager.PURCHASES_DATA_ID);
	}
	
	public void downloadSetWithId(Activity activity, SkanwordsSetInfo setInfo, IDowloadSetProgress progressListener) {
		SmappsScanwords.getExecutorService().execute(new DownloadSetDataRunnable(activity, SetType.SKANWORD_SET_TYPE, setInfo, progressListener));
	}


	public static interface IDowloadSetProgress{
		// progress  0....100  -1 error
		void onProgress(int progress);
		
		int ERROR_PROGRESS = - 1;
	}
	
	
	private class DownloadSetDataRunnable implements Runnable{
		
		private final SetType setType;
		private final SkanwordsSetInfo setInfo;
		private final IDowloadSetProgress dowloadSetProgress;
		private Activity mCallerActivity;
		

		public DownloadSetDataRunnable(Activity activity, SetType setType, SkanwordsSetInfo setInfo, IDowloadSetProgress dowloadSetProgress) {
			super();
			mCallerActivity = activity;
			this.setType = setType;
			this.setInfo = setInfo;
			this.dowloadSetProgress = dowloadSetProgress;
		}


		public void run() {
			String urlFile = null;
			String url = null;
			SkanwordsSetDataObject setDataObject = null;
			if (this.setType == SetType.SKANWORD_SET_TYPE) {
				Map<String, Integer> data = new HashMap<String, Integer>();
				data.put("set_id", setInfo.getId());
				data.put("return", MainNetworkManager.SKAN_DATA_DATA | MainNetworkManager.SKAN_DATA_PROGRESS);
				List<Map<String, Integer>> requestParam = new ArrayList<Map<String, Integer>>();
				requestParam.add(data);
				setDataObject = MainNetworkManager.getInstance().skanwordSetGet(mCallerActivity, requestParam);
				if (setDataObject == null) {
					dowloadSetProgress.onProgress(IDowloadSetProgress.ERROR_PROGRESS);
					return;
				}
				if (setDataObject.getFristScanwordSet() != null){
					url = setDataObject.getFristScanwordSet().getMediaUrl();
					mSkanwordsDataManager.storeSkanwordsSet(setDataObject, setInfo);
					urlFile = mSkanwordsDataManager.setMediaFileUrlForSetInfo(url, this.setInfo.getId());
				}
			}
			
			if (url == null) {
				MainNetworkManager.getInstance().serverErrorLog("scanword_set_get", "no url");
				dowloadSetProgress.onProgress(IDowloadSetProgress.ERROR_PROGRESS);
			} else {
				DownloaderArhiveFile arhiveFile = new DownloaderArhiveFile();
				arhiveFile.setProgress(dowloadSetProgress, 10);
				try {
					arhiveFile.dowload(new  URL(url), urlFile);
				} catch (Exception e) {
					MainNetworkManager.getInstance().serverErrorLog("downloading_archive", "exeption with message : " + e.getMessage());
					SmappsScanwords.sendCaughtException(e);
					dowloadSetProgress.onProgress(IDowloadSetProgress.ERROR_PROGRESS);
				}
			}
		}

	}

	public void addDownloadingProgress(SkanwordsSetInfo info) {
		setsProgresses.put(info.getId(), 1);
		SmappsScanwords.LOG.warning("addDownloadingProgress - "
				+ setIsDownloading(info));
	}

	public void removeDownloadingProgress(SkanwordsSetInfo info) {
		setsProgresses.remove(info.getId());
	}
	public void updateProgress(SkanwordsSetInfo info, int progress) {
		setsProgresses.put(info.getId(), progress);
	}

	public boolean setIsDownloading(SkanwordsSetInfo info) {
		return setsProgresses.get(info.getId()) != null;
	}
	public int getSetDownloadingProgress(Integer id) {
		if (setsProgresses == null || id == null)
			return 0;
		return setsProgresses.get(id);
	}

	public static enum SetType {SKANWORD_SET_TYPE }
	public static enum UpdateLayout {LAYOUT_MAIN, LAYOUT_RATING, LAYOUT_AUTO}
}
