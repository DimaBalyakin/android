package org.skanword.and.datamanager;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

import org.skanword.and.SmappsScanwords;
import org.skanword.and.datamanager.ShopDataManager.Shop;
import org.skanword.and.datamanager.Skanword.Keyword;
import org.skanword.and.datamanager.Skanword.Keyword.KeywordCell;
import org.skanword.and.datamanager.UserUpdatesManager.UserUpdateData;
import org.skanword.and.etc.SarializableManager;
import org.skanword.and.etc.Utils;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

@SuppressLint("NewApi")
public class DataBaseManager extends SQLiteOpenHelper {
	public static final String DATABASE_NAME = "smappsscanword";
	public static final int DATABASE_VERSION = 6;

	public static final int UPDATES_DATA_ID = 1;
	public static final int UPDATE_LAST_DATA_ID = 2;
	public static final int SHOP_DATA_ID = 3;
	public static final int NEWS_DATA_ID = 4;
	public static final int RATING_DATA_ID = 5;
	public static final int USER_DATA_ID = 6;
	public static final int FRIENDS_DATA_ID = 7;
	public static final int OPTIONS_DATA_ID = 8;
	public static final int PURCHASES_DATA_ID = 9;
	public static final int NOTIFICATIONS_DATA_ID = 10;
	public static final int ADS_DATA_ID = 11;
	public static final int OFFERS_DATA_ID = 12;
	public static final int OFFER_OPENED_TASKS_DATA_ID = 13;
	public static final int READ_NEWS_DATA_ID = 14;
	public static final int NEWS_HASH_ID = 15;
	public static final int RATING_HASH_ID = 16;
	public static final int SHOP_HASH_ID = 17;
	public static final int VERSIONS_DATA_ID = 18;
	public static final int DAILY_BONUS_DATA_ID = 19;

	// scanword progress data
	public static final String TABLE_SKANWORDS = "scanwords";
	public static final String COLUMN_SKANWORD_ID = BaseColumns._ID;
	public static final String COLUMN_SKANWORD_HASH = "hash";
	public static final String COLUMN_SKANWORD_LEVEL = "level";
	public static final String COLUMN_SKANWORD_DISPLAY_ID = "display_id";
	public static final String COLUMN_SKANWORD_SETTINGS = "settings";
	public static final String COLUMN_SKANWORD_QUESTIONS = "questions";
	public static final String COLUMN_SKANWORD_COVER = "skanword_cover";
	public static final String COLUMN_SKANWORD_REPLACES = "replaces";
	public static final String COLUMN_SKANWORD_KEYWORD = "keyword";
	public static final String COLUMN_SKANWORD_FILES_DIR = "files_dir";
	public static final String COLUMN_SKANWORD_QUESTIONS_FINISHED = "questions_finished_count";
	public static final String COLUMN_SKANWORD_QUESTIONS_COUNT = "questions_count";
	public static final String COLUMN_SKANWORD_LETTERS_MASK = "mask";
	public static final String COLUMN_SKANWORD_UPDATE_TIME = "update_time";
	public static final String COLUMN_SKANWORD_ANSWERS_MASK = "answers";
	public static final String COLUMN_SKANWORD_HINTS_MASK = "hints";
	public static final String COLUMN_SKANWORD_FINISHED = "finished";
	public static final String COLUMN_SKANWORD_STARTED = "started";
	public static final String COLUMN_SKANWORD_OPENED = "opened";

	private Integer skanwordsProgressMutex = 0;

	private void createCrosswordsTable(SQLiteDatabase db) {
		db.execSQL("CREATE TABLE IF NOT EXISTS " + TABLE_SKANWORDS + "("
				+ COLUMN_SKANWORD_ID + " INTEGER PRIMARY KEY NOT NULL, "
				+ COLUMN_SET_ID + " INTEGER, " + COLUMN_SKANWORD_HASH
				+ " VARCHAR(32), " + COLUMN_SKANWORD_LEVEL + " INTEGER, "
				+ COLUMN_SKANWORD_DISPLAY_ID + " INTEGER, "
				+ COLUMN_SKANWORD_SETTINGS + " TEXT, "
				+ COLUMN_SKANWORD_QUESTIONS + " TEXT, "
				+ COLUMN_SKANWORD_REPLACES + " TEXT, "
				+ COLUMN_SKANWORD_FILES_DIR + " TEXT, "
				+ COLUMN_SKANWORD_LETTERS_MASK + " TEXT, "
				+ COLUMN_SKANWORD_QUESTIONS_FINISHED + " INTEGER DEFAULT 0, "
				+ COLUMN_SKANWORD_UPDATE_TIME + " BIGINT DEFAULT 0, "
				+ COLUMN_SKANWORD_STARTED + " INTEGER DEFAULT 0, "
				+ COLUMN_SKANWORD_FINISHED + " INTEGER DEFAULT 0, "
				+ COLUMN_SKANWORD_OPENED + " INTEGER DEFAULT 0, "
				+ COLUMN_SKANWORD_ANSWERS_MASK + " TEXT, "
				+ COLUMN_SKANWORD_HINTS_MASK + " TEXT,"
				+ COLUMN_SKANWORD_KEYWORD + " TEXT,"
				+ COLUMN_SKANWORD_COVER + " VARCHAR( 255 ) DEFAULT NULL,"
				+ COLUMN_SKANWORD_QUESTIONS_COUNT + " INTEGER DEFAULT 0 )"

		);
	}

	// list scanwords sets
	public static final String TABLE_SETS_INFO = "scanword_sets_data";
	public static final String COLUMN_SET_ID = "set_id";
	public static final String COLUMN_SET_NAME = "set_name";
	public static final String COLUMN_SET_COVER = "set_cover";
	public static final String COLUMN_SET_SKANWORDS_COUNT = "set_scanwords_count";
	public static final String COLUMN_SET_SKANWORDS_FINISHED_COUNT = "set_scanwords_finished_count";
	public static final String COLUMN_SET_SKANWORDS_STARTED_COUNT = "set_scanwords_started_count";
	public static final String COLUMN_SET_DISPLAY_NAME = "set_display_name";
	public static final String COLUMN_SET_DATA_HASH = "set_data_hash";
	public static final String COLUMN_SET_SAVE_HASH = "set_save_hash";
	public static final String COLUMN_SET_FREE_IDS = "set_free_ids";
	public static final String COLUMN_SET_OFFER_IDS = "set_offer_ids";
	public static final String COLUMN_SET_DOWNLOAD_TIME_LEFT = "set_download_time_left";
	public static final String COLUMN_SET_DOWNLOADED_TIME = "set_downloaded_time";
	public static final String COLUMN_SET_DATA = "set_data";

	private void createSetsTable(SQLiteDatabase db) {
		db.execSQL("CREATE TABLE IF NOT EXISTS " + TABLE_SETS_INFO + "(" +

		COLUMN_SET_ID + " INTEGER PRIMARY KEY NOT NULL, "
				+ COLUMN_SET_NAME + " VARCHAR(15), "
				+ COLUMN_SET_DISPLAY_NAME + " VARCHAR(15), "
				+ COLUMN_SET_COVER + " VARCHAR(255), "
				+ COLUMN_SET_DATA_HASH + " VARCHAR(32), "
				+ COLUMN_SET_SAVE_HASH + " VARCHAR(32), "
				+ COLUMN_SET_SKANWORDS_COUNT + " INTEGER, "
				+ COLUMN_SET_SKANWORDS_STARTED_COUNT + " INTEGER, "
				+ COLUMN_SET_SKANWORDS_FINISHED_COUNT + " INTEGER, "
				+ COLUMN_SET_DOWNLOAD_TIME_LEFT + " INTEGER DEFAULT 0, "
				+ COLUMN_SET_DOWNLOADED_TIME + " BIGINT DEFAULT 0, "
				+ COLUMN_SET_FREE_IDS + " TEXT, "
				+ COLUMN_SET_OFFER_IDS + " TEXT );");
	}

	// general data table
	public static final String TABLE_GENERAL_DATA = "table_general_data";
	public static final String COLUMN_GENERAL_DATA_ID = BaseColumns._ID;
	public static final String COLUMN_GENERAL_DATA = "general_data";

	// crosswords data in set
	public static final String TABLE_SET_DATA = "scanword_set_data";
	public static final String COLUMN_SET_DATA_ID = BaseColumns._ID;
	public static final String COLUMN_SET_IMAGE_FOLDER_NAME = "set_file";
	public static final String COLUMN_SET_DATA_SRC = "set_data";
	

	private static final String DATABASE_ALTER_TEAM_1 = "ALTER TABLE "
		    + TABLE_SETS_INFO + " ADD COLUMN " + COLUMN_SET_COVER + "  VARCHAR( 255 ) DEFAULT NULL;";
	private static final String DATABASE_ALTER_TEAM_2 = "ALTER TABLE "
		    + TABLE_SKANWORDS + " ADD COLUMN " + COLUMN_SKANWORD_COVER + "  VARCHAR( 255 ) DEFAULT NULL ;";
	private static final String DATABASE_ALTER_TEAM_3 = "ALTER TABLE "
		    + TABLE_SKANWORDS + " ADD COLUMN " + COLUMN_SKANWORD_QUESTIONS_COUNT + "  INTEGER DEFAULT 0;";
	private static final String DATABASE_ALTER_TEAM_4 = "ALTER TABLE "
		    + TABLE_SKANWORDS + " ADD COLUMN " + COLUMN_SKANWORD_OPENED + "  INTEGER DEFAULT 0;";
	private static final String DATABASE_ALTER_TEAM_5 = "ALTER TABLE "
		    + TABLE_SKANWORDS + " ADD COLUMN " + COLUMN_SKANWORD_KEYWORD + "  TEXT;";

	private static final Logger LOG = Logger
			.getLogger("org.sknanwords.android");

	public DataBaseManager(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// Log.v("", "Creating SQLite database");

		createCrosswordsTable(db);
		createSetsTable(db);
		createGeneralDataTable(db);

	}

	private void createGeneralDataTable(SQLiteDatabase db) {
		db.execSQL("CREATE TABLE IF NOT EXISTS " + TABLE_GENERAL_DATA + "("
				+ COLUMN_GENERAL_DATA_ID + " BIGINT PRIMARY KEY, "
				+ COLUMN_GENERAL_DATA + " BLOB NOT NULL );");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		LOG.info("Upgrading SQLite database from version " + oldVersion
				+ " to version " + newVersion);
		if (oldVersion < 2) {
	         db.execSQL(DATABASE_ALTER_TEAM_1);
	    }
		if (oldVersion < 3) {
	         db.execSQL(DATABASE_ALTER_TEAM_2);
		}
		if (oldVersion < 4) {
	         db.execSQL(DATABASE_ALTER_TEAM_3);
		}
		if(oldVersion < 5) {
			db.execSQL(DATABASE_ALTER_TEAM_4);
		}
		if(oldVersion < 6) {
			db.execSQL(DATABASE_ALTER_TEAM_5);
		}
	}

	/**
	 * Workaround for http://code.google.com/p/android/issues/detail?id=16391 -
	 * copies the database file to external storage so that it can be downloaded
	 * and debugged.
	 */
	public void debugCopyDatabaseFileToExternalStorage() {
		try {
			String dbPath = getReadableDatabase().getPath();
			FileInputStream fileInputStream = new FileInputStream(dbPath);
			File tempFile = new File(SmappsScanwords.TEMP_DIR, "crosswords.db");
			LOG.info("Copying " + dbPath + " ==> " + tempFile);
			FileOutputStream fileOutputStream = new FileOutputStream(tempFile);

			fileOutputStream.getChannel().transferFrom(fileOutputStream.getChannel(), 0, fileOutputStream.getChannel().size());
			fileInputStream.close();
			fileOutputStream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static class IDAndFilename {
		public IDAndFilename(long id, String filename) {
			this.id = id;
			this.filename = filename;
		}

		public long id;
		public String filename;
	}

	public void clearProgress(List<Integer> setIds) {
		if (setIds.size() < 1) {
			return;
		}
		SQLiteDatabase db = getWritableDatabase();
		try {
			Log.v("", "clearProgress");
			db.execSQL("UPDATE " + TABLE_SKANWORDS + " SET "
					+ COLUMN_SKANWORD_STARTED	+ "=" + 0 + ", "
					+ COLUMN_SKANWORD_FINISHED	+ "=" + 0 + ", "
					+ COLUMN_SKANWORD_ANSWERS_MASK	+ "=" + "''" + ", "
					+ COLUMN_SKANWORD_HINTS_MASK	+ "=" + "''" + ", "
					+ COLUMN_SKANWORD_QUESTIONS_FINISHED	+ "=" + 0 + ", "
					+ COLUMN_SKANWORD_QUESTIONS_COUNT	+ "=" + 0
					+ " WHERE " + COLUMN_SET_ID + " IN " + "(" + Utils.join(setIds, ",") + ")"
						);
		} catch (Exception e) {
			Log.v("", "clearProgress exeption");
			e.printStackTrace();
		}
	}

	public void removeSkanwordSetInfoAndDataWithId(Integer id) {
		SQLiteDatabase db = getWritableDatabase();
		db.execSQL("DELETE FROM " + TABLE_SETS_INFO + " WHERE " + COLUMN_SET_ID
				+ "=" + id);
		db.execSQL("DELETE FROM " + TABLE_SKANWORDS + " WHERE " + COLUMN_SET_ID
				+ "=" + id);
	}

	public void updateSkanwordProgress(Skanword skanword, boolean checkTime) {
		synchronized (this.skanwordsProgressMutex) {
			try {
				ContentValues values = new ContentValues();
				values.put(COLUMN_SKANWORD_ID, skanword.getId());
				values.put(COLUMN_SKANWORD_ANSWERS_MASK, skanword.getAnswers());
				values.put(COLUMN_SKANWORD_HINTS_MASK, skanword.getHints());
				values.put(COLUMN_SKANWORD_STARTED, skanword.isStarted());
				values.put(COLUMN_SKANWORD_FINISHED, skanword.isFinished());
				values.put(COLUMN_SKANWORD_OPENED, skanword.isOpened());
				values.put(COLUMN_SKANWORD_KEYWORD,
						new Gson().toJson(skanword.getKeyword()));
				values.put(COLUMN_SKANWORD_QUESTIONS_FINISHED,
						skanword.getFinishedQuestionsCount());
				if (checkTime) {
					values.put(COLUMN_SKANWORD_UPDATE_TIME,
							new Date().getTime() / 1000);
				}
				Log.v("", "updateSkanwordProgress  " + skanword.isStarted());
				SQLiteDatabase db = getWritableDatabase();

				db.update(TABLE_SKANWORDS, values, COLUMN_SKANWORD_ID + "="
						+ skanword.getId(), null);
				List<Integer> setInfoId = new ArrayList<Integer>();
				setInfoId.add(skanword.getSetId());
				updateSetInfosProgress(setInfoId);
			} catch (Exception e) {
				Log.v("", "updateSkanwordProgress exeption");
				e.printStackTrace();
			}
		}
	}

	public void insertSkanwordProgress(List<List<String>> scanProgresses,
			boolean finished) {
		try {
			SQLiteDatabase db = getWritableDatabase();
			for (List<String> scanProgress : scanProgresses) {
				ContentValues values = new ContentValues();

				values.put(COLUMN_SET_ID, Integer.valueOf(scanProgress.get(0)));
				values.put(COLUMN_SKANWORD_ID,
						Integer.valueOf(scanProgress.get(1)));

				values.put(COLUMN_SKANWORD_ANSWERS_MASK, scanProgress.get(2));
				values.put(COLUMN_SKANWORD_HINTS_MASK, scanProgress.get(3));
				if (!finished && scanProgress.size() > 4)
					values.put(COLUMN_SKANWORD_QUESTIONS_FINISHED,
							scanProgress.get(4));
				values.put(COLUMN_SKANWORD_STARTED, !finished);
				values.put(COLUMN_SKANWORD_FINISHED, finished);


				db.update(TABLE_SKANWORDS, values, COLUMN_SKANWORD_ID + "="
						+ scanProgress.get(1), null);
				Log.v("SkanwordsFunc", scanProgress + "   progress");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public void updateSetInfosProgress(List<Integer> setIds) {
		for (Integer setId : setIds) {
			List<Skanword> skanwords = getFullSkanwordDataWithSelection(COLUMN_SET_ID + "=" + setId);
			Integer finishedCount = 0, startedCount = 0;
			for (Skanword skanword : skanwords) {
				if (skanword.isFinished())
					finishedCount ++;
				else if (skanword.isStarted())
					startedCount ++;
			}
			try {
				String hash = SkanwordsDataManager.getSkanwordSyncHash(skanwords);
				SQLiteDatabase db = getWritableDatabase();
				String query = "UPDATE " + TABLE_SETS_INFO + " SET "
						+ COLUMN_SET_SKANWORDS_STARTED_COUNT	+ "=" + startedCount + ", " 
						+ COLUMN_SET_SKANWORDS_FINISHED_COUNT	+ "=" + finishedCount + ", "
						+ COLUMN_SET_SAVE_HASH	+ "=" + "'" + hash + "'"
						+ " WHERE " + COLUMN_SET_ID + "=" + setId;
				db.execSQL(query);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public void importSkanwordData(List<Skanword> skanwords) {
		synchronized (skanwordsProgressMutex) {
			try {
				SQLiteDatabase db = getWritableDatabase();
				for (Skanword skanword : skanwords) {
					ContentValues values = new ContentValues();

					values.put(COLUMN_SET_ID, skanword.getSetId());
					values.put(COLUMN_SKANWORD_ID, skanword.getId());
					values.put(COLUMN_SKANWORD_HASH, skanword.getHash());
					values.put(COLUMN_SKANWORD_LEVEL, skanword.getLevel());
					values.put(COLUMN_SKANWORD_DISPLAY_ID, skanword.getViewId());
					values.put(COLUMN_SKANWORD_FILES_DIR, skanword.getImageDir());
					values.put(COLUMN_SKANWORD_COVER, skanword.getCover());
					values.put(COLUMN_SKANWORD_QUESTIONS_COUNT, skanword.getQuestionsCount());
					values.put(COLUMN_SKANWORD_QUESTIONS,
							new Gson().toJson(skanword.getQuestions()));
					values.put(COLUMN_SKANWORD_REPLACES,
							new Gson().toJson(skanword.getReplaces()));
					values.put(COLUMN_SKANWORD_SETTINGS,
							new Gson().toJson(skanword.getSettings()));
					values.put(COLUMN_SKANWORD_LETTERS_MASK,
							new Gson().toJson(skanword.getMask()));
					values.put(COLUMN_SKANWORD_KEYWORD,
							new Gson().toJson(skanword.getKeyword()));


					db.insertWithOnConflict(TABLE_SKANWORDS, null,
							values, SQLiteDatabase.CONFLICT_IGNORE);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public Skanword getLastPlayedSkanword() {
		List<Skanword> skanwords = getSkanwordsWithSelection(COLUMN_SKANWORD_UPDATE_TIME + ">0 AND "
						+ COLUMN_SKANWORD_FINISHED + " =0 AND "
						+ COLUMN_SKANWORD_STARTED + " =1", COLUMN_SKANWORD_UPDATE_TIME + " DESC", "1");
		
		return skanwords.size() > 0 ? skanwords.get(0) : null;
	}

	public List<Skanword> getSkanwordsWithSelection(String selection) {
		return getSkanwordsWithSelection(selection, null, null);
	}
	public List<Skanword> getSkanwordsWithSelection(String selection, String order, String limit) {
		synchronized (skanwordsProgressMutex) {
			Log.v("SkanwordsSpeed", "getSkanwordsWithSelection start");
			List<Skanword> result = new ArrayList<Skanword>();
			List<Skanword> toUpdate = new ArrayList<Skanword>();
			String[] columns = new String[] {
					COLUMN_SKANWORD_ID,
					COLUMN_SET_ID,
					COLUMN_SKANWORD_HASH,
					COLUMN_SKANWORD_LEVEL,
					COLUMN_SKANWORD_DISPLAY_ID,
					COLUMN_SKANWORD_STARTED,
					COLUMN_SKANWORD_FINISHED,
					COLUMN_SKANWORD_QUESTIONS_FINISHED,
					COLUMN_SKANWORD_UPDATE_TIME,
					COLUMN_SKANWORD_FILES_DIR,
					COLUMN_SKANWORD_COVER,
					COLUMN_SKANWORD_QUESTIONS_COUNT,
					COLUMN_SKANWORD_OPENED,
					COLUMN_SKANWORD_KEYWORD
					};
			SQLiteDatabase db = getReadableDatabase();
			createSetsTable(db);
			Cursor cursor = db.query(TABLE_SKANWORDS, columns, // Columns
					selection, // Selection
					null, // Selection args
					null, // Group by
					null, // Having
					order, // Order by
					limit); // Limit

			while (cursor.moveToNext()) {
				try {
					Skanword skanword;
					if (cursor.getInt(11) > 0) {
						
						Keyword keyword = new Gson().fromJson(
								cursor.getString(13), Keyword.class);
						
						skanword = new Skanword(cursor.getInt(0),
								cursor.getInt(3), cursor.getInt(4), null,
								null, null, null, cursor.getString(2), keyword, null);
						skanword.setSetId(cursor.getInt(1));
						skanword.setStarted(cursor.getInt(5) != 0);
						skanword.setFinished(cursor.getInt(6) != 0);
						skanword.setFinishedQuestionsCount(cursor.getInt(7));
						skanword.setUpdateTime(cursor.getLong(8));
						skanword.setImageDir(cursor.getString(9));
						skanword.setCover(cursor.getString(10));
						skanword.setQuestionsCount(cursor.getInt(11));
						skanword.setOpened(cursor.getInt(12) != 0);
					} else {
						Log.v("SkanwordsFunc", "getFullskanwordData");
						skanword = getFullskanwordData(cursor.getInt(0));
						toUpdate.add(skanword);
					}
					result.add(skanword);
				} catch (Exception e) {
					 e.printStackTrace();
					cursor.close();
				}
			}
			cursor.close();
			if (toUpdate.size() > 0) {
				try {
					for (Skanword skanword : toUpdate) {
						ContentValues values = new ContentValues();
						values.put(COLUMN_SKANWORD_COVER, skanword.getCover());
						values.put(COLUMN_SKANWORD_QUESTIONS_COUNT, skanword.getQuestionsCount());
						values.put(COLUMN_SKANWORD_KEYWORD, new Gson().toJson(skanword.getKeyword()));
						db.update(TABLE_SKANWORDS, values, COLUMN_SKANWORD_ID + "="
								+ skanword.getId(), null);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			Log.v("SkanwordsSpeed", "getSkanwordsWithSelection end "+ result.size());
			return result;
		}

	}

	public List<Skanword> getFullSkanwordDataWithSelection(String selection) {
		synchronized (skanwordsProgressMutex) {
			List<Skanword> result = new ArrayList<Skanword>();
			String[] columns = new String[] {
					COLUMN_SKANWORD_ID,
					COLUMN_SET_ID,
					COLUMN_SKANWORD_HASH,
					COLUMN_SKANWORD_LEVEL,
					COLUMN_SKANWORD_DISPLAY_ID,
					COLUMN_SKANWORD_QUESTIONS,
					COLUMN_SKANWORD_REPLACES,
					COLUMN_SKANWORD_SETTINGS,
					COLUMN_SKANWORD_LETTERS_MASK,
					COLUMN_SKANWORD_ANSWERS_MASK,
					COLUMN_SKANWORD_HINTS_MASK,
					COLUMN_SKANWORD_STARTED,
					COLUMN_SKANWORD_FINISHED,
					COLUMN_SKANWORD_QUESTIONS_FINISHED,
					COLUMN_SKANWORD_UPDATE_TIME,
					COLUMN_SKANWORD_FILES_DIR,
					COLUMN_SKANWORD_COVER,
					COLUMN_SKANWORD_OPENED,
					COLUMN_SKANWORD_KEYWORD
					};
			SQLiteDatabase db = getReadableDatabase();
			createSetsTable(db);
			Cursor cursor = db.query(TABLE_SKANWORDS, columns, // Columns
					selection, // Selection
					null, // Selection args
					null, // Group by
					null, // Having
					null, // Order by
					null); // Limit

			while (cursor.moveToNext()) {
				try {
					List<Skanword.Question> questions = new Gson().fromJson(
							cursor.getString(5),
							new TypeToken<List<Skanword.Question>>() {
							}.getType());
					Skanword.Settings settings = new Gson().fromJson(
							cursor.getString(7), Skanword.Settings.class);
					List<Skanword.Replace> replaces = new Gson().fromJson(
							cursor.getString(6),
							new TypeToken<List<Skanword.Replace>>() {
							}.getType());
					String[][] mask = new Gson().fromJson(cursor.getString(8),
							new TypeToken<String[][]>() {
							}.getType());

					Keyword keyword = new Gson().fromJson(
							cursor.getString(18), Keyword.class);
					Skanword skanword = new Skanword(cursor.getInt(0),
							cursor.getInt(3), cursor.getInt(4), settings,
							questions, replaces, mask, cursor.getString(2), keyword, null);
					skanword.setSetId(cursor.getInt(1));
					skanword.setAnswers(cursor.getString(9));
					skanword.setHints(cursor.getString(10));
					skanword.setStarted(cursor.getInt(11) != 0);
					skanword.setFinished(cursor.getInt(12) != 0);
					skanword.setFinishedQuestionsCount(cursor.getInt(13));
					skanword.setUpdateTime(cursor.getLong(14));
					skanword.setImageDir(cursor.getString(15));
					skanword.setCover(cursor.getString(16));
					skanword.setOpened(cursor.getInt(17) != 0);
					
					skanword.clearKeyword();
					skanword.checkKeyword();
					
					result.add(skanword);
				} catch (Exception e) {
					 e.printStackTrace();
					cursor.close();
				}
			}
			cursor.close();
			
			Log.v("SkanwordsFunc", " got some skanwords  " + result.size());
			return result;
		}
	}
	public Skanword getFullskanwordData(Integer skanwordId) {
		List<Skanword> skanwords = getFullSkanwordDataWithSelection(COLUMN_SKANWORD_ID + "=" + skanwordId);
		return skanwords.size() > 0 ? skanwords.get(0) : null;
	}
	
	/*
	 * Skanwords Set infos data
	 * 
	 * */
	
	
	public long addSkanwordsSetInfo(SkanwordsSetInfo scanwordsSetInfo) {
		long id = -1;
		// LOG.info("addSkanwordsSetInfo " + scanwordsSetInfo.getId());

		try {
			ContentValues values = new ContentValues();

			values.put(COLUMN_SET_ID, scanwordsSetInfo.getId());
			values.put(COLUMN_SET_NAME, scanwordsSetInfo.getName());
			values.put(COLUMN_SET_DATA_HASH, scanwordsSetInfo.getDataHash());
			values.put(COLUMN_SET_SAVE_HASH, scanwordsSetInfo.getSaveHash());
			values.put(COLUMN_SET_DISPLAY_NAME,
					scanwordsSetInfo.getDisplayName());
			values.put(COLUMN_SET_SKANWORDS_COUNT,
					scanwordsSetInfo.getSkanwordsCount());
			values.put(COLUMN_SET_DOWNLOADED_TIME,
					scanwordsSetInfo.getDownloadedTime());
			values.put(COLUMN_SET_DOWNLOAD_TIME_LEFT,
					scanwordsSetInfo.getDownloadTimeLeft());
			values.put(COLUMN_SET_OFFER_IDS,
					new Gson().toJson(scanwordsSetInfo.getOfferIds()));
			values.put(COLUMN_SET_FREE_IDS,
					new Gson().toJson(scanwordsSetInfo.getFreeIds()));

			values.put(COLUMN_SET_SKANWORDS_FINISHED_COUNT, scanwordsSetInfo.getSkanwordsFinishedCount());
			values.put(COLUMN_SET_SKANWORDS_STARTED_COUNT, scanwordsSetInfo.getSkanwordsStartedCount());
			
			
			SQLiteDatabase db = getWritableDatabase();
			id = db.insertWithOnConflict(TABLE_SETS_INFO, null, values,
					SQLiteDatabase.CONFLICT_REPLACE);
			if (id == -1) {
				LOG.warning("Failed to insert puzzle into database: ");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return id;
	}

	public List<SkanwordsSetInfo> getSkanwordsSetsInfo() {
		List<SkanwordsSetInfo> result = new ArrayList<SkanwordsSetInfo>();

		String[] columns = new String[] { 
				COLUMN_SET_ID,
				COLUMN_SET_NAME,
				COLUMN_SET_DISPLAY_NAME, 
				COLUMN_SET_DATA_HASH,
				COLUMN_SET_SKANWORDS_COUNT, 
				COLUMN_SET_SKANWORDS_FINISHED_COUNT,
				COLUMN_SET_SKANWORDS_STARTED_COUNT, 
				COLUMN_SET_DOWNLOADED_TIME,
				COLUMN_SET_DOWNLOAD_TIME_LEFT, 
				COLUMN_SET_OFFER_IDS,
				COLUMN_SET_FREE_IDS, 
				COLUMN_SET_SAVE_HASH,
				COLUMN_SET_COVER
				};
		SQLiteDatabase db = getReadableDatabase();
		createSetsTable(db);
		Cursor cursor = db.query(TABLE_SETS_INFO, columns, // Columns
				null, // Selection
				null, // Selection args
				null, // Group by
				null, // Having
				null); // Order by

		SkanwordsSetInfo scanwordsSetInfo = null;
		while (cursor.moveToNext()) {
			try {
				List<Integer> freeIds = new Gson().fromJson(
						cursor.getString(10), new TypeToken<List<Integer>>() {
						}.getType());
				List<Integer> offerIds = new Gson().fromJson(
						cursor.getString(9), new TypeToken<List<Integer>>() {
						}.getType());
				scanwordsSetInfo = new SkanwordsSetInfo(
						cursor.getInt(0),
						cursor.getString(1),
						cursor.getString(2),
						cursor.getInt(4),
						cursor.getString(3),
						cursor.getString(11),
						cursor.getInt(8),
						freeIds,
						offerIds,
						cursor.getLong(7),
						cursor.getInt(6),
						cursor.getInt(5),
						cursor.getString(12)
						);
				// scanwordsSetInfo =
				// SarializableManager.fromBlob(cursor.getBlob(1),
				// SkanwordsSetInfo.class) ;
				if (scanwordsSetInfo != null) {
					scanwordsSetInfo.setLocalData(true);
					result.add(scanwordsSetInfo);
				}
			} catch (Exception e) {
				e.printStackTrace();
				cursor.close();
			}
		}
		cursor.close();

		return result;

	}

	public void updateSetInfoProgress(SkanwordsSetInfo scanwordsSetInfo) {
		try {
			ContentValues values = new ContentValues();
			values.put(COLUMN_SET_SKANWORDS_FINISHED_COUNT,
					new Gson().toJson(scanwordsSetInfo.getSkanwordsFinishedCount()));
			values.put(COLUMN_SET_SKANWORDS_STARTED_COUNT,
					new Gson().toJson(scanwordsSetInfo.getSkanwordsStartedCount()));

			Log.v("",
					"updateSetProgress set info  started count - "
							+ scanwordsSetInfo.getSkanwordsStartedCount()
							+ "  finished count - "
							+ scanwordsSetInfo.getSkanwordsFinishedCount());
			SQLiteDatabase db = getWritableDatabase();
			db.update(TABLE_SETS_INFO, values, COLUMN_SET_ID + "="
					+ scanwordsSetInfo.getId(), null);
		} catch (Exception e) {
			Log.v("", "failed to update set info");
		}
	}

	public void updateSetInfoDynamicData(SkanwordsSetInfo skanwordsSetInfo) {
		try {
			ContentValues values = new ContentValues();
			values.put(COLUMN_SET_DISPLAY_NAME,
					skanwordsSetInfo.getDisplayName());
			values.put(COLUMN_SET_OFFER_IDS,
					new Gson().toJson(skanwordsSetInfo.getOfferIds()));
			values.put(COLUMN_SET_FREE_IDS,
					new Gson().toJson(skanwordsSetInfo.getFreeIds()));
			values.put(COLUMN_SET_SKANWORDS_STARTED_COUNT, skanwordsSetInfo.getSkanwordsStartedCount());
			values.put(COLUMN_SET_SKANWORDS_FINISHED_COUNT, skanwordsSetInfo.getSkanwordsStartedCount());
			Log.v("",
					"updateSetProgress set info  started count - "
							+ skanwordsSetInfo.getSkanwordsStartedCount()
							+ "  finished count - "
							+ skanwordsSetInfo.getSkanwordsFinishedCount());
			SQLiteDatabase db = getWritableDatabase();
			db.update(TABLE_SETS_INFO, values, COLUMN_SET_ID + "="
					+ skanwordsSetInfo.getId(), null);
		} catch (Exception e) {
			Log.v("", "failed to update set info");
		}
	}
	/*
	 * End Skanwords set info data
	 * 
	 * */
	public void storeShopData(Shop shop) {
		storeGeneralData(shop, SHOP_DATA_ID);
	}

	public void storeGeneralData(Serializable obj, int dataId) {
		long id = -1;
		byte[] dataByte;
		// Log.v("", "storeGeneralData  " + dataId);
		try {
			dataByte = SarializableManager.toBlobe(obj);
			ContentValues values = new ContentValues();
			values.put(COLUMN_GENERAL_DATA_ID, dataId);
			values.put(COLUMN_GENERAL_DATA, dataByte);

			SQLiteDatabase db = getWritableDatabase();
			id = db.insertWithOnConflict(TABLE_GENERAL_DATA, null, values,
					SQLiteDatabase.CONFLICT_REPLACE);
			// id = db.insert(TABLE_SETS_INFO, null, values);
			if (id == -1) {
				LOG.warning("Failed to insert puzzle into database: ");
			}
		} catch (IOException e) {
			// Log.v("",e + " error storeGeneralData  " + dataId);
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return;
	}

	public void storeUserUpdateData(UserUpdateData data) {
		storeGeneralData(data, UPDATE_LAST_DATA_ID);
	}

	public void storeUserUpdateData(HashMap<String, UserUpdateData> data) {
		storeGeneralData(data, UPDATES_DATA_ID);
	}

	public <T> T retrieveGeneralData(int dataID) {
		T result = null;

		String[] columns = new String[] { COLUMN_GENERAL_DATA, };

		String selection = COLUMN_GENERAL_DATA_ID + "=" + dataID;
		SQLiteDatabase db = getReadableDatabase();
		createSetsTable(db);
		Cursor cursor = db.query(TABLE_GENERAL_DATA, columns, // Columns
				selection, // Selection
				null, // Selection args
				null, // Group by
				null, // Having
				null); // Order by

		if (cursor.moveToNext()) {
			try {
				result = SarializableManager.fromBlob(cursor.getBlob(0), null);
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
				cursor.close();
			} catch (IOException e) {
				e.printStackTrace();
				cursor.close();
			}
		}
		cursor.close();

		return result;
	}

	public void dropDatabase() {
		SQLiteDatabase db = getReadableDatabase();
		SQLiteDatabase.deleteDatabase(new File(db.getPath()));
	}

}