
package org.skanword.and.datamanager;
 
import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.skanword.and.SmappsScanwords;
import org.skanword.and.datamanager.Skanword.Question;
import org.skanword.and.datamanager.Skanword.Replace;
import org.skanword.and.etc.Utils;
import org.skanword.and.menu.MainMenuActivity;
import org.skanword.and.network.DownloaderArhiveFile;
import org.skanword.and.network.MainNetworkManager;
import org.skanword.and.network.SetsListObject;
import org.skanword.and.network.SkanwordsSetDataObject;

import android.annotation.SuppressLint;
import android.util.Log;

import com.musselwhizzle.dispatcher.events.Event;
import com.musselwhizzle.dispatcher.events.EventListener;
import com.musselwhizzle.dispatcher.events.SimpleEvent;
 
public class SkanwordsDataManager {
 
    public final static String UPDATE_ISSUES_LIST = "UPDATE_ISSUES_LIST";
     
    private List<SkanwordsSetInfo> mSetsInfos;
    private Skanword mLastPlayedSkanword;
    private SetsListObject mLastReceivedObject;
    private SkanwordsSetInfo mTomorrowSet = null;
     
    private List<Integer> mFreeSkanwords;
    private List<Integer> mOfferSkanwords;
     
    private boolean mTomorrowSetTimeUp = false;
    private EventListener mTimerEventListener;
    
    private Integer mSetInfosMutex = 1;
    private Integer mLastRecievedListMutex = 1;
 
    public SkanwordsDataManager() {
 
        mSetsInfos = Collections
                .synchronizedList(new ArrayList<SkanwordsSetInfo>());
        mTimerEventListener = new EventListener() {
            @Override
            public void onEvent(Event event) {
                appTimerTicked();
            }
        };
        SmappsScanwords.getEventsDispatcher().addListener(SmappsScanwords.APPLICATION_TIMER_EVENT, mTimerEventListener);
         
    }
     
    protected void appTimerTicked() {
        long timeLeft = timeTillTomorrowIssue();
        if (timeLeft < 0 && !mTomorrowSetTimeUp) {
            SmappsScanwords.getEventsDispatcher().dispatchEvent(new SimpleEvent(UPDATE_ISSUES_LIST));
            mTomorrowSetTimeUp = true;
        }
    }
 
    public void retrieveData() {
        getSkanwordsSetsFromDB();
         
        SmappsScanwords.getEventsDispatcher().dispatchEvent(new SkanwordsSetsUpdateEvent(MainDataManager.EVENT_SKANWORDS_SETS_INFOS_UPDATE, TaskListType.ALL_TASKS));
    }
    private void getSkanwordsSetsFromDB() {
        Log.v("SkanwordsFunc", "getSkanwordsSetsFromDB");
        synchronized (mSetInfosMutex) {
            mSetsInfos = Collections.synchronizedList(SmappsScanwords
                    .getDatabaseHelper().getSkanwordsSetsInfo());
            Collections.sort(mSetsInfos, new Comparator<SkanwordsSetInfo>() {
                public int compare(SkanwordsSetInfo lhs, SkanwordsSetInfo rhs) {
                    return (int) (rhs.getId() - lhs.getId());
                }
            });
		}
 
         
        updateFreeAndOfferSkanwords();
 
        mLastPlayedSkanword = SmappsScanwords
                .getDatabaseHelper().getLastPlayedSkanword();
    }
    private void updateFreeAndOfferSkanwords() {
        mFreeSkanwords = new ArrayList<Integer>();
        mOfferSkanwords = new ArrayList<Integer>();
        synchronized (mSetInfosMutex) {
        	for (SkanwordsSetInfo skanwordsSetInfo : mSetsInfos) {
                if (skanwordsSetInfo.getFreeIds().size() > 0) {
                    mFreeSkanwords.addAll(skanwordsSetInfo.getFreeIds());
                }
                if (skanwordsSetInfo.getOfferIds().size() > 0) {
                    mOfferSkanwords.addAll(skanwordsSetInfo.getOfferIds());
                }
            }
		}
    }
     
    public boolean isSkanwordFree(Integer id) {
        if (mFreeSkanwords != null && mFreeSkanwords.contains(id))
            return true;
        return false;
    }
    public boolean isSkanwordOffered(Integer id) {
        if (mOfferSkanwords != null && mOfferSkanwords.contains(id))
            return true;
        return false;
    }

    public void deleteIssuesWithId(final int id) {
    	deleteIssuesWithId(id, true);
    }
    public void deleteIssuesWithId(final int id, boolean reload) {
        if (id < 1)
            return;
        String fileName = DownloaderArhiveFile.getUnzipPath(getSkanwordSetInfoWithId(id).getMediaUrl());
        SmappsScanwords.deleteRecursive(new File(fileName), false);
        SmappsScanwords.getDatabaseHelper().removeSkanwordSetInfoAndDataWithId(id);
        getSkanwordsSetsFromDB();
    }
    public int getLatesIssueIdOfType(TaskListType listType) {
        if (getLastReceivedListObject() == null || getLastReceivedListObject().getListType() != listType)
            return 0;
        return getLastReceivedListObject().getLatestSetId();
    }
 
    public int getOldestIssueIdOfType(TaskListType listType) {
        if (getLastReceivedListObject() == null || getLastReceivedListObject().getListType() != listType)
            return 0;
        return getLastReceivedListObject().getOldestSetId();
    }
    public void openScanwordForOffer(Integer id) {
        synchronized (mSetInfosMutex) {
        	for (SkanwordsSetInfo skanwordsSetInfo : mSetsInfos) {
                if (skanwordsSetInfo.getOfferIds().contains(id)) {
                    skanwordsSetInfo.getFreeIds().add(id);
                    updateFreeAndOfferSkanwords();
                    SmappsScanwords.getDatabaseHelper().updateSetInfoDynamicData(skanwordsSetInfo);
                    break;
                }
            }
		}
        SmappsScanwords.getEventsDispatcher().dispatchEvent(
                new SimpleEvent(MainDataManager.EVENT_SKANWORDS_TASKS_UPDATE));
    }
 
    public void updateTomorrowSetInfo(SkanwordsSetInfo setInfo, boolean update) {
        if (setInfo == null) {
            if (update)
                mTomorrowSet = null;
            return;
        }
        mTomorrowSetTimeUp = false;
        mTomorrowSet = setInfo;
        setInfo.setDownloadedTime(new Date().getTime());
    }
 
    public String setMediaFileUrlForSetInfo(String url, Integer id) {
        return id + ".zip";
    }
    public void notifyLocalDataUsegae(TaskListType listType, TaskListDirrectionType dirrectionType) {
        Log.v("", "notifyLocalDataUsegae "+ listType);
        SmappsScanwords.getEventsDispatcher().dispatchEvent(new SkanwordsSetsUpdateEvent(MainDataManager.EVENT_SKANWORDS_SETS_INFOS_UPDATE, listType, dirrectionType != TaskListDirrectionType.CURRECT_DIRRECTION));
    }
    synchronized public void setsListReceived(SetsListObject setsList) {
        if (setsList == null)
            return;
        synchronized (mLastRecievedListMutex) {
            mLastReceivedObject = setsList;
            Log.v("SkanwordsFunc", "setsListReceived " + setsList.getListType());
            updateSkanwordsList(setsList.getSetsList());
            updateTomorrowSetInfo(getLastReceivedListObject().getTomorrowSet(), true);
            SmappsScanwords.getEventsDispatcher().dispatchEvent(new SkanwordsSetsUpdateEvent(MainDataManager.EVENT_SKANWORDS_SETS_INFOS_UPDATE, setsList.getListType(), setsList.getDirrectionType() != TaskListDirrectionType.CURRECT_DIRRECTION));
		}
    }
    @SuppressLint("UseSparseArrays")
    public void updateSkanwordsList(List<SkanwordsSetInfo> newSetsList) {
        Log.v("SkanwordsFunc", "updateSkanwordsList");
        if (newSetsList == null)
            return;
        synchronized (mSetInfosMutex) {
        	Map<Integer, String> idSet = new HashMap<Integer, String>();
            
            if (mSetsInfos != null && mSetsInfos.size() > 0) {
                for (SkanwordsSetInfo localSkanwordSetInfo : mSetsInfos) {
                    idSet.put(localSkanwordSetInfo.getId(),
                            localSkanwordSetInfo.getDataHash());
                }
            }
     
            List<Map<String, Integer>> requestParam = new ArrayList<Map<String, Integer>>();
            for (SkanwordsSetInfo skanwordsSetInfo : newSetsList) {
                String prevDataHash = idSet.remove(skanwordsSetInfo.getId());
                boolean newData = prevDataHash == null
                        || !prevDataHash.equals(skanwordsSetInfo.getDataHash());
                 
                if (newData) { //  Data changed 
                	if (prevDataHash != null)
                		deleteIssuesWithId(skanwordsSetInfo.getId(), false);
                     
                } else {
                     
                    SkanwordsSetInfo prevInfo = getSkanwordSetInfoWithId(skanwordsSetInfo
                            .getId());
                    prevInfo.setToday(skanwordsSetInfo.isToday());
                    boolean update = false;
                    if (!prevInfo.getDisplayName().equals(
                            skanwordsSetInfo.getDisplayName())) {
                        prevInfo.setDisplayName(skanwordsSetInfo.getDisplayName());
                        update = true;
                    }
                    if (!Utils.<Integer> compareLists(prevInfo.getFreeIds(),
                            skanwordsSetInfo.getFreeIds())) {
                        prevInfo.setFreeIds(skanwordsSetInfo.getFreeIds());
                        update = true;
                    }
                    if (!Utils.<Integer> compareLists(prevInfo.getOfferIds(),
                            skanwordsSetInfo.getOfferIds())) {
                        prevInfo.setOfferIds(skanwordsSetInfo.getOfferIds());
                        update = true;
                    }
                    Log.v("",skanwordsSetInfo.getId() + "  started count  "+ skanwordsSetInfo.getSkanwordsStartedCount() + "  finished count - " + skanwordsSetInfo.getSkanwordsFinishedCount());
     
                    if (prevInfo.getSkanwordsStartedCount() != skanwordsSetInfo.getSkanwordsStartedCount() || prevInfo.getSkanwordsFinishedCount() != skanwordsSetInfo.getSkanwordsFinishedCount()) {
                        prevInfo.setStartedCount(skanwordsSetInfo.getSkanwordsStartedCount());
                        prevInfo.setFinishedCount(skanwordsSetInfo.getSkanwordsFinishedCount());
                    }
                    if (!prevInfo.getSaveHash().equals(skanwordsSetInfo.getSaveHash())) {
                        Log.v("SkanwordsFunc", prevInfo.getId() + " - setId not same save hash  "+ prevInfo.getSaveHash() + "  new save hash - " + skanwordsSetInfo.getSaveHash());
                        Map<String, Integer> data = new HashMap<String, Integer>();
                        data.put("set_id", skanwordsSetInfo.getId());
                        data.put("return", MainNetworkManager.SKAN_DATA_PROGRESS);
                        requestParam.add(data);
                    }
                    if (update) {
                        SmappsScanwords.getDatabaseHelper().updateSetInfoDynamicData(prevInfo);
                    }
                }
            }
             
            updateFreeAndOfferSkanwords();
            Collections.sort(mSetsInfos, new Comparator<SkanwordsSetInfo>() {
                public int compare(SkanwordsSetInfo lhs, SkanwordsSetInfo rhs) {
                    return (int) (rhs.getId() - lhs.getId());
                }
     
            });
            if (requestParam.size() > 0)
                storeSkanwordsSet(MainNetworkManager.getInstance().skanwordSetGet(MainMenuActivity.instance, requestParam));
		}
    }
    public int getTomorrowSetId() {
        if (getLastReceivedListObject() == null || getLastReceivedListObject().getTomorrowSet() == null)
            return 0;
        return getLastReceivedListObject().getTomorrowSet().getId();
    }
    public boolean isPageAvailable(boolean next) {
        if (getLastReceivedListObject() == null || !MainNetworkManager.getInstance().hasNetworkConnection())
            return false;
        return next ? getLastReceivedListObject().isPageNextAvailable() : getLastReceivedListObject().isPagePreviousAvailable();
    }
    private SetsListObject getLastReceivedListObject() { 
    	synchronized (mLastRecievedListMutex) {
        	return mLastReceivedObject;
		}
    }
    public void importProgress(final SkanwordsSetDataObject object) {
        List<Integer> setIsdToClear = new ArrayList<Integer>();
        for (Map<String, Integer> requestSet : object.getRequestdSetIds()) {
            int retVal = requestSet.get("return");
            if ((retVal & MainNetworkManager.SKAN_DATA_PROGRESS) != 0) {
                setIsdToClear.add(requestSet.get("set_id"));
            }
        }
        if (setIsdToClear.size() < 1)
            return;
        if (object.getSaveSkanwords().size() + object.getSolvedSkanwords().size() > 0) {
        	
        }
        SmappsScanwords.getDatabaseHelper().clearProgress(setIsdToClear);
        SmappsScanwords.getDatabaseHelper().insertSkanwordProgress(object.getSaveSkanwords(), false);
        SmappsScanwords.getDatabaseHelper().insertSkanwordProgress(object.getSolvedSkanwords(), true);
        SmappsScanwords.getDatabaseHelper().updateSetInfosProgress(setIsdToClear);
    }
 
 
 
    public Skanword getLastPlayedSkanword() {
        return mLastPlayedSkanword;
    }
 
    public boolean readyToRateTheApp () {
        int count = 0;
        synchronized (mSetInfosMutex) {
        	for (SkanwordsSetInfo skanwordsSetInfo : mSetsInfos) {
                if (skanwordsSetInfo.isFinished() || skanwordsSetInfo.isStarted()) {
                    count ++;
                }
                if (count >= 2) {
                    return true;
                }
            }
            return false;
		}
    }
 
    private void replaceSkanwordSetInfo(SkanwordsSetInfo info) {
        synchronized (mSetInfosMutex) {
            SkanwordsSetInfo prevInfo = getSkanwordSetInfoWithId(info.getId());
            if (prevInfo != null) {
                mSetsInfos.set(mSetsInfos.indexOf(prevInfo), info);
                SmappsScanwords.getDatabaseHelper().addSkanwordsSetInfo(info);
            }
        }
    }
    private SkanwordsSetInfo getSkanwordSetInfoWithId(Integer id) {
        synchronized (mSetInfosMutex) {
        	for (SkanwordsSetInfo localSkanwordSetInfo : mSetsInfos) {
                if (localSkanwordSetInfo.getId().equals(id)) {
                    return localSkanwordSetInfo;
                }
            }
            return null;
		}
    }
 
    public List<Skanword> getSkanwordsForListType(TaskListType type) {
        if (type == TaskListType.STARTED_TASKS) {
            return SmappsScanwords.getDatabaseHelper().getSkanwordsWithSelection(DataBaseManager.COLUMN_SKANWORD_STARTED + "=1 AND " + DataBaseManager.COLUMN_SKANWORD_FINISHED + "=0 AND " + DataBaseManager.COLUMN_SKANWORD_QUESTIONS + "!=0 ORDER BY " + DataBaseManager.COLUMN_SKANWORD_ID + " DESC  LIMIT " + getMaxSkanwordsOnPageCount());
        } else if (type == TaskListType.FINISHED_TASKS) {
            return SmappsScanwords.getDatabaseHelper().getSkanwordsWithSelection(DataBaseManager.COLUMN_SKANWORD_FINISHED + "=1 AND " + DataBaseManager.COLUMN_SKANWORD_QUESTIONS + "!=0 ORDER BY " + DataBaseManager.COLUMN_SKANWORD_ID + " DESC  LIMIT " + getMaxSkanwordsOnPageCount());
        }
 
        return SmappsScanwords.getDatabaseHelper().getSkanwordsWithSelection(null);
    }
 
    public List<Skanword> getSkanwordsForListType(TaskListType type,
            Integer issueId) {
        return SmappsScanwords.getDatabaseHelper().getSkanwordsWithSelection(DataBaseManager.COLUMN_SET_ID + "=" + issueId);
    }
 
 
    public List<SkanwordsSetInfo> getSortedSetsInfosList(
            TaskListType taskListType, VisibilityListType visibility) {
         
    	synchronized(mSetInfosMutex) {
            List<SkanwordsSetInfo> setsList = new ArrayList<SkanwordsSetInfo>();
            if (visibility == VisibilityListType.DOWNLOADED_ISSUES || !MainNetworkManager.getInstance().hasNetworkConnection()) {
                setsList = new ArrayList<SkanwordsSetInfo>(mSetsInfos);
            } else if (getLastReceivedListObject() != null && taskListType == getLastReceivedListObject().getListType())  {
                setsList = new ArrayList<SkanwordsSetInfo>(getLastReceivedListObject().getSetsList());
                 
            }
            if (mTomorrowSet != null && setsList.size() > 0) {
                setsList.add(0, mTomorrowSet);
            }
            List<SkanwordsSetInfo> returnSetsList = new ArrayList<SkanwordsSetInfo>();
            for (SkanwordsSetInfo skanwordsSetInfo : setsList) {
                if (taskListType == TaskListType.FINISHED_TASKS && skanwordsSetInfo.isFinished()) {
                    returnSetsList.add(skanwordsSetInfo);
                } else if (taskListType == TaskListType.STARTED_TASKS && skanwordsSetInfo.isStarted()) {
                    returnSetsList.add(skanwordsSetInfo);
                } else if (taskListType == TaskListType.ISSUE_TASKS && skanwordsSetInfo.hasNew()) {
                    returnSetsList.add(skanwordsSetInfo);
                }
            }
            return returnSetsList;
    	}
    }
 
    public List<SkanwordsSetInfo> getSortedSetsInfosList(
            TaskListType taskListType) {
        return getSortedSetsInfosList(taskListType,
                VisibilityListType.ALL_ISSUES);
    }
 
 
    public boolean saveSkanwordState(final Skanword skanword, final boolean notify) {
        if (skanword != null) {
            if (skanword.isChanged()) {
                skanword.setChanged(false);
                SmappsScanwords.getExecutorService().execute(new Runnable() {
                    @Override
                    public void run() {
                        skanword.setChanged(false);
                        SmappsScanwords.getDatabaseHelper().updateSkanwordProgress(skanword, true);
                        getSkanwordsSetsFromDB();
                        if (notify) {
                            SmappsScanwords.getEventsDispatcher().dispatchEvent(new SkanwordsSetsUpdateEvent(MainDataManager.EVENT_SKANWORDS_SETS_INFOS_UPDATE));
                            SmappsScanwords.getEventsDispatcher().dispatchEvent(new SimpleEvent(MainDataManager.EVENT_SKANWORDS_TASKS_UPDATE));
                        }
                    }
                });
            } else if (notify) {
                SmappsScanwords.getEventsDispatcher().dispatchEvent(new SkanwordsSetsUpdateEvent(MainDataManager.EVENT_SKANWORDS_SETS_INFOS_UPDATE));
                SmappsScanwords.getEventsDispatcher().dispatchEvent(new SimpleEvent(MainDataManager.EVENT_SKANWORDS_TASKS_UPDATE));
            }
        }
        return true;
    }
    public SkanwordsSetInfo getTomorrowSet() {
        return mTomorrowSet;
    }
 
    public List<SkanwordsSetInfo> getSetsInfos() {
    	synchronized (mSetInfosMutex) {
            return mSetsInfos;
		}
    }
 
    public long timeTillTomorrowIssue() {
        if (getTomorrowSet() == null)
            return 0;
        return getTomorrowSet().getDownloadTimeLeft()
                - ((new Date()).getTime() - getTomorrowSet()
                        .getDownloadedTime()) / 1000;
    }
 
    public String getProgressOfSetInfo(SkanwordsSetInfo info,
            TaskListType listType) {
        String type = "";
        int count = 0;
         
        switch (listType) {
        case ISSUE_TASKS:
            type = "Новых ";
            count = info.getSkanwordsCount() - info.getSkanwordsFinishedCount() - info.getSkanwordsStartedCount();
            break;
        case STARTED_TASKS:
            type = "Начато ";
            count = info.getSkanwordsStartedCount();
            break;
        case FINISHED_TASKS:
            type = "Решено ";
            count = info.getSkanwordsFinishedCount();
            break;
 
        default:
            break;
        }
        return type + count + " из " + info.getSkanwordsCount();
    }
    public void storeSkanwordsSet(SkanwordsSetDataObject setDataObject) {
        if (setDataObject == null)
            return;
        importProgress(setDataObject);
        getSkanwordsSetsFromDB();
    }
    public void storeSkanwordsSet(SkanwordsSetDataObject setDataObject, SkanwordsSetInfo setInfo) {
        SkanwordSet set = setDataObject.getFristScanwordSet();
        SmappsScanwords.getDatabaseHelper().addSkanwordsSetInfo(setInfo);
        placeProgressToSkanwords(setDataObject, set.getSkanwords());
        for (Skanword skanword : set.getSkanwords()) {
            skanword.setImageDir(DownloaderArhiveFile.getUnzipPath(setInfo.getMediaUrl()));
            skanword.setSetId((int) set.getId());
            skanword.clearKeyword();
            skanword.checkKeyword();
        }
        
        SmappsScanwords.getDatabaseHelper().importSkanwordData(set.getSkanwords());
        importProgress(setDataObject);
        getSkanwordsSetsFromDB();
    }
    private void placeProgressToSkanwords(SkanwordsSetDataObject setDataObject, List<Skanword> skanwords) {
    	for (Skanword skanword : skanwords) {
			for (List<String> progress : setDataObject.getSaveSkanwords()) {
				if (skanword.getId() == Integer.valueOf(progress.get(1))) {
					skanword.setAnswers(progress.get(2));
					skanword.setHints(progress.get(3));
					break;
				}
			}
			for (List<String> progress : setDataObject.getSolvedSkanwords()) {
				if (skanword.getId() == Integer.valueOf(progress.get(1))) {
					skanword.setAnswers(progress.get(2));
					skanword.setHints(progress.get(3));
					break;
				}
			}
		}
    }
    public static String getSkanwordSyncHash(List<Skanword> skanwords) {
        if (skanwords.size() < 1)
            return "";
        List<Skanword> skanwordsFinished = new ArrayList<Skanword>();
        List<Skanword> skanwordsStarted = new ArrayList<Skanword>();
        for (Skanword skanword : skanwords) {
            if (skanword.isFinished()) {
                skanwordsFinished.add(skanword);
            } else if (skanword.isStarted()) {
                skanwordsStarted.add(skanword);
            }
        }
        if (skanwordsFinished.size() + skanwordsStarted.size() < 1) {
            return "";
        }
        Collections.sort(skanwordsFinished, new Comparator<Skanword>() {
            public int compare(Skanword lhs, Skanword rhs) {
                 
                if (rhs.isFinished() && lhs.isFinished()) {
                    return (int)(lhs.getId() - rhs.getId());
                }
                if (rhs.isFinished()) {
                    return 1;
                } 
                if (lhs.isFinished()) {
                    return -1;
                }
 
                return 0;
            }
        });
 
        Collections.sort(skanwordsStarted,  new Comparator<Skanword>() {
            public int compare(Skanword lhs, Skanword rhs) {
                if (rhs.isStarted() && lhs.isStarted()) {
                    return (int)(lhs.getId() - rhs.getId());
                }
                if (rhs.isStarted()) {
                    return 1;
                }
                if (lhs.isStarted()) {
                    return -1;
                }
                return 0;
            }
        });
        StringBuilder sb = new StringBuilder();
        for (Skanword skanword : skanwordsFinished) {
            sb.append(skanword.getId() + "_");
        }
        for (Skanword skanword : skanwordsStarted) {
            sb.append("c" + skanword.getId() + "_"
                    + skanword.getAnswers() + "_" + skanword.getHints()
                    + "_");
        }
        if (sb.length() > 0) {
            sb.append("c4303aa5d29797f08459d60fa9238da7");
        } else {
            sb.append("_c4303aa5d29797f08459d60fa9238da7");
        }
        return Utils.md5(sb.toString());
    }
 
    public static class SolveState implements Serializable {
        /**
         * 
         */
        private static final long serialVersionUID = 1L;
        private boolean changed;
        private String answers;
        private String hints;
        private boolean finished;
        private boolean started;
        private Long updateTime;
        private int finishedQuestions;
        private final Long skanwordId;
        private final int setId;
        private Skanword skanword;
 
        public SolveState(String answers, String hints, boolean finished,
                boolean started, Long updateTime, int finishedQuestions,
                Long skanwordId, int setId) {
            super();
            this.answers = answers;
            this.hints = hints;
            this.finished = finished;
            this.started = started;
            this.updateTime = updateTime;
            this.finishedQuestions = finishedQuestions;
            this.skanwordId = skanwordId;
            this.setId = setId;
        }
 
        public int getSetId() {
            return setId;
        }
 
        public String getAnswers() {
            return answers;
        }
 
        public String getHints() {
            return hints;
        }
 
        public boolean isFinished() {
            return finished;
        }
 
        public boolean isStarted() {
            return started;
        }
 
        public Long getUpdateTime() {
            return updateTime;
        }
 
        public int getFinishedQuestions() {
            return finishedQuestions;
        }
 
        public Long getScanwordId() {
            return skanwordId;
        }
 
        public boolean isChanged() {
            return changed;
        }
 
        public void setChanged(boolean changed) {
            this.changed = changed;
        }
 
        public void updated() {
            updateTime = new Date().getTime();
        }
 
        public void setScanword(Skanword scanword) {
            this.skanword = scanword;
        }
 
        public Skanword getScanword() {
            return skanword;
        }
 
        public boolean checkAnswersForScaword(Skanword scanword) {
            finishedQuestions = 0;
            for (Question question : scanword.getQuestions()) {
                if (question.isComplete()) {
                    increaseFinishedQuestions();
                    continue;
                }
                boolean vertical = question.getQuestionData()
                        .isVerticalOrientation();
                StringBuilder stringBuilder = new StringBuilder();
 
                boolean notComplete = false;
                stringBuilder = new StringBuilder();
                for (int i = 0; i < question.getAnswer().getLength(); i++) {
                    int x = question.getAnswer().getX() + (vertical ? 0 : i);
                    int y = question.getAnswer().getY() + (vertical ? i : 0);
                    int start = x * scanword.getSettings().getHeight() + y;
                    String letter = answers.substring(start, start + 1)
                            .toLowerCase();
                    if (letter.equals(" ")) {
                        notComplete = true;
                        break;
                    }
                    if (scanword.getReplaces() != null
                            && scanword.getReplaces().size() > 0) {
                        for (Replace replace : scanword.getReplaces()) {
                            if (x == replace.getCellX()
                                    && y == replace.getCellY()
                                    && letter.toLowerCase().equals(
                                            replace.getLetterSource()
                                                    .toLowerCase())) {
                                letter = replace.getLetterTarget();
                            }
                        }
                    }
                    stringBuilder.append(letter);
                }
                if (notComplete) {
                    continue;
                }
 
                String answer = stringBuilder.toString();
                StringBuilder builder = new StringBuilder();
                if (question
                        .getAnswer()
                        .getHash()
                        .equals(Utils.md5(builder
                                .append(MainNetworkManager.getInstance()
                                        .getNetId()).append('_')
                                .append(scanword.getId()).append('_')
                                .append(answer.toLowerCase()).append('_')
                                .append("ce9d660ffb810b5e59b444b84a712ca1")
                                .toString()))) {
                    question.setComplete(true);
                    increaseFinishedQuestions();
 
                }
            }
 
            return false;
        }
 
 
 
 
        public void increaseFinishedQuestions() {
            finishedQuestions++;
            started = true;
            finished = finishedQuestions >= skanword.getQuestions().size();
            // Log.v("", started + " increaseFinishedQuestions  " + finished);
        }
    }

	public static String md5Answer(String answer, Integer skanwordId) {
		StringBuilder builder = new StringBuilder();
        String str = builder.append(MainNetworkManager.getInstance().getNetId()).append('_')
                .append(skanwordId).append('_')
                .append(answer.toLowerCase()).append('_')
                .append("ce9d660ffb810b5e59b444b84a712ca1").toString();
		return Utils.md5(str);
	}
    public static String dirrectionStringRepresentation(TaskListDirrectionType dirrectionType ) {
        switch (dirrectionType) {
        case TOP_DIRRECTION:
            return "top";
        case CURRECT_DIRRECTION:
            return "current";
        case PREVIOUS_DIRRECTION:
            return "previous";
        case NEXT_DIRRECTION:
            return "next";
 
        default:
            return "top";
        }
    }
    public static String listTypeStringRepresentation(TaskListType listType ) {
        switch (listType) {
        case ISSUE_TASKS:
            return "new";
        case STARTED_TASKS:
            return "started";
        case FINISHED_TASKS:
            return "finished";
 
        default:
            return "top";
        }
    }
    public static class SkanwordsSetsUpdateEvent extends SimpleEvent {
         
        private final TaskListType mTaskListType;
        private boolean mScrollToTop = true;
 
        public SkanwordsSetsUpdateEvent(String type) {
            super(type);
            mTaskListType = TaskListType.ALL_TASKS;
             
        }
        public SkanwordsSetsUpdateEvent(String type, boolean scrollToTop) {
            super(type);
            mTaskListType = TaskListType.ALL_TASKS;
            mScrollToTop = scrollToTop;
        }
        public SkanwordsSetsUpdateEvent(String type, TaskListType listType) {
            super(type);
            mTaskListType = listType;
        }
        public SkanwordsSetsUpdateEvent(String type, TaskListType listType, boolean scrollToTop) {
            super(type);
            mTaskListType = listType;
            mScrollToTop = scrollToTop;
        }
        public boolean scrollToTop() {
            return mScrollToTop;
        }
        public TaskListType getTaskListType() {
            return mTaskListType;
        }
         
    }
    public static enum TaskListDirrectionType {
        TOP_DIRRECTION, CURRECT_DIRRECTION, PREVIOUS_DIRRECTION, NEXT_DIRRECTION
    }
    public static enum TaskListType {
        ISSUE_TASKS, STARTED_TASKS, FINISHED_TASKS, ALL_TASKS
    }
 
    public static enum VisibilityListType {
        ALL_ISSUES, DOWNLOADED_ISSUES, NOT_DOWNLOADED_ISSUES
    }
 
    public static enum ScanwordsSetsInfosSortType {
        DEFAULT, STARTED, FINISHED
    }
 
    public int getMaxSkanwordsOnPageCount() {
        return 100;
    }
}